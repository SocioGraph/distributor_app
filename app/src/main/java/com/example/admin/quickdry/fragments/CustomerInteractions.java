package com.example.admin.quickdry.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dynamicView.ShowDynamicView;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.model.CardSwipeDetails;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.model.ObjectData;
import com.example.admin.daveai.others.DaveAIHelper;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.AddScanProduct;
import com.example.admin.quickdry.activity.CategoryHierarchy;
import com.example.admin.quickdry.activity.CustomerProfile;
import com.example.admin.quickdry.adapter.InteractionAdapter;
import com.example.admin.quickdry.adapter.SubHeaderAdapter;
import com.example.admin.quickdry.dialogs.NextStageEventDialog;
import com.example.admin.quickdry.dialogs.OrderCheckoutDialog;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.utils.CRUDUtil;
import com.example.admin.quickdry.utils.CardSwipeListener;
import com.example.admin.quickdry.utils.PostInteractionUtil;
import com.example.admin.quickdry.utils.RecycleViewSwipe;
import com.example.admin.quickdry.storage.SharedPreference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import static com.example.admin.quickdry.activity.CustomerProfile.tabHeaderList;
import static com.example.admin.daveai.network.APIRoutes.locale;
import static com.example.admin.daveai.others.DaveAIStatic.interactionPosition;



public class CustomerInteractions extends Fragment implements CardSwipeListener,
         InteractionAdapter.InteractionAdapterCallback,DatabaseConstants , DaveConstants, AppConstants {

    private static final String TAG = "CustomerInteractions";
    private static final String TAB_POSITION = "tab_position";
    private static final String CUSTOMER_ID = "customer_id";
    private static final String TAB_TITLE = "tab_stage";
    private static final String CUSTOMER_DETAILS = "customer_details";
    private static final String ORDER_DETAILS = "order_details";


    LinearLayoutManager shLayoutManager;
    Button cancelBtn;
    RelativeLayout searchView, searchCancelView;
    EditText editTextSearch;
    TextView textView,errorMsgView,orderTitleView, orderDateView, orderStatusView,orderErrorMsg,orderRightAttr,noSearchFound;
    RelativeLayout mProfileView,interactionView;
    LinearLayout parentLayout;
    ImageView searchBtn,imageView, btnEditProfile,btnEditOrder,btnDeleteOrder;
    View includeInProfile,includeInStage,includeOrderLayout;

    InteractionAdapter interactionAdapter;
    private RecyclerView mRecyclerView, shRecyclerView,listExpandRecycler;
    SwipeRefreshLayout mSwipeRefreshLayout,profileRefreshLayout;
    ProgressBar progressBar;

    private DaveAIPerspective daveAIPerspective;
    private SharedPreference sharedPreference;
    private ModelUtil modelUtil;

    int tabPosition = 0;
    String tabTitle = "";
    String strCustomerId = null;
    private List<JSONObject> interactionData = new ArrayList<>();
    String getTotalQty = "";
    public  String customerDetails = "";
    JSONObject stageSubHeaderObj = new JSONObject();
    boolean isRefreshProfile = false;





    public CustomerInteractions() {
    }


    private OnInteractionStageListener onInteractionStageListener;
    public interface OnInteractionStageListener {
        void switchFragment(Fragment fragment);
        void onClickDynamicButtonAdded(String buttonName, String btnAction, HashMap<String, Object> buttonDetails);
        void onExpandView(String productID, JSONObject productDetails);

    }

    public static CustomerInteractions newInstance(int tabPosition, String objectId, String stage, String customerDetails) {

        CustomerInteractions fragment = new CustomerInteractions();
        Bundle args = new Bundle();
        args.putInt(TAB_POSITION, tabPosition);
        args.putString(CUSTOMER_ID, objectId);
        args.putString(TAB_TITLE, stage);
        args.putString(CUSTOMER_DETAILS, customerDetails);

        fragment.setArguments(args);


        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInteractionStageListener) {
            onInteractionStageListener = (OnInteractionStageListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnInteractionStageListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(tabPosition == 0)
            setHasOptionsMenu(true);

        if (getArguments() != null) {
            tabPosition= getArguments().getInt(TAB_POSITION);
            tabTitle = getArguments().getString(TAB_TITLE);
            strCustomerId = getArguments().getString(CUSTOMER_ID);
            customerDetails = getArguments().getString(CUSTOMER_DETAILS);

        }
       // Log.i(TAG,"Get tabPosition and Tab title:--------------------  "+tabPosition +" tabTitle:----------"+tabTitle);

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_customer_profile, container, false);

        hideSoftKeyword(rootView);
        parentLayout = rootView.findViewById(R.id.parentLayout);
        errorMsgView =  rootView.findViewById(R.id.errorMsgView);
        errorMsgView.setVisibility(View.GONE);
        mProfileView =  rootView.findViewById(R.id.mProfileView);
        interactionView =  rootView.findViewById(R.id.interactionView);
        textView =  rootView.findViewById(R.id.textView);
        imageView =  rootView.findViewById(R.id.imageView);
        mRecyclerView =  rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager((getActivity())));
        interactionView.setVisibility(View.GONE);
        btnEditProfile =  rootView.findViewById(R.id.btnEditProfile);
        listExpandRecycler = rootView.findViewById(R.id.listExpand);

        searchView =  rootView.findViewById(R.id.searchView);
        searchBtn =  rootView.findViewById(R.id.search_btn);

        searchCancelView = rootView.findViewById(R.id.search_cancel);
        editTextSearch =  rootView.findViewById(R.id.editTextSearch);
        cancelBtn = rootView.findViewById(R.id.cancelButton);
        searchCancelView.setVisibility(View.GONE);

        progressBar =  rootView.findViewById(R.id.progressBar);
        noSearchFound =  rootView.findViewById(R.id.noSearchFound);
        noSearchFound.setVisibility(View.GONE);

        includeOrderLayout =  rootView.findViewById(R.id.includeOrderLayout);
        btnEditOrder =  rootView.findViewById(R.id.btnEditOrder);
        btnDeleteOrder =  rootView.findViewById(R.id.btnDeleteOrder);
        orderErrorMsg =  rootView.findViewById(R.id.orderErrorMsg);
        orderTitleView =  rootView.findViewById(R.id.orderTilte);
        orderDateView =  rootView.findViewById(R.id.orderDate);
        orderStatusView =  rootView.findViewById(R.id.orderStatus);
        orderRightAttr =  rootView.findViewById(R.id.orderRightAttr);



        profileRefreshLayout = rootView.findViewById(R.id.profileRefreshLayout);
        profileRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout = rootView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,//R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);


        // Calling the subHeader RecyclerView
        shRecyclerView = rootView.findViewById(R.id.show_details);
        shRecyclerView.setHasFixedSize(true);

        // The number of Columns
        shLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        shRecyclerView.setLayoutManager(shLayoutManager);

        includeInProfile = rootView.findViewById(R.id.includeInProfile);
        includeInStage =  rootView.findViewById(R.id.includeInStage);


        daveAIPerspective= DaveAIPerspective.getInstance();
        modelUtil = new ModelUtil(getActivity());

        sharedPreference = new SharedPreference(getActivity());

        return rootView;
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getCustomerProfileFromServer();

        profileRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                progressBar.setVisibility(View.GONE);
                if(profileRefreshLayout != null ) {
                    profileRefreshLayout.setRefreshing(true);
                    isRefreshProfile = true;
                    getCustomerDetails();

                }
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                progressBar.setVisibility(View.GONE);
                if(mSwipeRefreshLayout != null ) {
                    mSwipeRefreshLayout.setRefreshing(true);
                   /* int queueCount = DatabaseManager.getInstance(getActivity()).checkAPIQueueCount();
                    if(queueCount== 0){
                        // TODO Fetching data from database
                        mPageNumber = 1;
                        childFragment.resetCategoryHierarchy();
                        getCustomersList(mPageNumber,true,true);

                    }else {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }*/

                    //Fetching data from database
                    getInteractionsList();
                }
            }
        });


        btnEditProfile.setOnClickListener(v -> {

            //Fragment fragment = DynamicForm.newInstance("Edit Profile","Update","UPDATE",
            Fragment fragment = DynamicForm.updateFormView("Edit Profile","Update", APIRequestMethod.UPDATE.name(),
                    modelUtil.getCustomerModelName(),daveAIPerspective.getCustomer_details_view(),new ModelUtil(getActivity()).getCustomerIdAttrName(),

                    customerDetails);
            ((OnInteractionStageListener) getActivity()).switchFragment(fragment);

        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.GONE);
                searchCancelView.setVisibility(View.VISIBLE);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.VISIBLE);
                searchCancelView.setVisibility(View.GONE);
                editTextSearch.setText("");
                hideSoftKeyword(v);
                interactionAdapter.resetPreviousCustomerList(interactionData);
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0)
                    methodToFilterItem(interactionData, editTextSearch.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        btnEditOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"clicked btnEditOrder");
                String currentOrderId =  sharedPreference.readString(strCustomerId);
                if (currentOrderId != null) {
                    OrderCheckoutDialog cdd = new OrderCheckoutDialog(getActivity(), currentOrderId,
                            new OrderCheckoutDialog.OrderCheckoutListener() {
                                @Override
                                public void onCheckoutOrder(JSONObject orderDetails) {
                                     showOrderDetails(orderDetails);
                                }

                                @Override
                                public void onCancelOrder() {

                                }
                            });
                    cdd.show();
                }else
                    Toast.makeText(getActivity(),"Please create New Order",Toast.LENGTH_SHORT).show();

            }
        });

        btnDeleteOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentOrderId =  sharedPreference.readString(strCustomerId);
                if (currentOrderId != null) {
                    DaveModels daveModels = new DaveModels(getActivity(),false);
                    daveModels.deleteObjectFromLocalDB(daveAIPerspective.getInvoice_model_name(), currentOrderId, null);
                    sharedPreference.removeKey(strCustomerId);
                    includeOrderLayout.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();

        daveAIPerspective= DaveAIPerspective.getInstance();
        modelUtil = new ModelUtil(getActivity());

        addDynamicButton();
        Log.e(TAG,"Print Tab Position:----------"+ tabPosition);
        if(tabPosition == 0)
            getInvoiceDetails();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onInteractionStageListener = null;
    }

  /*  @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if(tabPosition!= 0) {
            menu.clear();
        }
    }*/

    private void addDynamicButton(){

        HashMap<String,Object> profileDetails = new HashMap<>();
        profileDetails.put("customer_id",strCustomerId);
        //profileDetails.put("button_name",buttonDetailsList.getButtonName());
        profileDetails.put("customer_object",customerDetails);

        if(tabPosition ==0){
            LinearLayout addOnProfile = includeInProfile.findViewById(R.id.buttonListLayout);
            addOnProfile.removeAllViews();

            //Log.i(TAG,"Print recommendation button Name:----------------"+daveAIPerspective.getRecommendations_btn_name());
            if(!daveAIPerspective.getRecommendations_btn_name().equalsIgnoreCase("__NULL__")) {
                addOnProfile.setWeightSum(addOnProfile.getWeightSum() + 1);
                addOnProfile.addView(createNewButton(daveAIPerspective.getRecommendations_btn_name(), "_recommendation", profileDetails));

               // addOnProfile.setWeightSum(addOnProfile.getChildCount());

            }

            if(daveAIPerspective.isManage_product_from_customer_profile()) {
                addOnProfile.setWeightSum(addOnProfile.getWeightSum() + 1);
                addOnProfile.addView(createNewButton("Add Product", "_add_product", profileDetails));

                //addOnProfile.setWeightSum(addOnProfile.getChildCount());
            }

            if (daveAIPerspective.getInvoice_model_name() != null && !daveAIPerspective.getOrder_checkout_btn_name().equalsIgnoreCase("__NULL__") && !daveAIPerspective.getOrder_checkout_btn_name().equalsIgnoreCase("_NULL_"))
            {
                addOnProfile.setWeightSum(addOnProfile.getWeightSum() + 1);
                Log.e(TAG,"daveAIPerspective.getOrder_checkout_btn_name() == "+daveAIPerspective.getOrder_checkout_btn_name());

                // addOnProfile.addView(createNewButton("Order Checkout", "_order_checkout", profileDetails));
                addOnProfile.addView(createNewButton(daveAIPerspective.getOrder_checkout_btn_name(), "_order_checkout", profileDetails));
                addOnProfile.setWeightSum(addOnProfile.getChildCount());


            }

            if (daveAIPerspective.getScan_btn_name() != null  && !daveAIPerspective.getScan_btn_name().equalsIgnoreCase("__NULL__") && !daveAIPerspective.getScan_btn_name().equalsIgnoreCase("_NULL_")) {
                addOnProfile.setWeightSum(addOnProfile.getWeightSum()+1);
                addOnProfile.addView(createNewButton(daveAIPerspective.getScan_btn_name(),"_scan_product",profileDetails));

                addOnProfile.setWeightSum(addOnProfile.getChildCount());
            }

            if (daveAIPerspective.getForm_view_button_name() != null  && !daveAIPerspective.getForm_view_button_name().equalsIgnoreCase("__NULL__") && !daveAIPerspective.getForm_view_button_name().equalsIgnoreCase("_NULL_")) {
                addOnProfile.addView(createNewButton(daveAIPerspective.getForm_view_button_name(),"_form_view",profileDetails));
                //addOnProfile.setWeightSum(addOnProfile.getWeightSum()+1);
                addOnProfile.setWeightSum(addOnProfile.getChildCount());

            }

            if (daveAIPerspective.getSearch_by_image_button_name() != null && !daveAIPerspective.getSearch_by_image_button_name().isEmpty() && !daveAIPerspective.getSearch_by_image_button_name().equalsIgnoreCase("__NULL__") && !daveAIPerspective.getOrder_checkout_btn_name().equalsIgnoreCase("_NULL_")) {

                addOnProfile.addView(createNewButton(daveAIPerspective.getSearch_by_image_button_name(),"_search_by_image",profileDetails));
               // addOnProfile.setWeightSum(addOnProfile.getWeightSum()+1);
                addOnProfile.setWeightSum(addOnProfile.getChildCount());
            }


        }else {

            LinearLayout addOnStage = includeInStage.findViewById(R.id.buttonListLayout);
            addOnStage.removeAllViews();

            String stageName = tabHeaderList.get(tabPosition).toLowerCase(locale);
            profileDetails.put("interaction_stage", stageName);
            if( daveAIPerspective.getInteraction_qty()!=null)
                profileDetails.put("total_qty_attr",getTotalQty);

            if (daveAIPerspective.getInvoice_model_name() != null) {
                DatabaseManager databaseManager = DatabaseManager.getInstance(getActivity());
                String stageDetail = databaseManager.getObjectData(INTERACTION_STAGES_TABLE_NAME, stageName).toString();
                try{
                    JSONObject stageJobj = new JSONObject(stageDetail);
                    if(stageJobj.has("order_checkout_name")&&!stageJobj.getString("order_checkout_name").equals("__NULL__")&&!stageJobj.getString("order_checkout_name").equals("_NULL_")){
                        addOnStage.addView(createNewButton(stageJobj.getString("order_checkout_name"), "_order_checkout", profileDetails));
                        addOnStage.setWeightSum(addOnStage.getChildCount());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            HashMap<String, Object> shareButtonListMap = modelUtil.getInteractionStageButtonList();
            Log.e(TAG, "<<<<shareButtonListMap>>>>>>>>>>>>>" + shareButtonListMap);
            if (shareButtonListMap != null && !shareButtonListMap.isEmpty()) {
                if (shareButtonListMap.containsKey(stageName)  ) {
                    addOnStage.addView(createNewButton(shareButtonListMap.get(stageName).toString(), "_share", profileDetails));
                    addOnStage.setWeightSum(addOnStage.getChildCount());
                }
            }


        }

    }

    private Button createNewButton(String buttonName ,String buttonAction, HashMap<String,Object> profileDetails){

       // Log.i(TAG,"createNewButton button NAMe:-:----"+buttonName+"   ============BUttonMsg:- "+ buttonMsg );

        profileDetails.put("button_name",buttonName);

        Button dynamicButton = new Button(getActivity());
        LinearLayout.LayoutParams buttonParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);

        buttonParam.setMargins((int) getResources().getDimension(R.dimen._3sdp),0,
                (int) getResources().getDimension(R.dimen._1sdp),0);
        dynamicButton.setTag(buttonName);
        dynamicButton.setLayoutParams(buttonParam);

        dynamicButton.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        dynamicButton.setText(buttonName);
        dynamicButton.setMaxLines(1);
        dynamicButton.setTextSize((int) getResources().getDimension(R.dimen._5ssp));
        dynamicButton.setMinWidth((int) getResources().getDimension(R.dimen._90sdp));
        dynamicButton.setAllCaps(true);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            dynamicButton.setTextAppearance(getActivity(), android.R.style.TextAppearance_Medium);
        } else {
            dynamicButton.setTextAppearance(android.R.style.TextAppearance_Medium);
        }

        dynamicButton.setTextColor(getResources().getColor(R.color.btn_text_color));
       // dynamicButton.setTextAppearance();
        dynamicButton.setPadding((int) getResources().getDimension(R.dimen._5sdp),0,
                (int) getResources().getDimension(R.dimen._5sdp),0);

        dynamicButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onButtonClickAction(buttonName, buttonAction, profileDetails);
                if(onInteractionStageListener!=null){
                    // Log.e("Dynamic Button Clicked", "onInteractionStageListener***********  "+onInteractionStageListener );
                    onInteractionStageListener.onClickDynamicButtonAdded(buttonName, buttonAction, profileDetails);
                }

            }
        });

        return dynamicButton;
    }

    private void onButtonClickAction(String buttonName ,String buttonMsg, HashMap<String,Object> profileDetails){

        switch (buttonMsg){
            case "_recommendation":
                clickOnRecommendationBtn();
                break;

            case "_add_product":

                Bundle b = new Bundle();
                b.putString("action_name", AddScanProduct.ACTION_ADD_PRODUCT);
                b.putString("customer_id",strCustomerId);
                Intent intent= new Intent(getActivity(), AddScanProduct.class);
                intent.putExtras(b);
                startActivity(intent);

                break;
        }

    }


    private void getCustomerProfileFromServer() {
        if (interactionPosition == 0) {
            mProfileView.setVisibility(View.VISIBLE);
            interactionView.setVisibility(View.GONE);
            noSearchFound.setVisibility(View.GONE);
            showProfileDetails();
           // showOrderDetails();
            getInvoiceDetails();
        } else {
            mProfileView.setVisibility(View.GONE);
            interactionView.setVisibility(View.VISIBLE);
            getInteractionsList();

        }
    }

    private void showProfileDetails() {
        try {
            Model customerModel = Model.getModelInstance(getActivity(), modelUtil.getCustomerModelName());
            Log.e(TAG,"Print CustomerDetailsView List:---  "+ daveAIPerspective.getCustomer_details_view());
            if(daveAIPerspective.getCustomer_details_view()!= null) {
                if (customerDetails != null && !customerDetails.isEmpty()) {
                    //Log.i(TAG,"Get Customer Details:------------  "+customerDetails );

                    listExpandRecycler.setVisibility(View.VISIBLE);
                    JSONObject customerObj = new JSONObject(customerDetails);
                    if (customerObj.has(daveAIPerspective.getCustomer_profile_title()) && !customerObj.isNull(daveAIPerspective.getCustomer_profile_title())){
                        getActivity().setTitle(customerObj.getString(daveAIPerspective.getCustomer_profile_title()));
                    }else {
                        getActivity().setTitle(strCustomerId);
                    }

                    ShowDynamicView dynamicForm = new ShowDynamicView(getActivity());
                    dynamicForm.expandMultiView(getActivity(), listExpandRecycler, daveAIPerspective.getCustomer_details_view(), customerModel, customerObj);

                    if (customerObj.has(ERROR_MSG_ATTRIBUTE) && !customerObj.isNull(ERROR_MSG_ATTRIBUTE) && !customerObj.getString(ERROR_MSG_ATTRIBUTE).isEmpty()) {
                        errorMsgView.setVisibility(View.VISIBLE);
                        errorMsgView.setText(customerObj.getString(ERROR_MSG_ATTRIBUTE).replaceAll("\\s{2,}", " ").trim());
                        if(customerObj.getString(ERROR_MSG_ATTRIBUTE).equals(ERROR_MESSAGE_TEXT)){
                            errorMsgView.setTextColor(getActivity().getResources().getColor(R.color.sync_pending));
                        }
                    }else {
                        errorMsgView.setVisibility(View.GONE);
                    }
                    if(isRefreshProfile)
                        getInvoiceDetails();
                } else {
                    getCustomerDetails();
                }
            }else {
                Toast.makeText(getActivity(),"Details Not found...",Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "<<<<Customer Details Error:>>>>>>>>>>>>>>" + e.getMessage());
        }

    }

    private void getCustomerDetails(){
        Log.e(TAG,"getCustomerDetails:------------------------------------"+customerDetails);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    if(response!=null && response.length()>0) {
                        CustomerProfile.customerDetails = response.toString();
                        customerDetails = response.toString();
                        showProfileDetails();
                    }else {
                        Toast.makeText(getActivity(),"Details not Found.",Toast.LENGTH_LONG).show();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "Error during  getting Product Id +++++++++++" + e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();
            }
        };
        try {
            if(strCustomerId!=null && !strCustomerId.isEmpty()) {
                DaveModels daveModels = new DaveModels(getActivity(), true);
                daveModels.getObject(modelUtil.getCustomerModelName(), strCustomerId, daveAIListener, false, false,true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"error during getting customer details:--"+e.getMessage());

        }
    }

    public void getInvoiceDetails(){

        String strOpenOrder = sharedPreference.readString(strCustomerId);
        if(strOpenOrder==null || strOpenOrder.isEmpty())
            strOpenOrder = DaveAIHelper.preOrderId.get(strCustomerId);
        Log.e(TAG,"getInvoiceDetails Print OrderId"+ strOpenOrder +" CustomerID"+strCustomerId );
        if(strOpenOrder!=null && !strOpenOrder.isEmpty()) {
            DaveAIListener daveAIListener = new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject response) {
                    showOrderDetails(response);
                    profileRefreshLayout.setRefreshing(false);
                    isRefreshProfile = false;
                }

                @Override
                public void onResponseFailure(int requestCode, String responseMsg) {
                    Toast.makeText(getActivity(),requestCode+": "+responseMsg,Toast.LENGTH_SHORT).show();
                    includeOrderLayout.setVisibility(View.GONE);
                    profileRefreshLayout.setRefreshing(false);
                    isRefreshProfile = false;
                }
            };
            try {
                if (daveAIPerspective.getInvoice_model_name() != null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                    DaveModels daveModels = new DaveModels(getActivity(), true);
                    daveModels.getObject(daveAIPerspective.getInvoice_model_name(), strOpenOrder, daveAIListener);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("", "Exception Get Order Details:-------------  " + e.getMessage());
            }
        }else {
            includeOrderLayout.setVisibility(View.GONE);
            profileRefreshLayout.setRefreshing(false);
        }


    }


    private void showOrderDetails(JSONObject orderDetails){

        Log.i(TAG,"showOrderDetails Print order Details:--------- "+ orderDetails);
        if(orderDetails!=null && orderDetails.length()>0) {
            includeOrderLayout.setVisibility(View.VISIBLE);
            try {
                if (daveAIPerspective.getInvoice_card_header() != null && orderDetails.has(daveAIPerspective.getInvoice_card_header()) && !orderDetails.isNull(daveAIPerspective.getInvoice_card_header())) {

                    String orderTitle = "";
                    if (daveAIPerspective.getInvoice_model_name() != null) {
                        Model modelInstance = ModelUtil.getModelInstance(getActivity(), daveAIPerspective.getInvoice_model_name());
                        orderTitle = modelInstance.getAttributeTitle(daveAIPerspective.getInvoice_card_header());
                    } else
                        orderTitle = daveAIPerspective.getInvoice_card_header();

                    orderTitleView.setText(orderTitle + ": " + orderDetails.getString(daveAIPerspective.getInvoice_card_header()));
                }

                if (daveAIPerspective.getInvoice_date_attr() != null && orderDetails.has(daveAIPerspective.getInvoice_date_attr()) && !orderDetails.isNull(daveAIPerspective.getInvoice_date_attr())) {
                    orderDateView.setText(orderDetails.getString(daveAIPerspective.getInvoice_date_attr()));
                }

                if (daveAIPerspective.getInvoice_close_attr_name() != null && orderDetails.has(daveAIPerspective.getInvoice_close_attr_name()) && !orderDetails.isNull(daveAIPerspective.getInvoice_close_attr_name())) {

                    if (orderDetails.getBoolean(daveAIPerspective.getInvoice_close_attr_name())) {
                        orderStatusView.setText("Order Checked out");
                    } else {
                        orderStatusView.setText("You have an open order.");
                    }
                }

                if(orderDetails.has(ERROR_MSG_ATTRIBUTE) && !orderDetails.isNull(ERROR_MSG_ATTRIBUTE) && !orderDetails.getString(ERROR_MSG_ATTRIBUTE).isEmpty()){
                    orderErrorMsg.setVisibility(View.VISIBLE);
                    orderErrorMsg.setText(orderDetails.getString(ERROR_MSG_ATTRIBUTE).replaceAll("\\s{2,}", " ").trim());
                    btnDeleteOrder.setVisibility(View.VISIBLE);
                    if(orderDetails.getString(ERROR_MSG_ATTRIBUTE).equals(ERROR_MESSAGE_TEXT)){
                        orderErrorMsg.setTextColor(getActivity().getResources().getColor(R.color.sync_pending));

                    }else {
                        orderErrorMsg.setTextColor(getActivity().getResources().getColor(R.color.sync_failed));
                    }
                }else {
                    orderErrorMsg.setVisibility(View.INVISIBLE);
                    btnDeleteOrder.setVisibility(View.GONE);

                }

                if(daveAIPerspective.getInvoice_card_right_attr()!=null && orderDetails.has(daveAIPerspective.getInvoice_card_right_attr()) &&
                        !orderDetails.isNull(daveAIPerspective.getInvoice_card_right_attr())) {
                    orderRightAttr.setVisibility(View.VISIBLE);
                    orderRightAttr.setText(orderDetails.getString(daveAIPerspective.getInvoice_card_right_attr()));
                }else {
                    orderRightAttr.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            includeOrderLayout.setVisibility(View.GONE);
        }

    }

    private void getInteractionsList(){

        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                    //Log.d(TAG,"GetInteractions Details:----"+response);
                    interactionData = ObjectData.getJSONObjectDataList(response);
                    Log.e(TAG,"Print Interaction Data length :---- "+interactionData.size());
                    if (interactionData != null && interactionData.size() > 0) {
                        noSearchFound.setVisibility(View.GONE);
                        interactionAdapter = new InteractionAdapter(CustomerInteractions.this,getActivity(),
                                interactionData, strCustomerId);

                        mRecyclerView.setAdapter(interactionAdapter);
                        interactionAdapter.notifyDataSetChanged();

                        //todo testing new swipe feature
                        RecycleViewSwipe recycleViewSwipeHelper = new RecycleViewSwipe(CustomerInteractions.this,getActivity());
                        ItemTouchHelper touchHelper = new ItemTouchHelper(recycleViewSwipeHelper);
                        touchHelper.attachToRecyclerView(mRecyclerView);

                        JSONObject getTotal = response.getJSONObject("__TOTAL__");
                        stageSubHeaderObj = getTotal.getJSONObject(tabTitle.toLowerCase(locale));
                       // Log.e(TAG, "getInteractionsList*******stageSubHeaderObj Details ***********  "+stageSubHeaderObj );

                        setSubHeaderDetails(stageSubHeaderObj);

                    }else {
                        Log.e(TAG,"no search found*************************");
                        searchView.setVisibility(View.GONE);
                        interactionView.setVisibility(View.GONE);
                        noSearchFound.setVisibility(View.VISIBLE);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                progressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();

            }
        };
        try {
            String  interactionModelName = new ModelUtil(getActivity()).getInteractionModelName();
            if(interactionModelName!=null && !interactionModelName.isEmpty()) {
                progressBar.setVisibility(View.VISIBLE);
                DaveModels daveModels = new DaveModels(getActivity(), true);
                Log.i(TAG,"getInteractionsList Print Str customer id:------------"+strCustomerId + " tabtitle:-----------"+ tabTitle);
                daveModels.getInteractions(strCustomerId, tabTitle, daveAIListener, false,false,false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During showing Interactions LIst:-------------  "+e.getMessage());
        }

    }

    private void methodToFilterItem(List<JSONObject> interactionDetails, String query) {

        List<JSONObject> filteredModelList = new ArrayList<>();
        if (interactionDetails != null && interactionDetails.size() > 0) {
            for (JSONObject model : interactionDetails) {
                try {
                    JSONObject productDetails = model.getJSONObject("details");
                   /* String text = productDetails.getString("product_name").toLowerCase(locale);
                    if (text.contains(query.toLowerCase(locale))) {
                        filteredModelList.add(model);
                    }*/
                    Iterator<?> keys = productDetails.keys();
                    while(keys.hasNext() ) {
                        String key = (String)keys.next();
                        String value = productDetails.get(key).toString().toLowerCase();
                        if(!filteredModelList.contains(model) && value.contains(query.toLowerCase())){
                            Log.e(TAG," Search term and item :- "+query +" value:- "+ value);
                            filteredModelList.add(model);
                        }

                    }

                } catch (JSONException e) {
                    Log.e(TAG,"Error during filter Interaction List****************:- " + e.getMessage());
                }
            }
        }
        Log.e(TAG,"Set Filter List****************:- " + filteredModelList.size() + "  "+interactionData.size());
        interactionAdapter.setFilteredList(filteredModelList);
    }

    private void clickOnRecommendationBtn() {

        Intent intent = new Intent(getActivity(), CategoryHierarchy.class);
        intent.putExtra("NewCustomerId", strCustomerId);
        try {
            JSONObject cusDetails = new JSONObject(customerDetails);
            if(cusDetails.has(ERROR_MSG_ATTRIBUTE)&& !cusDetails.isNull(ERROR_MSG_ATTRIBUTE) && !cusDetails.getString(ERROR_MSG_ATTRIBUTE).isEmpty())
            {
                if(cusDetails.getString(ERROR_MSG_ATTRIBUTE).equals(ERROR_MESSAGE_TEXT)){
                    intent.putExtra("isOnlineMode", false);
                    startActivity(intent);
                }else{
                    Toast.makeText(getActivity(),"Error saving customer, please check profile to resolve problem",Toast.LENGTH_SHORT).show();
                }
            } else{
                intent.putExtra("isOnlineMode", true);
                startActivity(intent);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    @Override
    public void onUpdateInteractionQty(JSONObject previousQtyAttr, JSONObject updatedResponse) {

        updateHeaderValues(previousQtyAttr,updatedResponse);

    }

    @Override
    public void onExpandView(String productId,JSONObject productDetails) {

        // Check that there's actually something to send
        if (productId!=null && !productId.isEmpty()) {
            sharedPreference.writeString(DISPLAY_PRODUCT_ID,productId);
            if(onInteractionStageListener!=null)
                onInteractionStageListener.onExpandView(productId,productDetails);
        }
    }

    public Object getDefaultValueIfNotExist(JSONObject obj,String k, Object _default){
        try {
            if(obj.has(k) && !obj.isNull(k))
                return obj.get(k);
            else
                return _default;
        } catch (JSONException e) {
            return _default;
        }
    }
    static Double toDouble(Object value) {
        if (value instanceof Double) {
            return (Double) value;

        } else if (value instanceof Number) {

            return ((Number) value).doubleValue();

        } else if (value instanceof String) {
            try {
                return Double.valueOf((String) value);

            } catch (NumberFormatException ignored) {
            }
        }
        return null;
    }

    static Integer toInteger(Object value) {

        if (value instanceof Integer) {
            return (Integer) value;
        } else if (value instanceof Number) {
            return ((Number) value).intValue();
        } else if (value instanceof String) {
            try {
                return (int) Double.parseDouble((String) value);
            } catch (NumberFormatException ignored) {
            }
        }
        return null;
    }


    private void updateHeaderValues(JSONObject preObj, JSONObject newObj){

        Log.e(TAG,"updateHeaderValues preObj>>>>>>>>>>>>>>>>>"+preObj+ " New Obj:---------"+ newObj+ " "+daveAIPerspective.getSummation_attributes());

        Model interactionModel = Model.getModelInstance(getActivity(), new ModelUtil(getActivity()).getInteractionModelName());
        if(newObj == null){
            newObj = new JSONObject();
        }
        if(preObj ==null)
            preObj= new JSONObject();

        for(Object s: daveAIPerspective.getSummation_attributes()){
            String k = (String)s;
            try {
                assert interactionModel != null;
                String attrType = interactionModel.getAttributeType(k);
                Log.e(TAG,"Print getAttributeType attrNAme:-------- "+k+" AttrType== "+interactionModel.getAttributeType(k));

                if(attrType!=null) {
                    if (attrType.equals("number")) {
                        int totalNumber = toInteger(getDefaultValueIfNotExist(newObj, k, 0)) - toInteger(getDefaultValueIfNotExist(preObj, k, 0));
                        stageSubHeaderObj.put(k, toInteger(getDefaultValueIfNotExist(stageSubHeaderObj, k, 0)) + totalNumber);

                    } else if (attrType.equals("price")) {
                        double totalPrice = toDouble(getDefaultValueIfNotExist(newObj, k, 0)) - toDouble(getDefaultValueIfNotExist(preObj, k, 0));
                        stageSubHeaderObj.put(k, toDouble(getDefaultValueIfNotExist(stageSubHeaderObj, k, 0)) + totalPrice);

                       // DecimalFormat df2 = new DecimalFormat(".##");
                       // stageSubHeaderObj.put(k, df2.format(toDouble(getDefaultValueIfNotExist(stageSubHeaderObj, k, 0)) + totalPrice));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setSubHeaderDetails(stageSubHeaderObj);

    }

    private void setSubHeaderDetails(JSONObject getStageTotal){

        Log.e(TAG,"setSubHeaderDetails:----HeaderAttrsList:---"+ daveAIPerspective.getSummation_attributes()+" getStageTotal Details:-- "+getStageTotal );

        Model interactionModel = Model.getModelInstance(getActivity(), new ModelUtil(getActivity()).getInteractionModelName());
        ArrayList<JSONObject> subHeaderList = new ArrayList<>();
        try {
            JSONObject  totalCount= new JSONObject();
            totalCount.put("Total Number", interactionAdapter.getItemCount());
            subHeaderList.add(totalCount);

            if (interactionModel != null && daveAIPerspective.getSummation_attributes() != null ) {

                for (int i = 0; i < daveAIPerspective.getSummation_attributes().size(); i++) {
                    JSONObject setSubHeaderData = new JSONObject();
                    String key = daveAIPerspective.getSummation_attributes().get(i).toString();
                    if (getStageTotal.has(key)) {

                        if(getStageTotal.get(key) instanceof Double){

                           // DecimalFormat formater = new DecimalFormat("#.00");

                            DecimalFormat df2 = new DecimalFormat("#.##");
                           // Log.e(TAG,"getStageTotal.get(key) instanceof Double:----------   "+getStageTotal.get(key) +" "+df2.format(getStageTotal.get(key)));
                            setSubHeaderData.put(interactionModel.getAttributeTitle(key),df2.format(getStageTotal.get(key)));

                        }else {
                            setSubHeaderData.put(interactionModel.getAttributeTitle(key), getStageTotal.get(key));
                        }
                        subHeaderList.add(setSubHeaderData);
                    }
                }
                //Log.e(TAG, "setSubHeaderDetails SUbHEADer __Details__ *******  " + subHeaderList);

                SubHeaderAdapter subHeaderAdapter = new SubHeaderAdapter(getActivity(), subHeaderList);
                shRecyclerView.setAdapter(subHeaderAdapter);
            }
            if(getStageTotal.has(daveAIPerspective.getInteraction_qty()) && !getStageTotal.isNull(daveAIPerspective.getInteraction_qty())) {
                getTotalQty = getStageTotal.getString(daveAIPerspective.getInteraction_qty());
            }

            // update order details:---
            Log.e(TAG, "Update OrderID  if stage equal to pref :----   " + daveAIPerspective.getInvoice_stage_attr()+" == "+tabTitle);
            if(daveAIPerspective.getInvoice_stage_attr()!= null && daveAIPerspective.getInvoice_stage_attr().equalsIgnoreCase(tabTitle)) {
                Log.i(TAG, "Print OrderID of customer:----" + strCustomerId +" "   + getStageTotal);
                String cOrderId = new SharedPreference(getActivity()).readString(strCustomerId);
                List summationAttrs = daveAIPerspective.getSummation_attributes();
                if(cOrderId!=null && !cOrderId.isEmpty() && summationAttrs != null && subHeaderList.size()>0)
                {
                    JSONObject orderDetails = new JSONObject();

                    Log.e(TAG,"Print order id:-------- " +daveAIPerspective.getInvoice_id_attr_name()+"  "+ cOrderId+"\n totalCountAttr: "+
                            daveAIPerspective.getInvoice_total_count_attr()+" TotalValueAttr: "+ daveAIPerspective.getInvoice_total_value_attr());
                    if(daveAIPerspective.getInvoice_total_count_attr()!=null && !daveAIPerspective.getInvoice_total_count_attr().isEmpty())
                    {
                        if(getStageTotal.has(daveAIPerspective.getSummation_attributes().get(0).toString()))
                        orderDetails.put(daveAIPerspective.getInvoice_total_count_attr(),
                                getStageTotal.get(daveAIPerspective.getSummation_attributes().get(0).toString()));


                       // int index= summationAttrs.indexOf(daveAIPerspective.getInvoice_total_count_attr());
                        //orderDetails.put(daveAIPerspective.getInvoice_total_count_attr(), getStageTotal.get(summationAttrs.get(index).toString()));
                    }

                    if(daveAIPerspective.getInvoice_total_value_attr()!=null && !daveAIPerspective.getInvoice_total_value_attr().isEmpty()) {
                        if(getStageTotal.has(daveAIPerspective.getSummation_attributes().get(1).toString()))
                        orderDetails.put(daveAIPerspective.getInvoice_total_value_attr(),
                                getStageTotal.get(daveAIPerspective.getSummation_attributes().get(1).toString()));
                    }

                    if(orderDetails.length()>0){
                        orderDetails.put(daveAIPerspective.getInvoice_id_attr_name(),cOrderId);
                        CRUDUtil crudUtil = new CRUDUtil(getActivity());
                        crudUtil.updateInvoiceDetails(orderDetails);
                    }

                }

            }



        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "Error In  Summary Attr**** " + e.getLocalizedMessage());
        }

    }


    private void hideSoftKeyword(View view){
        InputMethodManager imm = ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE));
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

   /* @Override
    public CardSwipeDetails setDirectionFlags(int cardViewPosition) {

        String interactionStage= "";
        String previousStage= "";
        String nextStage= "";
        int swipeFlags = 0;

        CardSwipeDetails cardSwipeDetails = new CardSwipeDetails();
        try {

            JSONObject  cardItemDetails = interactionAdapter.getJsonObject(cardViewPosition);
           // Log.e(TAG,"GetsetDirectionFlags "+cardItemDetails);
            cardSwipeDetails.setCardItemDetails(cardItemDetails);

            if(cardItemDetails.has("details") && !cardItemDetails.isNull("details") ) {
                JSONObject productDetails = cardItemDetails.getJSONObject("details");
                cardSwipeDetails.setProductDetails(productDetails);
            }

            if(cardItemDetails.has("quantity_attributes")&& !cardItemDetails.isNull("quantity_attributes")) {
                JSONObject quantityAttrs = cardItemDetails.getJSONObject("quantity_attributes");
                cardSwipeDetails.setQuantityAttrs(quantityAttrs);
            }

            if(cardItemDetails.has("next_stage_quantity_attributes")&& !cardItemDetails.isNull("next_stage_quantity_attributes")) {
                JSONArray nextStageQuantityAttrs = cardItemDetails.getJSONArray("next_stage_quantity_attributes");
                cardSwipeDetails.setNextStageQuantityAttrs(nextStageQuantityAttrs);
            }

            if(cardItemDetails.has("next_stage_tagged_products")&& !cardItemDetails.isNull("next_stage_tagged_products") ) {
                JSONArray nextStageTaggedProducts = cardItemDetails.getJSONArray("next_stage_tagged_products");
                cardSwipeDetails.setNextStageTaggedProduct(nextStageTaggedProducts);

            }
            if(cardItemDetails.has("next_stage_new_attributes") && !cardItemDetails.isNull("next_stage_new_attributes") ) {
                JSONArray nextStageNewAttrs = cardItemDetails.getJSONArray("next_stage_new_attributes");
                cardSwipeDetails.setNextStageNewAttributes(nextStageNewAttrs);
            }

            if (cardItemDetails.has("interaction_stage")) {
                interactionStage = cardItemDetails.getString("interaction_stage");
                cardSwipeDetails.setInteractionStage(interactionStage);
            }
            if (cardItemDetails.has("previous_stage")) {
                previousStage = cardItemDetails.getString("previous_stage");
                cardSwipeDetails.setPreviousStage(previousStage);
            }
            if (cardItemDetails.has("next_stage")) {
                nextStage = cardItemDetails.getString("next_stage");
                cardSwipeDetails.setNextStage(nextStage);
            }

            if (previousStage.equals("__NULL__")) {
                swipeFlags = ItemTouchHelper.RIGHT;
            } else if (nextStage.equals("__NULL__")) {
                swipeFlags = ItemTouchHelper.LEFT;
            }
            else {
                swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
            }
            cardSwipeDetails.setSwipeDirection(swipeFlags);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Set  InteractionSwipe Details Error********* " + e.getLocalizedMessage());
        }

        return cardSwipeDetails;
    }

    @Override
    public void onCardSwipe(RecyclerView.ViewHolder viewHolder, int direction) {
        Log.e(TAG, "*********** onCardSwipe direction**********:==  " + direction);

    }

    @Override
    public void onCardSwipe(String customerDetails, JSONObject cardDetails, String currentStage, String previousStage) {

        // Log.e(TAG, "*********onCardSwipe customerDetails**********: " + cardDetails);
        try {
            JSONObject currentQtyDetails = cardDetails.getJSONObject("quantity_attributes");
            updateHeaderValues(currentQtyDetails,null);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onItemRestore(int itemPosition, JSONObject cardDetails) {
        //Log.e(TAG, "*********onCardSwipe customerDetails**********: " + cardDetails);
        try {
            JSONObject currentQtyDetails = cardDetails.getJSONObject("quantity_attributes");
            updateHeaderValues(null,currentQtyDetails);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }*/

    @Override
    public CardSwipeDetails setDirectionFlags(int cardViewPosition) {

        String interactionStage= "";
        String previousStage= "";
        String nextStage= "";
        int swipeFlags = 0;

        CardSwipeDetails cardSwipeDetails = new CardSwipeDetails();
        try {

            JSONObject  cardItemDetails = interactionAdapter.getJsonObject(cardViewPosition);
            // Log.e(TAG,"GetsetDirectionFlags "+cardItemDetails);
            cardSwipeDetails.setCardItemDetails(cardItemDetails);

            if (cardItemDetails.has("interaction_stage")) {
                interactionStage = cardItemDetails.getString("interaction_stage");
                cardSwipeDetails.setInteractionStage(interactionStage);
            }
            if (cardItemDetails.has("previous_stage")) {
                previousStage = cardItemDetails.getString("previous_stage");
                cardSwipeDetails.setPreviousStage(previousStage);
            }
            if (cardItemDetails.has("next_stage")) {
                nextStage = cardItemDetails.getString("next_stage");
                cardSwipeDetails.setNextStage(nextStage);
            }

            if (previousStage.equals("__NULL__")) {
                swipeFlags = ItemTouchHelper.RIGHT;
            } else if (nextStage.equals("__NULL__")) {
                swipeFlags = ItemTouchHelper.LEFT;
            }
            else {
                swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
            }
            cardSwipeDetails.setSwipeDirection(swipeFlags);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Set  InteractionSwipe Details Error********* " + e.getLocalizedMessage());
        }

        return cardSwipeDetails;
    }

    @Override
    public void onCardSwipe(RecyclerView.ViewHolder viewHolder, int direction) {

        try{
            int itemPosition = viewHolder.getAdapterPosition();
           // Log.e(TAG, "*********** onCardSwipe direction**********:==  " + direction +" cardPosition:- "+ itemPosition);

            JSONObject cardItemDetails = interactionAdapter.getJsonObject(itemPosition);
            String cardProductId = interactionAdapter.getRemoveItemId(itemPosition);

            PostInteractionUtil postInteractionUtil = new PostInteractionUtil(getActivity(),cardItemDetails);
            String swipeStage = postInteractionUtil.getCardSwipeStageName(direction);
            interactionAdapter.remove(itemPosition);

            JSONObject currentQtyDetails = cardItemDetails.getJSONObject("quantity_attributes");
            updateHeaderValues(currentQtyDetails,null);

            boolean isNextStageAction = postInteractionUtil.checkNextStageEvent(direction);
            if(isNextStageAction){
                NextStageEventDialog nextStageEventDialog = new NextStageEventDialog(getActivity(), new NextStageEventDialog.NextStageEventListener() {
                    @Override
                    public void onNextStageEventCancel() {
                        try {
                            interactionAdapter.restoreItem(cardItemDetails, itemPosition);
                            JSONObject currentQtyDetails = cardItemDetails.getJSONObject("quantity_attributes");
                            updateHeaderValues(null,currentQtyDetails);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNextStageEventSubmit(JSONObject formDetails) {
                        postInteractionUtil.postInteraction(strCustomerId,cardProductId, swipeStage,formDetails);
                        showSnackBar(swipeStage);
                        JSONArray jsonArray = postInteractionUtil.getNextStageTaggedProducts();
                        if(jsonArray.length()>0 ){
                            addTaggedProducts(0,jsonArray,swipeStage,postInteractionUtil);
                        }
                    }
                },"Main Product",cardProductId,postInteractionUtil.getNextStageNewAttributes(),cardItemDetails.toString());
                nextStageEventDialog.show();

            }else {
                postInteractionUtil.postInteraction(strCustomerId,cardProductId, swipeStage,null);
                showSnackBar(swipeStage);
            }

        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Exception onCardSwipe********* " + e.getLocalizedMessage());
        }

    }

    @Override
    public void onCardSwipe(String customerDetails, JSONObject cardDetails, String currentStage, String previousStage) {

    }

    @Override
    public void onItemRestore(int itemPosition, JSONObject cardDetails) {

    }

    private void showSnackBar(String swipeStage) {
        Snackbar.make(mProfileView, swipeStage, Snackbar.LENGTH_SHORT).show();
    }

    private void addTaggedProducts(int index ,JSONArray taggedList ,String swipeStage, PostInteractionUtil postIUtil){

        if(index < taggedList.length()) {
            try{
                String productId = taggedList.getString(index);
                NextStageEventDialog nextStageEventDialog = new NextStageEventDialog(getActivity(), new NextStageEventDialog.NextStageEventListener() {
                    @Override
                    public void onNextStageEventCancel() {

                    }
                    @Override
                    public void onNextStageEventSubmit(JSONObject formDetails) {
                        postIUtil.postTaggedProduct(strCustomerId, productId, swipeStage, formDetails);
                        showSnackBar(swipeStage);
                        int tempIndex = index;
                        tempIndex++;
                        addTaggedProducts(tempIndex,taggedList,swipeStage,postIUtil);
                    }
                },"Tagged Products", productId, postIUtil.getNextStageNewAttributes(),null);
                nextStageEventDialog.show();
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Exception onCardSwipe********* " + e.getLocalizedMessage());
            }
        }

    }

}

  /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_customer_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);

        if (daveAIPerspective.getMenu_title_for_order_list() != null && !daveAIPerspective.getMenu_title_for_order_list().isEmpty() && !daveAIPerspective.getMenu_title_for_order_list().equals("__NULL__")) {
            menu.findItem(R.id.actionOrderList).setVisible(true);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.actionOrderList) {
            Intent intent = new Intent(getActivity(), DaveFunction.class);
            intent.putExtra(CUSTOMER_ID,strCustomerId);
            intent.putExtra(CUSTOMER_TITLE,getActivity().getTitle());
            intent.putExtra(DaveFunction.DAVE_ACTION, DaveFunction.CUSTOMER_ORDER_LIST);
            getActivity().startActivityForResult(intent,CustomerProfile.CUSTOMER_ORDER_CLICKED_REQUEST_CODE);

            return true;

        }else if (id == R.id.actionNewOrder) {
            if(onInteractionStageListener!=null)
                onInteractionStageListener.onClickNewOrder();

            return true;

        }
        else if (id == R.id.scanDisplay) {
            if(onInteractionStageListener!=null)
                onInteractionStageListener.onClickNewOrder();

            return true;

        }
        else if (id == R.id.displaySetting) {
            if(onInteractionStageListener!=null)
                onInteractionStageListener.onClickNewOrder();

            return true;

        }

        return super.onOptionsItemSelected(item);
    }*/



