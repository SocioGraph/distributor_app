package com.example.admin.quickdry.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;

import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.CustomerProfile;
import com.example.admin.quickdry.activity.FormView;
import com.example.admin.quickdry.adapter.OrderListAdapter;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.utils.CRUDUtil;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.app.Activity.RESULT_OK;


public class OrderList extends Fragment  implements OrderListAdapter.RecyclerViewClickListener, AppConstants {

    private final String TAG = getClass().getSimpleName();
    public static final String SHORT_BY_DATE = "short_by_date";
    //public static final String CUSTOMER_ID = "customer_id";
    private static final String TITLE = "title";

    private TextView noOrderFound ,viewSortByDate;
    private RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    View includeSearchLayout;
    EditText editTextSearch;
    ImageView searchViewIcon,searchButton;
    Button btnCancelSearch;
    RelativeLayout searchView;
    ProgressBar progressBar;
    FrameLayout frameLayout;

    private String strCustomerId = "";

    private OrderListAdapter orderListAdapter;
    private DaveAIPerspective daveAIPerspective;

    private int mPageNumber = 1;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean isPageLoading = true;

    HashMap<String ,Object> filterAttrMap = new HashMap<>();
    private String actionName = SHORT_BY_DATE;
    private String strSortByDate = "";
    boolean isAnyActionApply = false;
    private String strSearchTerm;
    private String fragmentTitle = "";


    public OrderList() {
        // Required empty public constructor
    }

    private OnOrderListListener mListener;
    public interface OnOrderListListener {
        void onOrderClicked(HashMap<String, Object> orderDetails);

    }

    public static OrderList newInstance(String title ,String customerId) {
        OrderList fragment = new OrderList();
        Bundle bundle = new Bundle();
        bundle.putString(TITLE, title);
        bundle.putString(CUSTOMER_ID, customerId);
        fragment.setArguments(bundle);

        return fragment;
    }

    /*public static OrderList newInstance(String actionName,HashMap<String ,Object> filterAttrMap) {
        OrderList fragment = new OrderList();
        Bundle bundle = new Bundle();
        bundle.putSerializable("filter_attrs", filterAttrMap);
        bundle.putString("actionName", actionName);
        fragment.setArguments(bundle);

        return fragment;
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       // Log.e(TAG,"onAttach:------------------");
        /*if (context instanceof OnOrderListListener) {
            mListener = (OnOrderListListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnOrderListListener");
        }*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,"------------------------------------------onCreate:------------------------------------");
        if (getArguments() != null)
        {
            filterAttrMap = (HashMap<String, Object>) getArguments().getSerializable("filter_attrs");
            actionName = getArguments().getString("actionName");

            if(getArguments().containsKey(CUSTOMER_ID))
                strCustomerId = getArguments().getString(CUSTOMER_ID);

            if(getArguments().containsKey(TITLE))
                fragmentTitle = getArguments().getString(TITLE);

           // Log.e(TAG,"Get Filter Attr :----"+filterAttrMap+" == "+ actionName);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView = inflater.inflate(R.layout.fragment_order_details, container, false);

        Log.e(TAG,"onCreateView:------------------");
        viewSortByDate =  mainView.findViewById(R.id.sortByDate);
        noOrderFound =  mainView.findViewById(R.id.noOrderFound);
        frameLayout =  mainView.findViewById(R.id.frameLayout);

        mRecyclerView =  mainView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //orderListAdapter = new OrderListAdapter(mListener,getActivity(), new ArrayList<>());
        orderListAdapter = new OrderListAdapter(OrderList.this,getActivity(), new ArrayList<>());
        mRecyclerView.setAdapter(orderListAdapter);
        //mRecyclerView.scrollToPosition(pastVisiblesItems);

        searchView = mainView.findViewById(R.id.search_view);
        searchViewIcon = mainView.findViewById(R.id.searchViewIcon);
        includeSearchLayout =  mainView.findViewById(R.id.includeSearchLayout);
        includeSearchLayout.setVisibility(View.GONE);
        editTextSearch = includeSearchLayout.findViewById(R.id.editTextSearch);
        btnCancelSearch = includeSearchLayout.findViewById(R.id.cancelButton);
        searchButton = includeSearchLayout.findViewById(R.id.searchButton);
        progressBar =  mainView.findViewById(R.id.progressBar);

        daveAIPerspective= DaveAIPerspective.getInstance();

        //Log.e(TAG, "<<<<Start order LIst>>>>>>>>>>>>>" + shareButtonListMap);

        return mainView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        //Objects.requireNonNull(getActivity()).setTitle(daveAIPerspective.getMenu_title_for_customer_list());

        if(fragmentTitle!=null && !fragmentTitle.isEmpty())
            getActivity().setTitle(fragmentTitle);

       // strDate = CRUDUtil.methodToday();
        //viewSortByDate.setText(strDate);
       // getListByDate(strDate);


        getOrderList( true,true);

        Calendar cal = Calendar.getInstance();
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, monthOfYear);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                strSortByDate = df.format(cal.getTime());

                if(daveAIPerspective.getInvoice_date_attr()!=null) {
                    viewSortByDate.setText(strSortByDate);
                    isAnyActionApply = true;
                    strSearchTerm ="";
                    mPageNumber = 1;
                    getOrderList( true,false);

                }else{
                    Toast.makeText(getActivity(),"Please set Invoice date attr in perspective",Toast.LENGTH_SHORT).show();
                }

            }

        };
        viewSortByDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(), dateSetListener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH)).show();


            }
        });


        searchViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.GONE);
                viewSortByDate.setVisibility(View.GONE);
                includeSearchLayout.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.VISIBLE);
                strSearchTerm = "";
                //orderListAdapter.notifyDataSetChanged();
            }
        });
        btnCancelSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                noOrderFound.setVisibility(View.GONE);
                includeSearchLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);
                viewSortByDate.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.VISIBLE);
                editTextSearch.setText("");
                strSearchTerm = "";
                hideSoftKeyword(v);
                orderListAdapter.resetPreviousOrderList();


            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyword(v);
                actionOnSearchButton();
            }
        });

        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Log.e(TAG,"IME_ACTION_SEARCH");
                    hideSoftKeyword(editTextSearch);
                    actionOnSearchButton();
                    return true;
                }
                return false;
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {
            boolean searchedFlag = false;
            Timer timer;
            Handler handler;

            public void afterTextChanged(Editable s) {
                strSearchTerm = s.toString().trim();
                timer = new Timer();
                handler = new Handler();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                //do Stuff here
                                if(strSearchTerm.length() > 0) {
                                    getListBasedOnSearch();
                                }
                            }
                        });

                    }
                }, 2000); // 2000ms delay before the timer executes the „run“ method from TimerTask
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                String searchedTerm = query.toString().trim();
                strSearchTerm = query.toString().trim();
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
                if(!searchedFlag && searchedTerm.length() > 2){
                    searchedFlag= true;
                    getListBasedOnSearch();

                }
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (isPageLoading && CRUDUtil.checkNextPage(orderListAdapter.getItemCount())  &&(visibleItemCount + pastVisiblesItems) >= totalItemCount ) {
                        isPageLoading = false;
                        mPageNumber++;

                        //Do pagination.. i.e. fetch new data from Server
                        if(isAnyActionApply){
                            getOrderList(false,false);
                        }else {
                            getOrderList(false,true);
                        }

                    }
                }
            }
        });



    }

    @Override
    public void onStart(){
        super.onStart();
       // Log.e(TAG,"onStart:------------------");

    }

    @Override
    public void onDetach() {
        super.onDetach();
        //Log.e(TAG,"onDetach:------------------");
        mListener = null;
    }

    private void hideSoftKeyword(View view){
        InputMethodManager imm = ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE));
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void actionOnSearchButton(){
        strSearchTerm = editTextSearch.getText().toString().trim();
        if(strSearchTerm.length()>0)
            getListBasedOnSearch();
        else
            Toast.makeText(getActivity(),"Please Enter Search Term",Toast.LENGTH_SHORT).show();
    }

    private void getListBasedOnSearch(){
        isAnyActionApply = true;
        mPageNumber = 1;
        strSortByDate="";
        getOrderList(true,false);

    }


    private void getOrderList(boolean isNewList, boolean isUpdateDefault){

        //orderListAdapter.clear();
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if( response!= null && response.has("data") && response.getJSONArray("data").length()>0){
                        noOrderFound.setVisibility(View.GONE);
                        frameLayout.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = response.getJSONArray("data");
                        List<JSONObject> objectData = new ArrayList<>();

                        for(int i =0 ;i< jsonArray.length();i++){
                            objectData.add(jsonArray.getJSONObject(i));
                        }
                        if(isNewList) {
                            orderListAdapter.addNewSeedList(objectData,isUpdateDefault);
                        }
                        else {
                            orderListAdapter.updateSeedList(objectData,isUpdateDefault);
                        }
                        isPageLoading = true;


                    }else {
                        Log.i(TAG,"Print data not found:-----------"+orderListAdapter.getItemCount());
                        if(orderListAdapter.getItemCount() == 0 ){
                            frameLayout.setVisibility(View.GONE);
                            noOrderFound.setVisibility(View.VISIBLE);
                        }else {
                            frameLayout.setVisibility(View.VISIBLE);
                            noOrderFound.setVisibility(View.GONE);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error updateRecyclerAdapter:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();

            }
        };
        try {
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                if(isNewList) {
                    orderListAdapter.clear();
                }
                progressBar.setVisibility(View.VISIBLE);
                DaveModels daveModels = new DaveModels(getActivity(), true);
                HashMap<String ,Object> queryParams = new HashMap<>();
                queryParams.put("_page_size", 50);
                queryParams.put("_page_number",mPageNumber);

                if(strSortByDate!=null && !strSortByDate.isEmpty())
                    queryParams.put(daveAIPerspective.getInvoice_date_attr(), strSortByDate);

                if(strSearchTerm!=null && !strSearchTerm.isEmpty())
                    queryParams.put("_keyword", strSearchTerm);

                if(strCustomerId!=null && !strCustomerId.isEmpty())
                    queryParams.put(new ModelUtil(getActivity()).getCustomerIdAttrName(),strCustomerId);


                Log.e(TAG, "<<<<<<<<<<<<<<<<<<< Print QueryParam>>>>>>>>>>>>>>>>" + queryParams);
                daveModels.getObjects(daveAIPerspective.getInvoice_model_name(), queryParams, daveAIListener, false, false,false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During showing Order LIst:-------------  "+e.getMessage());
        }

    }


    @Override
    public void onClickCard(int position, HashMap<String, Object> orderDetails) {

        Log.e(TAG,"Print FormVIew Button Name:--------"+ daveAIPerspective.getForm_view_button_name() +"orderDetails:- "+orderDetails);
        if(daveAIPerspective.getForm_view_button_name()!=null && !daveAIPerspective.getForm_view_button_name().isEmpty())
        {
            Intent intent = new Intent(getActivity(), FormView.class);
            intent.putExtra("msg_to_form_view", orderDetails);
            startActivityForResult(intent, 10001);

        }else{

            HashMap<String, Object> intent_details = new HashMap<>();
            intent_details.put("openOrderId", orderDetails.get("order_id").toString());
            intent_details.put("customer_id", orderDetails.get("customer_id").toString());
            intent_details.put("customerDetails", "");
            intent_details.put("orderDetails", orderDetails.get("orderDetails"));

            Intent intent = new Intent(getActivity(), CustomerProfile.class);
            intent.putExtra("CustomerData", intent_details);

            if(strCustomerId!= null && !strCustomerId.isEmpty()){
               // getActivity().setResult(CustomerProfile.CUSTOMER_ORDER_CLICKED_REQUEST_CODE,intent);
                getActivity().setResult(Activity.RESULT_OK,intent);
                getActivity().finish();
            }else {
                startActivity(intent);
            }

            //getCustomerDetails(orderDetails.get("customer_id").toString(),intent_details);
        }

    }

    @Override
    public void onDeleteCard(int cardPosition, String orderId, JSONObject orderDetails) {
        if(orderListAdapter.getItemCount()==0){
            noOrderFound.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.GONE);
        }

    }

   /* private void getCustomerDetails(String customerId, HashMap<String, String> intent_details){
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {

                    if(response!=null && response.length()>0) {
                        if (response.has(daveAIPerspective.getCustomer_profile_title()) && !response.isNull(daveAIPerspective.getCustomer_profile_title()))
                            intent_details.put("customer_name", response.getString(daveAIPerspective.getCustomer_profile_title()));

                        intent_details.put("customerDetails", response.toString());

                        Intent intent = new Intent(getActivity(), CustomerProfile.class);
                        intent.putExtra("CustomerData", intent_details);
                        startActivity(intent);
                    }else {
                        Toast.makeText(getActivity(),"Customer Details not found.",Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "Error during  getting Product Id +++++++++++" + e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                // Log.e(TAG, "<<<<<<<<<<<<<<onResponseFailure>>>>>>>>>>>"+productID);
                //showTitle.setText(strCustomerId);
                Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_LONG).show();
            }
        };
        try {
            if(customerId!=null && !customerId.isEmpty()) {
                DaveModels daveModels = new DaveModels(getActivity(), true);
                daveModels.getObject(new ModelUtil(getActivity()).getCustomerModelName(), customerId, daveAIListener, false, false,true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"error during getting customer details:--"+e.getMessage());

        }
    }*/

}
