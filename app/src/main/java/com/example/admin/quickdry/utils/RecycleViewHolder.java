package com.example.admin.quickdry.utils;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.example.admin.daveai.dynamicView.ShowDynamicView;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.adapter.CustomerAdapter;
import com.example.admin.quickdry.fragments.ImageFullView;
import com.squareup.okhttp.HttpUrl;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecycleViewHolder extends RecyclerView.ViewHolder  {

            private static final String TAG = "RecycleViewHolder";
            private Context mContext;
            public TextView errorMsgView,productHeader, productSubHeader,productRightAttr,showTimeStamp,showItemCount,stageName;
            public ImageView conceptImageView,shareImage,zoomImage,errorIconView,deleteView,editInteraction,editProduct;
            public Button getSimilar;
            public RelativeLayout expandViewLayout,collapseViewLayout;
            private TextView productTitle;
            public RecyclerView listExpandRecycler,interactRecyclerView;
            public LinearLayout linearExpandDetails;
	        private DaveAIPerspective daveAIPerspective;






    public RecycleViewHolder(View view) {
        super(view);


        daveAIPerspective = DaveAIPerspective.getInstance();


        shareImage = view.findViewById(R.id.shareImage);
        zoomImage = view.findViewById(R.id.zoomImage);
        conceptImageView =  view.findViewById(R.id.conceptImageView);
        deleteView =  view.findViewById(R.id.deleteView);


        collapseViewLayout = view.findViewById(R.id.collapseViewLayout);
        errorIconView =  view.findViewById(R.id.errorIconView);
        productHeader =  view.findViewById(R.id.productHeader);
        productSubHeader = view.findViewById(R.id.productSubHeader);
        productRightAttr = view.findViewById(R.id.productRightAttr);
        showItemCount = view.findViewById(R.id.showItemCount);
        showTimeStamp = view.findViewById(R.id.showTimeStamp);

        expandViewLayout = view.findViewById(R.id.expandViewLayout);
        errorMsgView = view.findViewById(R.id.errorMsgView);
        getSimilar = view.findViewById(R.id.get_similar);
        editProduct = view.findViewById(R.id.editProduct);
        productTitle = view.findViewById(R.id.productTitle);
        listExpandRecycler = view.findViewById(R.id.listExpand);
        linearExpandDetails = view.findViewById(R.id.linearExpandDetails);

        interactRecyclerView = view.findViewById(R.id.interactRecyclerView);
        editInteraction = view.findViewById(R.id.editInteraction);

        stageName = view.findViewById(R.id.stageName);

    }

    public String bindQuickView(Context context, JSONObject productDetails) {
        this.mContext=context;
        StringBuilder stringBuilder = new StringBuilder();
       // Log.i(TAG,"bindQuickView******productDetails************:- " + daveAIPerspective.getProduct_card_image_view() +" \n ProductDetails:-"+ productDetails );
        try{
            if(daveAIPerspective.getProduct_card_image_view()!= null && productDetails.has(daveAIPerspective.getProduct_card_image_view())
                    && !productDetails.isNull(daveAIPerspective.getProduct_card_image_view())) {

               // Log.e(TAG,"bindQuickView******productImage************:- " + daveAIPerspective.getProduct_card_image_view() +"\n "+productDetails.getString(daveAIPerspective.getProduct_card_image_view())
               // +" \n productId:-"+productDetails.getString(new ModelUtil(context).getProductIdAttrName()));

                String conceptImageUrl = productDetails.getString(daveAIPerspective.getProduct_card_image_view());
                Log.i(TAG,"trying to laod image = "+  conceptImageUrl);



                if(conceptImageUrl.startsWith("http")) {

                    Picasso.with(context)
                            .load(conceptImageUrl)
                            //.networkPolicy(NetworkPolicy.OFFLINE)
                            .placeholder(R.drawable.placeholder)
                            .error(R.drawable.no_image)
                            //.resize(0, 200)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(conceptImageView, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Log.i(TAG, "PICASSO SUccess!!!");
                                }

                                @Override
                                public void onError() {
                                    Log.e(TAG, "PICASSO ERROR!!!" );
                                }
                            });

                }
                else{

                    Glide.with(context)
                        .load(conceptImageUrl)
                        .asBitmap()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.no_image)
                        .listener(new RequestListener<String, Bitmap>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                               // progressBar.setVisibility(View.GONE);
                                Log.e(TAG,"Glide error issue "+e+" target:----------"+ target+" isFirstResource:-----"+isFirstResource);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                //progressBar.setVisibility(View.GONE);
                                Log.e(TAG,"Glide Success onResourceReady target:----------"+ resource+" isFirstResource:-----"+isFirstResource+
                                        "\n width "+ resource.getWidth() +" height: "+ resource.getHeight());
                                Log.e(TAG,"imageview width :- "+ conceptImageView.getWidth() +" Height: "+ conceptImageView.getHeight());
                                return false;
                            }
                        })
                        .into(conceptImageView);
                }


            }else{
                Log.e(TAG,"no image url  Available :------------");
                if(conceptImageView!=null) {
                    Glide.clear(conceptImageView);
                    conceptImageView.setImageResource(R.drawable.no_image);

                }


            }

            if(daveAIPerspective.getProduct_card_header()!=null && productDetails.has(daveAIPerspective.getProduct_card_header()) &&
                    !productDetails.isNull(daveAIPerspective.getProduct_card_header())) {
                productHeader.setText(productDetails.getString(daveAIPerspective.getProduct_card_header()));
                stringBuilder.append(daveAIPerspective.getProduct_card_header().replaceAll("-"," ") +": "+productHeader.getText() +",\n");
            }else {
                productHeader.setText("");
            }

            if(daveAIPerspective.getProduct_card_sub_header()!=null && productDetails.has(daveAIPerspective.getProduct_card_sub_header()) &&
                    !productDetails.isNull(daveAIPerspective.getProduct_card_sub_header())) {
                productSubHeader.setText(productDetails.getString(daveAIPerspective.getProduct_card_sub_header()));
                stringBuilder.append(daveAIPerspective.getProduct_card_sub_header().replaceAll("-"," ") +": "+productSubHeader.getText() + "\n");
            }else {
                productSubHeader.setText("");
            }
           // Log.i(TAG,"bindQuickView******daveAIPerspective.getProduct_card_right_attr()***********:- " +daveAIPerspective.getProduct_card_right_attr());
            if(daveAIPerspective.getProduct_card_right_attr()!=null && productDetails.has(daveAIPerspective.getProduct_card_right_attr()) &&
                    !productDetails.isNull(daveAIPerspective.getProduct_card_right_attr())) {
                productRightAttr.setText(productDetails.getString(daveAIPerspective.getProduct_card_right_attr()));
            }else {
                productRightAttr.setText("");
            }

            if(daveAIPerspective.getShare_image_attributes()!=null && daveAIPerspective.getShare_image_attributes().size()>0){

                ArrayList<String> attrs = daveAIPerspective.getShare_image_attributes();
                for(String attr : attrs){
                    if(productDetails.has(attr)){
                        try {
                            stringBuilder.append(attr + " : " + productDetails.get(attr) + "\n");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error during Product bindCardView_______  " + e.getMessage());
        }

        return stringBuilder.toString();

    }

    public HashMap<String, String> bindExpandView(Context context, Model productModel, JSONObject productDetails) {
        HashMap<String, String> similarFilterDetails = new HashMap<>();
        //Log.e(TAG,"bindExpandView******productDetails************:- " +productDetails);

        try {
            listExpandRecycler.setVisibility(View.VISIBLE);
           // linearExpandDetails.removeAllViews();
            if(productDetails != null){
                if(daveAIPerspective.getProduct_expand_view_title()!=null && productDetails.has(daveAIPerspective.getProduct_expand_view_title())
                && !productDetails.isNull(daveAIPerspective.getProduct_expand_view_title())) {
                    productTitle.setText(productDetails.getString(daveAIPerspective.getProduct_expand_view_title()));
                }else {
                    productTitle.setText("");
                }
                if(daveAIPerspective.getProduct_expand_view() != null) {
                    ShowDynamicView dynamicForm = new ShowDynamicView(context);
                    dynamicForm.expandMultiView(context, listExpandRecycler, daveAIPerspective.getProduct_expand_view(), productModel, productDetails);

                }
                if(daveAIPerspective.getProduct_similar_attributes()!=null){
                    for(int i=0; i<daveAIPerspective.getProduct_similar_attributes().size(); i++){
                        String key = daveAIPerspective.getProduct_similar_attributes().get(i).toString();
                        if(productDetails.has(key)){
                            similarFilterDetails.put(key,productDetails.get(key).toString());
                            /*String keyType = productModel.getAttributeType(key);
                            if(!productModel.getAttributeId(key) && Model.FILTER_TYPE_LIST.contains(keyType)){
                                product_details.put(key,productDetails.get(key).toString());
                            }*/
                        }
                    }
                }

            }


        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error during Product bindExpandView_________  " + e.getMessage());
        }
        return similarFilterDetails;
    }

    public HashMap<String, String> getSimilarFilterDetails(JSONObject productDetails) {
        // Log.e(TAG,"bindExpandView******getSimilarFilterDetails************:- " +productDetails);
        HashMap<String, String> similarFilterDetails = new HashMap<>();
        try {
            if(productDetails != null){
                if(daveAIPerspective.getProduct_similar_attributes()!=null){
                    for(int i=0; i<daveAIPerspective.getProduct_similar_attributes().size(); i++){
                        String key = daveAIPerspective.getProduct_similar_attributes().get(i).toString();
                        if(productDetails.has(key)){
                            similarFilterDetails.put(key,productDetails.get(key).toString());
                        }
                    }
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error during get Similar filter Details_________  " + e.getMessage());
        }
        return similarFilterDetails;
    }



}
