package com.example.admin.quickdry.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.models.EditTextModel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.admin.daveai.others.DaveAIStatic.defaultQuantityStatus;


public class QuickOrderAdapter extends RecyclerView.Adapter<QuickOrderAdapter.CustomViewHolder> implements RecyclerView.OnClickListener {

            private  final String TAG = getClass().getSimpleName();
            private List<JSONObject> seedProductList;
            private List<JSONObject> defaultItemsList;
            private Context mContext;
            private DaveAIPerspective daveAIPerspective;
            private HashMap<String,ArrayList<Object>> selectedItemDetails;// = new HashMap<>();
            private ArrayList<EditTextModel> editModelArrayList;// = new ArrayList<>();
            private ArrayList<EditTextModel> defaultEditModelArrayList;// = new ArrayList<>();
           // public static ArrayList<EditTextModel> editModelArrayList = new ArrayList<>();




    private QuickOrderListener quickOrderListener;
    public interface QuickOrderListener {
        void onQuantityUpdate(HashMap<String ,ArrayList <Object>> itemDetails);
        void onQuantityUpdate(String strProductId, int quantity, double qtyPrice);

    }

    public QuickOrderAdapter(QuickOrderListener selectedDetailsListener, Context context, List<JSONObject> seedProductList) {
        this.mContext = context;
        this.seedProductList = seedProductList;
        defaultItemsList = new ArrayList<>();

        daveAIPerspective = DaveAIPerspective.getInstance();
        this.quickOrderListener = selectedDetailsListener;
        editModelArrayList = new ArrayList<>();
        defaultEditModelArrayList = new ArrayList<>();
        selectedItemDetails = new HashMap<>();

    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_form_content_view,viewGroup, false);
        CustomViewHolder holder = new CustomViewHolder(view);
        holder.itemView.setOnClickListener(QuickOrderAdapter.this);
        holder.itemView.setTag(holder);

        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder customViewHolder, int position) {

        try{
            JSONObject seedItem = seedProductList.get(position);
           // Log.e(TAG," On bind view get Qty at position:---"+seedItem);

            if(defaultQuantityStatus != -1)
                customViewHolder.setQuantity.setText(String.valueOf(defaultQuantityStatus));
            else {
                customViewHolder.setQuantity.setText(editModelArrayList.get(customViewHolder.getAdapterPosition()).getEditTextValue());
            }

            if(daveAIPerspective.getProduct_card_image_view()!= null && seedItem.has(daveAIPerspective.getProduct_card_image_view()) &&
                    !seedItem.isNull(daveAIPerspective.getProduct_card_image_view())) {
                Glide.with(mContext)
                    .load(seedItem.getString(daveAIPerspective.getProduct_card_image_view()))
                    .placeholder(com.example.admin.daveai.R.drawable.placeholder)
                    .error(com.example.admin.daveai.R.drawable.no_image)
                    //.skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(customViewHolder.cardImageView);

            }else{
                Glide.clear(customViewHolder.cardImageView);
            }
            if(daveAIPerspective.getProduct_card_header()!=null && seedItem.has(daveAIPerspective.getProduct_card_header()) &&
                    !seedItem.isNull(daveAIPerspective.getProduct_card_header()))
                customViewHolder.productHeader.setText(seedItem.getString(daveAIPerspective.getProduct_card_header()));

            if(daveAIPerspective.getProduct_card_sub_header()!=null && seedItem.has(daveAIPerspective.getProduct_card_sub_header()) &&
                    !seedItem.isNull(daveAIPerspective.getProduct_card_sub_header()))
                customViewHolder.productSubHeader.setText(seedItem.getString(daveAIPerspective.getProduct_card_sub_header()));



           /* if(daveAIPerspective.getInteraction_qty()!=null && !daveAIPerspective.getInteraction_qty().isEmpty()) {
                if (seedItem.has(daveAIPerspective.getInteraction_qty())) {
                    editModelArrayList.get(position).setEditTextValue(String.valueOf(seedItem.getInt(daveAIPerspective.getInteraction_qty())));
                }
            }else {
                editModelArrayList.get(position).setEditTextValue("0");
            }*/


        }catch (Exception e){
            e.printStackTrace();
            //Log.e(TAG, "Error during FormView  Card Details +++++++++++" + e.getMessage());
        }


       /* customViewHolder.setQuantity.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            customViewHolder.setQuantity.setSelectAllOnFocus(true);


        });*/

        customViewHolder.addQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkQty = String.valueOf(customViewHolder.setQuantity.getText());
                int counter = Integer.parseInt(checkQty);
                counter++;
                customViewHolder.setQuantity.setText(String.valueOf(counter));
                updateQuantity(customViewHolder.getAdapterPosition(),String.valueOf(customViewHolder.setQuantity.getText()));
            }
        });

        customViewHolder.subtractQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkQty = String.valueOf(customViewHolder.setQuantity.getText());
                int counter= Integer.parseInt(checkQty);
                if(counter > 0) {
                    counter--;
                    customViewHolder.setQuantity.setText(String.valueOf(counter));
                    updateQuantity(customViewHolder.getAdapterPosition(),String.valueOf(customViewHolder.setQuantity.getText()));
                }


            }
        });

       /* customViewHolder.setQuantity.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if(customViewHolder.setQuantity.hasFocus()) {
                    if(s.toString().isEmpty())
                        customViewHolder.setQuantity.setText("0");
                    updateQuantity(customViewHolder.getAdapterPosition(),customViewHolder.setQuantity.getText().toString());
                }else {
                    Log.e(TAG,"no focus on edit text:--"+customViewHolder.setQuantity.hasFocus());
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editModelArrayList.get(customViewHolder.getAdapterPosition()).setEditTextValue(String.valueOf(customViewHolder.setQuantity.getText()));
            }
        });
*/
    }


    @Override
    public int getItemCount() {
        return (null != seedProductList ? seedProductList.size() : 0);
    }


    @Override
    public void onClick(View view) {
/*
       CustomViewHolder holder = (CustomViewHolder) view.getTag();
        int cardPosition = holder.getAdapterPosition();*/

    }

    public void addNewSeedList(List<JSONObject> newItemList,ArrayList<EditTextModel> newEditQtyList,boolean isUpdateDefault) {
        if(isUpdateDefault) {
            defaultEditModelArrayList.clear();
            defaultEditModelArrayList.addAll(newEditQtyList);
            defaultItemsList.clear();
            defaultItemsList.addAll(newItemList);
        }
        if(seedProductList.size()>0){
            seedProductList.clear();
            notifyDataSetChanged();
        }
        editModelArrayList.clear();
        editModelArrayList.addAll(newEditQtyList);
        seedProductList.addAll(newItemList);
        notifyItemRangeInserted(0,seedProductList.size()-1);
        Log.e(TAG," After addNewItem size In form Adapter ****************:- " + seedProductList.size());

    }

    public void updateSeedList(List<JSONObject> newItemList,ArrayList<EditTextModel> updateEditQtyList,boolean isUpdateDefault) {
        int startPosition = seedProductList.size();
        seedProductList.addAll(newItemList);
        notifyItemRangeInserted(startPosition,seedProductList.size());
        // notifyDataSetChanged();
        editModelArrayList.addAll(editModelArrayList.size(),updateEditQtyList);

        if(isUpdateDefault) {
            defaultItemsList.addAll(newItemList);
            defaultEditModelArrayList.addAll(defaultEditModelArrayList.size(),updateEditQtyList);
        }
        Log.e(TAG," After addNewItem size In form Adapter ****************:- " + seedProductList.size());
    }

    public void resetPreviousList(){

        if(editModelArrayList.size()>0)
            editModelArrayList.clear();
        editModelArrayList = new ArrayList<>(defaultEditModelArrayList);

        if(seedProductList.size()>0)
            seedProductList.clear();
        seedProductList = new ArrayList<>(defaultItemsList);

        //notifyItemInserted(seedSimilarOrRecommList.size());
        notifyItemRangeChanged(0,seedProductList.size()-1);
        //Log.e(TAG," resetPreviousCustomerList() of adapter****************:- " +seedCustomerList.size()+"  "+ defaultCustomerList.size());

    }

    public void clearPreviousList(){
        seedProductList.clear();
        editModelArrayList.clear();
        notifyDataSetChanged();

    }

    private void  populateEditModelList(int setSize){
        int totalCount = editModelArrayList.size() + setSize;
        for(int i = editModelArrayList.size(); i < totalCount; i++){
            EditTextModel editModel = new EditTextModel();
            editModel.setEditTextValue("0");
            editModelArrayList.add(editModel);
        }
       // Log.e(TAG," After editModelArrayList Size ****************:- " + editModelArrayList.size());

    }
    private void clearEditQtyList(){
        editModelArrayList.clear();
    }

    private void addEditQtyList(String strProductId, int qty){

        EditTextModel editModel = new EditTextModel();
        editModel.setStrProductId(strProductId);
        editModel.setEditTextValue(String.valueOf(qty));
        editModelArrayList.add(editModel);
    }

    private void updateEditQtyList(String strProductId, int qty){
        EditTextModel editModel = new EditTextModel();
        editModel.setStrProductId(strProductId);
        editModel.setEditTextValue(String.valueOf(qty));
        editModelArrayList.add(editModel);
    }

   /* public void getObjectDetails(String productId){

        for(int i = 0 ;i<seedProductList.size();i++) {
            if (seedProductList.get(i).getJSONObject().)
                selectedItemDetails.remove(productId);
        }

    }*/




    public void removeSelectedItemFromList(String productId){
        if(selectedItemDetails.containsKey(productId))
            selectedItemDetails.remove(productId);

    }

    public void removeAllSelectedView(){
        selectedItemDetails.clear();
       // notifyDataSetChanged();
    }



    class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView productHeader,productSubHeader;
        EditText setQuantity;
        ImageView addQty,subtractQty,cardImageView;

        CustomViewHolder(View view) {
            super(view);

            cardImageView = view.findViewById(R.id.cardImageView);
            productHeader = view.findViewById(R.id.productHeader);
            productSubHeader = view.findViewById(R.id.productSubHeader);
            subtractQty =  view.findViewById(R.id.subtract_qty);
            setQuantity = view.findViewById(R.id.quantity_count);
            addQty = view.findViewById(R.id.add_qty);

            // We also want to disable editing when the user exits the field.
            // This will make the button the only non-programmatic way of editing it.
         /*   setQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    // If it loses focus...
                    if (!hasFocus) {
                        // Hide soft keyboard.
                        InputMethodManager imm = (InputMethodManager)mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                        assert imm != null;
                        imm.hideSoftInputFromWindow(setQuantity.getWindowToken(), 0);
                        // Make it non-editable again.
                        setQuantity.setKeyListener(null);
                    }
                }
            });*/

            setQuantity.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                    if(setQuantity.hasFocus()) {
                        updateQuantity(getAdapterPosition(),setQuantity.getText().toString());
                    }/*else {
                        Log.e(TAG,"no focus on edit text:--"+setQuantity.hasFocus());
                    }*/
                }
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    editModelArrayList.get(getAdapterPosition()).setEditTextValue(String.valueOf(setQuantity.getText()));
                }
            });
        }
    }

    private void updateQuantity(int cardPosition, String strQty){
        try{
            if(strQty.isEmpty())
                strQty = "0";
            editModelArrayList.get(cardPosition).setEditTextValue(strQty);
           // Log.e(TAG, "updateQuantity editModelArrayList +++++++++++" +editModelArrayList.get(cardPosition).getEditTextValue());
            int qtyCount = Integer.parseInt(strQty);
            String productIdAttrName = new ModelUtil(mContext).getProductIdAttrName();
            if (productIdAttrName != null && !productIdAttrName.isEmpty()) {
                String cProductId = seedProductList.get(cardPosition).getString(productIdAttrName);
                double itemPrice = seedProductList.get(cardPosition).getDouble(daveAIPerspective.getProduct_price_attr());
                if (qtyCount == 0) {
                    int preQty = seedProductList.get(cardPosition).getInt(daveAIPerspective.getInteraction_qty());
                    if (preQty <= qtyCount) {
                        qtyCount = -1;
                    }
                }
                if (quickOrderListener != null) {
                    quickOrderListener.onQuantityUpdate(cProductId,qtyCount,itemPrice);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during  update FormView  Card Details +++++++++++" + e.getMessage());
        }


    }

    /*private void updateQuantity(int cardPosition, String strQty){
        try{
            int qtyCount = Integer.parseInt(strQty);
            if (productIdAttrName != null && !productIdAttrName.isEmpty()) {
                String cProductId = seedProductList.get(cardPosition).getString(productIdAttrName);
                if (qtyCount > 0) {
                    double qtyPrice = seedProductList.get(cardPosition).getDouble("price");
                    double totalPrice = (qtyCount * qtyPrice);
                    ArrayList<Object> qtyPriceList = new ArrayList<>();
                    qtyPriceList.add(qtyCount);
                    qtyPriceList.add(totalPrice);
                    selectedProductIds.add(cProductId);
                    selectedItemDetails.put(cProductId, qtyPriceList);

                } else if (qtyCount == 0) {
                    int preQty = seedProductList.get(cardPosition).getInt(daveAIPerspective.getInteraction_qty());
                    if (preQty > 0) {
                        if (selectedItemDetails.containsKey(cProductId)) {
                            ArrayList<Object> qtyPriceList = new ArrayList<>();
                            qtyPriceList.add(qtyCount);
                            qtyPriceList.add(0.0);
                            selectedItemDetails.put(cProductId, qtyPriceList);
                        }
                    } else {
                        selectedProductIds.remove(cProductId);
                        if (selectedItemDetails.containsKey(cProductId))
                            selectedItemDetails.remove(cProductId);
                    }

                }
                //editModelArrayList.get(getAdapterPosition()).setEditTextValue(String.valueOf(setQuantity.getText()));
                if (quickOrderListener != null) {
                    quickOrderListener.onQuantityUpdate(selectedItemDetails);
                }
            }

        }catch (Exception e){
            Log.e(TAG, "Error during  update FormView  Card Details +++++++++++" + e.getMessage());
        }


    }

*/


}
                  /*
                   //customViewHolder.setQuantity.setFocusable(true);
                    /*customViewHolder.setQuantity.setClickable(true);
                    customViewHolder.setQuantity.setEnabled(true);

                  try{
                        // is only executed if the EditText was directly changed by the user
                        //if(setQuantity.hasFocus()) {
                        int qtyCount = Integer.parseInt(s.toString());
                        if (productIdAttrName != null && !productIdAttrName.isEmpty()) {
                            String cProductId = seedProductList.get(getAdapterPosition()).getString(productIdAttrName);
                            if (qtyCount > 0) {
                                double qtyPrice = seedProductList.get(getAdapterPosition()).getDouble("price");
                                double totalPrice = (qtyCount * qtyPrice);
                                ArrayList<Object> qtyPriceList = new ArrayList<>();
                                qtyPriceList.add(qtyCount);
                                qtyPriceList.add(totalPrice);
                                selectedProductIds.add(cProductId);
                                selectedItemDetails.put(cProductId, qtyPriceList);

                            } else if (qtyCount == 0) {
                                int preQty = seedProductList.get(getAdapterPosition()).getInt(daveAIPerspective.getInteraction_qty());
                                //int previousValue = Integer.parseInt(editModelArrayList.get(getAdapterPosition()).getEditTextValue());
                                Log.e(TAG, "Get Qty COunt:---" + qtyCount + " == " + preQty);
                                if (preQty > 0) {
                                    if (selectedItemDetails.containsKey(cProductId)) {
                                        ArrayList<Object> qtyPriceList = new ArrayList<>();
                                        qtyPriceList.add(qtyCount);
                                        qtyPriceList.add(0.0);
                                        selectedItemDetails.put(cProductId, qtyPriceList);
                                    }
                                } else {
                                    selectedProductIds.remove(cProductId);
                                    if (selectedItemDetails.containsKey(cProductId))
                                        selectedItemDetails.remove(cProductId);
                                }

                            }
                            //editModelArrayList.get(getAdapterPosition()).setEditTextValue(String.valueOf(setQuantity.getText()));
                            if (quickOrderListener != null) {
                                quickOrderListener.onQuantityUpdate(selectedItemDetails);
                            }
                        }

                    }catch (Exception e){
                        Log.e(TAG, "Error during  update FormView  Card Details +++++++++++" + e.getMessage());
                    }
*/