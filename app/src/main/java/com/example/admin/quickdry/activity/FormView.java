package com.example.admin.quickdry.activity;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dialogbox.CustomProgressDialog;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveAIStatic;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.adapter.QuickOrderAdapter;
import com.example.admin.quickdry.dialogs.OrderCheckoutDialog;
import com.example.admin.quickdry.models.EditTextModel;
import com.example.admin.quickdry.storage.SharedPreference;

import org.json.JSONArray;
import org.json.JSONObject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import static com.example.admin.daveai.others.DaveAIStatic.defaultQuantityStatus;
import static com.example.admin.quickdry.storage.SharedPreference.USER_ID;


public class FormView extends BaseActivity implements QuickOrderAdapter.QuickOrderListener {

    private static final String TAG = "FormView";
    private Context context;

    private RecyclerView mRecyclerView;
    private QuickOrderAdapter formViewAdapter;
    private LinearLayoutManager mLayoutManager;
    LinearLayout menuItemActionLayout,breadCrumbsLayout,subHeaderLayout,formParentLayout;
    RelativeLayout itemDetailsView;
    TextView priceMin, priceMax,productNotFound;
    CrystalRangeSeekbar rangeSeekbar;
    HorizontalScrollView breadcrumbWrapper,itemDetailsParent;
    private Button btnSaveOrder;
    ArrayAdapter<String> filterParentAdapter,filterChildAdapter;
    ListView filterParentView,filterChildView;
    CustomProgressDialog customProgressDialog;

    String strCustomerId = "";
    private DaveAIPerspective daveAIPerspective;
    private String currentOrderId = null;
    SharedPreference sharedPreference;
    Model productModelInstance;
    ModelUtil modelUtil;

    //pagination
    private int mPageNumber = 1;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean nextPageLoading = true;
    private boolean isLastPage =false;
    String productModelName;
    String interactionModelName;

    //category
    ArrayList hierarchyAttributes = new ArrayList<>();
    HashMap<String, String> hierarchyAttrTitleMap = new HashMap<>();
    private ArrayList<Object> breadCrumbDetails = new ArrayList<>();
    Stack<Map<Object, Object>> categoryStackFlow = new Stack<>();

    boolean isAnyActionApply = false;
    private String searchedTerm="";

    //filter
    int filter_position = 0;
    HashMap<String,ArrayList<String>> filterListMap;
    HashMap<String, String> filterTitleName = new HashMap<>();
    ArrayList<String> filterAttr = new ArrayList<>();
    HashMap<String, ArrayList<String>> selectedCategoryMap = new HashMap<>();
    HashMap<String, ArrayList<String>> selectedFilterMap = new HashMap<>();

    int orderedProductCount = 0;
    private JSONObject orderedProductDetails = new JSONObject();
    HashMap<String, ArrayList<Object>> orderedProductQtyMap = new HashMap<>();
    HashMap<String, ArrayList<Object>> tempSelectedItemList = new HashMap<>();
    JSONObject productQtyMap = new JSONObject();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_view);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        formParentLayout = findViewById(R.id.formParentLayout);
        TextView textView =  findViewById(R.id.showToolbarTitle);
        itemDetailsParent = findViewById(R.id.itemDetailsParent);
        subHeaderLayout = findViewById(R.id.subHeaderLayout);
        btnSaveOrder = findViewById(R.id.btnSaveOrder);
        breadcrumbWrapper = findViewById(R.id.breadcrumbWrapper);
       // breadcrumbWrapper.setVisibility(View.GONE);
        breadCrumbsLayout = findViewById(R.id.breadCrumbsLayout);
        menuItemActionLayout = findViewById(R.id.menuItemActionLayout);
        productNotFound = findViewById(R.id.productNotFound);
        itemDetailsView = findViewById(R.id.itemDetailsView);
        Button btnCheckout = findViewById(R.id.checkout);

        defaultQuantityStatus=-1;
        context= FormView.this;
        daveAIPerspective= DaveAIPerspective.getInstance();
        sharedPreference = new SharedPreference(FormView.this);
        modelUtil = new ModelUtil(FormView.this);
        strCustomerId = sharedPreference.readString( SharedPreference.CUSTOMER_ID);
        currentOrderId = sharedPreference.readString(strCustomerId);
        productModelName = modelUtil.getProductModelName();
        interactionModelName = modelUtil.getInteractionModelName();
        filterListMap = modelUtil.getProductFilterAttrList();
        productModelInstance = Model.getModelInstance(FormView.this, productModelName);
        customProgressDialog = new CustomProgressDialog(FormView.this);


        Log.i(TAG, "Print customer and Order id:--:----" +strCustomerId +" &&:- "+currentOrderId);

        textView.setText(daveAIPerspective.getForm_view_button_name());
        formViewAdapter = new QuickOrderAdapter(FormView.this,FormView.this, new ArrayList<>());
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(formViewAdapter);

        try {
           HashMap<String, Object> hashMap2 = (HashMap<String, Object>) getIntent().getSerializableExtra("msg_to_form_view");
           //Log.i(TAG,"hashMap2***************:- " + hashMap2);
           if (hashMap2 != null) {

              /* if(hashMap2.containsKey("customer_id")){
                   strCustomerId = hashMap2.get("customer_id").toString();
               }*/

               if(hashMap2.containsKey("bread_crumb_details")){
                   breadCrumbDetails= (ArrayList<Object>) hashMap2.get("bread_crumb_details");
               }

               if (hashMap2.containsKey("productDetails") && hashMap2.get("productDetails").toString() != null) {
                   HashMap<String, Object> productAttr = (HashMap<String, Object>) hashMap2.get("productDetails");
                   if (productAttr != null)
                       for (String key : productAttr.keySet()) {
                           if (productAttr.get(key) instanceof ArrayList) {
                               selectedFilterMap.put(key, (ArrayList<String>) productAttr.get(key));
                               selectedCategoryMap.put(key, (ArrayList<String>) productAttr.get(key));
                           } else
                               addCheckedItemToMapDefault(key, productAttr.get(key).toString());
                       }
               }
               /*if(hashMap2.containsKey("order_id")){
                   currentOrderId =  hashMap2.get("order_id").toString();
               }*/

           }

           Model productModel = Model.getModelInstance(context, productModelName);
           if (filterListMap != null && !filterListMap.isEmpty()) {
               for (Map.Entry<String, ArrayList<String>> entry : filterListMap.entrySet()) {
                   if (productModel != null)
                       filterTitleName.put(productModel.getAttributeTitle(entry.getKey()), entry.getKey());
               }
               filterAttr.addAll(filterTitleName.keySet());
           }

           hierarchyAttributes = daveAIPerspective.getProduct_category_hierarchy();
           if (hierarchyAttributes != null && !hierarchyAttributes.isEmpty()) {
                for(int i = 0;i < hierarchyAttributes.size();i++) {
                    if(productModelInstance!=null){
                        hierarchyAttrTitleMap.put(hierarchyAttributes.get(i).toString(),productModelInstance.getAttributeTitle(hierarchyAttributes.get(i).toString()));
                    }
                }
                Log.e(TAG,"Print hierarchyAttrTitleMap:------------"+hierarchyAttrTitleMap);
                showCategoryHierarchy();
           }
           addDynamicSubHeaderView("Total Count ", "0");
           addDynamicSubHeaderView("Total Qty ", "0");
           addDynamicSubHeaderView("Total Price ", "0.0");

           getQtyCountFromInteraction();
           getProductsList(true,true,true);

       }catch (Exception e){
           e.printStackTrace();
       }
       mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if(nextPageLoading && daveAIPerspective.isInvoice_show_zero_qty()) {
                        if (!isLastPage && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            nextPageLoading = false;
                            mPageNumber++;
                            //Do pagination.. i.e. fetch new data from Server
                            if(isAnyActionApply){
                                getProductsList(false,false,false);
                            }else {
                                getProductsList(false,false,true);
                            }
                        }
                    }
                }
            }
       });

       btnSaveOrder.setOnClickListener(v -> {
           saveOrderOpenReviewPopUp(false);
       });

       btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOrderOpenReviewPopUp(true);

            }
       });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.home_page, menu);

        getMenuInflater().inflate(R.menu.menu_dave_function, menu);
        if(daveAIPerspective.getScan_btn_name()!=null) {
            menu.findItem(R.id.actionScanProduct)
           .setTitle(daveAIPerspective.getScan_btn_name())
           .setVisible(true);
        }
        if(daveAIPerspective.isEnable_manage_product()) {
            menu.findItem(R.id.actionAddProduct).setVisible(true);
        }
        menu.findItem(R.id.actionSort).setVisible(false);
        menu.findItem(R.id.actionCategory).setVisible(false);

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        searchedTerm= "";
        menuItemActionLayout.removeAllViews();
        if (id == R.id.actionSearch) {
            actionOnSearchMenuButton();
            return true;

        } else if (id == R.id.actionFilter) {
            actionOnFilterMenuButton();
            return true;

        } else if (id ==R.id.actionCategory) {
            actionOnCategoryMenuButton();
            return true;

        } else if (id ==R.id.actionSetQuantity) {
            actionOnSetQtyMenuButton();
            return true;
        }
        else if (id == R.id.actionScanProduct) {

            Bundle b= new Bundle();
            b.putString("action_name", AddScanProduct.ACTION_SCAN_PRODUCT);
            b.putString("customer_id",strCustomerId);
            Intent intent= new Intent(context, AddScanProduct.class);
            intent.putExtras(b);
            startActivity(intent);

            return true;
        }
        else if (id ==R.id.actionAddProduct) {

            Bundle b= new Bundle();
            b.putString("action_name",AddScanProduct.ACTION_ADD_PRODUCT);
            b.putString("customer_id",strCustomerId);
            Intent intent= new Intent(context, AddScanProduct.class);
            intent.putExtras(b);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        /*Intent intent = new Intent(getApplicationContext(), LoginScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);*/
    }

    @Override
    public void onPause(){
        super.onPause();
        // save order and intent to splash screen
        saveSelectedData(new OnSaveQuickOrderCallback() {
            @Override
            public void onClickSaveButton() {
                   Intent intent = new Intent(getApplicationContext(), LoginScreen.class);
                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                   startActivity(intent);

            }
            //},true);
        },false);


    }

    @Override
    public void onBackPressed() {

        // onBackPressed();
        if (selectedProductIds.isEmpty()) {
            Log.i(TAG,"when no any interaction>>>>>>>>>>>>>>>>>>>>>>>>>>.");

            if (currentOrderId != null) {
                OrderCheckoutDialog cdd = new OrderCheckoutDialog(context, currentOrderId,false,
                    new OrderCheckoutDialog.OrderCheckoutListener() {
                    @Override
                    public void onCheckoutOrder(JSONObject orderDetails) {
                        Log.i(TAG,"onCheckoutOrder>>>>>>>>>>>>>>>>>>>>>>>>>>.");
                        setResult(10001);
                        finish();


                    }

                    @Override
                    public void onCancelOrder() {
                        Log.i(TAG,"onCancelOrder>>>>>>>>>>>>>>>>>>>>>>>>>>.");
                        setResult(10001);
                        finish();

                    }
                });
                cdd.show();
            }
        } else{
            saveSelectedData(new OnSaveQuickOrderCallback() {
                @Override
                public void onClickSaveButton() {
                    Log.i(TAG," Click on button Checkout Check Current Order  Id:---------   "+currentOrderId);
                    if (currentOrderId != null) {
                        OrderCheckoutDialog cdd = new OrderCheckoutDialog(context, currentOrderId,false,
                                new OrderCheckoutDialog.OrderCheckoutListener() {
                                    @Override
                                    public void onCheckoutOrder(JSONObject orderDetails) {
                                        setResult(10001);
                                        finish();

                                    }

                                    @Override
                                    public void onCancelOrder() {
                                        setResult(10001);
                                        finish();
                                    }
                                });
                        cdd.show();
                    }else
                        Toast.makeText(context,"Please create New Order",Toast.LENGTH_SHORT).show();
                }
                //},true);
            },false);
        }

    }

    private void saveOrderOpenReviewPopUp(boolean isCheckout){
        if (selectedProductIds.isEmpty()) {
            if (currentOrderId != null) {
                OrderCheckoutDialog cdd = new OrderCheckoutDialog(context, currentOrderId,isCheckout,
                    new OrderCheckoutDialog.OrderCheckoutListener() {
                    @Override
                    public void onCheckoutOrder(JSONObject orderDetails) {
                        if(isCheckout){
                            setResult(10001);
                            finish();
                        }

                    }

                    @Override
                    public void onCancelOrder() {


                    }
                });
                cdd.show();
            }
        } else{
            saveSelectedData(new OnSaveQuickOrderCallback() {
                @Override
                public void onClickSaveButton() {
                    Log.i(TAG," Click on button Checkout Check Current Order  Id:---------   "+currentOrderId);
                    if (currentOrderId != null) {
                        OrderCheckoutDialog cdd = new OrderCheckoutDialog(context, currentOrderId,isCheckout,
                                new OrderCheckoutDialog.OrderCheckoutListener() {
                                    @Override
                                    public void onCheckoutOrder(JSONObject orderDetails) {
                                        if(isCheckout){
                                            setResult(10001);
                                            finish();
                                        }

                                    }

                                    @Override
                                    public void onCancelOrder() {


                                    }
                                });
                        cdd.show();
                    }else
                        Toast.makeText(context,"Please create New Order",Toast.LENGTH_SHORT).show();
                }
                //},true);
            },false);
        }


    }

    /*
       method to get Ordered Product from Interaction based on orderId and interaction details.
       mapping with:- 1) product id to qty (productQtyMap)
                      2)product id to interaction Details (orderedProductDetails)
     */
    private void getQtyCountFromInteraction(){
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    if(response!= null && response.has("data") && !response.isNull("data")){
                        JSONArray jsonArr = response.getJSONArray("data");
                        Log.i(TAG, "get QtyCountFromInteraction>>>>>>>>>>>>>>>>>>>>>   "+jsonArr.length());
                        if(jsonArr!=null && jsonArr.length()>0) {
                            for(int i =0;i<jsonArr.length();i++) {
                                JSONObject jsonObject = jsonArr.getJSONObject(i);
                                String productId = jsonObject.getString(modelUtil.getProductIdAttrName());
                                orderedProductDetails.put(productId,jsonObject);
                                int qty = jsonObject.getInt(daveAIPerspective.getInteraction_qty());
                                if(productQtyMap.has(productId)){
                                    productQtyMap.put(productId,qty + productQtyMap.getInt(productId));
                                }else {
                                    productQtyMap.put(productId,qty);
                                }
                            }
                        }
                        orderedProductCount = 0;
                        getOrderedProductDetails(jsonArr);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error show Qty for productId:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(modelUtil.getInteractionModelName()!=null && !modelUtil.getInteractionModelName().isEmpty()) {
                HashMap<String, Object> queryParam = new HashMap<>();
                queryParam.put(daveAIPerspective.getInvoice_id_attr_name(),currentOrderId);
                Log.e(TAG,"Print stage attr name and value :----------"+ modelUtil.getInteractionStageAttrName()+" "+ daveAIPerspective.getInvoice_stage_attr());
                queryParam.put(modelUtil.getInteractionStageAttrName(),daveAIPerspective.getInvoice_stage_attr());
                DaveModels daveModels = new DaveModels(context, true);
                daveModels.getObjects(new ModelUtil(FormView.this).getInteractionModelName(), queryParam, daveAIListener,
                        false, false);
            }else {
                Log.e(TAG, "interaction model is empty or null :- :---------   " +modelUtil.getInteractionModelName());
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error during getting Qty based on orderId and ProductID:-------------  "+e.getMessage());
        }

    }


    /*
     method to get product Details of Orederd product and add product details of ordered product in orderedProductDetails
     */
    private void getOrderedProductDetails(JSONArray interactionList ){
        try {
            if (orderedProductCount < interactionList.length()) {
                JSONObject interactionObject = interactionList.getJSONObject(orderedProductCount);
                String productID = interactionObject.getString(modelUtil.getProductIdAttrName());
                String interactionId = interactionObject.getString(modelUtil.getInteractionIdAttrName());
                DaveAIListener daveAIListener = new DaveAIListener() {
                    @Override
                    public void onReceivedResponse(JSONObject response) {
                        try {
                            JSONObject iObject = interactionList.getJSONObject(orderedProductCount);
                            iObject.put("details", response);
                            orderedProductDetails.put(productID,iObject);
                            orderedProductCount++;
                            getOrderedProductDetails(interactionList);
                        } catch (Exception e) {
                            Log.e(TAG, "Error during  getting Product Id +++++++++++" + e.getMessage());
                        }
                    }

                    @Override
                    public void onResponseFailure(int requestCode, String responseMsg) {
                         Log.e(TAG, "<<<<<<<<<<<<<<onResponseFailure>>>>>>>>>>>"+productID);

                    }
                };

                if (productID != null && !productID.isEmpty()) {
                    DaveModels daveModels = new DaveModels(context, true);
                    daveModels.getObject(modelUtil.getProductModelName(), productID, daveAIListener, false, false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "error during getting product details:--" + e.getMessage());
        }
    }

    /*
      method to get product list
      1) First load previous ordered product details
      2) load product details if invoice_show_zero_qty is true

     */
    private void getProductsList(boolean loader,boolean isLoadNewData, boolean isUpdateDefault){
        try {
            //show  custom loader
            if(loader){
                ProgressBar progressBar = customProgressDialog.showProgressBar();
                itemDetailsView.addView(progressBar);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)progressBar.getLayoutParams();
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                progressBar.setLayoutParams(layoutParams);
            }
            if(isLoadNewData) {
                tempSelectedItemList.clear();
                tempSelectedItemList = new HashMap<>();
                formViewAdapter.clearPreviousList();
                Log.i(TAG," clear tempSelectedItemList list && print Filter attr before getting data  "+selectedFilterMap);
            }

            String productIdAttrName =  new ModelUtil(FormView.this).getProductIdAttrName();
            List<JSONObject> objectData = firstLoadOrderedProduct();
            Log.i(TAG,"<<<<<<<<<<<<<<< getProductsList Print Ordered product Count:---"+objectData.size());
            ArrayList<EditTextModel> editModelArrayList = new ArrayList<>();
            ArrayList<String> productIds = new ArrayList<>();
            //add Ordered product data in adapter list
            if(objectData.size()> 0) {
                for (int i = 0; i < objectData.size(); i++) {
                    JSONObject jProductObject = objectData.get(i);
                    EditTextModel editModel = new EditTextModel();
                    editModel.setStrProductId(jProductObject.getString(productIdAttrName));
                    editModel.setEditTextValue(String.valueOf(jProductObject.getInt(daveAIPerspective.getInteraction_qty())));
                    editModelArrayList.add(editModel);
                    productIds.add(jProductObject.getString(productIdAttrName));

                    int qtyCount =0;
                    double productPrice = 0.0;
                    if(daveAIPerspective.getInteraction_qty()!=null && jProductObject.has(daveAIPerspective.getInteraction_qty()) && !jProductObject.isNull(daveAIPerspective.getInteraction_qty())){
                        qtyCount = jProductObject.getInt(daveAIPerspective.getInteraction_qty());
                    }
                    if(daveAIPerspective.getProduct_price_attr()!=null && jProductObject.has(daveAIPerspective.getProduct_price_attr()) && !jProductObject.isNull(daveAIPerspective.getProduct_price_attr())){
                        productPrice =jProductObject.getDouble(daveAIPerspective.getProduct_price_attr());
                    }
                    updateQuantityStatus(jProductObject.getString(productIdAttrName), qtyCount, productPrice, false);

                }
            }else {
                updateQuantityStatus("", 0, 0.0, false);
            }
            if(daveAIPerspective.isInvoice_show_zero_qty()){
                //todo make call to get product data
                String productModelName = new ModelUtil(FormView.this).getProductModelName();
                if(productModelName!= null && !productModelName.isEmpty()) {
                    HashMap<String, Object> queryParam = new HashMap<>();
                    queryParam.put("_page_size", 50);
                    queryParam.put("_page_number", mPageNumber);

                    for (String Getkey : selectedFilterMap.keySet()) {
                        ArrayList<String> get_lis = selectedFilterMap.get(Getkey);
                        for (int j = 0; j < get_lis.size(); j++) {
                            if(get_lis.get(j)!= null && !get_lis.get(j).equalsIgnoreCase("null"))
                                queryParam.put(Getkey, get_lis.get(j));
                        }
                    }
                    if(searchedTerm!=null && !searchedTerm.isEmpty()){
                        queryParam.put("_keyword", searchedTerm);
                    }
                    Log.i(TAG,"<<<<<<<<<<<<<<<<<<queueHashMap>>>>>>>>>>>>>>>>>>>>> "+queryParam);
                    DaveModels daveModels = new DaveModels(context, true);
                    daveModels.getObjects(productModelName, queryParam, new DaveAIListener() {
                        @Override
                        public void onReceivedResponse(JSONObject response) {
                            try {
                                customProgressDialog.hideProgressBar(itemDetailsView);
                                //add product data in adapter list
                                if(response!= null && response.has("data") && !response.isNull("data")){
                                    JSONArray jsonArray = response.getJSONArray("data");
                                    if(jsonArray!=null && jsonArray.length()>0) {
                                        for(int i =0 ;i< jsonArray.length();i++){
                                            JSONObject productObject = jsonArray.getJSONObject(i);
                                            if(!productIds.contains(productObject.getString(productIdAttrName))) {
                                                if(daveAIPerspective.getInteraction_qty()!= null)
                                                    productObject.put(daveAIPerspective.getInteraction_qty(),0);
                                                objectData.add(productObject);
                                                EditTextModel editModel = new EditTextModel();
                                                editModel.setStrProductId(productObject.getString(productIdAttrName));
                                                //editModel.setEditTextValue("0");
                                                if(productObject.has(daveAIPerspective.getInteraction_qty()))
                                                    editModel.setEditTextValue(String.valueOf(productObject.getInt(daveAIPerspective.getInteraction_qty())));
                                                else
                                                    editModel.setEditTextValue("0");

                                                editModelArrayList.add(editModel);
                                            }
                                        }
                                    /*if (isLoadNewData)
                                        formViewAdapter.addNewSeedList(objectData,editModelArrayList,isUpdateDefault);
                                    else
                                        formViewAdapter.updateSeedList(objectData,editModelArrayList,isUpdateDefault);

                                    productNotFound.setVisibility(View.GONE);
                                    itemDetailsView.setVisibility(View.VISIBLE);
                                    mRecyclerView.scrollToPosition(pastVisiblesItems);
                                    nextPageLoading = true;*/

                                    }/*else{
                                    if(formViewAdapter.getItemCount()==0){
                                        productNotFound.setVisibility(View.VISIBLE);
                                        itemDetailsView.setVisibility(View.GONE);
                                    }
                                }*/
                                }else {
                                    Log.e(TAG,"Get  product based on queryPAram  not found :- "+response);
                                }
                                //add data list in adapter
                                setDataInAdapter(objectData,editModelArrayList,isLoadNewData,isUpdateDefault);

                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e(TAG,"Error updateRecyclerAdapter:-------------  "+e.getMessage());
                            }

                        }

                        @Override
                        public void onResponseFailure(int requestCode, String responseMsg) {
                            customProgressDialog.hideProgressBar(itemDetailsView);
                            Toast.makeText(context,responseMsg,Toast.LENGTH_SHORT).show();
                            //add data list in adapter
                            setDataInAdapter(objectData,editModelArrayList,isLoadNewData,isUpdateDefault);

                        }
                    }, false, false);
                }
            }else{
                //todo set previous ordered product data
                customProgressDialog.hideProgressBar(itemDetailsView);
                setDataInAdapter(objectData,editModelArrayList,isLoadNewData,isUpdateDefault);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error during getting ProductList:-------------  "+e.getMessage());
        }

    }

    //add data list in adapter
    private void setDataInAdapter( List<JSONObject> objectData,ArrayList<EditTextModel> editModelArrayList, boolean isLoadNewData,
                                   boolean isUpdateDefault){

        Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<setDataInAdapter object count:- "+objectData.size());
        if(objectData.size()>0){
            if (isLoadNewData)
                formViewAdapter.addNewSeedList(objectData,editModelArrayList,isUpdateDefault);
            else
                formViewAdapter.updateSeedList(objectData,editModelArrayList,isUpdateDefault);

            productNotFound.setVisibility(View.GONE);
            itemDetailsView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mRecyclerView.scrollToPosition(pastVisiblesItems);
            nextPageLoading = true;

        }else{
            if(formViewAdapter.getItemCount()==0){
                productNotFound.setVisibility(View.VISIBLE);
                itemDetailsView.setVisibility(View.GONE);
            }
        }
    }

    /*
     * method to get list of ordered Product based on with or without filter
     * list contains 1st details which has qty is greater than 0 and at last add all products details which has 0 qty.
     * @return ArrayList<JSONObject> of ordered product
     */
    private ArrayList<JSONObject> firstLoadOrderedProduct(){
        ArrayList<JSONObject> objectData = new ArrayList<>();
        ArrayList<JSONObject> zeroQtyData = new ArrayList<>();
        try {
            if(orderedProductDetails.length() > 0) {
                Log.i(TAG,"firstLoadOrderedProduct Total Count of Ordered Product:----"+orderedProductDetails.length()+" filterMap:-"+selectedFilterMap);
                Iterator<String> keys = orderedProductDetails.keys();
                while (keys.hasNext()) {
                    String productId = keys.next();
                    JSONObject iDetail = orderedProductDetails.getJSONObject(productId);
                    int qty = iDetail.getInt(daveAIPerspective.getInteraction_qty());
                    JSONObject pDetails = iDetail.getJSONObject("details");
                    boolean isProductExist = false;
                    if (selectedFilterMap.size() > 0) {
                        // search with multiple key in product jsonobject
                        Log.i(TAG,"firstLoadOrderedProduct with selectedFilterMap:----"+selectedFilterMap);
                        ArrayList<String> productIds = new ArrayList<>();
                        for (String attrKey : selectedFilterMap.keySet()) {
                            ArrayList<String> attrValue = selectedFilterMap.get(attrKey);
                            Log.i(TAG, " firstLoadOrderedProduct  with print selected FilterKey**************** " + attrKey + " value:- " + attrValue);
                            if (attrValue.size() > 0 && !attrValue.contains("null")) {
                                for (int j = 0; j < attrValue.size(); j++) {
                                    if (pDetails.has(attrKey) && !pDetails.isNull(attrKey) && pDetails.getString(attrKey).equalsIgnoreCase(attrValue.get(j))) {
                                        isProductExist = true;
                                    }else {
                                        Log.e(TAG, " <<<<<<<<<<<<<<<<<<<<<<<<else not exist>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
                                        isProductExist = false;
                                        break;
                                    }
                                }
                                if(!isProductExist)
                                    break;
                            }
                        }
                        if(isProductExist){
                            pDetails.put(daveAIPerspective.getInteraction_qty(), qty);
                            Log.i(TAG, " Exist get Product Details of interaction:------" + iDetail.get("details"));
                            if(!productIds.contains(productId)) {
                                productIds.add(productId);
                                if (qty > 0)
                                    objectData.add(pDetails);
                                else
                                    zeroQtyData.add(pDetails);
                            }
                        }
                    }else {
                        pDetails.put(daveAIPerspective.getInteraction_qty(), qty);
                        if(qty>0)
                            objectData.add(pDetails);
                        else
                            zeroQtyData.add(pDetails);
                    }

                }
                if(zeroQtyData.size()>0)
                    objectData.addAll(zeroQtyData);
            }

        }catch (Exception e){
            e.printStackTrace();
            return objectData;
        }
       // Log.e(TAG,"loadPreviousOrderProduct:--------------"+objectData.size());
        return objectData;
    }

    private boolean searchKeyInJsonObject(String key,Object value, JSONObject jsonObject){
        try {
            if (jsonObject.has(key) && !jsonObject.isNull(key) && jsonObject.get(key) == value) {
               return true;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
       return false;
    }


    private void showCategoryHierarchy(){
        Log.i(TAG,"get Product Category List :------------"+ hierarchyAttributes);
        breadcrumbWrapper.setVisibility(View.VISIBLE);
        for(int index = 0 ; index< hierarchyAttributes.size(); index++){

            final ViewGroup crumbLayout = (ViewGroup) LayoutInflater.from(context)
                    .inflate(R.layout.bread_crumbs_view, breadcrumbWrapper, false);

            TextView crumbArrow = crumbLayout.findViewById(R.id.crumbArrow);
            crumbArrow.setVisibility(View.GONE);
            Spinner crumbSpinner = crumbLayout.findViewById(R.id.crumbSpinner);
            breadCrumbsLayout.addView(crumbLayout);

            if(index>0) {
                crumbArrow.setVisibility(View.VISIBLE);
            }
            String hierarchyTitle = hierarchyAttrTitleMap.get(hierarchyAttributes.get(index));
            breadCrumbDetails.add(index, hierarchyTitle);
            ArrayList<String> categories = new ArrayList<String>();
            categories.add(hierarchyTitle);
            if(index==0) {
                Map<String, Object> categoryLevelData =  new ModelUtil(context).getProductCategoryHierarchy();
                Map<Object, Object> updatedLevel = new HashMap<>();
                updatedLevel.put(hierarchyTitle, categoryLevelData);
                categoryStackFlow.add(index, updatedLevel);
                ArrayList<String> cat = new ArrayList(categoryLevelData.keySet());
                Collections.sort(cat);
                categories.addAll(cat);

            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, categories);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            crumbSpinner.setAdapter(dataAdapter);
            crumbSpinner.setSelection(crumbSpinner.getSelectedItemPosition(),false);

            final HashMap<String, Object> intentDetails = new HashMap<>();
            intentDetails.put("category_level", index);
            intentDetails.put("back_navigation_item", hierarchyTitle);
            intentDetails.put("back_navigation_position", index);

            crumbSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                   /* if(position>0) {
                        String strSelected = crumbSpinner.getSelectedItem().toString();
                        // Log.e(TAG, " onItemSelected  item value  :- " + strSelected + "  &&  SpinnerPosition " + position);
                        intentDetails.put("selected_item", crumbSpinner.getSelectedItem());
                        onClickCategoryHierarchy(intentDetails);
                    }*/

                    intentDetails.put("selected_item", crumbSpinner.getSelectedItem());
                    onClickCategoryHierarchy(intentDetails);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }


    private void onClickCategoryHierarchy(HashMap<String, Object> hashMap ){
        try {
            int categoryLevel = (int) hashMap.get("category_level");
            String preValue = breadCrumbDetails.get(categoryLevel).toString();
            String selectedItem = hashMap.get("selected_item").toString();
            String categoryName = daveAIPerspective.getProduct_category_hierarchy().get(categoryLevel).toString();
            String hierarchyTitle = hierarchyAttrTitleMap.get(categoryName);

            Log.e(TAG,"************onClickCategoryHierarchy Start position: "+ categoryLevel +" preValue: "+preValue+ " selectedItem: "+selectedItem );
            if(categoryLevel == 0) {
                selectedFilterMap.clear();

            }else{
                for (int i = categoryLevel; i < breadCrumbDetails.size(); i++) {
                    if (selectedFilterMap.containsKey(daveAIPerspective.getProduct_category_hierarchy().get(i).toString())) {
                        selectedFilterMap.remove(daveAIPerspective.getProduct_category_hierarchy().get(i).toString());
                    }
                }
            }
            Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Print Filter selectedFilterMap== "+selectedFilterMap);
            if (!preValue.equalsIgnoreCase(selectedItem) ||
                    (preValue.equalsIgnoreCase(hierarchyTitle) && !selectedItem.equalsIgnoreCase(hierarchyTitle))){

                Map<Object, Object> getLevelValue = categoryStackFlow.get(categoryLevel);
                //Log.i(TAG, " Print getLevelValue>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + getLevelValue);
                for(int i= categoryStackFlow.size()-1; i >= categoryLevel;i--){
                    categoryStackFlow.pop();
                }
                //Log.i(TAG, "Print After pop categoryStackFlow:-------"+categoryStackFlow);

                Map<String, Object> categoryLevelData = (Map<String, Object>)getLevelValue.get(preValue);
                Log.i(TAG, " Print categoryLevelData>>>>>>>>>>>>>>" + categoryLevelData);

                //add current level selected item to breadcrumbDetails
                breadCrumbDetails.set(categoryLevel, selectedItem);
                Map<Object, Object> updatedLevel = new HashMap<>();
                updatedLevel.put(selectedItem, categoryLevelData);
                categoryStackFlow.add(categoryLevel, updatedLevel);

                Log.e(TAG," Print After updating breadcrumb and stackFlow:-"+ breadCrumbDetails +" "+ categoryStackFlow);

                if (categoryLevel < breadCrumbDetails.size() - 1 ) {

                    String nextHierarchyTitle = hierarchyAttrTitleMap.get(hierarchyAttributes.get(categoryLevel + 1));
                    breadCrumbDetails.set(categoryLevel + 1, nextHierarchyTitle);
                    View v = breadCrumbsLayout.getChildAt(categoryLevel + 1);
                    Spinner getSpinner = v.findViewById(R.id.crumbSpinner);
                    ArrayList<String> nextCategoriesList = new ArrayList<String>();
                    nextCategoriesList.add(nextHierarchyTitle);

                    if(!selectedItem.equalsIgnoreCase(hierarchyTitle)){
                        categoryLevelData = (Map<String, Object>) categoryLevelData.get(selectedItem);
                        Map<Object, Object> updatedNextLevel = new HashMap<>();
                        updatedNextLevel.put(nextHierarchyTitle, categoryLevelData);
                        categoryStackFlow.add(categoryLevel + 1, updatedNextLevel);

                        ArrayList<String> cat = new ArrayList(categoryLevelData.keySet());
                        Collections.sort(cat);
                        nextCategoriesList.addAll(cat);
                        getSpinner.performClick();
                    }else{
                        Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<<<Selected value is not select>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    }
                    Log.i(TAG, "nextLevelDetails List Element :- " + nextCategoriesList + " nextLevelDetails:- " + categoryLevelData);
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, nextCategoriesList);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    getSpinner.setAdapter(dataAdapter);
                    getSpinner.setSelection(0, false);

                    //Log.i(TAG, " if nextLevelData   :- " + (categoryLevel + 1) + " is ==  " + (breadCrumbDetails.size() - 1));
                    if (categoryLevel + 1 < breadCrumbDetails.size() - 1) {
                        for (int k = categoryLevel + 2; k < breadCrumbsLayout.getChildCount(); k++) {
                            View view = breadCrumbsLayout.getChildAt(k);
                            Spinner getNextSpinner = view.findViewById(R.id.crumbSpinner);
                            ArrayList<String> nextSelectList = new ArrayList<String>();
                            nextSelectList.add(hierarchyAttrTitleMap.get(hierarchyAttributes.get(k)));
                            //nextSelectList.add("Select");
                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, nextSelectList);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            getNextSpinner.setAdapter(spinnerAdapter);
                            getNextSpinner.setSelection(0, false);
                            breadCrumbDetails.set(k, hierarchyAttrTitleMap.get(hierarchyAttributes.get(k)));
                            Log.i(TAG, " for loop  if nextLevelData  SpinnerPosition :-"+k+" breadCrumbDetails" + breadCrumbDetails +"categoryStackFlow:- "+categoryStackFlow);
                        }

                    }
                }
                if(!selectedItem.equalsIgnoreCase(hierarchyTitle)) {
                    addCheckedItemToMapDefault(categoryName, selectedItem);
                }
                Log.i(TAG, "onClickCategoryHierarchy End  preValue is Select breadCrumbDetails :- " +breadCrumbDetails+" && selectedFilterMap:-"+ selectedFilterMap
                        +" \n categoryStackFlow:-  "+categoryStackFlow);
                mPageNumber = 1;
                isAnyActionApply= false;
                if (selectedProductIds.isEmpty()) {
                    getProductsList(true,true,true);

                } else{
                    saveSelectedData(new OnSaveQuickOrderCallback() {
                        @Override
                        public void onClickSaveButton() {
                            getProductsList( true,true,true);
                        }
                    }, false);
                }

            }else{
                Log.e(TAG,"Previous value is same as current selected value>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
            }
            //Log.i(TAG, "onClickCategoryHierarchy End  preValue is Select breadCrumbDetails :- " +breadCrumbDetails+" && selectedFilterMap:-"+ selectedFilterMap);
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during updateBreadcrumb :- " + e.getMessage());
        }
    }

    private void actionOnSearchMenuButton(){

        selectedFilterMap.clear();
        Log.i("ActionSearch", "*****Click On Search Button *******"+selectedFilterMap);

        View searchView = getLayoutInflater().inflate(R.layout.search_layout, menuItemActionLayout, true);
        RelativeLayout searchLayout= searchView.findViewById(R.id.searchLayout);
        final EditText editTextSearch = searchView.findViewById(R.id.editTextSearch);
        final ImageView searchButton = searchView.findViewById(R.id.searchButton);
        Button cancelButton = searchView.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearch.setText("");
                searchedTerm= "";
                menuItemActionLayout.removeAllViews();
                productNotFound.setVisibility(View.GONE);
                itemDetailsView.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.VISIBLE);
                formViewAdapter.resetPreviousList();
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionOnSearchButton(editTextSearch);

            }
        });
        editTextSearch.addTextChangedListener(new TextWatcher() {
            boolean searchedFlag = false;
            Timer timer;
            Handler handler;

            public void afterTextChanged(Editable s) {
                // final String searchedTerm = s.toString();
                searchedTerm = s.toString();
                timer = new Timer();
                handler = new Handler();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                //do Stuff here
                                if(searchedTerm.length() > 0)
                                    getListBasedOnSearch(false);
                            }
                        });
                        
                    }
                }, 2000); // 2000ms delay before the timer executes the „run“ method from TimerTask
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                // final String term = query.toString();
                searchedTerm = query.toString();
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
                if(!searchedFlag && searchedTerm.length() > 2){
                    searchedFlag= true;
                    getListBasedOnSearch(false);

                }
            }
        });

        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    actionOnSearchButton(editTextSearch);
                    return true;
                }
                return false;
            }
        });
    }



    private void actionOnSearchButton(EditText editTextSearch){
        searchedTerm=editTextSearch.getText().toString();
        if(searchedTerm.length()>0)
            getListBasedOnSearch(true);
        else
            Toast.makeText(context,"Please Enter Search Term",Toast.LENGTH_SHORT).show();
    }
    
    private void getListBasedOnSearch(boolean loader){
        isAnyActionApply = true;
        selectedFilterMap.clear();
        mPageNumber = 1;
        getProductsList(loader, true,false);
    }


/*    private void getFilteredItemFromServer(View view) {

        InputMethodManager imm = ((InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE));
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                Model pModelInstance = Model.getModelInstance(FormView.this,productModelName);
                ObjectData getResponse = new ObjectData();
                ArrayList objects_list = getResponse.getJSONObjectList(response,pModelInstance);
                isLastPage= (boolean) objects_list.get(1);
                List<JSONObject> productData = (List<JSONObject>)objects_list.get(2);
                if(productData!= null && productData.size()>0){
                    //formViewAdapter.updateSeedList(productData);
                    formViewAdapter = new QuickOrderAdapter(FormView.this,context, productData);
                    mRecyclerView.setAdapter(formViewAdapter);
                    mRecyclerView.scrollToPosition(pastVisiblesItems);
                    nextPageLoading = true;
                    formViewAdapter.notifyDataSetChanged();

                }*//*else {
                objectsListView.setVisibility(View.GONE);
                View notFound = getLayoutInflater().inflate(R.layout.result_not_found, objectsParentLayout, true);
            }*//*


            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {

            }
        };
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context, "GET", false)
                    .execute(addParamsToURL(APIRoutes.objectsAPI(productModelName,mPageNumber)));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }

    }*/


    private void actionOnFilterMenuButton(){
        if(selectedFilterMap==null || selectedFilterMap.isEmpty()){
            selectedFilterMap=selectedCategoryMap;
        }
        Log.e("ActionFilter", "***** filterListMap*******"+filterListMap);
       // Log.e("ActionFilter", "***** After pasting Data Filter Action*******"+selectedFilterMap);

        View  filterView = getLayoutInflater().inflate(R.layout.filter_layout, menuItemActionLayout, true);
        filterParentView = filterView.findViewById(R.id.filterParentView);
        filterChildView = filterView.findViewById(R.id.filterChildView);
        final RelativeLayout priceRangeLayout = (RelativeLayout) filterView.findViewById(R.id.priceRangeLayout);
        rangeSeekbar = (CrystalRangeSeekbar) filterView.findViewById(R.id.rangeSeekbar1);
        priceMin =  filterView.findViewById(R.id.priceMin);
        priceMax =  filterView.findViewById(R.id.priceMax);
        Button btnFilter= filterView.findViewById(R.id.btnFilter);
        Button btnCancel= filterView.findViewById(R.id.btnCancel);

        if(filterAttr!=null && filterAttr.size()>0) {
            filterParentAdapter = new ArrayAdapter<>(context, R.layout.filter_title, filterAttr);
            filterParentView.setAdapter(filterParentAdapter);
        }

        if(filterTitleName!=null && !filterTitleName.isEmpty()) {
            String filterFirstItem = filterTitleName.get(filterAttr.get(0));
            filterChildAdapter = new ArrayAdapter<>(context, R.layout.checked_textview, filterListMap.get(filterFirstItem));
            filterChildView.setAdapter(filterChildAdapter);
            setCheckedMultipleItemAgain(filterFirstItem, filterChildAdapter);
        }
        filterParentView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String key_name = (String) parent.getItemAtPosition(position);
                String filterKey = filterTitleName.get(key_name);
                filter_position = position;
                if (filterListMap != null && !filterListMap.isEmpty() && filterListMap.containsKey(filterKey)) {
                    if(productModelInstance.getAttributeType(filterKey).equalsIgnoreCase("price")||
                            productModelInstance.getAttributeType(filterKey).equalsIgnoreCase("discount")) {
                        Log.e(TAG, "Print Filter attr type>>>>>>>>>>>>>>>>>>>>>>" + productModelInstance.getAttributeType(filterKey));
                        filterChildView.setVisibility(View.GONE);
                        priceRangeLayout.setVisibility(View.VISIBLE);
                        showPriceRangeLayout(filterKey, filterListMap.get(filterKey));
                    }
                    else {
                        priceRangeLayout.setVisibility(View.GONE);
                        filterChildView.setVisibility(View.VISIBLE);
                        filterChildAdapter = new ArrayAdapter<>(context, R.layout.checked_textview, filterListMap.get(filterKey));
                        filterChildView.setAdapter(filterChildAdapter);
                        filterChildAdapter.notifyDataSetChanged();
                        setCheckedMultipleItemAgain(filterKey, filterChildAdapter);
                    }
                }

            }
        });
        filterParentView.setSelection(0);
        filterParentView.setItemChecked(0, true);

        filterChildView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < filterParentView.getAdapter().getCount(); i++) {
                    if (i == filter_position) {
                        String filterKeyName = filterTitleName.get(filterParentView.getAdapter().getItem(i).toString());
                        saveMultiselectedList(filterKeyName);
                        break;
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemActionLayout.removeAllViews();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedFilterMap != null && !selectedFilterMap.isEmpty()) {
                    menuItemActionLayout.removeAllViews();
                   // makeServerCall(mPageNumber,true);
                    mPageNumber =1;
                    isAnyActionApply = true;
                    getProductsList(true,true,false);
                }else
                    Toast.makeText(context,"Please select filter option",Toast.LENGTH_SHORT).show();
            }
        });

    }




    private void actionOnCategoryMenuButton(){
        if(!selectedProductIds.isEmpty()) {
            openPopupToAskSaveOrder(new OnSaveQuickOrderCallback() {
                @Override
                public void onClickSaveButton() {
                    DaveAIStatic.breadCrumbsStatus = false;
                    DaveAIStatic.categoryBackStack.clear();
                    selectedFilterMap.clear();
                    selectedCategoryMap.clear();
                    Intent intent = new Intent(context, QuickOrderHierarchy.class);
                    startActivity(intent);
                    finish();

                }
            });
        }
        else{
            DaveAIStatic.breadCrumbsStatus = false;
            DaveAIStatic.categoryBackStack.clear();
            selectedFilterMap.clear();
            selectedCategoryMap.clear();
            Intent intent = new Intent(context, QuickOrderHierarchy.class);
            startActivity(intent);
            finish();
        }

    }

    int counter = 0;
    private void actionOnSetQtyMenuButton(){

        View  setQtyView = getLayoutInflater().inflate(R.layout.set_quantity_layout, menuItemActionLayout, true);
        Button btnUnSetQty, btnSetQty;
        final EditText applyQtyEditText;
        ImageView addQty,subtractQty;

        btnUnSetQty = setQtyView.findViewById(R.id.btnUnSetQty);
        btnSetQty =  setQtyView.findViewById(R.id.btnSetQty);
        applyQtyEditText = setQtyView.findViewById(R.id.applyQtyEditText);
        subtractQty = setQtyView.findViewById(R.id.subtract_qty);
        addQty = setQtyView.findViewById(R.id.add_qty);

        if(defaultQuantityStatus!= -1)
            applyQtyEditText.setText( String.valueOf(defaultQuantityStatus));

        addQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkQty = String.valueOf(applyQtyEditText.getText());
                if(checkQty!=null && !checkQty.isEmpty())
                    counter=Integer.parseInt(checkQty);
                counter++;
                applyQtyEditText.setText(Integer.toString(counter));
            }
        });

        subtractQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkQty = String.valueOf(applyQtyEditText.getText());
                if(checkQty!=null && !checkQty.isEmpty())
                    counter=Integer.parseInt(checkQty);
                if (counter <=1) {
                    counter = 1;
                } else {
                    counter--;
                }
                applyQtyEditText.setText(Integer.toString(counter));

            }
        });
        btnUnSetQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyQtyEditText.setText("");
                defaultQuantityStatus = -1;
                menuItemActionLayout.removeAllViews();
                Log.e("SetQty", "*****After Unset value ******* "+defaultQuantityStatus);
                formViewAdapter.notifyDataSetChanged();
            }
        });

        btnSetQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isQtyEmpty = applyQtyEditText.getText().toString().trim().equals("");
                if (isQtyEmpty) {
                    Toast.makeText(context, "Please Enter Qty", Toast.LENGTH_SHORT).show();
                } else {
                    defaultQuantityStatus = Integer.parseInt(applyQtyEditText.getText().toString());
                    menuItemActionLayout.removeAllViews();
                    formViewAdapter.notifyDataSetChanged();
                }

            }
        });

    }

    private void addCheckedItemToMapDefault(String key, String value) {
        ArrayList<String> defaultselectedItems = new ArrayList<>();
        defaultselectedItems.add(value);
        selectedFilterMap.put(key, defaultselectedItems);
    }




    private void saveMultiselectedList(String key ) {
        SparseBooleanArray checked = filterChildView.getCheckedItemPositions();
        ArrayList<String> selectedItems = new ArrayList<>();
        for (int i = 0; i < checked.size(); i++) {
            int pos = checked.keyAt(i);
            if (checked.valueAt(i)) {
                Log.e(TAG, "***********Print selectedItems.add :-  " + filterChildAdapter.getItem(pos));
                selectedItems.add(filterChildAdapter.getItem(pos));
            }
        }
        selectedFilterMap.put(key, selectedItems);
        Log.e(TAG, "***********Print FILTERAttr HashMap :-  " + selectedFilterMap);
    }

    private void setCheckedMultipleItemAgain(String key, ArrayAdapter<String> adapter) {
        System.out.println("setCheckedMultipleItemAgain****************:- " + selectedFilterMap);
        for (String Getkey : selectedFilterMap.keySet()) {
            if (Getkey.equalsIgnoreCase(key)) {
                ArrayList<String> get_lis = selectedFilterMap.get(Getkey);
                for (int j = 0; j < get_lis.size(); j++) {
                    int checked_position = adapter.getPosition(get_lis.get(j));
                    filterChildView.setItemChecked(checked_position, true);
                }
            }
        }
    }

    private void showPriceRangeLayout(final String key, ArrayList<String> priceRange) {

        if (priceRange != null && !priceRange.isEmpty()) {
            int minValue = Integer.parseInt(priceRange.get(0));
            int maxValue = Integer.parseInt(priceRange.get(1));
            rangeSeekbar.setMinValue(minValue);
            rangeSeekbar.setMaxValue(maxValue);
        }
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                priceMin.setText(String.valueOf(minValue));
                priceMax.setText(String.valueOf(maxValue));
                String joinedValue = String.valueOf(minValue) + "," + String.valueOf(maxValue);
                ArrayList<String> priceList = new ArrayList<>();
                priceList.add(joinedValue);
                if (selectedFilterMap.containsKey(key)) {
                    selectedFilterMap.remove(key);
                }
                selectedFilterMap.put(key, priceList);
            }
        });
    }


    private void addDynamicSubHeaderView(String title ,Object value){

        final ViewGroup detailsLayout = (ViewGroup) LayoutInflater.from(context)
                .inflate(R.layout.show_qty_details, subHeaderLayout, false);

        if(title.equalsIgnoreCase("Total Price ")){
            detailsLayout.findViewById(R.id.view).setVisibility(View.GONE);

        }
        TextView tvItemValue = detailsLayout.findViewById(R.id.item_value);
        TextView tvItemName =  detailsLayout.findViewById(R.id.item_name);

        //Log.e(TAG,"addDynamicItemDetails :---"+title+" && value:-"+value);
        tvItemName.setText(title);
        tvItemValue.setText(value.toString());

        subHeaderLayout.addView(detailsLayout);


    }

    @Override
    public void onQuantityUpdate(HashMap<String,ArrayList<Object>> itemDetails) {
        Log.e(TAG," onQuantityUpdate:------------  "+itemDetails);
       /* if(!itemDetails.isEmpty()) {
            selectedItemDetails = itemDetails;
            int qtyCount = 0;
            double totalPrice = 0.0;
            for (Map.Entry<String, ArrayList<Object>> entry : itemDetails.entrySet()) {
                ArrayList<Object> objectArrayList = entry.getValue();
                if (objectArrayList != null && !objectArrayList.isEmpty()) {
                    qtyCount += (int) objectArrayList.get(0);
                    totalPrice += (double) objectArrayList.get(1);
                }

            }

            updateSubHeaderValue(0,""+itemDetails.size());
            updateSubHeaderValue(1,""+qtyCount);
            updateSubHeaderValue(2,""+totalPrice);
            if (btnSaveOrder.getText().toString().equalsIgnoreCase("Saved"))
                btnSaveOrder.setText("Save");

            //subHeaderLayout.setVisibility(View.VISIBLE);
        }else {

            updateSubHeaderValue(0,"0");
            updateSubHeaderValue(1,"0");
            updateSubHeaderValue(2,"0.0");

        }*/

    }

    @Override
    public void onQuantityUpdate(String strProductId, int quantity, double qtyPrice) {
        updateQuantityStatus(strProductId,quantity,qtyPrice,true);
    }


    private void updateQuantityStatus(String strProductId, int quantity, double qtyPrice ,boolean isAddInIdsList){
        try {
            Log.e(TAG, "updateQuantityStatus Print quantity:------------" + quantity+" && qtyPrice:--"+qtyPrice);
            if(!strProductId.isEmpty()) {
                if (quantity == -1) {
                    selectedProductIds.remove(strProductId);
                    if (orderedProductQtyMap.containsKey(strProductId))
                        orderedProductQtyMap.remove(strProductId);

                    if (tempSelectedItemList.containsKey(strProductId))
                        tempSelectedItemList.remove(strProductId);

                } else {
                    ArrayList<Object> qtyPriceList = new ArrayList<>();
                    qtyPriceList.add(quantity);
                    qtyPriceList.add(quantity * qtyPrice);
                    if (isAddInIdsList)
                        selectedProductIds.add(strProductId);
                    orderedProductQtyMap.put(strProductId, qtyPriceList);
                    tempSelectedItemList.put(strProductId, qtyPriceList);
                }
            }
           // Log.e(TAG, " updateQuantityStatus orderedProductQtyMap:------------" + orderedProductQtyMap);
           // Log.e(TAG, " updateQuantityStatus tempSelectedItemList:------------" + tempSelectedItemList);

            int totalCount = 0;
            int qtyCount = 0;
            double totalPrice = 0.0;
            if(!tempSelectedItemList.isEmpty()) {
                for (Map.Entry<String, ArrayList<Object>> entry : tempSelectedItemList.entrySet()) {
                    ArrayList<Object> objectArrayList = entry.getValue();
                    if (objectArrayList != null && !objectArrayList.isEmpty()) {
                        qtyCount += (int) objectArrayList.get(0);
                        totalPrice += (double) objectArrayList.get(1);
                        if((int)objectArrayList.get(0) > 0) {
                            totalCount++;
                        }
                    }
                }
            }
           // Log.e(TAG, "After updateQuantityStatus Total count:-:------------" + totalCount + " totalQty:-" + qtyCount + "totalPrice:-" + totalPrice);
            updateSubHeaderValue(0, "" + totalCount);
           // updateSubHeaderValue(0, "" + tempSelectedItemList.size());
            updateSubHeaderValue(1, "" + qtyCount);
           // updateSubHeaderValue(2, "" + totalPrice);
            DecimalFormat df2 = new DecimalFormat(".##");
            df2.format(totalPrice);
            updateSubHeaderValue(2, "" + String.format("%.2f", totalPrice));
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void updateSubHeaderValue(int index ,String value){
        TextView textValue = subHeaderLayout.getChildAt(index).findViewById(R.id.item_value);
        textValue.setText(value);
        //Log.e(TAG,"updateSubHeaderValue  :--------------- " + value  +" && Text Value " +textValue.getText());

    }

    private String getSubHeaderValue(int index){
        TextView textValue = subHeaderLayout.getChildAt(index).findViewById(R.id.item_value);

        // Log.e(TAG,"updateSubHeaderValue  :--------------- " + value  +" && Text Value " +textValue.getText());
        return textValue.getText().toString();

    }

    private void openPopupToAskSaveOrder(OnSaveQuickOrderCallback onSaveQuickOrderCallback){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(FormView.this);
        builder1.setMessage("Are you want to save your current order?");
        builder1.setCancelable(false);

        builder1.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                saveSelectedData(onSaveQuickOrderCallback,true);
                }
        });

        builder1.setNeutralButton("Don't Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });

        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public static Set<String> selectedProductIds = new HashSet<String>();
    int postObjectCount = 0;
    ProgressDialog pDialog;
   // private OnSaveQuickOrderCallback onSaveQuickOrderCallback;
    public interface OnSaveQuickOrderCallback{
        void onClickSaveButton();
    }
    private void saveSelectedData(OnSaveQuickOrderCallback onSaveQuickOrderCallback ,boolean asyncStatus ){
        try {
        Log.e(TAG,"selectedProductIds ==  "+selectedProductIds.size() +"   ==   "+ selectedProductIds);
        if(!selectedProductIds.isEmpty()) {
            postObjectCount = 0;
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait your order is saving...");
            pDialog.setCancelable(false);
            pDialog.show();
            postInteraction(onSaveQuickOrderCallback);
        }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "********postInteraction Error*********   " + e.getLocalizedMessage());
        }


    }

    private void postInteraction(OnSaveQuickOrderCallback onSaveQuickOrderCallback){

        List<String> postList = new ArrayList<String>(selectedProductIds);
        //Log.e(TAG,"postInteraction print   postObjectCount:- "+ postObjectCount+"postList:- "+ postList);
       /* if(postObjectCount <= postList.size()){
            // todo index exists
        }*/
        try {
            String productId = postList.get(postObjectCount);
            JSONObject post_body = new JSONObject();
            ModelUtil modelUtil = new ModelUtil(context);
            post_body.put(modelUtil.getInteractionCustomerId(), strCustomerId);
            post_body.put(modelUtil.getInteractionProductId(), productId);
            post_body.put(modelUtil.getInteractionStageAttrName(), daveAIPerspective.getInvoice_stage_attr());

            if(orderedProductDetails.has(productId)) {
               post_body.put(new ModelUtil(FormView.this).getInteractionIdAttrName(), orderedProductDetails.getJSONObject(productId).get(modelUtil.getInteractionIdAttrName()));
           /*    Log.e(TAG,"Print Productid with Interaction id:->>>>>>>>>>"+productId+"\n InteractionId:-"+
                       orderedProductDetails.getJSONObject(productId).get(modelUtil.getInteractionIdAttrName()));*/
            }

            if (orderedProductQtyMap.containsKey(productId)) {
                ArrayList<Object> objectArrayList = orderedProductQtyMap.get(productId);
                post_body.put(daveAIPerspective.getInteraction_qty(), objectArrayList.get(0));
            }

            if (currentOrderId != null) {
                post_body.put(daveAIPerspective.getInvoice_id_attr_name(), currentOrderId);
            } else {
                if (daveAIPerspective.getUser_login_id_attr_name() != null) {
                    String userId = new SharedPreference(FormView.this).readString( USER_ID);
                    post_body.put(daveAIPerspective.getUser_login_id_attr_name(), userId);
                }
            }
            Log.e(TAG, "********Post FormView Interaction Body**********   "+productId+" ==" + post_body.toString());

            DaveModels daveModels = new DaveModels(context, false);
            daveModels.postObject(interactionModelName, post_body, new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    try {
                        postObjectCount++;
                        formViewAdapter.removeSelectedItemFromList(productId);
                        if (postObjectCount < postList.size()) {
                            postInteraction(onSaveQuickOrderCallback);
                        }
                        else{
                            Log.e(TAG, "Is it Equal  :--------" +postObjectCount + " == " +selectedProductIds.size()+""+onSaveQuickOrderCallback);
                            getQtyCountFromInteraction();
                            updateOrderDetails(onSaveQuickOrderCallback);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailure(int requestCode, String responseMsg) {
                    Log.e(TAG, "Error During Post :--------" + responseMsg);
                    if (pDialog.isShowing())
                        pDialog.dismiss();

                }
            });



        } catch(Exception e){
            e.printStackTrace();
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
            Toast.makeText(context,"Error saving order. Please try again.",Toast.LENGTH_LONG).show();
            Log.e(TAG, "********postInteraction Error*********   " + e.getLocalizedMessage());
        }

    }

    public void updateOrderDetails(OnSaveQuickOrderCallback onSaveQuickOrderCallback){
        try {
            int qtyCount = 0;
            double totalPrice = 0.0;
            for (Map.Entry<String, ArrayList<Object>> entry : orderedProductQtyMap.entrySet()) {
                ArrayList<Object> objectArrayList = entry.getValue();
                if (objectArrayList != null && !objectArrayList.isEmpty()) {
                    qtyCount += (int) objectArrayList.get(0);
                    totalPrice += (double) objectArrayList.get(1);
                }
            }
            JSONObject updateBody = new JSONObject();
            if(daveAIPerspective.getInvoice_total_count_attr()!=null && !daveAIPerspective.getInvoice_total_count_attr().isEmpty()) {
                updateBody.put(daveAIPerspective.getInvoice_total_count_attr(), qtyCount);

            }
            if(daveAIPerspective.getInvoice_total_value_attr()!=null && !daveAIPerspective.getInvoice_total_value_attr().isEmpty()) {
                updateBody.put(daveAIPerspective.getInvoice_total_value_attr(),totalPrice);
            }

            Log.e(TAG,"Order update  post data>>>>>>>>>>>>"+currentOrderId +" == "+updateBody);
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                DaveModels daveModels = new DaveModels(context, true);
                daveModels.patchObject(daveAIPerspective.getInvoice_model_name(), currentOrderId, updateBody, new DaveAIListener() {
                    @Override
                    public void onReceivedResponse(JSONObject jsonObject) {
                        Toast.makeText(FormView.this, "Your order is saved", Toast.LENGTH_SHORT).show();
                        if (pDialog.isShowing())
                            pDialog.dismiss();
                        selectedProductIds.clear();
                        formViewAdapter.removeAllSelectedView();
                        //todo get interaction after saving order
                        if (onSaveQuickOrderCallback != null) {
                            onSaveQuickOrderCallback.onClickSaveButton();
                        }

                    }

                    @Override
                    public void onResponseFailure(int i, String s) {
                        Toast.makeText(FormView.this, "Something went wrong Your order isn't saved", Toast.LENGTH_SHORT).show();
                        if (pDialog.isShowing())
                            pDialog.dismiss();

                    }
                });
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During update Order Details:-------------  "+e.getMessage());
        }


    }


}



