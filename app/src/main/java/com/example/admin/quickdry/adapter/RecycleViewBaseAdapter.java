package com.example.admin.quickdry.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.utils.RecycleViewHolder;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;




public class RecycleViewBaseAdapter extends RecyclerView.Adapter<RecycleViewHolder>
        implements RecyclerView.OnClickListener {

    private static final String TAG = "RecycleViewBaseAdapter";
    private Context mContext;
    private List<JSONObject> seedBaseInteractionList;



    public RecycleViewBaseAdapter(Context context, ArrayList<JSONObject> seedInteractionList) {
        this.seedBaseInteractionList = seedInteractionList;
        this.mContext = context;

    }


    @Override
    public RecycleViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.details_product, viewGroup, false);
        RecycleViewHolder holder = new RecycleViewHolder(view);
        holder.itemView.setOnClickListener(RecycleViewBaseAdapter.this);
        holder.itemView.setTag(holder);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecycleViewHolder customViewHolder, int position) {


        JSONObject seedItem = seedBaseInteractionList.get(position);
        try {
            if (seedItem.has("details")) {
                JSONObject productDetails = seedItem.getJSONObject("details");
                //customViewHolder.bindQuickView(mContext, productModel.getQuick_views().getMobile_card_view(), productDetails);
            }
        } catch (Exception e) {
            Log.e(TAG, "Error Iterating BaseInteractionsJsonObject +++++++++++" + e.getMessage());
        }


    }

    @Override
    public int getItemCount() {
        return (null != seedBaseInteractionList ? seedBaseInteractionList.size() : 0);
    }

    public JSONObject getJsonObject(int position) {
        return seedBaseInteractionList.get(position);
    }

    public void remove(int position) {
        seedBaseInteractionList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, seedBaseInteractionList.size());
    }

  /*  public String getRemoveItemId(int position) {
        if (seedBaseInteractionList.size() > 0)
            try {
                return seedBaseInteractionList.get(position).getJSONObject("details").getString(interactionProductId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return null;
    }*/

    public void restoreItem(JSONObject deletedItem, int position) {
        seedBaseInteractionList.add(position, deletedItem);
        // notify item added by position
        notifyItemInserted(position);
    }

    //This method will filter the list
    public void setFilter(List<JSONObject> productDetails) {
        seedBaseInteractionList = new ArrayList<>();
        seedBaseInteractionList.addAll(productDetails);
        notifyDataSetChanged();
    }


    @Override
    public void onClick(View view) {


    }

}
