package com.example.admin.quickdry.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.storage.SharedPreference;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;


import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class Reports extends Fragment {

    private final String TAG = getClass().getSimpleName();

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private SwipeRefreshLayout swipeView;
    RelativeLayout lineChartParentView,areaMangerBarParentView;
    ProgressBar progressBarSalesLine, progressBarUserBar,progressBarAreaManager ;
    BarChart barChartMonthlyUserSales,barChartAreaManager;
    LineChart lineChartSales;
    TextView textViewSalesLineError, textViewUserBarError,textViewManagerBarError;
    ImageView imgRefreshView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    DaveAIPerspective daveAIPerspective;
    String strUserId = "";
    ArrayList<BarEntry> barEntriesUser,barEntriesAreaManager;




    private OnFragmentInteractionListener mListener;
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public Reports() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Reports.
     */
    // TODO: Rename and change types and number of parameters
    public static Reports newInstance(String param1, String param2) {
        Reports fragment = new Reports();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    //it is called only once when it is attached with activity.
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }*/
    }

    //It is used to initialize the fragment.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    //creates and returns view hierarchy.
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reports, container, false);

       /* swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swipe_view);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeView.setRefreshing(true);
                        //todo call api
                    }
                }, 1000);
            }
        });
        swipeView.setColorSchemeColors(Color.GRAY, Color.GREEN, Color.BLUE,
                Color.RED, Color.CYAN);
        swipeView.setDistanceToTriggerSync(20);// in dips
        swipeView.setSize(SwipeRefreshLayout.DEFAULT);// LARGE also can be used
*/
        imgRefreshView = view.findViewById(R.id.refreshView);
        lineChartParentView = view.findViewById(R.id.lineChartParentView);
        progressBarSalesLine = view.findViewById(R.id.p_line_sales);
        lineChartSales = view.findViewById(R.id.linechart);
        textViewSalesLineError = view.findViewById(R.id.textViewSalesError);

        progressBarUserBar = view.findViewById(R.id.p_bar_user);
        barChartMonthlyUserSales = view.findViewById(R.id.bar_chart_sales_person);
        barChartMonthlyUserSales.setVisibility(View.GONE);
        textViewUserBarError = view.findViewById(R.id.textViewUserError);
        textViewUserBarError.setVisibility(View.GONE);

        areaMangerBarParentView = view.findViewById(R.id.areaMangerBarParentView);
        progressBarAreaManager = view.findViewById(R.id.p_bar_manager);
        barChartAreaManager = view.findViewById(R.id.bar_chart_area_manager);
        textViewManagerBarError = view.findViewById(R.id.textViewManagerError);
        textViewManagerBarError.setVisibility(View.GONE);

        daveAIPerspective = DaveAIPerspective.getInstance();
        SharedPreference sharedPreference = new SharedPreference(getActivity());
        strUserId = sharedPreference.readString(SharedPreference.USER_ID);
        getUserSingleton();

        imgRefreshView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserSingleton();
            }
        });

        return view;
    }

    //This is particularly useful when inheriting the onCreateView() implementation but we need to configure the resulting views,
    // such as with a ListFragment and when to set up an adapter
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    //If there is something that’s needed to be initialised in the fragment that depends upon the
    //activity’s onCreate() having completed its work then onActivityCreated() can be used for that initialisation work
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    //The onStart() method is called once the fragment gets visible
    @Override
    public void onStart(){
        super.onStart();

    }

    //makes the fragment interactive.
    @Override
    public void onResume(){
        super.onResume();

    }

    //is called when fragment is no longer interactive
    @Override
    public void onPause(){
        super.onPause();

    }

    // is called when fragment is no longer visible.
    @Override
    public void onStop(){
        super.onStop();

    }

    // It’s called before onDestroy(). This is the counterpart to onCreateView() where we set up the UI. If there are things that are needed
    // to be cleaned up specific to the UI, then that logic can be put up in onDestroyView()
    @Override
    public void onDestroyView(){
        super.onDestroyView();

    }

    //onDestroy() called to do final clean up of the fragment’s state but Not guaranteed to be called by the Android platform.
    @Override
    public void onDestroy(){
        super.onDestroy();

    }

    //It’s called after onDestroy(), to notify that the fragment has been disassociated from its hosting activity
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getUserSingleton(){
        try {
            progressBarUserBar.setVisibility(View.VISIBLE);
            DaveModels daveModels = new DaveModels(getActivity(), true);
            daveModels.getSingleton(daveAIPerspective.getUser_login_model_name(),"login", new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject response) {
                    progressBarUserBar.setVisibility(View.GONE);
                    try {
                        if (response != null && response.length()>0) {
                            float monthlyTargetSales =  BigDecimal.valueOf(response.getDouble("monthly_target_sales")).floatValue();
                            float monthsActualSales =  BigDecimal.valueOf(response.getDouble("this_months_actual_sales")).floatValue();
                            Log.i(TAG, "<<<<<<<<<<<<<<<<<<getUserSingleton abc:-" +response.get("monthly_target_sales").getClass());

                            Log.i(TAG, "getUserSingleton monthlyTargetSales :-" + monthlyTargetSales + " monthsActualSales:- " + monthsActualSales);

                            // set x axis
                            XAxis xAxis = barChartMonthlyUserSales.getXAxis();
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            ArrayList<String> xLabels = new ArrayList<>();
                            xLabels.add("Monthly Target Sales");
                            xLabels.add("Monthly Actual Sales");

                            ArrayList<BarEntry> yEntry = new ArrayList<>();
                            yEntry.add(new BarEntry(monthlyTargetSales, 0));
                            yEntry.add(new BarEntry(monthsActualSales, 1));

                            BarDataSet barDataSet = new BarDataSet(yEntry, "Monthly Target Vs. Achieved sales");
                            barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                            barDataSet.setBarSpacePercent(75f);


                            //BarData barData = new BarData(barDataSet);
                            BarData barData = new BarData(xLabels, barDataSet);
                            barChartMonthlyUserSales.setVisibility(View.VISIBLE);
                            barChartMonthlyUserSales.setData(barData);
                            barChartMonthlyUserSales.setDescription("");
                            barChartMonthlyUserSales.animateY(3000);

                            // get the legend (only possible after setting data) // modify the legend ...
                            Legend l = barChartMonthlyUserSales.getLegend();
                            l.setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
                            l.setWordWrapEnabled(true);


                            getAverageSalesDetails(response);
                            getAreaManagerDetails(response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onResponseFailure(int requestCode, String responseMsg) {
                    progressBarUserBar.setVisibility(View.GONE);
                    textViewUserBarError.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();

                }
           // },true,true,false); //online
            },false,true,false); //offline

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During get User Singleton:-------------  "+e.getMessage());
        }
    }

    private void getAverageSalesDetails(JSONObject salesPersonDetails){
        lineChartParentView.setVisibility(View.VISIBLE);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                getDayByDaySalesDetails(salesPersonDetails,response);
            }
            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                progressBarSalesLine.setVisibility(View.GONE);
                textViewSalesLineError.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();

            }
        };
        try {
            ///pivot/retailer_order?_action=average&_over=total_mrp_value&sales_person_id=<user_id>&date=<start of this month>,<today>
            HashMap<String,String> queryParam = new HashMap<>();
            queryParam.put("_action","average");
            queryParam.put("_over","total_mrp_value");
            queryParam.put("sales_person_id",strUserId);
            queryParam.put("date",MultiUtil.methodFirstDayOfMonth()+","+ MultiUtil.methodToday());
            DaveModels daveModels = new DaveModels(getActivity(), true);
            daveModels.getPivot(daveAIPerspective.getInvoice_model_name(),"average_user_sales", queryParam,daveAIListener,false,true);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During create new order:-------------  "+e.getMessage());
        }

    }

    private void getDayByDaySalesDetails(JSONObject salesPersonDetails, JSONObject avgSalesDetails){

        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                //show Line graph of target Sales vs Avrage sales vs day by day daily sales
                try {
                    progressBarSalesLine.setVisibility(View.GONE);
                    Log.e(TAG,"getAverageSalesDetails:-------"+response);
                    if (response != null && response.has("data") && response.getJSONObject("data").length()>0) {

                        DecimalFormat df2 = new DecimalFormat(".##");
                        float targetSales = Float.parseFloat(df2.format(salesPersonDetails.getDouble("daily_target_sales")));
                        float averageSales = Float.parseFloat(df2.format(avgSalesDetails.getJSONObject("data").getDouble("total_mrp_value")));
                        float maxYValue = targetSales > averageSales ? targetSales :averageSales;
                        JSONObject jsonDailySales = response.getJSONObject("data");

                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        dateFormat.setLenient(false);
                        Calendar cal = Calendar.getInstance();

                        ArrayList<String> xLabels = new ArrayList<>();
                        ArrayList<Entry> dataEntryDailySales = new ArrayList<Entry>();
                        List<Entry> entries = new ArrayList<Entry>();
                        if(jsonDailySales!=null) {
                            int count= 0;
                            Iterator<?> keys = jsonDailySales.keys();
                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                xLabels.add(key);

                                cal.setTime(dateFormat.parse(key.split(" ")[0]));
                                int date = cal.get(Calendar.DATE);
                                //xLabels.add(String.valueOf(date));

                                float dailySale = Float.parseFloat(df2.format(jsonDailySales.get(key)));
                                maxYValue = maxYValue > dailySale ? maxYValue :dailySale;
                                dataEntryDailySales.add(new Entry(dailySale,count));
                                // turn your data into Entry objects
                               // entries.add(new Entry(key, dailySale));
                                Log.e(TAG,"Print x axis key :---------"+ key+" Date:-"+date+" dailySale:- "+dailySale );
                                count++;
                            }
                        }
                        Log.i(TAG, "getAverageSalesDetails targetSales:-" + targetSales + " averageSales:- " + averageSales +" maxYValue:-"+maxYValue );
                        ArrayList<ILineDataSet> lineDataSets = new ArrayList<> ();
                        LineDataSet lDataSetDaily = new LineDataSet(dataEntryDailySales, "Daily Sales");
                        lDataSetDaily.setColor(Color.GREEN);
                        lDataSetDaily.setCircleColor(Color.GREEN);
                        lDataSetDaily.setValueTextSize(9f);
                        lDataSetDaily.setDrawCubic(true);
                        lineDataSets.add(lDataSetDaily);



                        // create a data object with the datasets
                        //lineChartSales.setData(new LineData(lineDataSets));
                        lineChartSales.setData(new LineData(xLabels, lineDataSets));
                        lineChartSales.setVisibility(View.VISIBLE);
                        lineChartSales.setDescription("");
                        lineChartSales.animateXY(3000, 3000);
                        // lineChartSales.animateX(2500, Easing.EasingOption.EaseInOutQuart);
                        // lineChartSales.saveToGallery("mychart.jpg", 85); // 85 is the quality of the image

                        // get the legend (only possible after setting data) // modify the legend ...
                        Legend l = lineChartSales.getLegend();
                        l.setPosition(Legend.LegendPosition.ABOVE_CHART_CENTER);
                       // l.setXEntrySpace(15f); // set the space between the legend entries on the x-axis
                        //l.setYEntrySpace(15f);
                        l.setWordWrapEnabled(true);
                        //l.setForm(Legend.LegendForm.CIRCLE);
                        // l.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);

                        lineChartSales.getAxisLeft().setAxisMaxValue(maxYValue);
                        lineChartSales.getAxisRight().setAxisMaxValue(maxYValue);
                        //lineChartSales.getAxisRight().setEnabled(false);

                        //draw target line
                        LimitLine lTarget = new LimitLine(targetSales, "Target ("+targetSales+")");
                        lTarget.setLineColor(Color.RED);
                        lTarget.setLineWidth(2f);
                        lTarget.setTextSize(9f);
                        lTarget.setTextStyle(Paint.Style.FILL);
                        // lTarget.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
                        //lineChartSales.getAxisLeft().addLimitLine(lTarget);

                        //draw average line
                        LimitLine lAverage = new LimitLine(averageSales, "Average ("+averageSales+")");
                        lAverage.setLineColor(Color.BLACK);
                        lAverage.setLineWidth(2f);
                        lAverage.setTextSize(9f);
                        lAverage.setTextStyle(Paint.Style.FILL);
                        // lineChartSales.getAxisLeft().addLimitLine(lAverage);

                        //set Y axis Left value
                        YAxis leftAxis = lineChartSales.getAxisLeft();
                        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
                        leftAxis.addLimitLine(lTarget);
                        leftAxis.addLimitLine(lAverage);
                        //leftAxis.setSpaceTop(20f);
                        //leftAxis.setAxisMinValue(0);
                        //leftAxis.setYOffset(20f);
                        //leftAxis.enableGridDashedLine(10f, 10f, 0f);
                        leftAxis.setDrawZeroLine(false);
                        leftAxis.setDrawLimitLinesBehindData(true);


                        // set x axis
                        XAxis xAxis = lineChartSales.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        xAxis.setTextSize(9f);
                        xAxis.setAxisLineWidth(1.5f);
                        xAxis.setLabelsToSkip(0);

                        xAxis.setLabelRotationAngle(-90f);
                        xAxis.mLabelRotatedWidth=2;
                        xAxis.mLabelRotatedHeight=2;
                        //xAxis.setDrawAxisLine(true);

                        //xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));

                        // xAxis.setSpaceBetweenLabels(6);
                        //xAxis.setDrawLabels(true);

                        // xAxis.setAvoidFirstLastClipping(true);

                        // dont forget to refresh the drawing
                        lineChartSales.invalidate();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error during showing line Graph:-------------"+e.getMessage());
                }
            }
            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                progressBarSalesLine.setVisibility(View.GONE);
                textViewSalesLineError.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();
            }
        };
        try {
            ///pivot/retailer_order/date?_resolution=daily&_action=sum&_over=total_mrp_value&sales_person_id=<user_id>&date=<start of this month>,<today>
            HashMap<String,String> queryParam = new HashMap<>();
            queryParam.put("_resolution","daily");
            queryParam.put("_action","sum");
            queryParam.put("_over","total_mrp_value");
            queryParam.put("sales_person_id",strUserId);
            queryParam.put("date",MultiUtil.methodFirstDayOfMonth()+","+ MultiUtil.methodToday());
            DaveModels daveModels = new DaveModels(getActivity(), true);
            daveModels.getPivot(daveAIPerspective.getInvoice_model_name()+"/date","daily_user_sales",
                    queryParam,daveAIListener,false,true);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During create new order:-------------  "+e.getMessage());
        }

    }



    private void getAreaManagerDetails(JSONObject userDetails){
        try {
            String areaManagerId = userDetails.getString("area_manager_id");
            areaMangerBarParentView.setVisibility(View.VISIBLE);
            DaveModels daveModels = new DaveModels(getActivity(), true);
            daveModels.getSingleton("area_manager",areaManagerId,"area_manager_details", new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject response) {
                    try {
                       // String salePersonRegion = userDetails.getString("region");
                        getAreaManagerAchievedDetails(response);
                    }catch (Exception e){
                        e.printStackTrace();
                        Log.e(TAG,"Error during get AreaManager Details:--"+e.getMessage());
                    }

                }

                @Override
                public void onResponseFailure(int requestCode, String responseMsg) {
                    progressBarAreaManager.setVisibility(View.GONE);
                    textViewManagerBarError.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();

                }
            },false,true,false);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During create new order:-------------  "+e.getMessage());
        }

    }

    private void getAreaManagerAchievedDetails(JSONObject areaManagerDetails){

        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    progressBarAreaManager.setVisibility(View.GONE);
                    Log.e(TAG,"getAreaManagerAchievedDetails:-------"+response);
                    if (response != null && response.has("data") && response.getJSONObject("data").length()>0) {
                        DecimalFormat df2 = new DecimalFormat(".##");
                        float monthlyTargetSales = Float.parseFloat(df2.format(areaManagerDetails.getDouble("monthly_target_sales")));
                        float monthsAchievedSales = Float.parseFloat(df2.format(response.getJSONObject("data").getDouble("total_mrp_value")));

                        Log.i(TAG, "getAreaManagerAchievedDetails monthlyTargetSales:-" + monthlyTargetSales + " monthsActualSales:- " + monthsAchievedSales);

                        // set x axis
                        XAxis xAxis = barChartAreaManager.getXAxis();
                        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                        ArrayList<String> xLabels = new ArrayList<>();
                        xLabels.add("Monthly Target Sales");
                        xLabels.add("Total Mrp Value");

                        ArrayList<BarEntry> yEntry = new ArrayList<>();
                        yEntry.add(new BarEntry(monthlyTargetSales, 0));
                        yEntry.add(new BarEntry(monthsAchievedSales, 1));

                        BarDataSet barDataSet = new BarDataSet(yEntry, "Monthly Target Vs. Achieved sales");
                        barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                        barDataSet.setBarSpacePercent(75f);

                        //BarData barData = new BarData(barDataSet);
                        BarData barData = new BarData(xLabels, barDataSet);
                        barChartAreaManager.setVisibility(View.VISIBLE);
                        barChartAreaManager.setData(barData);
                        barChartAreaManager.setDescription("");
                        barChartAreaManager.animateY(3000);
                        // get the legend (only possible after setting data) // modify the legend ...
                        Legend l = barChartAreaManager.getLegend();
                        l.setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
                        l.setWordWrapEnabled(true);
                    }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                progressBarAreaManager.setVisibility(View.GONE);
                textViewManagerBarError.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();

            }
        };
        try {
            HashMap<String,String> queryParam = new HashMap<>();
            queryParam.put("_action","sum");
            queryParam.put("_over","total_mrp_value");
           // queryParam.put("region",salesPersonRegion);
            queryParam.put("area_manager_id",areaManagerDetails.getString("area_manager_id"));
            queryParam.put("date",MultiUtil.methodFirstDayOfMonth()+","+ MultiUtil.methodToday());
            DaveModels daveModels = new DaveModels(getActivity(), true);
            daveModels.getPivot(daveAIPerspective.getInvoice_model_name(),"achieved_manager_sales",
                    queryParam,daveAIListener,false,true);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During create new order:-------------  "+e.getMessage());
        }

    }
}
