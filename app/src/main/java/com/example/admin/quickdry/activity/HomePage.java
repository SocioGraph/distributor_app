package com.example.admin.quickdry.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.daveUtil.MultiUtil;

import com.example.admin.daveai.fragments.DynamicForm;

import com.example.admin.daveai.others.ConnectDaveAI;
import com.example.admin.daveai.others.DaveAIHelper;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveException;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.BuildConfig;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.dialogs.ConnectToDisplay;
import com.example.admin.quickdry.fragments.CustomerList;
import com.example.admin.quickdry.fragments.OrderList;
import com.example.admin.quickdry.fragments.ProductList;
import com.example.admin.quickdry.fragments.Reports;
import com.example.admin.quickdry.fragments.UserProfile;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.storage.SharedPreference;
import com.example.admin.quickdry.utils.CRUDUtil;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;





public class HomePage extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        DynamicForm.OnCreateDynamicFormListener,
        CustomerList.CustomerSelectedListener ,AppConstants, DaveConstants {

    private final String TAG = getClass().getSimpleName();
    private NavigationView navigationView;
    private Toolbar toolbar;
    private int selectedPosition = 0;
    DaveAIPerspective daveAIPerspective;
    SharedPreference sharedPreference;
    private ModelUtil modelUtil;
    TextView textViewQueueLength;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        textViewQueueLength = header.findViewById(R.id.queueLength);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //set version and url name in Navigation Item
        TextView baseUrl = findViewById(R.id.baseUrl);
        TextView versionName = findViewById(R.id.versionName);
        baseUrl.setText("URL: "+ MultiUtil.getManifestMetadata(this,MANIFEST_BASE_URL));
        versionName.setText("version "+ BuildConfig.VERSION_NAME);



        /* daveAIPerspective = DaveAIPerspective.getInstance();
        sharedPreference = new SharedPreference(HomePage.this);
        modelUtil = new ModelUtil(HomePage.this);

        setUpPreferenceValue();*/

        Bundle intent = getIntent().getExtras();
        Log.e(TAG,"Print Intent Data :----------------"+intent);
        if(intent!=null){
            showPageFromNotification(intent);
        }else {
            toolbar.setTitle(navigationView.getMenu().findItem(R.id.nav_customer).getTitle());
            displaySelectedScreen(R.id.nav_customer);
        }

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                getQueueLength();

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });



    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<onStart>>>>>>>>>>>>>>>>>>");
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<onResume>>>>>>>>>>>>>>>>>>");
        daveAIPerspective = DaveAIPerspective.getInstance();
        sharedPreference = new SharedPreference(HomePage.this);
        modelUtil = new ModelUtil(HomePage.this);

        setUpPreferenceValue();

    }

    @Override
    public void onPause(){
        super.onPause();

        SharedPreference sharedPreference = new SharedPreference(this);
        sharedPreference.writeBoolean("is_activity_paused", true);
        sharedPreference.writeString("activity_paused_name", "HomePage");


        Log.e(TAG,"<<<<<<<<<<<<<<<<<<onPause>>>>>>>>>>>>>>>>>>");

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<onDestroy>>>>>>>>>>>>>>>>>>");
    }


    @Override
    public void onRestart() {
        super.onRestart();
        // do some stuff here
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<onRestart>>>>>>>>>>>>>>>>>>");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }else {
            // super.onBackPressed();
            if (selectedPosition != R.id.nav_customer) {
                navigationView.setCheckedItem(R.id.nav_customer);
                displaySelectedScreen(R.id.nav_customer);

            } else {
                super.onBackPressed();
            }
        }
    }

 /*   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_page, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        displaySelectedScreen(item.getItemId());
        return true;
    }

    public void displaySelectedScreen(int itemId) {
        selectedPosition = itemId;
        Fragment fragment = null;
        String fragmentName = "";

        if (itemId == R.id.nav_profile) {
            fragment = new UserProfile();
            setActionBarTitle(itemId);

        }
        else if (itemId == R.id.nav_sign_up) {
            setActionBarTitle(itemId);
            fragment = DynamicForm.newInstance("", "Save", "POST",modelUtil.getCustomerModelName(),
                    daveAIPerspective.getCustomer_sign_up_list(),"","");

        } else if (itemId == R.id.nav_customer) {
            fragment = new CustomerList();
           // fragment = new CustomerList.newInstance(daveAIPerspective.getMenu_title_for_customer_list());
            setActionBarTitle(itemId);

        } else if (itemId == R.id.nav_product) {
            fragment = new ProductList();
            setActionBarTitle(itemId);

        } else if (itemId == R.id.nav_add_product) {

            Bundle b= new Bundle();
            b.putString("action_name",AddScanProduct.ACTION_ADD_PRODUCT);
            Intent intent1 = new Intent(HomePage.this, AddScanProduct.class);
            intent1.putExtras(b);
            //startActivity(intent1);
            startActivityForResult(intent1, AddScanProduct.ADD_PRODUCT_REQUEST_CODE);// Activity is started with requestCode 2

        } else if (itemId == R.id.nav_scan) {
            Bundle b = new Bundle();
            b.putString("action_name", AddScanProduct.ACTION_SCAN_PRODUCT);
            Intent intent1 = new Intent(HomePage.this, AddScanProduct.class);
            intent1.putExtras(b);
            startActivity(intent1);

        } else if (itemId == R.id.nav_order) {
            fragment = new OrderList();
            setActionBarTitle(itemId);

        }
        else if (itemId == R.id.nav_sync_data) {
            if(DatabaseManager.getInstance(HomePage.this).checkErrorRows().size() == 0) {
                syncDataWithServer();
            }else{
                StringBuilder errorMsg = new StringBuilder("Some data is not saved due to errors");

                errorMsg.append(" ... Please correct these errors before syncing.");

                AlertDialog.Builder builder1 = new AlertDialog.Builder(HomePage.this);
                builder1.setMessage(errorMsg.toString());
                builder1.setCancelable(false);
                builder1.setPositiveButton("Discard & Sync", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        syncDataWithServer();
                    }
                });

                builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        }
        else if (itemId == R.id.nav_searchByImage) {
            Intent intent = new Intent(HomePage.this, DaveFunction.class);
            intent.putExtra(DaveFunction.DAVE_ACTION, DaveFunction.SEARCH_BY_IMAGE_ACTION);
            startActivity(intent);

        }
        else if (itemId == R.id.nav_primary_order) {
            checkedCustomerListOnMenu();
           Toast.makeText(HomePage.this,"'Coming Soon'",Toast.LENGTH_SHORT).show();

        }
       /* else if (itemId == R.id.nav_connect_display) {
            new ConnectToDisplay(this).show();
        }*/
        else if (itemId == R.id.nav_incentives) {
            checkedCustomerListOnMenu();
            Toast.makeText(HomePage.this,"'Coming Soon'",Toast.LENGTH_SHORT).show();

        }
        else if (itemId == R.id.nav_report) {
            fragment = new Reports();
            setActionBarTitle(itemId);

        }
        else if (itemId == R.id.nav_logout) {
            showLogoutPopUp();

        }
        replaceFragment(fragment);

    }

    public void replaceFragment(Fragment fragment) {
        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            //  ft.addToBackStack(null);
            ft.commit();
            //  getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    public void setActionBarTitle(int selected_id) {
        ((TextView)toolbar.findViewById(R.id.show_title)).setText(navigationView.getMenu().findItem(selected_id).getTitle());
        toolbar.setTitle(navigationView.getMenu().findItem(selected_id).getTitle());
    }

    private void hideNavigationItem(int navId) {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(navId).setVisible(false);
    }

    private void showNavigationItem(int navId,String title) {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(navId)
                .setVisible(true)
                .setTitle(title);

    }

    private void  showPageFromNotification(Bundle bundle){

        String menuItemName =  bundle.getString("_menuItemName");
        Log.e(TAG,"Print Intent Data menuItemName:----------------"+menuItemName);

        if(menuItemName.equalsIgnoreCase("user")){
            toolbar.setTitle(navigationView.getMenu().findItem(R.id.nav_profile).getTitle());
            displaySelectedScreen(R.id.nav_profile);

        }
        else if(menuItemName.equalsIgnoreCase("customers")){
            toolbar.setTitle(navigationView.getMenu().findItem(R.id.nav_customer).getTitle());
            displaySelectedScreen(R.id.nav_customer);
        }
        else if(menuItemName.equalsIgnoreCase("products")){
            toolbar.setTitle(navigationView.getMenu().findItem(R.id.nav_product).getTitle());
            displaySelectedScreen(R.id.nav_product);
        }
        else if(menuItemName.equalsIgnoreCase("orders")){
            toolbar.setTitle(navigationView.getMenu().findItem(R.id.nav_order).getTitle());
            displaySelectedScreen(R.id.nav_order);
        }
        else {
            toolbar.setTitle(navigationView.getMenu().findItem(R.id.nav_customer).getTitle());
            displaySelectedScreen(R.id.nav_customer);
        }

    }

    private boolean checkMenuItemValNull(String s){
        if(s.equalsIgnoreCase("__NULL__")||s.equalsIgnoreCase("_NULL_")){
            return true;
        }else{
            return false;
        }
    }

    private void setUpPreferenceValue(){
        try {
            Log.e(TAG,"Set Up Menu Item and button  get MyProfile:---------- "+ daveAIPerspective.getUser_login_model_name());
            //on swipe cal post Interaction
           /* if (daveAIPerspective.getInvoice_model_name() != null) {
                postInteraction();
            }*/

            Menu menu = navigationView.getMenu();

            if (daveAIPerspective.getUser_login_model_name() != null) {
                showNavigationItem(R.id.nav_profile,"My Profile");
            }

            if(!checkMenuItemValNull(daveAIPerspective.getMenu_title_for_sign_up())) {
                showNavigationItem(R.id.nav_sign_up, daveAIPerspective.getMenu_title_for_sign_up());
            }

            MenuItem nav_customer = menu.findItem(R.id.nav_customer);
            nav_customer.setTitle(daveAIPerspective.getMenu_title_for_customer_list());

            MenuItem nav_product = menu.findItem(R.id.nav_product);
            nav_product.setTitle(daveAIPerspective.getMenu_title_for_product_list());

            if (daveAIPerspective.getMenu_title_for_order_list() != null && !daveAIPerspective.getMenu_title_for_order_list().isEmpty() && !checkMenuItemValNull(daveAIPerspective.getMenu_title_for_order_list())) {
                showNavigationItem(R.id.nav_order, daveAIPerspective.getMenu_title_for_order_list());
            }

            if(!checkMenuItemValNull(daveAIPerspective.getMenu_title_for_primary_orders()) && !daveAIPerspective.getMenu_title_for_primary_orders().isEmpty()&&daveAIPerspective.getMenu_title_for_primary_orders()!=null) {
                showNavigationItem(R.id.nav_primary_order, daveAIPerspective.getMenu_title_for_primary_orders());
            }

            if (daveAIPerspective.isEnable_manage_product()&&!checkMenuItemValNull(daveAIPerspective.getAdd_product_title())&&!daveAIPerspective.getAdd_product_title().isEmpty()&&daveAIPerspective.getAdd_product_title()!=null) {
                showNavigationItem(R.id.nav_add_product, daveAIPerspective.getAdd_product_title());
            }

            if(!checkMenuItemValNull(daveAIPerspective.getMenu_title_for_primary_orders()) && !daveAIPerspective.getMenu_title_for_primary_orders().isEmpty()&&daveAIPerspective.getMenu_title_for_primary_orders()!=null) {
                showNavigationItem(R.id.nav_primary_order, daveAIPerspective.getMenu_title_for_primary_orders());
            }
            if(!checkMenuItemValNull(daveAIPerspective.getMenu_title_for_incentives())&&!daveAIPerspective.getMenu_title_for_incentives().isEmpty()&&daveAIPerspective.getMenu_title_for_incentives()!=null) {
                showNavigationItem(R.id.nav_incentives, daveAIPerspective.getMenu_title_for_incentives());
            }
            if(!checkMenuItemValNull(daveAIPerspective.getMenu_title_for_reports())&&!daveAIPerspective.getMenu_title_for_reports().isEmpty()&&daveAIPerspective.getMenu_title_for_reports()!=null) {
                showNavigationItem(R.id.nav_report, daveAIPerspective.getMenu_title_for_reports());
            }
            if (daveAIPerspective.getScan_btn_name() != null && !checkMenuItemValNull(daveAIPerspective.getScan_btn_name() )) {
                showNavigationItem(R.id.nav_scan, daveAIPerspective.getScan_btn_name());

            }
            if (daveAIPerspective.getSearch_by_image_button_name() != null && !daveAIPerspective.getSearch_by_image_button_name().isEmpty() && !checkMenuItemValNull(daveAIPerspective.getSearch_by_image_button_name())) {
                showNavigationItem(R.id.nav_searchByImage, daveAIPerspective.getSearch_by_image_button_name());
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public Fragment addCurrentFragment(String title, String buttonName, String methodName, String modelName,String requestType,
                                   ArrayList sequenceList,String objectId, String objectDetails) {

        return new DynamicForm().updateSingletonView(title,buttonName,methodName,modelName,requestType,sequenceList,objectDetails);

       // return DynamicForm.newInstance(title, buttonName, methodName,modelName,sequenceList,objectId,customerDetails);
    }

    // Call Back method  to get the Message form other Activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG,"onActivityResult:--------   "+requestCode+" &&  "+resultCode);
    }
    // sync_data data with server
    private  void syncDataWithServer(){

        ConnectDaveAI connectDaveAI = new ConnectDaveAI((HomePage.this));
        try{

        connectDaveAI.syncBulkData("Syncing Data with the server, \nThis may take some time.", new ConnectDaveAI.OnSyncDataListener() {

            @Override
            public void onDataSynced(boolean b, String s) {
                Toast.makeText(HomePage.this, s, Toast.LENGTH_SHORT).show();
                getQueueLength();
                checkedCustomerListOnMenu();
                displaySelectedScreen(R.id.nav_customer);
            }
            @Override
            public void onDataSyncFailed(boolean isSynced,String daveMsg) {
                Toast.makeText(HomePage.this, daveMsg, Toast.LENGTH_SHORT).show();
            }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    //update queue length
    private void getQueueLength(){
        DatabaseManager databaseManager = DatabaseManager.getInstance(HomePage.this);
        textViewQueueLength.setText("Queue Length : "+ databaseManager.forceUpdateQueueSize());
    }

    private void showLogoutPopUp(){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(HomePage.this);
        builder1.setMessage("Are you sure you want logout?");
        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        builder1.setCancelable(false);
        builder1.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                // check 1) Queue length if >0 show popup 2) Error msg if there show popup 3) make server call 4) Intent to login page after get response from server
                DatabaseManager databaseManager = DatabaseManager.getInstance(HomePage.this);
                long queueSize = databaseManager.forceUpdateQueueSize();
                ArrayList<String> errorRowList = databaseManager.checkErrorRows();
                Log.i(TAG,"Check Queue Size During Logout:-------"+queueSize +" errorRowList:-"+errorRowList );
                if(queueSize > 0){
                    showAPIQueueLengthPopUp(errorRowList);
                }
                else if(errorRowList!= null && errorRowList.size()>0){
                    showErrorLengthPopUp(errorRowList);
                }
                else{
                    logoutApiCall();
                }
            }
        });

        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                checkedCustomerListOnMenu();
               // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void showAPIQueueLengthPopUp(ArrayList<String> errorRowList){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(HomePage.this);
        builder1.setMessage("Unsaved Data Present. Logging out now may lead to loss of data. You can try again later.");
        builder1.setCancelable(false);
        builder1.setPositiveButton("Logout Now", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if(errorRowList!=null && errorRowList.size()>0){
                    showErrorLengthPopUp(errorRowList);
                }else {
                    logoutApiCall();
                }

            }
        });

        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                checkedCustomerListOnMenu();

            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void showErrorLengthPopUp(ArrayList<String> errorRowList){

        StringBuilder errorMsg = new StringBuilder("Some data is not saved due to errors in ");
        for(int i =0;i<errorRowList.size();i++){
            errorMsg.append(errorRowList.get(i)).append(",");
        }
        errorMsg.append(" ... Please correct these errors before logging out.");

        AlertDialog.Builder builder1 = new AlertDialog.Builder(HomePage.this);
        builder1.setMessage(errorMsg.toString());
        builder1.setCancelable(false);
        builder1.setPositiveButton("Discard & Logout", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                logoutApiCall();
            }
        });

        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                checkedCustomerListOnMenu();

            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void logoutApiCall(){
        //todo add loader(Progress Dialog)
        DaveModels daveModels = new DaveModels(HomePage.this,false);
        daveModels.logout(true, new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject jsonObject) {
                Toast.makeText(HomePage.this,"you have been logged out",Toast.LENGTH_SHORT).show();
                SharedPreference sharedPreferences = new SharedPreference(HomePage.this) ;
                sharedPreferences.removeAll();
                Intent intent = new Intent(HomePage.this, LoginScreen.class);
                intent.putExtra("isShowLoginView",true);
                startActivity(intent);
                finish();
            }

            @Override
            public void onResponseFailure(int i, String s) {
                if(i == 401){
                    Toast.makeText(HomePage.this,"you have been logged out",Toast.LENGTH_SHORT).show();
                    SharedPreference sharedPreferences = new SharedPreference(HomePage.this) ;
                    sharedPreferences.removeAll();
                    Intent intent = new Intent(HomePage.this, LoginScreen.class);
                    intent.putExtra("isShowLoginView",true);
                    startActivity(intent);
                    finish();
                }else {
                    Toast.makeText(HomePage.this, s + " cannot logout.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    private void checkedCustomerListOnMenu(){
        navigationView.setCheckedItem(R.id.nav_customer);
    }

    @Override
    public void onCreateOrUpdateView(String objectDetails) {

        if(selectedPosition== R.id.nav_sign_up){
            Toast.makeText(HomePage.this, "Sign Up Success", Toast.LENGTH_SHORT).show();
            try {
                checkedCustomerListOnMenu();
                displaySelectedScreen(R.id.nav_customer);
                JSONObject customerDetails = new JSONObject(objectDetails);

                String strCustomerId = customerDetails.optString(modelUtil.getCustomerIdAttrName());
                sharedPreference.writeString(SharedPreference.CUSTOMER_ID, strCustomerId);


               /* if(!daveAIPerspective.getRecommendations_btn_name().equalsIgnoreCase("__NULL__")) {
                    Intent intent = new Intent(HomePage.this, CategoryHierarchy.class);
                    intent.putExtra("NewCustomerId", strCustomerId);
                    startActivity(intent);


                }else*/
                if(daveAIPerspective.getForm_view_button_name() != null){
                    //create orderId and intent to Formview PAge
                    //createNewOrderId(strCustomerId,objectDetails);
                    new CRUDUtil(this).createNewOrderId(strCustomerId, new CRUDUtil.OnCreateNewOderListener() {
                        @Override
                        public void onCreateNewOrder(String orderId, JSONObject orderDetails) {

                            Intent intent = new Intent(HomePage.this, FormView.class);
                            //intent.putExtra("msg_to_form_view", objectDetails);
                            startActivity(intent);
                        }

                        @Override
                        public void onFailedCreateOrder(int requestCode, String responseMsg) {

                        }
                    });


                }else{
                    HashMap<String, Object> intent_details = new HashMap<>();
                    intent_details.put("customer_id", strCustomerId);
                    intent_details.put("customer_name", customerDetails.getString(daveAIPerspective.getCustomer_profile_title()));
                    intent_details.put("customerDetails",objectDetails);

                    Intent intent = new Intent(HomePage.this, CustomerProfile.class);
                    intent.putExtra("CustomerData", intent_details);
                    startActivity(intent);

                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "<<<<Error msg in catch in DynamicForm >>>>>>>>>>>>>>" + e.getMessage());
            }
        }else {
            Toast.makeText(HomePage.this, "Profile Update Successfully", Toast.LENGTH_SHORT).show();
            displaySelectedScreen(R.id.nav_profile);
        }

    }

    @Override
    public void onFailureCreateOrUpdate(String errorMsg) {
        Toast.makeText(HomePage.this,errorMsg,Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onCustomerSelected(final String customerId, String customerTitle , JSONObject customerDetails) {

        sharedPreference.writeString( SharedPreference.CUSTOMER_ID, customerId);

        HashMap<String, Object> intent_details = new HashMap<>();
        intent_details.put("customer_id", customerId);
        intent_details.put("customer_name", customerTitle);
        intent_details.put("customerDetails", customerDetails.toString());

        Intent intent = new Intent(HomePage.this, CustomerProfile.class);
        intent.putExtra("CustomerData", intent_details);
        startActivity(intent);

    }

    public  void postInteraction() {
        DaveAIHelper.BeforePostInteractionListener d = new DaveAIHelper.BeforePostInteractionListener() {
            @Override
            public void beforePostInteraction(final Context context, final String retailerId, final DaveAIHelper.PostInteractionListener piListener) {

                String currentOrderId = sharedPreference.readString(retailerId);
                String userId = sharedPreference.readString( SharedPreference.USER_ID);

                Log.e(TAG," Check Current Order Id during Post Interaction:-"+currentOrderId+" &&RetailerID:-"+retailerId+" &&UserID:-"+userId);
                if (currentOrderId != null) {
                    piListener.postInteraction();

                }else {
                    if (retailerId != null && userId != null) {
                        DaveAIListener daveAIListener = new DaveAIListener() {
                            @Override
                            public void onReceivedResponse(JSONObject response) {
                                if (response != null) {
                                    try {
                                        Log.e(TAG," After Create Order details************ :-  "+response);
                                        String createNewOrderId = response.getString(daveAIPerspective.getInvoice_id_attr_name());
                                        sharedPreference.writeString(retailerId, createNewOrderId);
                                        sharedPreference.writeString(CUSTOMER_ORDER_DETAILS, response.toString());

                                        DaveAIHelper daveAIHelper = new DaveAIHelper();
                                        daveAIHelper.setCurrentOrderId(retailerId, createNewOrderId);
                                        daveAIHelper.setPreOrderId(retailerId, createNewOrderId);
                                        daveAIHelper.addInteractionFilterAttr(daveAIPerspective.getInvoice_id_attr_name(), createNewOrderId);



                                        piListener.postInteraction();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onResponseFailure(int requestCode, String responseMsg) {

                            }
                        };
                        try {
                            JSONObject postObject = new JSONObject();
                            postObject.put(modelUtil.getCustomerIdAttrName(), retailerId);
                            if(daveAIPerspective.getUser_login_id_attr_name()!=null){
                                postObject.put(daveAIPerspective.getUser_login_id_attr_name(), userId);
                            }
                            if(daveAIPerspective.getInvoice_date_attr()!=null){
                                postObject.put(daveAIPerspective.getInvoice_date_attr(), CRUDUtil.methodToday());
                            }
                            postObject.put(daveAIPerspective.getInvoice_close_attr_name(), false);
                            Log.e(TAG,"Create Order Post Body************ :-  "+postObject);
                            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                                DaveModels daveModels = new DaveModels(HomePage.this, true);
                                daveModels.postObject(daveAIPerspective.getInvoice_model_name(),postObject, daveAIListener);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG,"Error During create new order:-------------  "+e.getMessage());
                        }
                    }
                }
            }
        };
        DaveAIHelper.setBeforePostInteractionListener(d);
    }

    private void createNewOrderId(String strCustomerId, String objectDetails){


        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
             /*   if (pDialog!=null && pDialog.isShowing())
                    pDialog.dismiss();*/
                if (response != null) {
                    try {
                        Log.e(TAG," After Create Order id for form view************ :-  "+response);
                        String orderId = response.getString(daveAIPerspective.getInvoice_id_attr_name());
                        sharedPreference.writeString( strCustomerId, orderId);
                        sharedPreference.readString(strCustomerId);
                        sharedPreference.writeString(CUSTOMER_ORDER_DETAILS, response.toString());
                        Log.e(TAG," After Save order id************ :-  "+orderId);

                        DaveAIHelper daveAIHelper = new DaveAIHelper();
                        daveAIHelper.setCurrentOrderId(strCustomerId, orderId);
                        daveAIHelper.setPreOrderId(strCustomerId, orderId);
                        daveAIHelper.addInteractionFilterAttr(daveAIPerspective.getInvoice_id_attr_name(), orderId);

                        Intent intent = new Intent(HomePage.this, FormView.class);
                        //intent.putExtra("msg_to_form_view", objectDetails);
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                /*if (pDialog!=null && pDialog.isShowing())
                    pDialog.dismiss();*/
            }
        };
        try {
            String userId = sharedPreference.readString( SharedPreference.USER_ID);
            JSONObject postObject = new JSONObject();
            postObject.put(new ModelUtil(HomePage.this).getCustomerIdAttrName(), strCustomerId);
            if(daveAIPerspective.getUser_login_id_attr_name()!=null){
                postObject.put(daveAIPerspective.getUser_login_id_attr_name(), userId);
            }
            if(daveAIPerspective.getInvoice_date_attr()!=null){
                postObject.put(daveAIPerspective.getInvoice_date_attr(), CRUDUtil.methodToday());
            }
            postObject.put(daveAIPerspective.getInvoice_close_attr_name(), false);
            //Log.e(TAG,"Create Order Post Body************ :-  "+postObject);
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                DaveModels daveModels = new DaveModels(HomePage.this, true);
                daveModels.postObject(daveAIPerspective.getInvoice_model_name(),postObject, daveAIListener);
            }
        } catch (Exception e) {
            Log.e(TAG,"Error During create new OrderId:-------------  "+e.getMessage());
        }

    }

}



