package com.example.admin.quickdry.background;

import android.content.Context;
import android.util.Log;

import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler, DatabaseConstants {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private DatabaseManager databaseManager;

    public MyNotificationReceivedHandler(Context context){
        this.context = context;
        databaseManager= DatabaseManager.getInstance(context);
    }

    //fired when your app is in focus or in the background.
    @Override
    public void notificationReceived(OSNotification notification) {

        JSONObject data = notification.payload.additionalData;
        String notificationID = notification.payload.notificationID;
        String title = notification.payload.title;
        String body = notification.payload.body;
        String smallIcon = notification.payload.smallIcon;
        String largeIcon = notification.payload.largeIcon;
        String bigPicture = notification.payload.bigPicture;
        String smallIconAccentColor = notification.payload.smallIconAccentColor;
        String sound = notification.payload.sound;
        String ledColor = notification.payload.ledColor;
        int lockScreenVisibility = notification.payload.lockScreenVisibility;
        String groupKey = notification.payload.groupKey;
        String groupMessage = notification.payload.groupMessage;
        String fromProjectNumber = notification.payload.fromProjectNumber;
        String rawPayload = notification.payload.rawPayload;


        Log.e(TAG, "<<<<<<<<<notificationReceived NotificationID received: " + notificationID +" data:---  "+ data);



    }
}
