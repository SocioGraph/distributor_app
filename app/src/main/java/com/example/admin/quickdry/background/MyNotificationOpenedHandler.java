package com.example.admin.quickdry.background;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.activity.BaseActivity;
import com.example.admin.quickdry.activity.CustomerProfile;
import com.example.admin.quickdry.activity.HomePage;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import org.json.JSONObject;

import java.util.HashMap;


//{"page" : "customer_profile", "id": "some id", "model": "retailer", "action": "get", "params" : {}}
// Actions can be get, patch, post, delete and list. List and get actions should be on click of notificarion others should be on receive.
//Id and model can be none. You can create a mapping between page and model

public class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    private final String TAG = getClass().getSimpleName();
    private Context context;




    public MyNotificationOpenedHandler(Context context){
        this.context = context;
    }

/*
https://test.iamdave.ai/transaction/notification

 _action can be open, delete, post, update
_model, _id and _instance can also be provided...

    {
        "recipient": "6b51cd48-3886-487a-a6e7-d94a8729e83f",
            "message": "test",
            "notification_data": {
               "enterprise_id": "shivatexyarn",
                "_action": "open",
                "_page": "customer",
                "_id": "waves_retailtolichowkitelangana",
                "order_id": "d2F2ZXNfcmV0YWlsdG9saWNob3draXRlbGFuZ2FuYTIwMTktMDItMjY"
            }
    }*/


    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject additionalData = result.notification.payload.additionalData; // notification_data
       // String launchUrl = result.notification.payload.launchURL; // update docs launchUrl

        Log.e(TAG,"Testing notificationOpened:---------actionType>>>>>>>>   "+ actionType +"    <<<<<<<<<additionalData"+ additionalData);

        String defaultEnterpriseId = MultiUtil.getManifestMetadata(context, DaveConstants.MANIFEST_ENTERPRISE_ID);

        // additionalData
        // {
        // "_page":"customer",
        // "_action":"open",
        // "_id":"waves_retailtolichowkitelangana",
        // "enterprise_id":"shivatexyarn",
        // "order_id":"d2F2ZXNfcmV0YWlsdG9saWNob3draXRlbGFuZ2FuYTIwMTktMDItMjY"
        // }


    }

}
