package com.example.admin.quickdry.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.admin.quickdry.R;

import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;


public class QuantityAdapter extends RecyclerView.Adapter<QuantityAdapter.RecycleCustomHolder>
        implements RecyclerView.OnClickListener {

    private static final String TAG = "QuantityAdapter";
    private List<JSONObject> seedQuantityList;
    private Context mContext;
    int counter = 0;
    public static double predicatedQty=0.0;
    private  String strInsight="";

    public QuantityAdapter(Context context,String strInsight, List<JSONObject> seedQuantityList) {
        this.seedQuantityList = seedQuantityList;
        this.mContext = context;
        this.strInsight=strInsight;
    }

    @Override
    public QuantityAdapter.RecycleCustomHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_quantity_card_view, viewGroup, false);
        RecycleCustomHolder holder = new RecycleCustomHolder(view);
        holder.itemView.setOnClickListener(QuantityAdapter.this);
        holder.itemView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecycleCustomHolder customViewHolder, int position) {

        JSONObject seedItem = seedQuantityList.get(position);
        customViewHolder.showInSight.setText(strInsight);
        try {
            Iterator<?> rKeys = seedItem.keys();
            while( rKeys.hasNext() ) {
                String key = (String)rKeys.next();
              //  double d = (double) seedItem.get(key);
                int value= ((Number)seedItem.get(key)).intValue();
               // int value = (int) d;
                predicatedQty= value;
                if(!key.contains("Previous")){
                   // System.out.println("*************Recomeended Qty ***********" + key+" "+ value);
                    customViewHolder.recommendedQtyName.setText(key+" (Recommended)");
                    customViewHolder. recommendedQtyCount.setText(String.valueOf(value));
                }else{
                   // System.out.println("*************Previous Qty***********" +  key+" "+ value);
                    customViewHolder.previousQtyLayout.setVisibility(View.VISIBLE);
                    customViewHolder.previousQtyCount.setText(String.valueOf(value));
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "<<<<<<<<<Get Qty bindAttr Error>>>>>>>>>>" + e.getLocalizedMessage());

        }

        customViewHolder.addQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkQty = String.valueOf(customViewHolder.recommendedQtyCount.getText());
                counter=Integer.parseInt(checkQty);
                counter++;
                customViewHolder.recommendedQtyCount.setText(Integer.toString(counter));
            }
        });

        customViewHolder.subtractQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String checkQty = String.valueOf(customViewHolder.recommendedQtyCount.getText());
                counter=Integer.parseInt(checkQty);
               // counter--;
                if (counter <=1) {
                    counter = 1;
                } else {
                    counter--;
                }
                customViewHolder.recommendedQtyCount.setText(Integer.toString(counter));

            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != seedQuantityList ? seedQuantityList.size() : 0);
    }

    @Override
    public void onClick(View view) {

        RecycleCustomHolder holder = (RecycleCustomHolder) view.getTag();
        int cardPosition = holder.getAdapterPosition();
    }

    class RecycleCustomHolder extends RecyclerView.ViewHolder {

        TextView recommendedQtyName, previousQtyCount,showInSight;
        EditText recommendedQtyCount;
        ImageView addQty,subtractQty;
        LinearLayout previousQtyLayout;

        RecycleCustomHolder(View view) {
            super(view);

            showInSight= view.findViewById(R.id.showInSight);
            recommendedQtyName= view.findViewById(R.id.recommended_qty);
            subtractQty =  view.findViewById(R.id.subtract_qty);
            recommendedQtyCount= view.findViewById(R.id.quantity_count);
            addQty = view.findViewById(R.id.add_qty);
            previousQtyLayout=view.findViewById(R.id.previousQtyLayout);
            previousQtyCount = view.findViewById(R.id.previousQtyCount);
            previousQtyLayout.setVisibility(View.GONE);


            recommendedQtyCount.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    recommendedQtyCount.setSelectAllOnFocus(true);

                    //((EditText)v).selectAll();

                }
            });


        }
    }
}

  /*   Object v = seedItem.get(key);
                if (v instanceof Integer ) {
                    int intToUse = ((Number)v).intValue();

                } else if (v instanceof Double) {
                    double doubleValue = ((Number)v).doubleValue();

                } else {
                    String stringToUse = seedItem.getString(key);
                }*/
