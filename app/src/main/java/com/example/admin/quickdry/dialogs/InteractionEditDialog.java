package com.example.admin.quickdry.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Objects;
import static android.content.Context.INPUT_METHOD_SERVICE;



public class InteractionEditDialog extends Dialog {

    private final String TAG = getClass().getSimpleName();
    private final Context mContext;
    private LinearLayout interactionLayout;
    private String interactionId="";
    private JSONObject interactionDetails;
    private DynamicForm dynamicForm;
    private HashMap<String, Boolean> requiredFieldHashMap= new HashMap<>();
    private ModelUtil modelUtil;


    private InteractionEditCallback interactionEditCallback;
    public interface InteractionEditCallback {
        void onInteractionUpdated(JSONObject updatedInteractionResult);
        void onInteractionUpdatedFailed(int requestCode, String errorMsg);
    }


    public InteractionEditDialog(InteractionEditCallback interactionEditCallback,
                                 Context mContext,JSONObject interactionDetails,String interactionId) {
        super(mContext);
        this.mContext = mContext;
        this.interactionDetails=interactionDetails;
        this.interactionId=interactionId;
        this.interactionEditCallback=interactionEditCallback;

        modelUtil = new ModelUtil(mContext);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.interaction_edit_dialog);

        interactionLayout = findViewById(R.id.interactionLayout);
        Button btnSubmit = findViewById(R.id.btnSubmit);
        Button btnCancel = findViewById(R.id.btnCancel);

        DaveAIPerspective daveAIPerspective = DaveAIPerspective.getInstance();

        dynamicForm = new DynamicForm();
        dynamicForm.createDynamicForm(new DynamicForm.OnFormViewCreatedListener() {
                 @Override
                 public void onFormVewCreated(HashMap<String, Boolean> hashMap) {
                     requiredFieldHashMap= hashMap;
                 }
             },mContext, interactionLayout,new ModelUtil(mContext).getInteractionModelName(),
               daveAIPerspective.getInteraction_stage_attr_list(), interactionDetails.toString());

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                dismiss();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try  {
                    InputMethodManager imm = (InputMethodManager)mContext.getSystemService(INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);

                    JSONObject interactDetails = dynamicForm.methodValidateDynamicView(interactionLayout,requiredFieldHashMap,
                            interactionDetails.toString());

                    Log.i(TAG,"click on submit button:---"+interactDetails +" "+ modelUtil.getInteractionIdAttrName());
                    if(interactDetails!= null){
                        interactDetails.put(modelUtil.getInteractionIdAttrName(),interactionId);
                        updateInteractionAttributes(interactDetails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"error Click on submit Interaction :---   "+e.getMessage());

                }

            }
        });
    }

    private void updateInteractionAttributes(JSONObject interactionDetails){
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                if (response != null) {
                    dismiss();
                    Toast.makeText(mContext,"Updated Successfully",Toast.LENGTH_SHORT).show();
                    if(interactionEditCallback!=null)
                        interactionEditCallback.onInteractionUpdated(response);

                }else {
                    Toast.makeText(mContext,"Something is wrong with the server connection...please try after sometime",
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                Toast.makeText(mContext,requestCode +": "+responseMsg,Toast.LENGTH_SHORT).show();
                if(interactionEditCallback!=null)
                    interactionEditCallback.onInteractionUpdatedFailed(requestCode,responseMsg);

            }
        };
        try {
            if (interactionId!=null && !interactionId.isEmpty()) {
                DaveModels daveModels = new DaveModels(mContext, true);
                daveModels.patchObject(new ModelUtil(mContext).getInteractionModelName(), interactionDetails, daveAIListener);
            }

        } catch (Exception e) {
            e.printStackTrace();

        }


    }


}
