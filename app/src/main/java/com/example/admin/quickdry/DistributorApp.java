package com.example.admin.quickdry;


import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import com.example.admin.daveai.broadcasting.PushNotifications;
import com.example.admin.daveai.others.DaveAIHelper;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.activity.AddScanProduct;
import com.example.admin.quickdry.activity.CustomerProfile;
import com.example.admin.quickdry.activity.HomePage;
import com.example.admin.quickdry.storage.SharedPreference;
import com.example.admin.quickdry.background.NetWatcher;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;


public class DistributorApp extends Application implements PushNotifications.PushNotificationsListener  {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private static DistributorApp instance;
    private NetWatcher netWatcher;

   /* public static Context getContext() {
        return context;
    }*/
    public static DistributorApp getInstance() {
        return instance;
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }



    @Override
    public void onCreate() {
        instance = this;
        context = getApplicationContext();
        super.onCreate();

        netWatcher = new NetWatcher();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

       // registerReceiver(netWatcher, filter);
       // Fabric.with(this, new Crashlytics());

        // Make sure we use vector drawables
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
/*
        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
        String oneSignalPlayerId = status.getSubscriptionStatus().getUserId();
        Log.i(TAG,"One Signal Player id :-----------"+oneSignalPlayerId);*/
        // Logging set to help debug issues, remove before releasing your app.
        //OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.WARN);

        //MyNotificationOpenedHandler : This will be called when a notification is tapped on.
        //MyNotificationReceivedHandler : This will be called when a notification is received while your app is running.
       /* OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
               // .autoPromptLocation(true)
               // .setNotificationOpenedHandler(new MyNotificationOpenedHandler(this))
               // .setNotificationReceivedHandler( new MyNotificationReceivedHandler(this))
                .init();*/

        // Call syncHashedEmail anywhere in your app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // OneSignal.syncHashedEmail(userEmail);

        PushNotifications pushNotifications  = new PushNotifications(this,this);
        pushNotifications.startPushNotification();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.e(TAG,"ON LOW MEMORY <><><><><><>");
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onNotificationReceived(OSNotification notification, JSONObject jsonObject) {

       // Log.e(TAG," <<<<<<<<<<<<<<<<<<<<<<<<<<<<onNotificationReceived:-------"+jsonObject);

        String title = notification.payload.title;
        String body = notification.payload.body;
        String smallIcon = notification.payload.smallIcon;
        String largeIcon = notification.payload.largeIcon;
        String bigPicture = notification.payload.bigPicture;
        String smallIconAccentColor = notification.payload.smallIconAccentColor;
        String sound = notification.payload.sound;
        String ledColor = notification.payload.ledColor;
        int lockScreenVisibility = notification.payload.lockScreenVisibility;
        String groupKey = notification.payload.groupKey;
        String groupMessage = notification.payload.groupMessage;
        String fromProjectNumber = notification.payload.fromProjectNumber;
        String rawPayload = notification.payload.rawPayload;

        Log.e(TAG,"Print title "+title+" body:- "+ body +" smallIcon:-"+ smallIcon);


    }

    // notificationData
    // {
    // "_page":"customer",
    // "_action":"open",
    // "_id":"waves_retailtolichowkitelangana",
    // "enterprise_id":"shivatexyarn",
    // "order_id":"d2F2ZXNfcmV0YWlsdG9saWNob3draXRlbGFuZ2FuYTIwMTktMDItMjY"
    // }

    @Override
    public void onNotificationOpened(OSNotificationOpenResult osNotificationOpenResult, JSONObject notificationData) {

        Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<,onNotificationOpened notificationData :-------"+notificationData);
        String actionName= notificationData.optString("_action",null);
        if(actionName!= null && !actionName.isEmpty()){
            try {
                Intent intent = null;
                Object activityToLaunch = HomePage.class;
                if(actionName.equalsIgnoreCase("open"))
                {
                    DaveAIPerspective.getInstance().savePerspectivePreferenceValue(context); //set perspective
                    String intentPage = notificationData.optString("_page", null);
                     Log.e(TAG, "<<<<<<<<<<<<<<<Print IntentPage Name>>>>>>>>>>>>>>> " + intentPage );

                    if (intentPage != null)
                    {
                        switch (intentPage) {

                            case "user":
                                intent = new Intent(context, (Class<?>) activityToLaunch);
                                intent.putExtra("_menuItemName",intentPage);
                                break;

                            case "customers":
                                intent = new Intent(context, (Class<?>) activityToLaunch);
                                intent.putExtra("_menuItemName",intentPage);
                                break;

                            case "products":
                                intent = new Intent(context, (Class<?>) activityToLaunch);
                                intent.putExtra("_menuItemName",intentPage);
                                break;

                            case "orders":
                                intent = new Intent(context, (Class<?>) activityToLaunch);
                                intent.putExtra("_menuItemName",intentPage);
                                break;

                            case "customer":
                                activityToLaunch = CustomerProfile.class;
                                intent = new Intent(context, (Class<?>) activityToLaunch);

                                HashMap<String, String> intentData = new HashMap<>();
                                intentData.put("customer_id", notificationData.get("_id").toString());
                                if(notificationData.has("_order_id") && !notificationData.isNull("_order_id"))
                                    intentData.put("openOrderId", notificationData.get("_order_id").toString());
                                intentData.put("customerDetails", "");
                                intent.putExtra("CustomerData", intentData);

                                break;

                            case "product":

                                activityToLaunch = AddScanProduct.class;
                                intent = new Intent(context, (Class<?>) activityToLaunch);

                                Bundle b = new Bundle();
                                b.putString("action_name", AddScanProduct.ACTION_SHOW_PRODUCT);
                                b.putString("_product_id",notificationData.get("_id").toString());

                                if(notificationData.has("_instance") && !notificationData.isNull("_instance")){
                                    JSONObject instanceDetails = notificationData.getJSONObject("_instance");

                                    String strCustomerId ="";
                                    String orderId = "";

                                    if(notificationData.has("_customer_id") && !notificationData.isNull("_customer_id")) {
                                       strCustomerId = instanceDetails.getString("_customer_id");
                                        b.putString("customer_id",strCustomerId);
                                    }

                                    if(instanceDetails.has("_order_id") && !instanceDetails.isNull("_order_id")) {
                                         orderId = instanceDetails.getString("_order_id");
                                    }
                                    // save orderId in share preference
                                    if(strCustomerId!=null && !strCustomerId.isEmpty() && orderId!=null && !orderId.isEmpty()){

                                        SharedPreference sharedPreference = new SharedPreference(context);
                                        sharedPreference.writeString( strCustomerId , orderId);

                                        DaveAIHelper setData = new DaveAIHelper();
                                        setData.setPreOrderId(strCustomerId, orderId);
                                        setData.setCurrentOrderId(strCustomerId, orderId);
                                        DaveAIPerspective daveAIPerspective = DaveAIPerspective.getInstance();
                                        if(daveAIPerspective.getInvoice_id_attr_name()!= null)
                                            setData.addInteractionFilterAttr(daveAIPerspective.getInvoice_id_attr_name(),orderId);

                                    }

                                }
                                intent.putExtras(b);

                                break;

                            default:
                                intent = new Intent(context, (Class<?>) activityToLaunch);
                                break;
                        }

                    }

                }

                if(intent != null) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                   // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                   // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else {
                    Log.e(TAG,"<<<<<<<<<<<<<<<<Error in intent >>>>>>>>>>>>>>>>  "+ intent);
                }

            }
            catch (JSONException t) {
                t.printStackTrace();

            }catch (Throwable t) {
                t.printStackTrace();
            }

        }else {
            Log.e(TAG,"<<<<<Error Action Name is null or Empty>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+actionName);
        }
    }




    /* @Override
    public void onResume(){
        super.onResume();
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<onResume>>>>>>>>>>>>>>>>>>");

    }

    @Override
    public void onPause(){
        super.onPause();
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<onPause>>>>>>>>>>>>>>>>>>");

    }

    @Override
    public void onRestart() {
        super.onRestart();
        // do some stuff here
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<onRestart>>>>>>>>>>>>>>>>>>");
    }*/


   /* private class MyNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {

            JSONObject data = notification.payload.additionalData;
            String notificationID = notification.payload.notificationID;
            String title = notification.payload.title;
            String body = notification.payload.body;
            String smallIcon = notification.payload.smallIcon;
            String largeIcon = notification.payload.largeIcon;
            String bigPicture = notification.payload.bigPicture;
            String smallIconAccentColor = notification.payload.smallIconAccentColor;
            String sound = notification.payload.sound;
            String ledColor = notification.payload.ledColor;
            int lockScreenVisibility = notification.payload.lockScreenVisibility;
            String groupKey = notification.payload.groupKey;
            String groupMessage = notification.payload.groupMessage;
            String fromProjectNumber = notification.payload.fromProjectNumber;
            String rawPayload = notification.payload.rawPayload;

            Log.i(TAG, "MyNotificationReceivedHandler NotificationID received: " + notificationID);

            String customKey;
            if (data != null) {
                customKey = data.optString("customkey", null);
                if (customKey != null)
                    Log.i(TAG, "customkey set with value: " + customKey);
            }
        }
    }

    private class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {

            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;
            String launchUrl = result.notification.payload.launchURL; // update docs launchUrl

            String customKey;
            String openURL = null;
            Object activityToLaunch = HomePage.class;

            if (data != null) {
                customKey = data.optString("customkey", null);
                openURL = data.optString("openURL", null);

                if (customKey != null)
                    Log.i(TAG, "customkey set with value: " + customKey);

                if (openURL != null)
                    Log.i(TAG, "openURL to webview with URL value: " + openURL);
            }

            if (actionType == OSNotificationAction.ActionType.ActionTaken) {
                Log.i(TAG, "Button pressed with id: " + result.action.actionID);

                if (result.action.actionID.equals("id1")) {
                    Log.i(TAG, "button id called: " + result.action.actionID);
                    //activityToLaunch = GreenActivity.class;
                } else
                    Log.i(TAG, "button id called: " + result.action.actionID);
            }

            // The following can be used to open an Activity of your choice.
            // Replace - getApplicationContext() - with any Android Context.
            // Intent intent = new Intent(getApplicationContext(), YourActivity.class);
            Intent intent = new Intent(getApplicationContext(), (Class<?>) activityToLaunch);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("openURL", openURL);
            Log.i(TAG, "openURL = " + openURL);
            startActivity(intent);

            // Add the following to your AndroidManifest.xml to prevent the launching of your main Activity if you are calling startActivity above.
            //   <application ...>
            //    <meta-data android:name="com.onesignal.NotificationOpened.DEFAULT" android:value="DISABLE" />
            //   </application>
        }
    }*/


}




