package com.example.admin.quickdry.utils;

import android.content.Context;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.InteractionStages;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.storage.SharedPreference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Iterator;
import java.util.List;
import static com.example.admin.quickdry.activity.CustomerProfile.customerDetails;
import static com.example.admin.quickdry.adapter.QuantityAdapter.predicatedQty;


public class PostInteractionUtil {

    private  final static String TAG = "PostInteractionUtil";

    private Context context;

    public String NEXT_STAGE_ATTRIBUTE_NAME = "next_stage";
    public String QUANTITY_ATTRIBUTES = "quantity_attributes";
    public String NEXT_STAGE_QUANTITY_ATTRIBUTES = "next_stage_quantity_attributes";
    public String NEXT_STAGE_TAGGED_PRODUCTS = "next_stage_tagged_products";
    public String NEXT_STAGE_NEW_ATTRIBUTES = "next_stage_new_attributes";



    private JSONObject currentStageDetails;
    private DaveAIPerspective daveAIPerspective;
    private SharedPreference sharedPreference;



    public PostInteractionUtil(Context context, JSONObject currentStageDetails){
        this.context = context;
        this.currentStageDetails = currentStageDetails;

        daveAIPerspective = DaveAIPerspective.getInstance();
        sharedPreference = new SharedPreference(context);
    }


    public String getCardSwipeStageName(int direction) {
        try {
            if (direction == ItemTouchHelper.LEFT) {
            return currentStageDetails.getString("previous_stage"); //previous_stage

            } else if (direction == ItemTouchHelper.RIGHT) {
                return currentStageDetails.getString(NEXT_STAGE_ATTRIBUTE_NAME); //next stage
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "__NULL__" ;
    }

    public boolean checkNextStageEvent(final int direction) {
        try{

           // checkNextStageQuantityAttrs();
            //checkNextStageTaggedProducts();
            //checkNextStageNewAttributes();
            if (direction == ItemTouchHelper.RIGHT && checkNextStageAttributeName() && (checkNextStageTaggedProducts()||checkNextStageNewAttributes())) {
                return true;
            }else {
                return false;
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }



    private boolean checkNextStageAttributeName(){
        try {
            Log.i(TAG, "<<<<<<<<<checkNextStageAttributeName>>>>>>>>>  " + currentStageDetails.getString(NEXT_STAGE_ATTRIBUTE_NAME));
            return ( !currentStageDetails.isNull(NEXT_STAGE_ATTRIBUTE_NAME) && !currentStageDetails.getString(NEXT_STAGE_ATTRIBUTE_NAME).equalsIgnoreCase("_NULL__"));
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean checkNextStageQuantityAttrs(){
        try {
            Log.i(TAG, "<<<<<<<<<checkNextStageQuantityAttrs>>>>>>>>>>  " + currentStageDetails.getJSONArray(NEXT_STAGE_QUANTITY_ATTRIBUTES));
            return ( !currentStageDetails.isNull(NEXT_STAGE_QUANTITY_ATTRIBUTES) && currentStageDetails.getJSONArray(NEXT_STAGE_QUANTITY_ATTRIBUTES).length()> 0);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean checkNextStageTaggedProducts(){
        try {
            Log.i(TAG, "<<<<<<<<<checkNextStageTaggedProducts>>>>>>>>>>  " + currentStageDetails.getJSONArray(NEXT_STAGE_TAGGED_PRODUCTS));
            return ( !currentStageDetails.isNull(NEXT_STAGE_TAGGED_PRODUCTS) && currentStageDetails.getJSONArray(NEXT_STAGE_TAGGED_PRODUCTS).length() > 0);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public JSONArray getNextStageTaggedProducts(){
        try {
            if(!currentStageDetails.isNull(NEXT_STAGE_TAGGED_PRODUCTS) && currentStageDetails.has(NEXT_STAGE_TAGGED_PRODUCTS))
                return currentStageDetails.getJSONArray(NEXT_STAGE_TAGGED_PRODUCTS);
            else
                return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }


    private boolean checkNextStageNewAttributes(){
        try {
            Log.i(TAG, "<<<<<<<<< checkNextStageNewAttributes>>>>>>>>>> " + currentStageDetails.getJSONArray(NEXT_STAGE_NEW_ATTRIBUTES));
            return ( !currentStageDetails.isNull(NEXT_STAGE_NEW_ATTRIBUTES) && currentStageDetails.getJSONArray(NEXT_STAGE_NEW_ATTRIBUTES).length() > 0);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public JSONArray getNextStageNewAttributes(){
        try {
            if(!currentStageDetails.isNull(NEXT_STAGE_NEW_ATTRIBUTES) && currentStageDetails.has(NEXT_STAGE_NEW_ATTRIBUTES))
                return currentStageDetails.getJSONArray(NEXT_STAGE_NEW_ATTRIBUTES);
            else
                return new JSONArray();
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }


    public void postTaggedProduct(String customerId,String productId, String interactionStage,JSONObject otherDetails){

        JSONObject post_body = new JSONObject();
        try {

            ModelUtil modelUtil = new ModelUtil(context);
            post_body.put(modelUtil.getInteractionCustomerId(), customerId);
            post_body.put(modelUtil.getInteractionProductId(), productId);
            post_body.put(modelUtil.getInteractionStageAttrName(), interactionStage);


            SharedPreference sharedPreference = new SharedPreference(context);
            String cOrderId = sharedPreference.readString(customerId);
            if(cOrderId!=null){
                post_body.put(daveAIPerspective.getInvoice_id_attr_name(),cOrderId);
            }
            else{
                if(daveAIPerspective.getUser_login_id_attr_name()!=null){
                    String userId = new SharedPreference(context).readString(SharedPreference.USER_ID);
                    post_body.put(daveAIPerspective.getUser_login_id_attr_name(),userId);
                }
            }

            if(otherDetails!=null && otherDetails.length()>0){
                Iterator<?> keys = otherDetails.keys();
                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    post_body.put(key, otherDetails.get(key));
                }
            }

            postInteraction(post_body);

        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG, "********post TaggedInteraction Error*********   " + e.getLocalizedMessage());
        }

    }

    public void postInteraction(String customerId, String productId, String interactionStage,JSONObject otherDetails) {

        String currentOrderId = sharedPreference.readString(customerId);
        String userId = sharedPreference.readString( SharedPreference.USER_ID);

        Log.e(TAG," Check Current Order Id during Post Interaction:-"+currentOrderId+" &&CustomerID:-"+customerId+" &&UserID:-"+userId);
        if (currentOrderId != null) {
            createPostBody(customerId,productId,interactionStage,otherDetails);

        }else {
            if (customerId != null && userId != null) {
                new CRUDUtil(context).createNewOrderId(customerId, new CRUDUtil.OnCreateNewOderListener() {
                    @Override
                    public void onCreateNewOrder(String orderId, JSONObject orderDetails) {
                        createPostBody(customerId,productId,interactionStage,otherDetails);
                    }

                    @Override
                    public void onFailedCreateOrder(int requestCode, String responseMsg) {

                    }
                });

            }
        }
    }

    private void createPostBody(String customerId, String productId, String interactionStage,JSONObject otherDetails){

        JSONObject post_body = new JSONObject();
        try {


            Log.e(TAG,"postInteractionAfterSwipe Get Card Swipe Details :----  "+ currentStageDetails);

            String interactionIdAttrName = new ModelUtil(context).getInteractionIdAttrName();
            if(currentStageDetails.has(interactionIdAttrName))
                post_body.put(interactionIdAttrName,currentStageDetails.getString(interactionIdAttrName));

            ModelUtil modelUtil = new ModelUtil(context);
            post_body.put(modelUtil.getInteractionCustomerId(), customerId);
            post_body.put(modelUtil.getInteractionProductId(), productId);
            post_body.put(modelUtil.getInteractionStageAttrName(), interactionStage);

            SharedPreference sharedPreference = new SharedPreference(context);

            String cOrderId = sharedPreference.readString(customerId);
            if(cOrderId!=null){
                post_body.put(daveAIPerspective.getInvoice_id_attr_name(),cOrderId);
            }
            else{
                if(daveAIPerspective.getUser_login_id_attr_name()!=null){
                    String userId = new SharedPreference(context).readString(SharedPreference.USER_ID);
                    post_body.put(daveAIPerspective.getUser_login_id_attr_name(),userId);
                }
            }

            if(daveAIPerspective.getPredicated_qty_attr()!= null)
                post_body.put(daveAIPerspective.getPredicated_qty_attr(),predicatedQty);

            if(otherDetails!=null && otherDetails.length()>0){
                Iterator<?> keys = otherDetails.keys();
                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    post_body.put(key, otherDetails.get(key));
                }
            }

            //todo add current Stage in post Interaction body
            // product_attributes,  customer_attributes,  tagged_products, interaction_stage_attr_list,
            if(currentStageDetails.has("current_stage")) {
                post_body = addStageAttributesInInteractionBody(currentStageDetails.getJSONObject("current_stage"),post_body);
            }else{
                post_body = addStageAttributesInInteractionBody(currentStageDetails, post_body);
            }

            //Log.e(TAG,"postInteractionAfterSwipe Get Card Swipe Details :------------------------:--------  "+cardDetails);
            InteractionStages interactionInstance = ModelUtil.getStageInstance(context,interactionStage);
            List<String> customerAttributes = interactionInstance.getCustomer_attributes();
            List<String> productAttributes = interactionInstance.getProduct_attributes();
            JSONObject productDetails = new JSONObject();
            if(currentStageDetails.has("details") && !currentStageDetails.isNull("details")  && currentStageDetails.getJSONObject("details").length()>0){
                productDetails = currentStageDetails.getJSONObject("details");

            }else if(currentStageDetails.has("product_attributes") && !currentStageDetails.isNull("product_attributes")&& currentStageDetails.getJSONObject("product_attributes").length()>0){
                productDetails = currentStageDetails.getJSONObject("product_attributes");
            }
            // Log.i(TAG,"Print current Stage  customer Attributes >>>>"+customerAttributes );
            // Log.i(TAG,"Print current Stage  product_attributes>>>>"+productAttributes +"  :--- "+ productDetails);
            post_body = addModelAttributesInPostInteraction(productAttributes,productDetails,post_body);

            Log.e(TAG,"Print customer details on swipe:----------"+customerDetails);
            if( customerDetails!=null && !customerDetails.isEmpty())
                post_body = addModelAttributesInPostInteraction(customerAttributes,new JSONObject(customerDetails),post_body);

            postInteraction(post_body);

        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG, "********postInteraction Error*********   " + e.getLocalizedMessage());
        }

    }

    private void postInteraction(JSONObject post_body){

        Log.e(TAG, "********Post Interaction Body**********   " + post_body);
        DaveModels daveModels = new DaveModels(context, false);
        daveModels.postObject(new ModelUtil(context).getInteractionModelName(), post_body,new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject jsonObject) {

            }
            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                Log.e(TAG,"Error During Post Interaction :--------"+responseMsg);

            }
        } );
    }


    public static JSONObject addStageAttributesInInteractionBody(JSONObject source, JSONObject target){

        // product_attributes,customer_attributes, stage, customer_id, product_id, invoice_id,
        //  quantity_attributes,new_interaction_attributes, tagged_products, interaction_stage_attr_list
        try {
            Log.i(TAG,"addCurrentStageAttributesInPostInteraction source :------------------------:--------  "+source);

            if(source.has("quantity_attributes") && !source.isNull("quantity_attributes") && source.getJSONObject("quantity_attributes").length()>0) {
                Log.i(TAG,"Post  quantity_attributes>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+source.get("quantity_attributes"));
                target = addOtherAttrInPostInteraction(source,target, "quantity_attributes", source.get("quantity_attributes"));
            }

            if(source.has("new_interaction_attributes") && !source.isNull("new_interaction_attributes")) {
                Log.i(TAG,"Post  new_interaction_attributes>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+source.get("new_interaction_attributes"));
                target = addOtherAttrInPostInteraction(source,target, "new_interaction_attributes", source.get("new_interaction_attributes"));
            }

            if(source.has("tagged_products") && !source.isNull("tagged_products")) {
                Log.i(TAG,"Post  tagged_products>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+source.get("tagged_products"));
                target = addOtherAttrInPostInteraction(source,target, "tagged_products", source.get("tagged_products"));
            }

            if(source.has("interaction_stage_attr_list") && !source.isNull("interaction_stage_attr_list")) {
                Log.i(TAG,"Post  interaction_stage_attr_list>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+source.get("interaction_stage_attr_list"));
                target = addOtherAttrInPostInteraction(source,target, "interaction_stage_attr_list", source.get("interaction_stage_attr_list"));
            }

            Log.i(TAG, "******** After addCurrentStageAttributesInPostInteraction target**********   " + target.toString());

        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG, "********postInteraction Error*********   " + e.getLocalizedMessage());
        }
        return target;
    }

    private static JSONObject addOtherAttrInPostInteraction(JSONObject source,JSONObject target,String key,Object keyValue){
        try {
            Log.e(TAG,"addOtherAttrInPostInteraction:-----"+key +"  getClassOfObject :- "+ keyValue.getClass() +" keyValue:-"+ keyValue );
            if (keyValue instanceof JSONObject) {
                JSONObject getJsonValue = (JSONObject) keyValue;
                Iterator<?> keys = getJsonValue.keys();
                while(keys.hasNext() ) {
                    String jsonKey = (String)keys.next();
                    try {
                        if(getJsonValue.get(jsonKey) instanceof JSONObject ) {
                            addOtherAttrInPostInteraction(source,target,jsonKey,getJsonValue.get(jsonKey));
                        }else {
                            if(!target.has(jsonKey))
                                target.put(jsonKey, getJsonValue.get(jsonKey));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }else if(keyValue instanceof JSONArray){
                JSONArray jsonArray = (JSONArray) keyValue;
                for(int i = 0;i<jsonArray.length();i++){
                    String arrKey = jsonArray.get(i).toString();
                    if(!target.has(arrKey) && source.has(arrKey))
                        target.put(arrKey,source.getString(arrKey));
                }
            } else {
                if(!target.has(key))
                    target.put(key, keyValue);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return target;
    }

    public static JSONObject addModelAttributesInPostInteraction (List<String> modelAttributes,JSONObject modelObjectDetail, JSONObject interactionPostBody){
        try {
            Log.i(TAG, "addModelAttributesInPostInteraction:-" + modelAttributes +" details:--"+ modelObjectDetail);
            if(modelAttributes!=null && !modelAttributes.isEmpty()){
                for(int i = 0; i<modelAttributes.size();i++){
                    String key = modelAttributes.get(i);
                    if(modelObjectDetail.has(key) && !modelObjectDetail.isNull(key) && !interactionPostBody.has(key))
                        interactionPostBody.put(key,modelObjectDetail.getString(key)) ;
                }

            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during  getting Product Id +++++++++++" + e.getMessage());
        }

        return  interactionPostBody;
    }

    private void getModelAttributes(String modelName,String objectID,List<String> modelAttributes,JSONObject jsonObject){

        //Log.e(TAG, " <<<<<<<<<<<<<<<<<<<Before calling getObject product Details with Id>>>>>>>>>>>:------"+productID);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    Log.i(TAG, "<<<<<<<<<<<<<getCustomerAttributes>>>Details:-" + response);
                    if(modelAttributes!=null && !modelAttributes.isEmpty()){
                        for(int i = 0; i<modelAttributes.size();i++){
                            String key = modelAttributes.get(i);
                            if(response.has(key) && !response.isNull(key) && !jsonObject.has(key))
                                jsonObject.put(key,response.getString(key)) ;
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "Error during  getting Product Id +++++++++++" + e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(objectID!=null && !objectID.isEmpty()) {
                DaveModels daveModels = new DaveModels(context, true);
                daveModels.getObject(modelName, objectID, daveAIListener, false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"error during getting product details:--"+e.getMessage());
        }
    }



}
