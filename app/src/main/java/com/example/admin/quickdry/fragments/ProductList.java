package com.example.admin.quickdry.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dialogbox.CustomProgressDialog;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.AddScanProduct;
import com.example.admin.quickdry.adapter.ProductAdapter;
import com.example.admin.quickdry.utils.CRUDUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;



public class ProductList extends Fragment implements ProductAdapter.ProductListListener , CategoryFilter.OnCategoryFilterListener {

        private final String TAG = getClass().getSimpleName();

        private static final String LAYOUT_ID = "layout_id";
        private static final String ADAPTER_LAYOUT_ID = "adapter_layout_id";
        private Context context;

        private RecyclerView mRecyclerView;
        LinearLayoutManager mLayoutManager,shLayoutManager;
        EditText editTextSearch;
        ImageView searchViewIcon,searchButton;
        Button cancelBtn;
        RelativeLayout searchView ;
        LinearLayout objectsParentLayout,objectsListView;
        View includeSearchLayout, resultNotFoundLayout;
        TextView resultNotFound,subHeaderTitle,subHeaderCount;
        LinearLayout  menuItemActionLayout,breadCrumbsLayout;
        ListView filterParentView,filterChildView;
        HorizontalScrollView breadcrumbWrapper,itemDetailsParent;
        CrystalRangeSeekbar rangeSeekbar;
        TextView priceMin, priceMax,notFound;
        View includeSubHeader;
        ProgressBar progressBar;
        SwipeRefreshLayout mSwipeRefreshLayout;
        FrameLayout frameLayout;
        RelativeLayout priceRangeLayout;

        ArrayAdapter<String> filterParentAdapter,filterChildAdapter;
        private ProductAdapter mAdapter;

        public  Model productModelInstance;
        private DaveAIPerspective daveAIPerspective;
        private ModelUtil modelUtil;
        CategoryFilter childFragment; // Fragment childFragment;
        CustomProgressDialog customProgressDialog;

        //pagination
        private int mPageNumber = 1;
        String strDefaultCount = "0";
        int pastVisiblesItems, visibleItemCount, totalItemCount;
        private boolean isPageLoading = true;
        // private boolean isLastPage =false;

        private int viewHolderPosition = -1;
        boolean isAnyActionApply = false;
        private String strSearchTerm="";

        //filter variables
        HashMap<String,ArrayList<String>> defaultFilterList = new HashMap<>();
        HashMap<String, String> filterAttrTitleMap = new HashMap<>();
        ArrayList<String> filterTitleList = new ArrayList<>();
        HashMap<String, Object> selectedFilterMap = new HashMap<>();
        int filterTitlePosition;






    public ProductList() {}

    public ProductList newInstance(int layoutId, int adapterLayout) {
        ProductList fragment = new ProductList();
        Bundle args = new Bundle();
        args.putInt(LAYOUT_ID, layoutId);
        args.putInt(ADAPTER_LAYOUT_ID, adapterLayout);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_recyclelist_view, container, false);
        context = getActivity();

        objectsParentLayout =  mainView.findViewById(R.id.customerParentLayout);
        objectsListView =  mainView.findViewById(R.id.objectListLayout);
        resultNotFound =   mainView.findViewById(R.id.noSearchFound);

        mRecyclerView = (RecyclerView) mainView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ProductAdapter(ProductList.this,getActivity(), new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.scrollToPosition(pastVisiblesItems);

        includeSearchLayout =  mainView.findViewById(R.id.includeSearchLayout);
        searchView = (RelativeLayout)mainView.findViewById(R.id.search_view);
        searchViewIcon = mainView.findViewById(R.id.searchViewIcon);

        includeSearchLayout =  mainView.findViewById(R.id.includeSearchLayout);
        includeSearchLayout.setVisibility(View.GONE);
        editTextSearch = includeSearchLayout.findViewById(R.id.editTextSearch);
        cancelBtn = includeSearchLayout.findViewById(R.id.cancelButton);
        searchButton = includeSearchLayout.findViewById(R.id.searchButton);


        includeSubHeader = mainView.findViewById(R.id.includeSubHeader);
        subHeaderTitle = includeSubHeader.findViewById(R.id.item_name);
        subHeaderCount = includeSubHeader.findViewById(R.id.item_value);
        subHeaderTitle.setText("Total Number");

        breadcrumbWrapper = mainView.findViewById(R.id.breadcrumbWrapper);
        breadcrumbWrapper.setVisibility(View.GONE);
        breadCrumbsLayout =  mainView.findViewById(R.id.breadCrumbsLayout);
        menuItemActionLayout =  mainView.findViewById(R.id.menuItemActionLayout);

        frameLayout =  mainView.findViewById(R.id.frameLayout);
        progressBar =  mainView.findViewById(R.id.progressBar);
        mSwipeRefreshLayout = mainView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, //R.color.colorAccent
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);


        modelUtil = new ModelUtil(context);
        daveAIPerspective = DaveAIPerspective.getInstance();
        customProgressDialog = new CustomProgressDialog(context);

        childFragment = CategoryFilter.newInstance(modelUtil.getProductModelName(),daveAIPerspective.getProduct_category_hierarchy(),
                modelUtil.getProductCategoryHierarchy(),null);
        Log.e(TAG,"Print Catgeory Filter instance1:----------"+childFragment);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.child_fragment_container, childFragment,"CategoryFilter").commit();
        childFragment.registerCategoryFilterCallback(ProductList.this);

     /*    new ShowcaseView.Builder(getActivity())
           // .setTarget(new ActionViewTarget(getActivity(), ActionViewTarget.Type.HOME))
            .setContentTitle("Dave Recommendations")
             .setContentText("Swipe card left to Go Previous and right to Go Next")
            .hideOnTouchOutside()
            .build();*/

        productModelInstance = Model.getModelInstance(getActivity(),modelUtil.getProductModelName());
        //get filter attr list
        defaultFilterList = modelUtil.getProductFilterAttrList();
        Log.e(TAG,"Print default Filter List:------------"+ defaultFilterList);
        if (defaultFilterList != null && !defaultFilterList.isEmpty()) {
            for (Map.Entry<String, ArrayList<String>> entry : defaultFilterList.entrySet()) {
                if (productModelInstance != null)
                    filterAttrTitleMap.put(productModelInstance.getAttributeTitle(entry.getKey()), entry.getKey());
            }
            filterTitleList.addAll(filterAttrTitleMap.keySet());
        }

        return mainView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
       // getActivity().setTitle("Products");

        getProductsList(mPageNumber,true,true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                progressBar.setVisibility(View.GONE);
                Log.e(TAG,"Print on pull gor refresh:-------------"+ isAnyActionApply);

                if(mSwipeRefreshLayout!=null){
                    if(!isAnyActionApply) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        mPageNumber =1;
                        selectedFilterMap.clear();
                        childFragment.resetCategoryHierarchy();
                        getProductsList(mPageNumber,true,true);
                    }else {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
                /*if(mSwipeRefreshLayout != null && !isAnyActionApply) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    mPageNumber =1;
                    childFragment.resetCategoryHierarchy();
                    getProductsList(mPageNumber,true,true);
                }else {
                    mSwipeRefreshLayout.setRefreshing(false);
                }*/

            }
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                  /*  if (isPageLoading && !showFilterItems) {
                        Log.e(TAG,"make next call on scroll:------------"+isLastPage +" == "+(visibleItemCount+pastVisiblesItems));
                        if (!isLastPage && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            isPageLoading = false;
                            mPageNumber++;
                            //Do pagination.. i.e. fetch new data from Server

                            if(isAnyActionApply){
                                getProductsList(mPageNumber,false,false);
                            }else {
                                getProductsList(mPageNumber,false,true);
                            }
                        }
                    }
*/
                    if (isPageLoading && CRUDUtil.checkNextPage(mAdapter.getItemCount()) &&(visibleItemCount + pastVisiblesItems) >= totalItemCount ) {
                        Log.e(TAG,"make next call on scroll:------------ == "+(visibleItemCount+pastVisiblesItems));
                        isPageLoading = false;
                        mPageNumber++;
                        //Do pagination.. i.e. fetch new data from Server
                        if(isAnyActionApply){
                            getProductsList(mPageNumber,false,false);
                        }else {
                            getProductsList(mPageNumber,false,true);
                        }

                    }
                }
            }
        });

        searchViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.GONE);
                includeSearchLayout.setVisibility(View.VISIBLE);
                strDefaultCount = subHeaderCount.getText().toString();

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                includeSearchLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);
                resultNotFound.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.VISIBLE);
                editTextSearch.setText("");
                strSearchTerm = "";
                hideSoftKeyword(v);
                isAnyActionApply=false;
                mAdapter.resetPreviousList();
                subHeaderCount.setText(strDefaultCount);
            }
        });


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyword(v);
                actionOnSearchButton();
            }
        });

        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideSoftKeyword(editTextSearch);
                    actionOnSearchButton();
                    return true;
                }
                return false;
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {
            boolean searchedFlag = false;
            Timer timer;
            Handler handler;

            public void afterTextChanged(Editable s) {
                String searchedTerm = s.toString().trim();
                timer = new Timer();
                handler = new Handler();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                //do Stuff here
                                if(searchedTerm.length() > 0) {
                                    //getFilteredItemFromServer(searchedTerm, false);
                                    // mPageNumber = 1;
                                    getListBasedOnSearch();
                                }
                            }
                        });

                    }
                }, 2000); // 2000ms delay before the timer executes the „run“ method from TimerTask
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                String searchedTerm = query.toString().trim();
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
                if(!searchedFlag && searchedTerm.length() > 2){
                    searchedFlag= true;
                    //mPageNumber = 1;
                    getListBasedOnSearch();

                }
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG,"onStart>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }


    @Override
    public void onResume(){
        super.onResume();
        Log.i(TAG,"onStart>>>>>>>>>>>>>>>>>>>>>>>>>>>");

    }

    @Override
    public void onPause(){
        super.onPause();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dave_function, menu);
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.actionSearch).setVisible(false);
        menu.findItem(R.id.actionCategory).setVisible(false);
        menu.findItem(R.id.actionSort).setVisible(false);

        if (defaultFilterList == null || defaultFilterList.isEmpty()) {
            menu.findItem(R.id.actionFilter).setVisible(false);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.actionSearch) {
            // addNewDataFlag=true;
            //actionOnSearchMenuButton();
            return true;

        } else if (id == R.id.actionFilter) {
            actionOnFilterMenuButton();
            //childFragment.actionOnFilterMenuButton();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void actionOnSearchButton(){
        strSearchTerm = editTextSearch.getText().toString().trim();
        if(strSearchTerm.length()>0) {
            getListBasedOnSearch();
        }
        else
            Toast.makeText(getActivity(),"Please Enter Search Term",Toast.LENGTH_SHORT).show();
    }

    private void getListBasedOnSearch(){
        isAnyActionApply = true;
        selectedFilterMap.clear();
        mPageNumber = 1;
        getProductsList(mPageNumber, true, false);
    }

    private void callToGetFreshData(){
        mPageNumber =1;
        isAnyActionApply = false;
        strSearchTerm = "";
        filterTitlePosition = 0;
        selectedFilterMap = new HashMap<>();
        includeSearchLayout.setVisibility(View.GONE);
        searchView.setVisibility(View.VISIBLE);
        subHeaderCount.setText("");
    }



    private void getProductsList(int mPageNumber,boolean isNewList, boolean isUpdateDefault){

        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    //customProgressDialog.hideProgressBar(objectsParentLayout);
                    progressBar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                    if(response!= null && response.length()>0 ){
                        Log.i(TAG,"Get  Total Product List count :- " + response.getJSONArray("data").length());
                        if(response.has("data") && !response.isNull("data") && response.getJSONArray("data").length()>0){
                            mRecyclerView.setVisibility(View.VISIBLE);
                            frameLayout.setVisibility(View.VISIBLE);
                            resultNotFound.setVisibility(View.GONE);
                            JSONArray jsonArray = response.getJSONArray("data");
                            showSubHeaderDetails(subHeaderCount.getText().toString(),jsonArray.length());
                            List<JSONObject> objectData = new ArrayList<>();

                            for(int i =0 ;i< jsonArray.length();i++){
                                objectData.add(jsonArray.getJSONObject(i));
                            }
                            if(isNewList) {
                                mAdapter.addNewSeedList(objectData,isUpdateDefault);
                            }
                            else {
                                mAdapter.updateSeedList(objectData,isUpdateDefault);
                            }
                            isPageLoading = true;
                            // mRecyclerView.scrollToPosition(pastVisiblesItems);
                            mAdapter.notifyDataSetChanged();


                        }else {
                            Log.e(TAG,"get adapter count:--------"+mAdapter.getItemCount());
                            if(mAdapter.getItemCount() == 0 ){
                                mRecyclerView.setVisibility(View.GONE);
                                frameLayout.setVisibility(View.GONE);
                                resultNotFound.setVisibility(View.VISIBLE);
                            }else {
                                frameLayout.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                resultNotFound.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error updateRecyclerAdapter:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                Toast.makeText(context,responseMsg,Toast.LENGTH_LONG).show();
               // customProgressDialog.hideProgressBar(objectsParentLayout);
                progressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                if(mAdapter.getItemCount() == 0 ){
                    mRecyclerView.setVisibility(View.GONE);
                    resultNotFound.setVisibility(View.VISIBLE);
                }else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    resultNotFound.setVisibility(View.GONE);
                }
            }
        };
        try {
            progressBar.setVisibility(View.VISIBLE);
            if(isNewList) {
                mAdapter.clearProductList();
                subHeaderCount.setText("0");
            }
            if(modelUtil.getProductModelName()!=null && !modelUtil.getProductModelName().isEmpty()) {
                HashMap<String, Object> queryParam = new HashMap<>();
                queryParam.put("_page_size", 50);
                queryParam.put("_page_number", mPageNumber);

                if(strSearchTerm!=null && !strSearchTerm.isEmpty())
                    queryParam.put("_keyword", strSearchTerm);

                for (String Getkey : selectedFilterMap.keySet()) {
                    queryParam.put(Getkey,selectedFilterMap.get(Getkey));
                }
                Log.i(TAG,"Print Product Query:------------isNewList------ "+ isNewList +" isUpdateDefault:-"+isUpdateDefault +"== "+queryParam);
                DaveModels daveModels = new DaveModels(getActivity(), true);
                daveModels.getObjects(modelUtil.getProductModelName(), queryParam, daveAIListener, false, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error updateRecyclerAdapter:-------------  "+e.getMessage());
        }

    }

    private void showSubHeaderDetails( String preValue, int currentCount){
        subHeaderCount.setText(CRUDUtil.calculatePageSize(preValue,currentCount));

    }


    EditText filterTypeView;
    private void actionOnFilterMenuButton(){
        try {
            View  filterView = getLayoutInflater().inflate(R.layout.filter_layout, menuItemActionLayout, true);
            filterParentView = filterView.findViewById(R.id.filterParentView);
            filterChildView = filterView.findViewById(R.id.filterChildView);
            priceRangeLayout =  filterView.findViewById(R.id.priceRangeLayout);
            rangeSeekbar =  filterView.findViewById(R.id.rangeSeekbar1);
            priceMin =  filterView.findViewById(R.id.priceMin);
            priceMax =  filterView.findViewById(R.id.priceMax);
            filterTypeView = filterView.findViewById(R.id.typeView);
            Button btnFilter= filterView.findViewById(R.id.btnFilter);
            Button btnCancel= filterView.findViewById(R.id.btnCancel);

            strSearchTerm = "";
            Log.i(TAG,"actionOnFilterMenuButton>>>>>>>>>>>>>>"+filterTitleList);
            if(filterTitleList!=null && filterTitleList.size()>0) {
                filterParentAdapter = new ArrayAdapter<>(getActivity(), R.layout.filter_title, filterTitleList);
                filterParentView.setAdapter(filterParentAdapter);
            }

            if(filterAttrTitleMap!=null && !filterAttrTitleMap.isEmpty()) {
                String filterFirstItem = filterAttrTitleMap.get(filterTitleList.get(0));
                clickOnParentFilterView(filterFirstItem);
            }
            filterParentView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    filterTitlePosition = position;
                    String keyName = (String) parent.getItemAtPosition(position);
                    String filterKey = filterAttrTitleMap.get(keyName);
                    clickOnParentFilterView(filterKey);

                }
            });
            filterParentView.setSelection(0);
            filterParentView.setItemChecked(0, true);

            filterChildView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    SparseBooleanArray checked = filterChildView.getCheckedItemPositions();
                    ArrayList<String> selectedItems = new ArrayList<>();
                    for (int i = 0; i < checked.size(); i++) {
                        int pos = checked.keyAt(i);
                        if (checked.valueAt(i)) {
                            selectedItems.add(filterChildAdapter.getItem(pos));
                        }
                    }
                    String filterKeyName = filterAttrTitleMap.get(filterParentView.getAdapter().getItem(filterTitlePosition).toString());
                    if(selectedItems.size() == 0 && selectedFilterMap.containsKey(filterKeyName))
                        selectedFilterMap.remove(filterKeyName);
                    else
                        selectedFilterMap.put(filterKeyName, selectedItems);
                    Log.e(TAG, "***********Print selectedFilterMap HashMap :-  " + selectedFilterMap);

                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuItemActionLayout.removeAllViews();
                }
            });

            btnFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedFilterMap != null && !selectedFilterMap.isEmpty()) {
                        menuItemActionLayout.removeAllViews();
                        //todo make server call
                        Log.i(TAG,"Print selected Filter list:----------"+selectedFilterMap);
                        isAnyActionApply = true;
                        mPageNumber =1;
                        getProductsList(mPageNumber,true,false);
                    }else
                        Toast.makeText(getActivity(),"Please select filter option",Toast.LENGTH_SHORT).show();
                }
            });

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<Error during Filter option:-"+e.getMessage());
        }

    }

    private void clickOnParentFilterView(String filterKey){

        if(checkCustomFilterAttr(productModelInstance.getAttributeType(filterKey))){
            filterChildView.setVisibility(View.GONE);
            priceRangeLayout.setVisibility(View.GONE);
            filterTypeView.setVisibility(View.VISIBLE);

            filterTypeView.setHint(filterKey);
            if(selectedFilterMap.containsKey(filterKey)) {
                filterTypeView.setText(selectedFilterMap.get(filterKey).toString().replace("~",""));
            }

            filterTypeView.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    String typeValue = s.toString();
                    if(typeValue.length()>0)
                        selectedFilterMap.put(filterKey,"~"+typeValue);
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                public void onTextChanged(CharSequence query, int start, int before, int count) {
                    String searchedTerm = query.toString().trim();

                }
            });


        }
        else if(productModelInstance.getAttributeType(filterKey).equalsIgnoreCase("price")||
                productModelInstance.getAttributeType(filterKey).equalsIgnoreCase("discount")){
            Log.i(TAG,"Print Filter attr type>>>>>>>>>>>>>>>>>>>>>>"+productModelInstance.getAttributeType(filterKey));
            filterChildView.setVisibility(View.GONE);
            priceRangeLayout.setVisibility(View.VISIBLE);
            filterTypeView.setVisibility(View.GONE);
            showPriceRangeLayout(filterKey, defaultFilterList.get(filterKey));
        }
        else {
            priceRangeLayout.setVisibility(View.GONE);
            filterTypeView.setVisibility(View.GONE);
            filterChildView.setVisibility(View.VISIBLE);
            filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(filterKey));
            filterChildView.setAdapter(filterChildAdapter);
            filterChildAdapter.notifyDataSetChanged();
            setCheckedMultipleItemAgain(filterKey, filterChildAdapter);
        }

    }

    private boolean checkCustomFilterAttr(String filterAttrType){

        return filterAttrType.equals("name") || filterAttrType.equals("uid") || filterAttrType.equals("email")
                || filterAttrType.equals("phone_number");

    }

    private void showPriceRangeLayout(final String key, ArrayList<String> priceRange) {
        if (priceRange != null && !priceRange.isEmpty()) {
            int minValue = Integer.parseInt(priceRange.get(0));
            int maxValue = Integer.parseInt(priceRange.get(1));
            if(minValue > maxValue){
                rangeSeekbar.setMinValue(maxValue);
                rangeSeekbar.setMaxValue(minValue);
            }else {
                rangeSeekbar.setMinValue(minValue);
                rangeSeekbar.setMaxValue(maxValue);
            }
        }
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                priceMin.setText(String.valueOf(minValue));
                priceMax.setText(String.valueOf(maxValue));
                String joinedValue = String.valueOf(minValue) + "," + String.valueOf(maxValue);
                ArrayList<String> priceList = new ArrayList<>();
                priceList.add(joinedValue);
                if (selectedFilterMap.containsKey(key)) {
                    selectedFilterMap.remove(key);
                }
                selectedFilterMap.put(key, priceList);
            }
        });
    }


    private void setCheckedMultipleItemAgain(String key, ArrayAdapter<String> adapter) {
       Log.e(TAG,"setCheckedMultipleItemAgain****************:- " + selectedFilterMap);
        for (String Getkey : selectedFilterMap.keySet()) {
            if (Getkey.equalsIgnoreCase(key)) {
                if(selectedFilterMap.get(Getkey) instanceof  ArrayList) {
                    ArrayList<String> get_lis = (ArrayList<String>) selectedFilterMap.get(Getkey);
                    for (int j = 0; j < get_lis.size(); j++) {
                        int checked_position = adapter.getPosition(get_lis.get(j));
                        filterChildView.setItemChecked(checked_position, true);
                    }
                }else{
                    int checked_position = adapter.getPosition(selectedFilterMap.get(Getkey).toString());
                    filterChildView.setItemChecked(checked_position, true);
                }
            }
        }
    }


    @Override
    public void onClickCategoryHierarchy(HashMap<String,Object> selectedCategoryMap) {
        selectedFilterMap = selectedCategoryMap;
        Log.e(TAG, "onClickCategoryHierarchy End selectedFilterMap:-"+ selectedFilterMap );
        mPageNumber = 1;
        //todo make server call
        getProductsList(mPageNumber, true, true);
    }

    @Override
    public void onApplyFilter(HashMap<String, ArrayList<String>> selectedFilterDetails) {

    }

    @Override
    public void onCancelFilter() {

    }

    private void addSelectedCategory(String key, String value) {
       /* ArrayList<String> defaultSelectedItems = new ArrayList<>();
        defaultSelectedItems.add(value);*/
        selectedFilterMap.put(key, value);
        Log.i(TAG,"addCheckedItemToMapDefault:-----"+selectedFilterMap);
    }


    @Override
    public void onClickEditProduct(int holderPosition, Bundle bundle) {

        viewHolderPosition = holderPosition;
        Intent intent= new Intent(getActivity(), AddScanProduct.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, AddScanProduct.EDIT_PRODUCT_REQUEST_CODE); //get activity result in fragment
    }

    @Override
    public void onCardToggle(View view,int holderPosition) {
        int viewbottomPosition = view.getBottom();
        Log.i(TAG,"get card bottom Position:---"+viewbottomPosition);
        // scroll to top of the view
       // mRecyclerView.scrollTo(0, viewbottomPosition);
        //mRecyclerView.scrollToPosition(holderPosition);
        mRecyclerView.getLayoutManager().smoothScrollToPosition(mRecyclerView, null, holderPosition);


    }

    // Call Back method  to get the Message form other Activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here
       // Log.e(TAG," AddScanProduct.EDIT_PRODUCT CODE:--------   "+ AddScanProduct.EDIT_PRODUCT);
        Log.i(TAG,"onActivityResult AddScanProduct.EDIT_PRODUCT CODE:--------   "+ requestCode+" && "+resultCode+" && Activity.RESULT_OK "+Activity.RESULT_OK);
        if(requestCode == AddScanProduct.EDIT_PRODUCT_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK){
                String getActivityResult = data.getSerializableExtra("_activity_result").toString();

                try {
                    JSONObject jsonObject = new JSONObject(getActivityResult);
                    Log.i(TAG," AddScanProduct.getActivityResult :--------   "+viewHolderPosition+" && "+  getActivityResult);
                    mAdapter.updateItem(jsonObject,viewHolderPosition);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String result = data.getStringExtra("_activity_result");

                Log.i(TAG," AddScanProduct result :--------   "+ result);
            }

        }

    }

    private void hideSoftKeyword(View view){
        InputMethodManager imm = ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE));
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}

