package com.example.admin.quickdry.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.admin.daveai.others.DaveAIPerspective;


public abstract class BaseActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();
    private static int sessionDepth = 0;

   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }*/

 /*   @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

    }*/

    @Override
    protected void onStart() {
        super.onStart();
        sessionDepth++;
        if(sessionDepth == 1){
            //app came to foreground;
            Log.e(TAG,"=======================================app came to foreground===============================================");
            DaveAIPerspective.getInstance().savePerspectivePreferenceValue(BaseActivity.this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (sessionDepth > 0)
            sessionDepth--;
        if (sessionDepth == 0) {
            // app went to background
            Log.e(TAG,"=======================================app went to background===============================================");

        }
    }

}
