package com.example.admin.quickdry.fragments;


import android.app.DialogFragment;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.admin.quickdry.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class ImageFullView extends DialogFragment {

    private static final String TAG = "ImageFullView";
    private static final String IMAGE_URL = "image_url";

    private String imageURL= "";


    public static ImageFullView newInstance(String imageUrl) {

        ImageFullView imageFullView = new ImageFullView();

        Bundle args = new Bundle();
        args.putString(IMAGE_URL, imageUrl);
        imageFullView.setArguments(args);

        return imageFullView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
        setStyle(STYLE_NO_FRAME,android.R.style.Theme_Holo_Light);

        Bundle bundle = getArguments();
        //if (getArguments() != null) {
        if (getArguments() != null) {
            imageURL = bundle.getString(IMAGE_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_full_view, container, false);

        // Do all the stuff to initialize your custom view

        ImageView imageView = view.findViewById(R.id.imageView);
        Button buttonCancel = view.findViewById(R.id.buttonCancel);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

            }
        });


        try {
            Log.i(TAG,"image loading from = "+imageURL);
            if(imageURL.startsWith("http")) {

                Picasso.with(getActivity())
                        .load(imageURL)
                        .placeholder(com.example.admin.daveai.R.drawable.placeholder)
                        .error(com.example.admin.daveai.R.drawable.no_image)
                        //.fit()

                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Log.e(TAG, "PICASSO ERROR!!!");
                            }
                        });
            }else{
                Glide.with(getActivity())
                        .load(imageURL)
                        .asBitmap()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.no_image)

                        // .skipMemoryCache(false)
                        // .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                       /* .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                //progressBar.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                // progressBar.setVisibility(View.GONE);

                                return false;
                            }
                        })*/
                        .into(imageView);
            }



        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During loading Image in ImageFUllView :- "+e.getLocalizedMessage());
        }



        return view;
    }

}
