package com.example.admin.quickdry.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.storage.SharedPreference;
import com.example.admin.quickdry.utils.AppConstants;



public class ConnectToDisplay extends Dialog  implements AppConstants {


    private final String TAG = getClass().getSimpleName();
    private static final int SERVER_PORT = 8000;
    private Context mContext;

    private LinearLayout commParentLayout, connectTCPLayout;
    private EditText serverPortView,ipAddressView;

    SharedPreference sharedPreference;

    private ConnectToDisplayCallback  connectToDisplayCallback;
    public interface ConnectToDisplayCallback {
        void OnConnectTCPSocket();
    }


    public ConnectToDisplay(Context context,ConnectToDisplayCallback connectToDisplayCallback){
        super(context);
        this.mContext = context;
        this.connectToDisplayCallback = connectToDisplayCallback;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        setContentView(R.layout.dialog_connect_to_display);

        commParentLayout = findViewById(R.id.commParentLayout);
        Button btnCancel = findViewById(R.id.btnCancel);
        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        Button btnIpAddress = findViewById(R.id.btnEditIpAddress);
        btnIpAddress.setVisibility(View.GONE);

        connectTCPLayout = findViewById(R.id.connectTCPLayout);
        connectTCPLayout.setVisibility(View.GONE);
        serverPortView = findViewById(R.id.serverPort);
        ipAddressView = findViewById(R.id.ip_address);
        Button btnTCPConnect = findViewById(R.id.btnTCPConnect);


        //check communication type if intranet show edit ipAddress  button show selected one
        sharedPreference = new SharedPreference(mContext);
        String  commType =  sharedPreference.readString(DISPLAY_COMMUNICATION_TYPE);
        Log.e(TAG,"Print display communication Type****************  " +commType);

        if(commType!=null && !commType.isEmpty()) {
            if (commType.equalsIgnoreCase("intranet")) {
                radioGroup.check(R.id.intranet);
                btnIpAddress.setVisibility(View.VISIBLE);
            }
            else if (commType.equalsIgnoreCase("bluetooth"))
                radioGroup.check(R.id.bluetooth);
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                dismiss();

            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                String commType =((RadioButton)findViewById(checkedId)).getText().toString();
                sharedPreference.writeString(DISPLAY_COMMUNICATION_TYPE,commType.toLowerCase());

                Log.e(TAG,"Print selected Communication Type***************** "+commType.toLowerCase());
                if(checkedId == R.id.bluetooth) {
                    btnIpAddress.setVisibility(View.GONE);

                } else if(checkedId == R.id.intranet) {
                    btnIpAddress.setVisibility(View.VISIBLE);
                }

                Toast.makeText(mContext, "choice: "+ commType , Toast.LENGTH_SHORT).show();
            }

        });


        btnIpAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* new IntranetSettings(mContext).show();
                dismiss();*/
                showIntranetSetting();
            }
        });

        btnTCPConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(TextUtils.isEmpty(ipAddressView.getText().toString())){
                    Toast.makeText(mContext,"Please Enter Ip Address.",Toast.LENGTH_SHORT).show();

                }else if(TextUtils.isEmpty(serverPortView.getText().toString())){
                    Toast.makeText(mContext,"Please Enter Server Port.",Toast.LENGTH_SHORT).show();

                }else{
                    Log.i(TAG,"Print ip address and port on connect with socket**********  " +ipAddressView.getText() +" port:- "+ serverPortView.getText());
                    sharedPreference.writeString(INTRANET_IP_ADDRESS,ipAddressView.getText().toString());
                    sharedPreference.writeInteger(TCP_SERVER_PORT,Integer.parseInt(serverPortView.getText().toString()));

                    if(connectToDisplayCallback!=null)
                        connectToDisplayCallback.OnConnectTCPSocket();
                    dismiss();

                }
            }
        });


    }

    private void showIntranetSetting(){

        commParentLayout.setVisibility(View.GONE);
        connectTCPLayout.setVisibility(View.VISIBLE);
        //if ip address not null set in edit text
        String strIpAddress = sharedPreference.readString(INTRANET_IP_ADDRESS);
        if(strIpAddress!=null)
            ipAddressView.setText(strIpAddress);

        int serverPort = sharedPreference.readInteger(TCP_SERVER_PORT);
        if(serverPort!= 0)
            serverPortView.setText(Integer.toString(serverPort));
        else
            serverPortView.setText(Integer.toString(SERVER_PORT));

    }



}
