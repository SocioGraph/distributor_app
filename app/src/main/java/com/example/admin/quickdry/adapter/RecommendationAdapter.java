package com.example.admin.quickdry.adapter;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.daveai.activity.Visualization;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.ImageFullScreen;
import com.example.admin.quickdry.utils.CRUDUtil;
import com.example.admin.quickdry.utils.RecycleViewHolder;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecommendationAdapter extends RecyclerView.Adapter<RecycleViewHolder>
        implements RecyclerView.OnClickListener {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private Model productModelInstance;
    private String productIdAttrName;
    private List<JSONObject> seedSimilarOrRecommList;
    private List<JSONObject> defaultItemsList;
    private int expandedPosition = -1;
    private DaveAIPerspective daveAIPerspective;



    private RecommendationAdapterListener recommendationAdapterListener;
    public interface RecommendationAdapterListener {
        void onClickSimilarProduct(String productId,HashMap<String, String> similarFilterDetails);


    }

    public RecommendationAdapter(RecommendationAdapterListener recommendationAdapterListener,Context context,
                                 List<JSONObject> seedSimilarOrRecommList ) {

        this.recommendationAdapterListener = recommendationAdapterListener;
        this.context = context;
        this.seedSimilarOrRecommList = seedSimilarOrRecommList;
        defaultItemsList = new ArrayList<>();
        daveAIPerspective = DaveAIPerspective.getInstance();
        String productModelName = new ModelUtil(context).getProductModelName();
        productModelInstance = Model.getModelInstance(context, productModelName);
        productIdAttrName =new ModelUtil(context).getProductIdAttrName();
    }

   /* public RecommendationAdapter(Context context, String customerId, List<JSONObject> seedSimilarOrRecommList ,Set<String> previousProductIds ) {
        this.context = context;
        this.customerId = customerId;
        this.seedSimilarOrRecommList = seedSimilarOrRecommList;
        this.previousProductIds = previousProductIds;
        productModelName= new ModelUtil(context).getProductModelName();
        productModel = Model.getModelInstance(context, productModelName);
        productIdAttrName =new ModelUtil(context).getProductIdAttrName();
    }

*/


    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_recomm_cardview,viewGroup, false);
        RecycleViewHolder holder = new RecycleViewHolder(view);
        holder.itemView.setOnClickListener(RecommendationAdapter.this);
        holder.itemView.setTag(holder);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder customViewHolder, int position) {

        if (customViewHolder.getAdapterPosition() == expandedPosition) {
            customViewHolder.collapseViewLayout.setVisibility(View.GONE);
            customViewHolder.expandViewLayout.setVisibility(View.VISIBLE);
        } else {
            customViewHolder.collapseViewLayout.setVisibility(View.VISIBLE);
            customViewHolder.expandViewLayout.setVisibility(View.GONE);
        }
        JSONObject seedItem = seedSimilarOrRecommList.get(customViewHolder.getAdapterPosition());
        try {
            //bind quick and expand view
           // if (seedItem.has("product_attributes")) { }
            JSONObject productDetails = seedItem.getJSONObject("product_attributes");
            String shareMsg = customViewHolder.bindQuickView(context,productDetails);
            customViewHolder.bindExpandView(context, productModelInstance,productDetails);

            if(daveAIPerspective.getProduct_card_image_view()!= null && productDetails.has(daveAIPerspective.getProduct_card_image_view())
                    && !productDetails.isNull(daveAIPerspective.getProduct_card_image_view())) {
                customViewHolder.shareImage.setVisibility(View.VISIBLE);
                customViewHolder.zoomImage.setVisibility(View.VISIBLE);
            }else {
                customViewHolder.shareImage.setVisibility(View.GONE);
                customViewHolder.zoomImage.setVisibility(View.GONE);
            }

            //get current stage details
            if(seedItem.has("current_stage")) {
                JSONObject stageDetails = seedItem.getJSONObject("current_stage");
                Log.e(TAG,"onBindViewHolder Current Stage Details:----"+customViewHolder.getAdapterPosition()+"  "+stageDetails);

                if (stageDetails.has("interaction_stage") && !stageDetails.isNull("interaction_stage")) {
                    customViewHolder.stageName.setVisibility(View.VISIBLE);
                    customViewHolder.stageName.setText(stageDetails.getString("interaction_stage"));
                }else{
                    customViewHolder.stageName.setVisibility(View.GONE);
                }
            }
            customViewHolder.shareImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context,"Sharing Image with.. ",Toast.LENGTH_SHORT).show();
                    CRUDUtil crudUtil = new CRUDUtil(context);

                    try {
                        JSONObject productObject = seedSimilarOrRecommList.get(customViewHolder.getAdapterPosition()).getJSONObject("product_attributes");
                        String product_id = productObject.getString(productIdAttrName);
                        Uri bitmap = crudUtil.getLocalBitmapUri(context,customViewHolder.conceptImageView);

                        //Log.e(TAG,"Print share image bitmap :-----------"+bitmap);
                        if(bitmap!=null)
                            crudUtil.shareImage(context,product_id,bitmap,shareMsg,true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            Log.e(TAG, "Error Iterating InteractionsCardDetails +++++++++++" + e.getMessage());
        }

        customViewHolder.getSimilar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> similarFilterDetails = new HashMap<>();
                try {
                    JSONObject productObject = seedSimilarOrRecommList.get(customViewHolder.getAdapterPosition()).getJSONObject("product_attributes");
                    if(daveAIPerspective.getProduct_similar_attributes()!=null){
                        for(int i=0; i<daveAIPerspective.getProduct_similar_attributes().size(); i++){
                            String key = daveAIPerspective.getProduct_similar_attributes().get(i).toString();
                            if(productObject.has(key)){
                                similarFilterDetails.put(key,productObject.get(key).toString());
                            }
                        }
                    }
                    String product_id = productObject.getString(productIdAttrName);
                    if(recommendationAdapterListener!=null) {
                        recommendationAdapterListener.onClickSimilarProduct(product_id,similarFilterDetails);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

      /*  customViewHolder.shareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Sharing Image with.. ",Toast.LENGTH_SHORT).show();
                CRUDUtil crudUtil = new CRUDUtil(context);
                Uri bitmap = crudUtil.getLocalBitmapUri(context,customViewHolder.conceptImageView);
                Log.d(TAG,"<<<<<<<<<<<<<<<<<<<Print share image bitmap>>>>>>>>>>>>>>>>>"+bitmap);
                if(bitmap!=null)
                    crudUtil.shareImage(context,bitmap);
            }
        });*/

        customViewHolder.zoomImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject productDetails = seedSimilarOrRecommList.get(customViewHolder.getAdapterPosition()).getJSONObject("product_attributes");
                    if(daveAIPerspective.getProduct_card_image_view()!= null && productDetails.has(daveAIPerspective.getProduct_card_image_view())
                            && !productDetails.isNull(daveAIPerspective.getProduct_card_image_view())) {

                        Intent intent = new Intent(context, ImageFullScreen.class);
                        intent.putExtra(ImageFullScreen.IMAGE_URL,productDetails.getString(daveAIPerspective.getProduct_card_image_view()));
                        context.startActivity(intent);

                      /*  Bundle b=new Bundle();
                        b.putString(Visualization.VISUALIZATION_VIEW_TYPE,Visualization.VisualisationType.IMAGE.toString());
                        b.putString(Visualization.IMAGE_URL,productDetails.getString(daveAIPerspective.getProduct_card_image_view()));

                        Intent intent = new Intent(context, Visualization.class);
                        intent.putExtras(b);
                        context.startActivity(intent);*/
                    }else{
                        Toast.makeText(context,"Image Not Available..",Toast.LENGTH_SHORT).show();
                        Log.e(TAG,"error during click on Zoom Image position  :-" +customViewHolder.getAdapterPosition() +" imageAttrName:- "+daveAIPerspective.getProduct_card_image_view() +" isDetaildHasImageUrl:-  "+productDetails.has(daveAIPerspective.getProduct_card_image_view())
                                +"\n productDetails:-"+productDetails);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != seedSimilarOrRecommList ? seedSimilarOrRecommList.size() : 0);
    }

    public void clearPreviousList() {
        seedSimilarOrRecommList.clear();
        notifyDataSetChanged();

    }

    public void addNewSeedList(List<JSONObject> newItemList,boolean isUpdateDefault) {
        if(isUpdateDefault) {
            defaultItemsList.clear();
            defaultItemsList.addAll(newItemList);
        }

        if(seedSimilarOrRecommList.size()>0) {
            seedSimilarOrRecommList.clear();
            notifyDataSetChanged();
        }
        this.seedSimilarOrRecommList.addAll(newItemList);
        notifyItemRangeInserted(0,seedSimilarOrRecommList.size()-1);

      /*  seedSimilarOrRecommList.clear();
        seedSimilarOrRecommList.addAll(newItemList);
        notifyItemInserted(seedSimilarOrRecommList.size());
        notifyDataSetChanged();*/
        Log.e(TAG," After addNewItem size of adapter****************:- " + seedSimilarOrRecommList.size());
    }


    public void updateSeedList(List<JSONObject> newItemList,boolean isUpdateDefault) {

        seedSimilarOrRecommList.addAll(newItemList);
        notifyItemInserted(seedSimilarOrRecommList.size());
        notifyDataSetChanged();

        if(isUpdateDefault)
            defaultItemsList.addAll(newItemList);
        /* int curSize = getItemCount();
        seedSimilarOrRecommList.addAll(newItemList);
        notifyItemRangeInserted(curSize, newItemList.size());*/

        Log.e(TAG," After updateSeedList seedCustomerList of adapter****************:- " + seedSimilarOrRecommList.size());

    }
    public void resetPreviousList(){
        if(seedSimilarOrRecommList.size()>0)
            seedSimilarOrRecommList.clear();
        seedSimilarOrRecommList = new ArrayList<>(defaultItemsList);
        //notifyItemInserted(seedSimilarOrRecommList.size());
        notifyItemRangeChanged(0,seedSimilarOrRecommList.size()-1);
        //Log.e(TAG," resetPreviousCustomerList() of adapter****************:- " +seedCustomerList.size()+"  "+ defaultCustomerList.size());

    }

    public void remove(int position) {
        seedSimilarOrRecommList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, seedSimilarOrRecommList.size());
    }

    public String getRemoveItemId(int position) {
        if (seedSimilarOrRecommList.size() > 0)
            try {
                return seedSimilarOrRecommList.get(position).getJSONObject("product_attributes").getString(productIdAttrName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return null;
    }

    public void restoreItem(JSONObject deletedItem, int position) {
        seedSimilarOrRecommList.add(position, deletedItem);
        // notify item added by position
        notifyItemInserted(position);
    }

    public JSONObject getJsonObject(int position) {

        Log.e(TAG,"Print position :---------------"+ position +" <="+ seedSimilarOrRecommList.size());
        if(position <= seedSimilarOrRecommList.size())
            return seedSimilarOrRecommList.get(position);
        else return new JSONObject();
    }

    public void refreshEvents(List<JSONObject> events) {
        this.seedSimilarOrRecommList.clear();
        this.seedSimilarOrRecommList.addAll(events);
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        RecycleViewHolder holder = (RecycleViewHolder) view.getTag();
        int cardPosition = holder.getAdapterPosition();
        if (holder.expandViewLayout.getVisibility() == View.VISIBLE) {
            expandedPosition = -1;
        } else {
            expandedPosition = cardPosition;
        }
        notifyDataSetChanged();

    }

}







