package com.example.admin.quickdry.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.RecommendationHelper;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.fragments.ScannerFragment;
import com.example.admin.daveai.model.CardSwipeDetails;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.adapter.ScanAdapter;
import com.example.admin.quickdry.dialogs.NextStageEventDialog;
import com.example.admin.quickdry.fragments.DaveRecommendations;
import com.example.admin.quickdry.utils.CardSwipeListener;
import com.example.admin.quickdry.utils.PostInteractionUtil;
import com.example.admin.quickdry.utils.RecycleViewSwipe;
import com.example.admin.quickdry.utils.RecycleViewSwipeHelper;
import com.google.zxing.Result;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.example.admin.daveai.others.DaveAIStatic.interactionPosition;
import static com.example.admin.daveai.others.DaveAIStatic.interactionStageTitle;
import static com.example.admin.quickdry.activity.CustomerProfile.tabHeaderList;


public class AddScanProduct extends BaseActivity implements CardSwipeListener,
        DynamicForm.OnCreateDynamicFormListener,ScannerFragment.OnScanCompleteListener{

    private static final String TAG = "AddScanProduct";

    public static final String ACTION_ADD_PRODUCT  = "_add_product_";
    public static final String ACTION_SCAN_PRODUCT = "_scan_product_";
    public static final String ACTION_EDIT_PRODUCT = "_edit_product_";
    public static final String ACTION_SHOW_PRODUCT = "_show_product_";

    public static final int ADD_PRODUCT_REQUEST_CODE  = 100;
    public static final int EDIT_PRODUCT_REQUEST_CODE = 101;

    FrameLayout frameContainer;
    RecyclerView detailRecyclerView;
    Context context = this;
    String strProductId = "";
    String strcustomerId = "";
    TextView showTitle;

    String actionName = "";
    private JSONObject getCurrentStageDetail;
    private DaveAIPerspective daveAIPerspective;
    private String currentObjectDetails= "";
    String productModelName;
    String productIdAttrName;
    boolean isEditProduct = false;
    ScanAdapter scanAdapter;
    private int viewHolderPosition = -1;
    private JSONObject showProductData = new JSONObject();
    private DatabaseManager databaseManager;
    ModelUtil modelUtil;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_scan_product);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        frameContainer =findViewById(R.id.frameContainer);
        detailRecyclerView=findViewById(R.id.detailRecyclerView);
        showTitle = findViewById(R.id.showTitle);


        daveAIPerspective = DaveAIPerspective.getInstance();
        modelUtil = new ModelUtil(AddScanProduct.this);
        productIdAttrName=modelUtil.getProductIdAttrName();
        productModelName=modelUtil.getProductModelName();
        databaseManager = DatabaseManager.getInstance(this);

        Bundle bundle = getIntent().getExtras();
        readBundle(bundle);

        Fragment fragment=null;
        if(actionName.equals(ACTION_ADD_PRODUCT) ){
            //showTitle.setText("Add Product");
            if(daveAIPerspective.getAdd_product_title()!=null)
                showTitle.setText(daveAIPerspective.getAdd_product_title());
            if(daveAIPerspective.getProduct_add_edit_List()!=null && !daveAIPerspective.getProduct_add_edit_List().isEmpty()) {
                fragment = DynamicForm.newInstance("Add Product", "Submit", "POST",
                        productModelName, daveAIPerspective.getProduct_add_edit_List(), null,
                        null);
            }
            else {
                Toast.makeText(context,"Please set product_add_edit_List in preference ",Toast.LENGTH_SHORT).show();
            }
            setFragmentInContainer(fragment,"AddProduct");

        }else if(actionName.equals(ACTION_SCAN_PRODUCT)){
            //showTitle.setText("Scan Product");
            if(daveAIPerspective.getScan_btn_name()!=null)
                showTitle.setText(daveAIPerspective.getScan_btn_name());

            Log.e(TAG,"ScanCode Product Attr NAme :------------------------"+ daveAIPerspective.getScan_code_attribute_name());
            // fragment =  ScannerFragment.newInstance(strCustomerId);
            fragment =  new ScannerFragment();
            setFragmentInContainer(fragment,"ScanProduct");
        }
        else if(actionName.equals(ACTION_EDIT_PRODUCT) ){
            addEditProductFragment();
        }
        else if(actionName.equals(ACTION_SHOW_PRODUCT) ){
            showTitle.setText("Product Details");
            getproductDetails(strProductId);
        }
    }

    @Override
    public void onResume(){
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        if(isEditProduct){
            getSupportFragmentManager().beginTransaction().
                    remove(getSupportFragmentManager().findFragmentByTag("EditProduct")).commit();
            detailRecyclerView.setVisibility(View.VISIBLE);
            if(frameContainer.getVisibility()== View.VISIBLE)
                frameContainer.setVisibility(View.GONE);
            isEditProduct = false;

        }else {
            //super.onBackPressed();
            Log.e(TAG,"Testing onBackPressed isTaskRoot :----------------"+ isTaskRoot());
            if(isTaskRoot()){
                Intent intent = new Intent(this,HomePage.class);
                startActivity(intent);
                finish();
            }else
                super.onBackPressed();
        }

    }

    private  void readBundle(Bundle bundle){
        try {
            if (bundle != null) {
                actionName = bundle.getString("action_name");
                if(bundle.containsKey("customer_id"))
                    strcustomerId = bundle.getString("customer_id");
                if(bundle.containsKey("_product_id"))
                    strProductId = bundle.getString("_product_id");

                if(bundle.containsKey("_object_details"))
                    currentObjectDetails = bundle.getString("_object_details");

                if(bundle.containsKey("showProductData") && bundle.get("showProductData")!= null) {
                    showProductData = new JSONObject(bundle.get("_object_details").toString());
                    strcustomerId = showProductData.getString("_customer_id");
                }
            }

            Log.e(TAG,"readBundle Print  strcustomerId :---- " +strcustomerId + " strProductId: "+ strProductId +" && "+ currentObjectDetails);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void addEditProductFragment(){
        showTitle.setText("Edit");
        Log.e(TAG,"actionName== EDIT_PRODUCT :---- "+daveAIPerspective.getProduct_add_edit_List() +" "+ strProductId);

        if(daveAIPerspective.getProduct_add_edit_List()!=null && !daveAIPerspective.getProduct_add_edit_List().isEmpty()) {
            Fragment fragment = DynamicForm.newInstance("Edit Product","Update","UPDATE",
                    productModelName,daveAIPerspective.getProduct_add_edit_List(),strProductId,
                    currentObjectDetails);
            setFragmentInContainer(fragment,"EditProduct");
        }else{
            Toast.makeText(context,"Please set product_add_edit_List in preference ",Toast.LENGTH_SHORT).show();
        }


    }

    private void setFragmentInContainer(Fragment fragment, String tagName){
        detailRecyclerView.setVisibility(View.GONE);
        if(frameContainer.getVisibility()== View.GONE)
            frameContainer.setVisibility(View.VISIBLE);
        if (fragment!=null) {
            FragmentTransaction ft =getSupportFragmentManager().beginTransaction();
           // ft.add(R.id.frameContainer, fragment,tagName);
            ft.replace(R.id.frameContainer, fragment,tagName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
        }
    }

    @Override
    public void onScanCompleted(Result scanResult) {
        Log.e(TAG, "<<<<onScanCompleted scanResult>>>>>>>>>>>>>>" + scanResult);
        try{
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("ScanProduct")).commit();
            if (scanResult.getText() != null) {
                getScannedProductDetails(context, scanResult.getText());
            }else {
                Toast.makeText(context,"Product Not Found",Toast.LENGTH_LONG).show();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOrUpdateView(String objectDetails) {
        if(frameContainer.getVisibility()== View.VISIBLE)
            frameContainer.setVisibility(View.GONE);

        if(objectDetails!=null && !objectDetails.isEmpty()) {
            if (actionName.equals(ACTION_ADD_PRODUCT))
            {
                Toast.makeText(AddScanProduct.this, "Product Added Successfully", Toast.LENGTH_SHORT).show();
                showProductDetails(objectDetails);
            } else if (actionName.equals(ACTION_EDIT_PRODUCT))
            {
                Toast.makeText(AddScanProduct.this, "Product Updated Successfully", Toast.LENGTH_SHORT).show();
                if (isEditProduct) {
                    detailRecyclerView.setVisibility(View.VISIBLE);
                    try {
                        JSONObject jsonObject = new JSONObject(objectDetails);
                        Log.e(TAG," AddScanProduct.getActivityResult :--------   "+viewHolderPosition);
                        scanAdapter.updateItem(jsonObject,viewHolderPosition);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("_activity_result", objectDetails);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish(); //finishing activity
                }


            }
        }

         /*   Intent returnIntent = new Intent();
            returnIntent.putExtra("_activity_result", objectDetails);
            setResult(Activity.RESULT_OK, returnIntent);
            finish(); //finishing activity*/
     }

    @Override
    public void onFailureCreateOrUpdate(String errorMsg) {

       /* Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish(); //finishing activity*/
    }

    public void getScannedProductDetails(final Context context, final String scannedCode) {
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {

                if (response != null) {
                    try {
                        JSONObject getJsonResponse = new JSONObject(response);
                        if(getJsonResponse.has("data") && getJsonResponse.getJSONArray("data").length()> 0) {
                            JSONArray data_array = getJsonResponse.getJSONArray("data");
                            JSONObject productDetails = data_array.getJSONObject(0);
                            ModelUtil modelUtil = new ModelUtil(context);
                            DatabaseManager databaseManager = DatabaseManager.getInstance(context);

                            // insert data in object table
                            databaseManager.insertOrUpdateObjectData(
                                    modelUtil.getProductModelName(),
                                    new ObjectsTableRowModel(productDetails.getString(modelUtil.getProductIdAttrName()),
                                            productDetails.toString(), System.currentTimeMillis())
                            );
                            showProductDetails(productDetails.toString());

                        }else {
                            Toast.makeText(AddScanProduct.this, "Product is not available in stock", Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (Exception e) {
                        Log.e("Details", "==" + e.getMessage());
                    }
                } else {
                    Toast.makeText(AddScanProduct.this, "Product is not available in stock", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                Toast.makeText(AddScanProduct.this, responseMsg, Toast.LENGTH_LONG).show();
            }
        };
        try {
            Model model = new Model(context);
            if(productModelName!=null){
                model.getObjects(productModelName, queryParamMap(scannedCode), true, postTaskListener);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private HashMap<String,Object> queryParamMap(String queryValue){
        HashMap<String,Object> queryParam = new HashMap<>();

        if(actionName.equals(ACTION_SCAN_PRODUCT))
            queryParam.put(daveAIPerspective.getScan_code_attribute_name(), queryValue);
        else if(actionName.equals(ACTION_SHOW_PRODUCT))
            queryParam.put(productIdAttrName, queryValue);

        return queryParam;
    }

    private void getproductDetails(String queryValue){

        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    //Log.e(TAG,"getproductDetails  :- "+response.getJSONArray("data").length());
                    if( response!= null && response.has("data") && response.getJSONArray("data").length()>0){
                        JSONObject productDetails = response.getJSONArray("data").getJSONObject(0);

                        // insert data in object table
                        databaseManager.insertObjectDataIfNotThere(
                               productModelName,
                                new ObjectsTableRowModel(productDetails.getString(productIdAttrName),
                                        productDetails.toString(), System.currentTimeMillis())
                        );
                        showProductDetails(productDetails.toString());


                    }else {
                       Toast.makeText(AddScanProduct.this,"Product not Found",Toast.LENGTH_LONG).show();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error updateRecyclerAdapter:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                Toast.makeText(context,responseMsg,Toast.LENGTH_LONG).show();

            }
        };
        try {
            if(productModelName!=null && !productModelName.isEmpty()) {
                DaveModels daveModels = new DaveModels(this, true);
                daveModels.getObjects(productModelName, queryParamMap(queryValue), daveAIListener, false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During get Product details :-------------  "+e.getMessage());
        }
    }

    private void showProductDetails(String objectDetails){
        if(frameContainer.getVisibility()== View.VISIBLE)
            frameContainer.setVisibility(View.GONE);
        detailRecyclerView.setVisibility(View.VISIBLE);
        Log.e(TAG,"Show Product details:----------------------"+objectDetails);
        try {
            JSONObject productDetail = new JSONObject(objectDetails);
            List<JSONObject> objectsList = new ArrayList<>();
            objectsList.add(productDetail);
            scanAdapter = new ScanAdapter(AddScanProduct.this, objectsList, new ScanAdapter.ScanAdapterListener() {
                @Override
                public void onClickEditProduct(int holderPosition, Bundle bundle) {

                    viewHolderPosition = holderPosition;
                    readBundle(bundle);
                    if (bundle != null) {
                        isEditProduct= true;
                        addEditProductFragment();
                    }
                }
            });
            detailRecyclerView.setAdapter(scanAdapter);
            if(productDetail.has(productIdAttrName) && strcustomerId != null && !strcustomerId.isEmpty()) {
                strProductId = productDetail.getString(productIdAttrName);
                //getCurrentStageDetails(productDetail.getString(productIdAttrName)); //online
                getCurrentStageDetails(); //offline
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "<<<< Show Product Details Error:>>>>>>>>>>>>>>" + e.getLocalizedMessage());
        }

    }


    public void getCurrentStageDetails(final String scanProductId) {
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                if (response != null) {
                    try {
                        getCurrentStageDetail = new JSONObject(response);
                        Log.e(TAG,"Print customer id :-----------"+strcustomerId +" CurrentStageDetails:-  "+ currentObjectDetails);
                        /*RecycleViewSwipeHelper callback = new RecycleViewSwipeHelper(AddScanProduct.this,
                                context, strcustomerId, scanProductId,getCurrentStageDetail);
                        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                        touchHelper.attachToRecyclerView(detailRecyclerView);
*/
                        //todo testing new swipe feature
                        RecycleViewSwipe recycleViewSwipeHelper = new RecycleViewSwipe(AddScanProduct.this,context);
                        ItemTouchHelper touchHelper = new ItemTouchHelper(recycleViewSwipeHelper);
                        touchHelper.attachToRecyclerView(detailRecyclerView);

                    } catch (Exception e) {
                        Log.e(TAG, "==" + e.getMessage());
                    }
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                Toast.makeText(AddScanProduct.this,responseMsg,Toast.LENGTH_LONG).show();

            }
        };
        new APICallAsyncTask(postTaskListener, context, "GET", false).
                execute(APIRoutes.nextInteractionAPI(strcustomerId, scanProductId));
    }


    public void getCurrentStageDetails() {
        try {
            RecommendationHelper recommendationHelper = new RecommendationHelper(context,strcustomerId,strProductId);
            getCurrentStageDetail =  recommendationHelper.getDefauleCurrentStageDetails();
            Log.e(TAG,"Print Current Stage Details:---"+getCurrentStageDetail);

            if(getCurrentStageDetail.length()>0){
                /*RecycleViewSwipeHelper callback = new RecycleViewSwipeHelper(AddScanProduct.this,
                        context, strcustomerId, strProductId,getCurrentStageDetail);
                ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                touchHelper.attachToRecyclerView(detailRecyclerView);*/

                //todo testing new swipe feature
                RecycleViewSwipe recycleViewSwipeHelper = new RecycleViewSwipe(AddScanProduct.this,context);
                ItemTouchHelper touchHelper = new ItemTouchHelper(recycleViewSwipeHelper);
                touchHelper.attachToRecyclerView(detailRecyclerView);

            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During get Current Stage details :-------------  "+e.getMessage());
        }
    }

    @Override
    public CardSwipeDetails setDirectionFlags(int cardViewPosition) {

        String interactionStage= "";
        String previousStage= "";
        String nextStage= "";
        int swipeFlags = 0;

        CardSwipeDetails cardSwipeDetails = new CardSwipeDetails();
        try {
            cardSwipeDetails.setCardItemDetails(getCurrentStageDetail);
            if (getCurrentStageDetail.has("interaction_stage")) {
                interactionStage = getCurrentStageDetail.getString("interaction_stage");
                cardSwipeDetails.setInteractionStage(interactionStage);
            }
            if (getCurrentStageDetail.has("previous_stage")) {
                previousStage = getCurrentStageDetail.getString("previous_stage");
                cardSwipeDetails.setPreviousStage(previousStage);
            }
            if (getCurrentStageDetail.has("next_stage")) {
                nextStage = getCurrentStageDetail.getString("next_stage");
                cardSwipeDetails.setNextStage(nextStage);
            }


            if (previousStage.equals("__NULL__")) {
                swipeFlags = ItemTouchHelper.RIGHT;
            } else if (nextStage.equals("__NULL__")) {
                swipeFlags = ItemTouchHelper.LEFT;
            } else {
                swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
            }
            cardSwipeDetails.setSwipeDirection(swipeFlags);

        } catch (JSONException e) {
            Log.e(TAG, "Set Scanned Product Swipe Direction Error>>>>>>>>>>" + e.getLocalizedMessage());
        }



        return cardSwipeDetails;

    }

    @Override
    public void onCardSwipe(RecyclerView.ViewHolder viewHolder, int direction) {
        try{
            PostInteractionUtil postInteractionUtil = new PostInteractionUtil(this,getCurrentStageDetail);
            String swipeStage = postInteractionUtil.getCardSwipeStageName(direction);

            boolean isNextStageAction = postInteractionUtil.checkNextStageEvent(direction);

            Log.e(TAG,"Print str productid during swipe :-----------" + strProductId);
            if(isNextStageAction){
                NextStageEventDialog nextStageEventDialog = new NextStageEventDialog(context, new NextStageEventDialog.NextStageEventListener() {
                    @Override
                    public void onNextStageEventCancel() {
                        //scanAdapter.restoreItem(cardItemDetails, itemPosition);
                    }

                    @Override
                    public void onNextStageEventSubmit(JSONObject formDetails) {
                        postInteractionUtil.postInteraction(strcustomerId,strProductId, swipeStage,formDetails);
                        showSnackBar(swipeStage);
                        JSONArray jsonArray = postInteractionUtil.getNextStageTaggedProducts();
                        if(jsonArray.length()>0 ){
                            addTaggedProducts(0,jsonArray,swipeStage,postInteractionUtil);
                        }else {
                            closeActivity();
                        }
                    }
                },"Main Product",strProductId,postInteractionUtil.getNextStageNewAttributes(),getCurrentStageDetail.toString());
                nextStageEventDialog.show();

            }else {
                postInteractionUtil.postInteraction(strcustomerId,strProductId, swipeStage,null);
                showSnackBar(swipeStage);
                closeActivity();

            }


        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Exception onCardSwipe********* " + e.getLocalizedMessage());
        }

    }

    @Override
    public void onCardSwipe(String customerDetails, JSONObject cardDetails, String currentStage, String previousStage) {

    }

    @Override
    public void onItemRestore(int itemPosition, JSONObject cardDetails) {

    }

    private void showSnackBar(String swipeStage) {
        Snackbar.make(detailRecyclerView, swipeStage, Snackbar.LENGTH_SHORT).show();
    }

    private void addTaggedProducts(int index ,JSONArray taggedList ,String swipeStage, PostInteractionUtil postIUtil){

        if(index < taggedList.length()) {
            try{
                String productId = taggedList.getString(index);
                NextStageEventDialog nextStageEventDialog = new NextStageEventDialog(this, new NextStageEventDialog.NextStageEventListener() {
                    @Override
                    public void onNextStageEventCancel() {

                    }
                    @Override
                    public void onNextStageEventSubmit(JSONObject formDetails) {
                        postIUtil.postTaggedProduct(strcustomerId, productId, swipeStage, formDetails);
                        showSnackBar(swipeStage);
                        int tempIndex = index;
                        tempIndex++;
                        addTaggedProducts(tempIndex,taggedList,swipeStage,postIUtil);
                    }
                },"Tagged Products", productId, postIUtil.getNextStageNewAttributes(),null);
                nextStageEventDialog.show();
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Exception onCardSwipe********* " + e.getLocalizedMessage());
            }
        }else {
            closeActivity();
        }

    }

    private void closeActivity(){
        detailRecyclerView.setVisibility(View.GONE);
        new CountDownTimer(1000, 10) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
               finish();
            }

        }.start();

    }

}


   /* public void getCurrentStageDetails() {
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    Log.e(TAG,"getCurrentStage  Details List count :- "+response.getJSONArray("data").length());
                    if( response!= null && response.has("data") && response.getJSONArray("data").length()>0){

                        RecommendationHelper recommendationHelper = new RecommendationHelper(context,strcustomerId,strProductId);
                        JSONObject currentStageDetail=  recommendationHelper.getDefauleCurrentStageDetails();

                        Log.e(TAG,"Print Current Stage Details:---"+currentStageDetail);





                    }else {
                        Toast.makeText(AddScanProduct.this,"Current Stage Details is not found." ,Toast.LENGTH_LONG).show();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error updateRecyclerAdapter:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                Toast.makeText(context,responseMsg,Toast.LENGTH_LONG).show();

            }
        };
        try {
            String interactionModelName = modelUtil.getInteractionModelName();
            HashMap<String,Object> queryParam = new HashMap<>();
            queryParam.put(modelUtil.getCustomerIdAttrName(),strcustomerId);
            queryParam.put(productIdAttrName,strProductId);

            SharedPreference sharedPreference = new SharedPreference(this);
            queryParam.put(daveAIPerspective.getInvoice_id_attr_name(),sharedPreference.readString(strcustomerId));

            if(interactionModelName!=null && !interactionModelName.isEmpty()) {
                DaveModels daveModels = new DaveModels(this, true);
                daveModels.getObjects(interactionModelName, queryParam, daveAIListener, false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During get Cuurent Stage details :-------------  "+e.getMessage());
        }
    }
*/