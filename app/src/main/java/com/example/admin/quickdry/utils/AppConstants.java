package com.example.admin.quickdry.utils;

public interface AppConstants {


    //Shared pref variable
    public String API_KEY = "api_key";
    public String USER_ID = "user_id";
    public String CUSTOMER_ID = "customer_id";
    public String CUSTOMER_TITLE = "customer_title";
    public String CUSTOMER_ORDER_DETAILS = "order_details";
    public String IS_LOGIN_SUCCESS = "is_login_success";


    public String DISPLAY_CATEGORY_LIST = "display_category_list";
    public String DISPLAY_ROOM_LIST = "display_room_list";
    public String DISPLAY_LAYOUT_LIST = "display_layout_list";
    public String DISPLAY_TEXTURE_LIST = "display_texture_list";

    public String DISPLAY_ROOM = "display_room";
    public String DISPLAY_CATEGORY = "display_category";
    public String DISPLAY_LAYOUT = "display_layout";
    public String DISPLAY_TEXTURE = "display_texture_quality";

    public String TCP_SERVER_PORT = "server_port";
    public String INTRANET_IP_ADDRESS = "intranet_ip_address";
    public String DISPLAY_COMMUNICATION_TYPE = "communication_type";
    public String DISPLAY_PRODUCT_ID = "display_product_id";

    public String APP_PERSPECTIVE_NAME = "mobile_application";





    //api call attr
    public String POST_PAGE_NUMBER = "_page_number";
    public String POST_PAGE_SIZE = "_page_size";

    public String GET_PAGE_NUMBER = "page_number";
    public String GET_PAGE_SIZE = "page_size";
    public String IS_LAST_PAGE = "is_last";
    public String RECOMMENDATION_ID = "recommendation_id";


    //2d_visualization pref
    public String PREF_2DVISUALIZATION_NAME = "2d_visualization";
    public String PREF_CATEGORY_ATTRIBUTE = "category_option_attribute";
    public String PREF_RAW_ROOM_IMAGES = "raw_room_images";
    public String PREF_LAYOUT_OPTIONS = "layout_options";
    public String PREF_TEXTURE_QUALITY = "quality_options";
    public String PREF_DEFAULT_CATEGORY = "default_category_option";
    public String PREF_DEFAULT_ROOM = "default_room_option";
    public String PREF_DEFAULT_LAYOUT = "default_layout";
    public String PREF_DEFAULT_TEXTURE = "default_quality";


    //in app used attr
    /*public String DAVE_ACTION = "dave_action";
    public String RECOMMENDATION_ACTION = "Recommendations";
    public String SIMILAR_ACTION = "Similar";
    public String SEARCH_BY_IMAGE_ACTION = "search_by_image";
    public String CUSTOMER_ORDER_LIST = "customer_order_list";*/



}
