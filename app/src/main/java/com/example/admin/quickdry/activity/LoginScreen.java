package com.example.admin.quickdry.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.example.admin.daveai.daveUtil.BulkDownloadUtil;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dialogbox.ProgressLoaderDialog;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.model.ObjectData;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.ConnectDaveAI;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveException;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.daveai.others.PerspectiveHelper;
import com.example.admin.daveai.others.PerspectiveParent;
import com.example.admin.daveai.sdkConnection.ConnectWithDaveAI;
import com.example.admin.quickdry.BuildConfig;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.background.Receiver;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.storage.SharedPreference;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;



public class LoginScreen extends BaseActivity implements ConnectDaveAI.OnLoginSuccessListener, AppConstants {


    private static final String TAG = "LoginScreen";
    private static final int PERMISSION_REQUEST_CODE = 1111;
    private static final int INSTALL_PACKAGES_REQUEST_CODE = 100;
    private static final int GET_UNKNOWN_APP_SOURCES = 101;

    private RelativeLayout splashLayout;
    private LinearLayout loginLayout;
    private Button loginBtn;
    private EditText enterUserId, enterPassword;

    private ConnectDaveAI connectDaveAI;
    private SharedPreference sharedPreference;
    private ConnectWithDaveAI connectWithDaveAI;
   // private DaveAIPerspective daveAIPerspective;

    //Perspective perspective;

    private boolean isShowLoginVIew = false;
    private BulkDownloadUtil bulkDownloadUtil = new BulkDownloadUtil();

    //download new apk variables
    int versionCode=0;
    int latestVersion=0;
    String appURI = "";
    String appName = "MyApp";
    private DownloadManager downloadManager;
    Receiver receiver;
    IntentFilter intentFilter;
    private long downloadReference;
    private ProgressDialog pDialog;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        //Crashlytics.getInstance().crash();

        splashLayout = findViewById(R.id.splashLayout);
        loginLayout = findViewById(R.id.loginLayout);
        enterUserId = findViewById(R.id.enterUserId);
        enterPassword = findViewById(R.id.enterPassword);
        loginBtn = findViewById(R.id.loginBtn);

        sharedPreference = new SharedPreference(LoginScreen.this);
        connectDaveAI = new ConnectDaveAI((LoginScreen.this));
        connectWithDaveAI = new ConnectWithDaveAI((LoginScreen.this));
       // daveAIPerspective =  DaveAIPerspective.getInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isShowLoginVIew = bundle.getBoolean("isShowLoginView");
        }

        //Broadcast receiver for the download manager
        //IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        registerReceiver(downloadReceiver, intentFilter);

        versionCode = BuildConfig.VERSION_CODE;
        if(isShowLoginVIew){
            splashLayout.setVisibility(View.GONE);
            loginLayout.setVisibility(View.VISIBLE);
            connectDaveAI.registerLoginCallback(this);
            showLoginView();
        }else {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkPermission()) {
                    uiInit();
                } else {
                    requestPermission(); // Code for camera and storage  permission
                }

            } else {
                uiInit();
            }
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmpty(enterUserId)) {
                    Toast.makeText(LoginScreen.this, "Please enter the UserId", Toast.LENGTH_SHORT).show();
                } else if (isEmpty(enterPassword)) {
                    Toast.makeText(LoginScreen.this, "Please enter the Password", Toast.LENGTH_SHORT).show();
                } else {
                    InputMethodManager imm = ((InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE));
                    assert imm != null;
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    callLoginApi(enterUserId.getText().toString().trim(),enterPassword.getText().toString().trim());
                }
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(downloadReceiver!=null)
            this.unregisterReceiver(downloadReceiver);
    }

    private void uiInit(){
        String apiKey = sharedPreference.readString( SharedPreference.API_KEY);
        String userId = sharedPreference.readString(SharedPreference.USER_ID);
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Get Login CRedential UserId:"+ userId+"  && API Key:"+apiKey);
        if (apiKey != null && userId != null) {
            getApkUpdateDetails(apiKey, userId);

        } else {
            splashLayout.setVisibility(View.GONE);
            loginLayout.setVisibility(View.VISIBLE);
            connectDaveAI.registerLoginCallback(this);
            showLoginView();
        }

    }


    // method to connect with Dave Sdk
    private void intiSDK() {
        Log.e(TAG,"Connect with Sdk **************************");
        connectDaveAI.registerConnectDaveCallback(daveMsg -> {
            Log.e(TAG,"OnDAveConnected:-----------------   "+daveMsg);
            if(daveMsg){
                showLoginView();
                //intentLoginToHomePage();
                update2dPerspective();

            }else {
                Toast.makeText(LoginScreen.this, "Connected  failed with dave", Toast.LENGTH_SHORT).show();
                showLoginView();
            }

        });
        connectDaveAI.initSDK(LoginScreen.this,"Connecting...");

    }

    public void getApkUpdateDetails(String apiKey, String userId) {

        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                if(response == null || response.isEmpty()){
                    intiSDK();
                }
                else {
                    try {
                       JSONObject data = new JSONObject(response);
                       Iterator<String> keys = data.keys();
                       while (keys.hasNext()) {
                           String key = keys.next();
                           if (data.get(key) instanceof JSONArray) {
                               JSONArray data_array = data.getJSONArray(key);
                               for (int i = 0; i < data_array.length(); i++) {
                                   JSONObject getData = data_array.getJSONObject(i);
                                   if (getData.has("version_code") && !getData.isNull("version_code"))
                                       latestVersion = getData.getInt("version_code");
                                   if (getData.has("app_url") && !getData.isNull("app_url"))
                                       appURI = getData.getString("app_url");
                                   if (getData.has("app_name") && !getData.isNull("app_name"))
                                       appName = getData.getString("app_name");
                               }
                           }
                       }
                       Log.e(TAG,"Current App  Version :----"+versionCode+"  ************Latest ServerApp Version:------------"+latestVersion);
                       if(latestVersion > versionCode){
                           dialogToAskUpdateApp();
                       }else {
                           intiSDK();

                       }

                    } catch (Exception e) {
                        Log.e("Exception:- ", "App Update Response  Error == " + e.getMessage());

                    }
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                Log.e(TAG, "App Update onTaskFailure  Error  (RequestCode)== " +requestCode);
                Toast.makeText(LoginScreen.this, responseMsg, Toast.LENGTH_SHORT).show();
                if(requestCode == 401 || requestCode == 402 || requestCode == 403){
                    SharedPreference  sharedPreference = new SharedPreference(LoginScreen.this);
                    sharedPreference.removeKey(SharedPreference.API_KEY);
                    showLoginView();
                }else {
                    intiSDK();

                }
            }
        };
        if (CheckNetworkConnection.networkHasConnection(this)) {
            Model model1 = new Model(LoginScreen.this);
            model1.getAppConfigurationDetails( userId,apiKey,postTaskListener);

        } else {
            intiSDK();

        }

    }

    private void dialogToAskUpdateApp(){

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginScreen.this);
        builder
            .setMessage("Upgrade is available for this application")
            .setCancelable(false)
            .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                //if the user agrees to upgrade
                public void onClick(DialogInterface dialog, int id) {

                    pDialog = new ProgressDialog(LoginScreen.this);
                    pDialog.setMessage("Please wait application is updating...");
                    pDialog.setCancelable(false);
                    pDialog.show();

                    //start downloading the file using the download manager
                    downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
                    Uri Download_Uri = Uri.parse(appURI);
                    DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                    request.setAllowedOverRoaming(false);
                    request.setTitle(appName);
                    request.setDescription("Downloading " + appName + ".apk");
                    request.setVisibleInDownloadsUi(true);
                    request.setMimeType("application/vnd.android.package-archive");
                    request.setDestinationInExternalFilesDir(LoginScreen.this, Environment.DIRECTORY_DOWNLOADS,"MyAndroidApp.apk");
                    downloadReference = downloadManager.enqueue(request);
                    // Log.e("downloadReference ","downloadReference:----"+downloadReference);
                }
            })
            .setNegativeButton("Remind Later", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                    dialog.cancel();
                    intiSDK();
                }
            });
        //show the alert message
        builder.create().show();

    }


    long downloadedReferenceId=0;
    //broadcast receiver to get notification about ongoing downloads
    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (pDialog != null)
                pDialog.dismiss();
            //check if the broadcast message is for our Enqueued download
            String action = intent.getAction();
            assert action != null;
            if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                downloadedReferenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                checkIsAndroidO();
            }

        }
    };

    private void installLatestApk(){
        try {
            if (downloadReference == downloadedReferenceId) {
                Uri uri = downloadManager.getUriForDownloadedFile(downloadReference);
                Log.e("Uri uri:----  ", "Uri uri before :----  " + uri);
                String filePath = "";
                if ("content".equals(uri.getScheme())) {
                    Cursor cursor = getContentResolver().query(uri, new String[]
                            {android.provider.MediaStore.Images.ImageColumns.DATA}, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();
                    filePath = cursor.getString(0);
                    cursor.close();
                    uri = Uri.fromFile(new File(filePath));
                }
                Log.e("Uri Path :----  ", "uri Path  after ----  " + uri);

                //start the installation of the latest version
                Intent installIntent;
                // Start the standard installation
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Log.e("filePath:----  ", "filePath----  " + filePath);
                    Uri fileUri = FileProvider.getUriForFile(getBaseContext(), BuildConfig.APPLICATION_ID + ".provider", new File(filePath));
                    installIntent = new Intent(Intent.ACTION_VIEW, fileUri);
                    installIntent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                    // downloadIntent.setDataAndType(fileUri, downloadManager.getMimeTypeForDownloadedFile(downloadReference));
                    installIntent.setDataAndType(fileUri, "application/vnd.android.package-archive");
                    installIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    installIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);


                } else {
                    Log.e("intent2:---- ", " Before Intent uri----" + uri);
                    installIntent = new Intent(Intent.ACTION_VIEW);
                    // intent2.setDataAndType(uri, downloadManager.getMimeTypeForDownloadedFile(downloadReference));
                    installIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                    installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    //installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

                }
                startActivity(installIntent);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(LoginScreen.this,TAG+"installLatestApk()"+e.getMessage(),Toast.LENGTH_LONG).show();
            Log.e(TAG, "Error during Installing App :---------- " + e.getMessage());
        }



    }



    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(LoginScreen.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int read_res = ContextCompat.checkSelfPermission(LoginScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int camPermission = ContextCompat.checkSelfPermission(LoginScreen.this, Manifest.permission.CAMERA);
        int contactPerm = ContextCompat.checkSelfPermission(LoginScreen.this, Manifest.permission.WRITE_CONTACTS);
        int contactreadPerm = ContextCompat.checkSelfPermission(LoginScreen.this, Manifest.permission.READ_CONTACTS);
        if (result == PackageManager.PERMISSION_GRANTED && read_res == PackageManager.PERMISSION_GRANTED &&
                camPermission == PackageManager.PERMISSION_GRANTED &&
                contactPerm == PackageManager.PERMISSION_GRANTED &&
                contactreadPerm == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(LoginScreen.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(LoginScreen.this, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(LoginScreen.this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,Manifest.permission.WRITE_CONTACTS,Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    uiInit();
                    Log.e(TAG, "Permission Granted, Now you can use local drive .");
                } else {
                    Log.e(TAG, "Permission Denied, You cannot use local drive .");
                }
                break;


            case INSTALL_PACKAGES_REQUEST_CODE:
                Log.e(TAG,"onRequestPermissionsResult INSTALL_PERMISSION_REQUEST_CODE:>>>>>>>>>>>>"+grantResults[0]);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
                    //todo call install apk method
                     installLatestApk();
                }
                else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, GET_UNKNOWN_APP_SOURCES);
                }

            break;


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG,"onActivityResult:---------"+requestCode+" "+requestCode);
        switch (requestCode) {
            case GET_UNKNOWN_APP_SOURCES:
                checkIsAndroidO();
                break;

            default:
                break;
        }
    }

    private void checkIsAndroidO() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            boolean result = getPackageManager().canRequestPackageInstalls();
            if (result) {
                installLatestApk();
            } else {
                // request the permission
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.REQUEST_INSTALL_PACKAGES}, INSTALL_PACKAGES_REQUEST_CODE);
            }
        } else {
            installLatestApk();
        }
    }

    public  void showLoginView(){
        Log.i(TAG,"<<<<<<<<<<<<<<<<<<Show Login VIew <<<<<<<<<<<<<<");

        String apiKey = sharedPreference.readString(SharedPreference.API_KEY);
        Log.i(TAG,"showLoginView*************APIKEY************  " +apiKey);
        if(apiKey!=null) {
            intentLoginToHomePage();
        }else {
            splashLayout.setVisibility(View.GONE);
            loginLayout.setVisibility(View.VISIBLE);
        }
    }

    public static boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().equals("");
    }

    private void callLoginApi(String userId, String password){
        try {
            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
            String oneSignalPlayerId = status.getSubscriptionStatus().getUserId();
            Log.i(TAG,"One Signal Player id :-----------"+oneSignalPlayerId);

            JSONObject post_body = new JSONObject();
            post_body.put("user_id", userId);
            post_body.put("password", password);
            post_body.put("redirect_to", "json");
            post_body.put("push_token",oneSignalPlayerId);

            ArrayList<String> prefList = new ArrayList<>();
            prefList.add(PREF_2DVISUALIZATION_NAME);


            connectDaveAI.login(LoginScreen.this,post_body,true,prefList);


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Error During Login with DaveAI:- " + e.getLocalizedMessage());
        }
    }

    @Override
    public void loginSucess(JSONObject loginDetails) {
        Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<loginSucess:--------- "+loginDetails);
        if(loginDetails!= null && loginDetails.length()>0){
            try {
                sharedPreference.writeString(API_KEY, loginDetails.get(API_KEY).toString());
                sharedPreference.writeString( USER_ID,  loginDetails.get(USER_ID).toString());
                Log.i(TAG,"Get Login Credential userID:--"+ sharedPreference.readString(USER_ID)+ "  API KEY:-"+sharedPreference.readString(API_KEY));

                if(loginDetails.get("api_key").toString()!=null){
                    intentLoginToHomePage();
                }
                read2dVisualizationPref();
            } catch (JSONException e) {
                e.printStackTrace();

            }
        }
    }

    @Override
    public void loginFailed(String userLoginFailed) {
        Log.e(TAG,"loginFailed:--------- "+userLoginFailed);
        Toast.makeText(LoginScreen.this,"Login failed!" +"\n"+userLoginFailed, Toast.LENGTH_LONG).show();

    }

    private void intentLoginToHomePage() {

        Log.e(TAG,"****************Intent to Home Page****************************");
        Intent intent = new Intent(this, HomePage.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void update2dPerspective(){

        PerspectiveHelper perspectiveHelper = new PerspectiveHelper();
        perspectiveHelper.updatePerspective(this, PREF_2DVISUALIZATION_NAME, new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject jsonObject) {
                read2dVisualizationPref();
            }

            @Override
            public void onResponseFailure(int i, String s) {

            }
        });
    }

    private void read2dVisualizationPref(){

        try {
            PerspectiveParent perspectiveParent = new PerspectiveParent();
            boolean isPrefExist =  perspectiveParent.checkPreferenceExist(this,PREF_2DVISUALIZATION_NAME);
            if(isPrefExist){
                //get room List
                JSONObject rowRoomImages = perspectiveParent.getFromPreference(PREF_RAW_ROOM_IMAGES,JSONObject.class,new JSONObject());
                if(rowRoomImages!=null && rowRoomImages.length()>0){
                    ArrayList<String> roomList =ObjectData.convertJsonArrayToArrayList(rowRoomImages.names());
                   // Log.e(TAG,"read2dVisualizationPref:----rowList-------"+ roomList );
                    if(roomList!=null && roomList.size()>0)
                        sharedPreference.saveList(DISPLAY_ROOM_LIST,roomList);
                }

                //get layout List
                JSONArray layoutOptions = perspectiveParent.getFromPreference(PREF_LAYOUT_OPTIONS,JSONArray.class, new JSONArray());
                if(layoutOptions!=null && layoutOptions.length()>0){
                    ArrayList<String> layoutList = ObjectData.convertJsonArrayToArrayList(layoutOptions);
                   // Log.e(TAG,"read2dVisualizationPref:----layoutList-------"+ layoutList );
                    if(layoutList!=null && layoutList.size()>0)
                        sharedPreference.saveList(DISPLAY_LAYOUT_LIST,layoutList);
                }

                //get texture Quality List
                JSONArray textures = perspectiveParent.getFromPreference(PREF_TEXTURE_QUALITY,JSONArray.class, new JSONArray());
                if(textures!=null && textures.length()>0){
                    ArrayList<String> texturesList = ObjectData.convertJsonArrayToArrayList(textures);
                    //Log.e(TAG,"read2dVisualizationPref:----texturesList-------"+ texturesList );
                    if(texturesList!=null && texturesList.size()>0)
                        sharedPreference.saveList(DISPLAY_TEXTURE_LIST,texturesList);
                }

                //get default room
                String defaultRoom = perspectiveParent.getFromPreference(PREF_DEFAULT_ROOM,String.class,"");
                //Log.e(TAG,"read2dVisualizationPref:----defaultRoom-------"+ defaultRoom);
                if(defaultRoom!=null && !defaultRoom.isEmpty())
                    sharedPreference.writeString(DISPLAY_ROOM,defaultRoom);

                //get default category
                String defaultCategory = perspectiveParent.getFromPreference(PREF_DEFAULT_CATEGORY,String.class,"");
               // Log.e(TAG,"read2dVisualizationPref:----defaultcategory-------"+ defaultCategory);
                if(defaultCategory!=null && !defaultCategory.isEmpty())
                    sharedPreference.writeString(DISPLAY_CATEGORY,defaultCategory);

                //get default layout
                String defaulLayout = perspectiveParent.getFromPreference(PREF_DEFAULT_LAYOUT,String.class,"");
               // Log.e(TAG,"read2dVisualizationPref:----defaulLayout-------"+ defaulLayout);
                if(defaulLayout!=null && !defaulLayout.isEmpty())
                    sharedPreference.writeString(DISPLAY_LAYOUT,defaulLayout);

                //get default layout
                String defaulTexture = perspectiveParent.getFromPreference(PREF_DEFAULT_TEXTURE,String.class,"");
                //Log.e(TAG,"read2dVisualizationPref:----defaulTexture-------"+ defaulTexture);
                if(defaulTexture!=null && !defaulTexture.isEmpty())
                    sharedPreference.writeString(DISPLAY_TEXTURE,defaulTexture);

                // get category option
                String categoryAttr = perspectiveParent.getFromPreference(PREF_CATEGORY_ATTRIBUTE,String.class,"");
               // Log.e(TAG,"read2dVisualizationPref:----categoryAttr-------"+ categoryAttr);
                if(categoryAttr!=null && !categoryAttr.isEmpty())
                    getDisplayCategoryList(categoryAttr);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void getDisplayCategoryList(String attrName){

        DaveModels daveModels = new DaveModels(this, true);
        try {
            daveModels.getPivot(new ModelUtil(this).getProductModelName(), attrName, new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject categoryResponse) {
                    Log.e(TAG, "get Display Category list : " + categoryResponse);
                    saveCategoryList(categoryResponse);
                }

                @Override
                public void onResponseFailure(int i, String s) {

                }
            }, false, false);
        } catch (DaveException e) {
            e.printStackTrace();
        }

    }

    private void saveCategoryList(JSONObject categoryResponse){

        try {
            if(categoryResponse.has("data") && !categoryResponse.isNull("data") && categoryResponse.getJSONObject("data").length()>0) {
                JSONObject jsonCategory = categoryResponse.getJSONObject("data");
                ArrayList<String> categoryList = ObjectData.convertJsonArrayToArrayList(jsonCategory.names());
                Log.e(TAG,"read2dVisualizationPref:----rowList-------"+ categoryList );
                if(categoryList!=null && categoryList.size()>0)
                    sharedPreference.saveList(DISPLAY_CATEGORY_LIST,categoryList);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

/******************************* Implement new Login Flow**********************/

    private void showLoader(String loaderMsg){

        ProgressLoaderDialog progressLoaderDialog = new ProgressLoaderDialog(this);
       // pDialog = progressLoaderDialog.progressDialog();
        if(TextUtils.isEmpty(loaderMsg)) {
            progressLoaderDialog.setMessage("Please wait...");
        }else {
            progressLoaderDialog.setMessage(loaderMsg);
        }
        pDialog.setCancelable(false);
        pDialog.show();

    }

    private void hideLoader(){
        if(pDialog!=null)
            pDialog.dismiss();
    }

    /*private void callLoginApi(String userId, String password) {

        ConnectWithDaveAI.OnLoginListener onLoginListener =  new ConnectWithDaveAI.OnLoginListener() {
            @Override
            public void loginSucess(JSONObject loginDetails) {
                Log.i(TAG,"<<<<<<<<<<<<<<<<<<<<<<<loginSucess:--------- "+loginDetails);
                if(loginDetails!= null && loginDetails.length()>0){
                    try {

                        //todo check login details with previous one
                        sharedPreference.writeString(API_KEY, loginDetails.get(API_KEY).toString());
                        sharedPreference.writeString( USER_ID,  loginDetails.get(USER_ID).toString());
                        Log.i(TAG,"Get Login Credential userID:--"+ sharedPreference.readString(USER_ID)+ "  API KEY:-"+sharedPreference.readString(API_KEY));

                        getPerspective();

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }

            @Override
            public void loginFailed(String userLoginFailed) {
                Log.e(TAG,"loginFailed:--------- "+userLoginFailed);
                Toast.makeText(LoginScreen.this,"Login failed!" +"\n"+userLoginFailed, Toast.LENGTH_LONG).show();
            }
        };
        try {

            OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();
            String oneSignalPlayerId = status.getSubscriptionStatus().getUserId();

            JSONObject post_body = new JSONObject();
            post_body.put("user_id", userId);
            post_body.put("password", password);
            post_body.put("redirect_to", "json");
            post_body.put("push_token",oneSignalPlayerId);

            String apiKey = sharedPreference.readString(API_KEY);
            String savedUserId = sharedPreference.readString(USER_ID);

            //todo check which api need to call
            if(apiKey!= null && userId.equals(savedUserId) ){
                Log.i(TAG,"************call connect sdk api********************");
                getPerspective();
            }else {
                Log.i(TAG,"************call loginWithDaveAI api********************");
                connectWithDaveAI.login(post_body,onLoginListener);

            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Error During Login with DaveAI:- " + e.getLocalizedMessage());
        }
    }*/

    private void getPerspective(){

        showLoader("First Time Login: This may take a long time");
        new PerspectiveHelper().getPerspective(this, APP_PERSPECTIVE_NAME, new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject jsonObject) {
                try {
                    if(jsonObject.length()>0){
                       //todo set Perspective details
                        Log.e(TAG, "completed perspective = " + jsonObject);
                      /*  daveAIPerspective.savePerspectivePreferenceValue(this, response);
                        setupModelCacheMetaData("perspective_settings", "perspective_settings", mContext);
                        databaseManager.insertOrUpdateSingleton(
                                "perspective_settings",
                                new SingletonTableRowModel("perspective_settings", response, System.currentTimeMillis(), "perspective_settings", "")
                        );*/

                        //connectSDK();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseFailure(int i, String s) {
                Toast.makeText(LoginScreen.this,i+ ": "+s,Toast.LENGTH_LONG).show();
                hideLoader();

            }
        });
    }

    // private int DEFAULT_OBJECT_COUNT = 200;
    /**
     *  call connectWithDaveAI() method to connect with Dave Sdk
     */
  /*  private void connectSDK(){

        ArrayList<String> prefList = new ArrayList<>();
        prefList.add(PREF_2DVISUALIZATION_NAME);

        ArrayList<String> coreModelList = new ArrayList<>();
        coreModelList.add("customer");
        coreModelList.add("product");
        coreModelList.add("interaction");


        ArrayList<String> modelNameList = new ArrayList<>();
        modelNameList.add(daveAIPerspective.getInvoice_model_name());
        modelNameList.add(daveAIPerspective.getUser_login_model_name());


        HashMap<String, Object> coreModelObjectsMap = new HashMap<>();
        coreModelObjectsMap.put("customer",200);
        coreModelObjectsMap.put("product",200);
        coreModelObjectsMap.put("interaction",200);

        HashMap<String, Object> modelObjectsMap = new HashMap<>();
        if(daveAIPerspective.getUser_login_model_name()!=null)
            modelObjectsMap.put(daveAIPerspective.getUser_login_model_name(),200);


        HashMap<String, ArrayList<String>> modelHierarchyMap = new HashMap<>();
        modelHierarchyMap.put("customer",daveAIPerspective.getCustomer_category_hierarchy());
        modelHierarchyMap.put("product",daveAIPerspective.getProduct_category_hierarchy());


        HashMap<String, ArrayList<String>> modelFiltersMap = new HashMap<>();
        modelFiltersMap.put("customer",daveAIPerspective.getCustomer_filter_attr());
        modelFiltersMap.put("product",daveAIPerspective.getProduct_filter_attr());


        //bulk download
        bulkDownloadUtil.setCoreModelObjectsMap(coreModelObjectsMap);
        bulkDownloadUtil.setModelObjectsMap(modelObjectsMap);
        bulkDownloadUtil.setModelHierarchyMap(modelHierarchyMap);
        bulkDownloadUtil.setModelFiltersMap(modelFiltersMap);
        bulkDownloadUtil.setConvertIntoInteractions(true);


        DaveConnectUtil daveConnectUtil = new DaveConnectUtil();
        daveConnectUtil.setPrefSettingList(prefList);
        daveConnectUtil.setCoreModelList(coreModelList);
        daveConnectUtil.setOtherModelList(modelNameList);
        daveConnectUtil.setCallInteractionStages(true);
        daveConnectUtil.setBulkDownloadUtil(bulkDownloadUtil);

        Log.i(TAG," connectSDK() Print modelObjectsMap "+ modelObjectsMap);
        Log.i(TAG,"connectSDK() Print modelHierarchyMap "+ modelHierarchyMap);
        Log.i(TAG,"connectSDK() Print modelFiltersMap "+ modelFiltersMap);

        Log.i(TAG,"connectSDK() Print modelName List :------------ "+ modelNameList);

        connectWithDaveAI.connectWithDaveAI("", daveConnectUtil, new ConnectWithDaveAI.OnDaveConnectedListener() {
            @Override
            public void onDaveConnected() {
                Log.e(TAG,"Print Connect sdk on connected :------------ ");
                hideLoader();
                Toast.makeText(LoginScreen.this,"Login Success!", Toast.LENGTH_LONG).show();
                sharedPreference.writeBoolean(IS_LOGIN_SUCCESS,true);
                read2dVisualizationPref();
                intentLoginToHomePage();
            }

            @Override
            public void onDaveConnectedFailed(String connectFailedMsg) {

                Log.e(TAG,"onDaveConnectedFailed:--------- "+connectFailedMsg);
                hideLoader();
                Toast.makeText(LoginScreen.this,"Login failed!" +"\n"+connectFailedMsg, Toast.LENGTH_LONG).show();

            }
        });
    }*/

}










