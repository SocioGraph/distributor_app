package com.example.admin.quickdry.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.daveai.activity.Visualization;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.AddScanProduct;
import com.example.admin.quickdry.activity.DaveFunction;
import com.example.admin.quickdry.activity.ImageFullScreen;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.utils.CRUDUtil;
import com.example.admin.quickdry.utils.RecycleViewHolder;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static com.example.admin.daveai.database.DatabaseConstants.ERROR_MESSAGE_TEXT;


public class ProductAdapter extends RecyclerView.Adapter<RecycleViewHolder> implements RecyclerView.OnClickListener,AppConstants,
        DaveConstants {

    private final String TAG = getClass().getSimpleName();
    private List<JSONObject> seedProductList;
    private List<JSONObject> mProductList;
    private Context mContext;
    private int expandedPosition = -1;
    private String product_id=null;
   // private HashMap<String, String> similarFilterDetails = new HashMap<>();
    private DaveAIPerspective daveAIPerspective;
    private Model productModel;
    private ModelUtil modelUtil;
    String productIdAttrName;
    String productModelName;


    private ProductListListener productListListener;
    public interface ProductListListener {
        void onClickEditProduct(int holderPosition, Bundle bundle);
        void onCardToggle(View view,int holderPosition);

    }


    public ProductAdapter(ProductListListener productListListener, Activity context, List<JSONObject> seedProductList) {
        this.productListListener =productListListener;
        this.seedProductList = seedProductList;
        this.mContext = context;

        daveAIPerspective= DaveAIPerspective.getInstance();
        mProductList = new ArrayList<>();

        modelUtil = new ModelUtil(mContext);
        productIdAttrName = modelUtil.getProductIdAttrName();
        productModelName = modelUtil.getProductModelName();
        productModel = Model.getModelInstance(mContext,productModelName);

    }

    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_product_cardview,viewGroup, false);

        // Sets the click adapter for the entire cell to the one in this class.
        RecycleViewHolder holder = new RecycleViewHolder(view);
        holder.itemView.setOnClickListener(ProductAdapter.this);
        holder.itemView.setTag(holder);
        // return new CustomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder customViewHolder, int position) {

        if(customViewHolder.getAdapterPosition() == expandedPosition) {
            customViewHolder.collapseViewLayout.setVisibility(View.GONE);
            customViewHolder.expandViewLayout.setVisibility(View.VISIBLE);
        }else {
            customViewHolder.collapseViewLayout.setVisibility(View.VISIBLE);
            customViewHolder.expandViewLayout.setVisibility(View.GONE);
        }

        JSONObject seedItem = seedProductList.get(customViewHolder.getAdapterPosition());
        try{
            Log.e(TAG,"onBindViewHolder:------------"+customViewHolder.getAdapterPosition() +" Details:- "+ seedItem);
            String shareMsg =  customViewHolder.bindQuickView(mContext, seedItem);
            customViewHolder.bindExpandView(mContext,productModel,seedItem);
            if(daveAIPerspective.isEnable_manage_product()) {
                customViewHolder.editProduct.setVisibility(View.VISIBLE);
            }
            if(seedItem.has(ERROR_MSG_ATTRIBUTE) && !seedItem.isNull(ERROR_MSG_ATTRIBUTE) && !seedItem.getString(ERROR_MSG_ATTRIBUTE).isEmpty()){
                customViewHolder.errorIconView.setVisibility(View.VISIBLE);
                customViewHolder.errorMsgView.setVisibility(View.VISIBLE);
                customViewHolder.deleteView.setVisibility(View.VISIBLE);
                if(seedItem.getString(ERROR_MSG_ATTRIBUTE).equals(ERROR_MESSAGE_TEXT)){
                    customViewHolder.errorIconView.setImageResource(R.drawable.ic_report_error_green);
                    customViewHolder.errorMsgView.setTextColor(mContext.getResources().getColor(R.color.sync_pending));
                }

                customViewHolder.errorMsgView.setText(seedItem.getString(ERROR_MSG_ATTRIBUTE).replaceAll("\\s{2,}", " ").trim());
            }else {
                customViewHolder.errorIconView.setVisibility(View.GONE);
                customViewHolder.errorMsgView.setVisibility(View.GONE);
                customViewHolder.deleteView.setVisibility(View.GONE);

            }

            if(daveAIPerspective.getProduct_card_image_view()!= null && seedItem.has(daveAIPerspective.getProduct_card_image_view())
                    && !seedItem.isNull(daveAIPerspective.getProduct_card_image_view())) {
                customViewHolder.shareImage.setVisibility(View.VISIBLE);
                customViewHolder.zoomImage.setVisibility(View.VISIBLE);
            }else {
                customViewHolder.shareImage.setVisibility(View.GONE);
                customViewHolder.zoomImage.setVisibility(View.GONE);
            }

            customViewHolder.shareImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext,"Sharing Image with.. ",Toast.LENGTH_SHORT).show();
                    CRUDUtil crudUtil = new CRUDUtil(mContext);
                    JSONObject productObject = seedProductList.get(customViewHolder.getAdapterPosition());
                    try {
                        String product_id = productObject.getString(productIdAttrName);
                        Uri bitmap = crudUtil.getLocalBitmapUri(mContext,customViewHolder.conceptImageView);
                        //Log.e(TAG,"Print share image bitmap :-----------"+bitmap);
                        boolean showViewUrl = true;
                        if(daveAIPerspective.getShare_image_attributes()!=null&&daveAIPerspective.getShare_image_attributes().size()>0){
                            showViewUrl = false;
                        }
                        if(bitmap!=null) {
                            crudUtil.shareImage(mContext, product_id, bitmap, shareMsg, showViewUrl);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during Binding Catd View +++++++++++" + e.getMessage());
        }
        customViewHolder.getSimilar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> intent_details = new HashMap<>();
                HashMap<String, String> similarFilterDetails = new HashMap<>();
                try {
                    JSONObject productObject = seedProductList.get(customViewHolder.getAdapterPosition());
                    if(daveAIPerspective.getProduct_similar_attributes()!=null){
                        for(int i=0; i<daveAIPerspective.getProduct_similar_attributes().size(); i++){
                            String key = daveAIPerspective.getProduct_similar_attributes().get(i).toString();
                            if(productObject.has(key))
                                similarFilterDetails.put(key,productObject.get(key).toString());
                        }
                    }
                    String product_id = productObject.getString(productIdAttrName);
                    intent_details.put("product_id", product_id);
                    intent_details.put("customer_id", "");
                    intent_details.put("productFilterDetails",similarFilterDetails);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e(TAG,"CLick on SImilar button:-------------------"+intent_details);
                Intent intent = new Intent(mContext, DaveFunction.class);
                intent.putExtra("msg_to_dave_func", intent_details);
                intent.putExtra(DaveFunction.DAVE_ACTION, DaveFunction.SIMILAR_ACTION);
                mContext.startActivity(intent);
               /* ProductFilterDialog cdd = new ProductFilterDialog(mContext,intent_details,product_details,false);
                cdd.show();*/
            }
        });

        customViewHolder.editProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(productListListener!=null){
                    Bundle b= new Bundle();
                    b.putString("action_name", AddScanProduct.ACTION_EDIT_PRODUCT);
                    b.putString("_object_details",seedItem.toString());
                    b.putString("_product_id",productIdAttrName);
                    productListListener.onClickEditProduct(customViewHolder.getAdapterPosition(),b);
                }
            }
        });




        customViewHolder.zoomImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject productDetails = seedProductList.get(customViewHolder.getAdapterPosition());
                    if(daveAIPerspective.getProduct_card_image_view()!= null && productDetails.has(daveAIPerspective.getProduct_card_image_view())
                           && !productDetails.isNull(daveAIPerspective.getProduct_card_image_view())) {


                       Intent intent = new Intent(mContext, ImageFullScreen.class);
                       intent.putExtra(ImageFullScreen.IMAGE_URL,productDetails.getString(daveAIPerspective.getProduct_card_image_view()));
                       mContext.startActivity(intent);

                        /*Bundle b=new Bundle();
                        b.putString(Visualization.VISUALIZATION_VIEW_TYPE,Visualization.VisualisationType.IMAGE.toString());
                        b.putString(Visualization.IMAGE_URL,productDetails.getString(daveAIPerspective.getProduct_card_image_view()));

                        Intent intent = new Intent(mContext, Visualization.class);
                        intent.putExtras(b);
                        mContext.startActivity(intent);*/
                    }else{
                       Toast.makeText(mContext,"Image Not Available..",Toast.LENGTH_SHORT).show();
                       Log.e(TAG,"error during click on Zoom Image position  :-" +customViewHolder.getAdapterPosition() +" imageAttrName:- "+daveAIPerspective.getProduct_card_image_view() +" isDetaildHasImageUrl:-  "+productDetails.has(daveAIPerspective.getProduct_card_image_view())
                       +"\n productDetails:-"+productDetails);

                    }
                }catch (Exception e){
                   e.printStackTrace();
                }

            }
        });

        customViewHolder.deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dialogToAskDeleteProduct(customViewHolder.getAdapterPosition(),
                            seedProductList.get(customViewHolder.getAdapterPosition()).getString(productIdAttrName));

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return (null != seedProductList ? seedProductList.size() : 0);
    }


    @Override
    public void onClick(View view) {

        RecycleViewHolder holder = (RecycleViewHolder) view.getTag();
        int cardPosition = holder.getAdapterPosition();

        if(holder.expandViewLayout.getVisibility()== View.VISIBLE){
            expandedPosition = -1 ;
        }else {
            expandedPosition = cardPosition;
        }
        //notifyItemChanged(cardPosition);
        notifyDataSetChanged();

       if(productListListener!=null)
           productListListener.onCardToggle(holder.itemView,cardPosition);


    }

    public void updateItem(JSONObject newItemDetails, int position) {
        seedProductList.set(position,newItemDetails);
        notifyItemChanged(position);
    }



    public void addNewSeedList(List<JSONObject> newItemList) {
        seedProductList.clear();
        notifyDataSetChanged();
        this.seedProductList.addAll(newItemList);
        notifyItemRangeInserted(0,seedProductList.size());
       // notifyDataSetChanged();

        // Log.e(TAG," After addNewItem size of adapter****************:- " + seedSimilarOrRecommList.size());
    }

    public void addNewSeedList(List<JSONObject> newItemList,boolean isDefaultData) {
        if(isDefaultData) {
            mProductList.clear();
            mProductList.addAll(newItemList);
        }
        if(seedProductList.size()>0) {
            seedProductList.clear();
        }

        seedProductList.addAll(newItemList);
        notifyDataSetChanged();

        Log.e(TAG," After addNewItem size of adapter>>>>>>>>>>>>:- " + seedProductList.size()+" == "+ mProductList.size());
    }


    public void updateSeedList(List<JSONObject> newItemList,boolean isDefaultData) {
        seedProductList.addAll(newItemList);
        notifyItemInserted(seedProductList.size());
        notifyDataSetChanged();
        if(isDefaultData)
            mProductList.addAll(newItemList);

        Log.e(TAG," After updateSeedList seedCustomerList of adapter>>>>>>>>>:- " + seedProductList.size()+"=="+mProductList.size());

    }

    public void resetPreviousList(){
        if(seedProductList.size()>0)
            seedProductList.clear();
        seedProductList = new ArrayList<>(mProductList);
       // notifyItemInserted(seedProductList.size());
        notifyItemRangeChanged(0,seedProductList.size()-1);

    }

    public void clearProductList(){
        final int size = seedProductList.size();
        if(size>0)
            seedProductList.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void deleteItem(int index) {
        seedProductList.remove(index);
        notifyItemRemoved(index);
    }

    private void dialogToAskDeleteProduct(int position, String objectID){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setMessage("Are you sure you want to delete ?");
        builder1.setCancelable(false);

        builder1.setPositiveButton("Yes", (dialog, id) -> {
            deleteItem(position);

            DaveModels daveModels = new DaveModels(mContext,true);
            daveModels.deleteObjectFromLocalDB(productModelName,objectID,null);

        });

        builder1.setNegativeButton("Cancel", (dialog, id) -> {
            dialog.cancel();

        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


}
