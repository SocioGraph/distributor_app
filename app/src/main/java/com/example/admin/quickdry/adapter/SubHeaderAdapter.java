package com.example.admin.quickdry.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.admin.quickdry.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Iterator;

public class SubHeaderAdapter extends RecyclerView.Adapter<SubHeaderAdapter.ViewHolder> {

    private Context context;
    private ArrayList<JSONObject> itemDetails;


    public SubHeaderAdapter(Context context,ArrayList<JSONObject>itemDetails ) {
        this.context = context;
        this.itemDetails = itemDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_sub_header_details, viewGroup, false);
       // ViewHolder viewHolder = new ViewHolder(v);
       // return viewHolder;
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

       JSONObject seeItem = itemDetails.get(i);
        /*for (Object o : seeItem.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            viewHolder.tvItemName.setText(pair.getKey().toString());
            viewHolder.tvItemValue.setText(pair.getValue().toString());
            // it.remove(); // avoids a ConcurrentModificationException
        }*/

        Iterator<?> keys = seeItem.keys();
        while(keys.hasNext() ) {
            String key = (String)keys.next();
            viewHolder.tvItemName.setText(key);
            try {
                viewHolder.tvItemValue.setText(seeItem.get(key).toString());
            } catch (JSONException e) {
                Log.e("SubHeader","Error during showing Subheader Details"+e.getMessage());
            }

        }
    }


    @Override
    public int getItemCount() {
        return itemDetails.size();
    }

    /*public int getCurrentCount(){
        if()
    }*/

    public void updateCountValue (int currentCount){

    }

    public String calculatePageSize( int preCount ,int currentCount){
        String pageSize = String.valueOf(preCount + currentCount);
        if(currentCount == 0 ||(currentCount > 0 && currentCount< 50) ){
            return pageSize;

        }
        else if(currentCount == 50) {
            return pageSize + "+";
        }
        else
            return pageSize;
    }

    public void getCountFromView(){
        for(int i =0 ;i <itemDetails.size();i++){
            if(itemDetails.get(i).has("Total Number") && itemDetails.get(i).isNull("Total Number")){

            }
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder  {

        TextView tvItemName,tvItemValue;
        ViewHolder(View itemView) {
            super(itemView);
            tvItemName = (TextView) itemView.findViewById(R.id.item_name);
            tvItemValue = (TextView) itemView.findViewById(R.id.item_value);


        }
    }

}
