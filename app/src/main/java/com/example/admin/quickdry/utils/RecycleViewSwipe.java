package com.example.admin.quickdry.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.model.CardSwipeDetails;


public class RecycleViewSwipe extends ItemTouchHelper.Callback implements DatabaseConstants , AppConstants{

    private final String TAG = getClass().getSimpleName();
    private Context context;

    private CardSwipeListener mSwipeCallBack;
    private CardSwipeDetails cardSwipeInstance;


    public RecycleViewSwipe(CardSwipeListener mSwipe, Context context) {
        this.mSwipeCallBack = mSwipe;
        this.context = context;
    }

    /*  @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }*/



    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

        int position = viewHolder.getAdapterPosition();
        if (mSwipeCallBack != null) {
            cardSwipeInstance = mSwipeCallBack.setDirectionFlags(position);
        }
        // return makeMovementFlags(dragFlags, swipeFlags);
        return makeMovementFlags(0, cardSwipeInstance.getSwipeDirection());
    }


    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        // mSwipe.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        if (mSwipeCallBack != null) {
            mSwipeCallBack.onCardSwipe(viewHolder, direction);
        }
    }



}

