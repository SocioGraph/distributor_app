package com.example.admin.quickdry.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class ShowCategoryAdapter extends RecyclerView.Adapter<ShowCategoryAdapter.SimpleViewHolder> {

    private final Context mContext;
    private ArrayList<String> sizeList;
    private String productName;

    public ShowCategoryAdapter(Context context, ArrayList<String> sizeList, String productName) {
        mContext = context;
        this.sizeList = sizeList;
        this.productName = productName;

    }

    public ShowCategoryAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(com.example.admin.daveai.R.layout.categorybreadcrump_row, parent, false);
        return new ShowCategoryAdapter.SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShowCategoryAdapter.SimpleViewHolder holder, final int position) {
        String sizeValue = "";
        if (holder.getAdapterPosition() == 0) {
            holder.productName.setVisibility(View.VISIBLE);
//            holder.productName.setText(productName + " > Selected Category > ");
            holder.productName.setText(productName + " > ");
        } else {
            holder.productName.setVisibility(View.GONE);
        }
        if (holder.getAdapterPosition() < sizeList.size() - 1) {
            sizeValue = sizeList.get(holder.getAdapterPosition()) + " , ";
        } else {
            sizeValue = sizeList.get(holder.getAdapterPosition());
        }
        Log.e("strarray", "onBindViewHolder: " + sizeValue);
        holder.selectedCategoryName.setText(sizeValue);
    }

    @Override
    public int getItemCount() {
        return sizeList.size();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView selectedCategoryName, productName;

        SimpleViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            selectedCategoryName = (TextView) view.findViewById(com.example.admin.daveai.R.id.selectedCategoryName);
            productName = (TextView) view.findViewById(com.example.admin.daveai.R.id.productName);

        }

        @Override
        public void onClick(View v) {


        }
    }
}


