package com.example.admin.quickdry.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.broadcasting.BluetoothService;
import com.example.admin.daveai.broadcasting.TCPSocketService;
import com.example.admin.daveai.broadcasting.DaveService;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dialogbox.ProgressLoaderDialog;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIHelper;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveAIStatic;
import com.example.admin.daveai.others.DaveException;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.dialogs.ConnectToDisplay;
import com.example.admin.quickdry.dialogs.DisplaySettings;
import com.example.admin.quickdry.dialogs.OrderCheckoutDialog;
import com.example.admin.quickdry.dialogs.ShareProductCatalogue;
import com.example.admin.quickdry.fragments.CustomerInteractions;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.storage.SharedPreference;
import com.example.admin.quickdry.utils.CRUDUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import static com.example.admin.daveai.others.DaveAIStatic.interactionPosition;
import static com.example.admin.daveai.others.DaveAIStatic.interactionStageTitle;


public class CustomerProfile extends BaseActivity implements CustomerInteractions.OnInteractionStageListener,
        DynamicForm.OnCreateDynamicFormListener, DisplaySettings.DisplaySettingCallback , AppConstants,ConnectToDisplay.ConnectToDisplayCallback {

    private final String TAG = getClass().getSimpleName();
    public final static int CUSTOMER_ORDER_CLICKED_REQUEST_CODE = 120;

    Context context;
    TabLayout tabLayout;
    public TextView showTitle;
    private ProgressBar progressBar;

    DaveAIPerspective daveAIPerspective;
    SharedPreference sharedPreference;
    Model modelInstance;
    ModelUtil modelUtil;
    CRUDUtil crudUtil;
    TCPSocketService tcpSocketService;

    private int tabPosition = 0 ;
    public static ArrayList<String> tabHeaderList;
    private String strCustomerId = "";
    public static String customerDetails;

    private String orderDetails = "";
    private String strOrderId ="";



    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getTheme().applyStyle(R.style.AppTheme, true);
        showTitle = findViewById(R.id.show_title);
        tabLayout = findViewById(R.id.tab_layout);
        progressBar = findViewById(R.id.progressBar);

        context= CustomerProfile.this;
        daveAIPerspective = DaveAIPerspective.getInstance();
        sharedPreference = new SharedPreference(this);
        modelInstance = new Model(this);
        modelUtil = new ModelUtil(this);
        crudUtil = new CRUDUtil(this);

        DaveAIStatic.defaultQuantityStatus = -1;

        tabHeaderList = new ArrayList<>();
        tabHeaderList.add("profile");
       // Log.e(TAG,"Print tabheaderlist:---------------"+modelUtil.getInteractionStageNameList());
        tabHeaderList.addAll(modelUtil.getInteractionStageNameList());

        sharedPreference.writeString(DISPLAY_PRODUCT_ID,"");


        try {
            Intent intent = getIntent();
            HashMap<String, Object> hashMap = (HashMap<String, Object>) intent.getSerializableExtra("CustomerData");
            if (hashMap != null) {
                if(hashMap.containsKey("customer_id") && hashMap.get("customer_id")!=null)
                    strCustomerId = hashMap.get("customer_id").toString();

                if(hashMap.containsKey("customerDetails") && hashMap.get("customerDetails")!=null)
                    customerDetails = hashMap.get("customerDetails").toString();

                if(hashMap.containsKey("openOrderId") && hashMap.get("openOrderId")!=null)
                    strOrderId = hashMap.get("openOrderId").toString();

                if(hashMap.containsKey("orderDetails") && hashMap.get("orderDetails")!= null) {
                    orderDetails = hashMap.get("orderDetails").toString() ;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        if (daveAIPerspective.getInvoice_model_name() != null) {
            postInteraction();
        }

        if(strOrderId!= null && !strOrderId.isEmpty()) {

            setOpenOrderIdInDaveHelper(strOrderId);
            if(orderDetails!=null && orderDetails.length()>0){  //navigate from orderList
                sharedPreference.writeString(AppConstants.CUSTOMER_ORDER_DETAILS,orderDetails);
                showTabView();

            }else{//navigate from Notification
                progressBar.setVisibility(View.VISIBLE);
                getInvoiceDetails(strOrderId, new InvioceDetailsCallback() {
                    @Override
                    public void onGetInvoiceDetails(JSONObject jsonObject) {
                        sharedPreference.writeString(AppConstants.CUSTOMER_ORDER_DETAILS,jsonObject.toString());
                        progressBar.setVisibility(View.GONE);
                        showTabView();
                    }
                    @Override
                    public void onFailedtInvoiceDetails(String responseMsg) {
                        progressBar.setVisibility(View.GONE);
                        showTabView();
                    }
                });
            }
        }else { //navigate from customerList, new SignUp
            progressBar.setVisibility(View.VISIBLE);
            checkOpenInvoiceId();
        }

        //todo testing interactions
      /*  DaveModels daveModels = new DaveModels(this,false);
        try {
            daveModels.getInteractions("MTU2MDc3NzU5OXJpbmtp", "rejected", new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {

                    Log.e(TAG,"Print final interactions objects :--- "+ jsonObject);

                }

                @Override
                public void onResponseFailure(int i, String s) {

                }
            },true,true,true);
        } catch (DaveException e) {
            e.printStackTrace();
        }*/

    }

    @Override
    public void onResume() {
        super.onResume();

        //Log.e(TAG,"*****************************On Resume****************************");
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        DynamicForm myFragment = (DynamicForm) fragmentManager.findFragmentByTag("EDIT_PROFILE");
        if (myFragment != null && myFragment.isVisible()) {
            setCurrentTabFragment(interactionPosition);
        }
        else {
           // Log.i(TAG,"Testing onBackPressed isTaskRoot :----------------"+ isTaskRoot());
            if(isTaskRoot()){
                Intent intent = new Intent(this,HomePage.class);
                startActivity(intent);
                finish();
            }else
                super.onBackPressed();
        }

        String communicationType =  sharedPreference.readString(DISPLAY_COMMUNICATION_TYPE);
        if(communicationType!=null && communicationType.equalsIgnoreCase("bluetooth")) {
            JSONObject jsonObject = new JSONObject();
            try {
                String userId = sharedPreference.readString(USER_ID);
                jsonObject.put("user_id",userId);
                jsonObject.put("status_update", "preview_stopped");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Get the message bytes and tell the BluetoothChatService to write
            BluetoothService bluetoothService = new BluetoothService(getApplicationContext());
            bluetoothService.sendMessgaeToPairedDevice(jsonObject.toString());
        }
    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer_profile, menu);

       if(tabPosition== 0){
           menu.findItem(R.id.connectToDisplay).setVisible(false);
           menu.findItem(R.id.displaySetting).setVisible(false);
       }else {
           menu.findItem(R.id.actionOrderList).setVisible(false);
           menu.findItem(R.id.actionNewOrder).setVisible(false);

       }

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.actionOrderList) {

            Intent intent = new Intent(this, DaveFunction.class);
            intent.putExtra(CUSTOMER_ID, strCustomerId);
            intent.putExtra(DaveFunction.DAVE_ACTION, DaveFunction.CUSTOMER_ORDER_LIST);
            startActivityForResult(intent, CustomerProfile.CUSTOMER_ORDER_CLICKED_REQUEST_CODE);

            return true;

        } else if (id == R.id.actionNewOrder) {
            crudUtil.createNewOrderId(strCustomerId, new CRUDUtil.OnCreateNewOderListener() {
                @Override
                public void onCreateNewOrder(String orderId, JSONObject orderDetails) {
                    Toast.makeText(context,"New Order created",Toast.LENGTH_LONG).show();
                    setCurrentTabFragment(0);
                    interactionStageTitle = String.valueOf(tabHeaderList.get(0));
                    interactionPosition = 0;

                }

                @Override
                public void onFailedCreateOrder(int requestCode, String responseMsg) {
                    Toast.makeText(context,requestCode +": "+responseMsg,Toast.LENGTH_LONG);

                }
            });
            return true;

        } else if (id == R.id.connectToDisplay) {
            // connect with pair device
            // tcp connection with server
            new ConnectToDisplay(this,this).show();
            return true;

        } else if (id == R.id.displaySetting) {
            ArrayList<String> roomList = sharedPreference.getList(DISPLAY_ROOM_LIST);
            ArrayList<String> categoryList = sharedPreference.getList(DISPLAY_CATEGORY_LIST);

            Log.e(TAG,"Print room List and CategoryList *******************"+ roomList +" CategoryList:- "+ categoryList);
            if ((roomList != null && roomList.size() > 0) || (categoryList != null && categoryList.size() > 0)) {
                DisplaySettings displaySettings = new DisplaySettings(this,this);
                 displaySettings.show();
            }
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void showTabView(){
        bindWidgetsWithAnEvent();
        setupTabLayout();
    }

    private void setupTabLayout() {
        if(tabHeaderList.size()>0)
            for (int i = 0; i < tabHeaderList.size(); i++) {
                tabLayout.addTab(tabLayout.newTab().setText(tabHeaderList.get(i)));
            }
    }


    private void bindWidgetsWithAnEvent() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition= tab.getPosition();
                setCurrentTabFragment(tab.getPosition());
                interactionStageTitle = String.valueOf(tab.getText());
                interactionPosition = tab.getPosition();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    public void setCurrentTabFragment(int tabPosition) {
        Fragment fragment= null;
        fragment = CustomerInteractions.newInstance(tabPosition, strCustomerId, tabHeaderList.get(tabPosition), customerDetails);
        if(fragment!=null){
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frame_container, fragment);
            //ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
        }

    }

    // DynamicForm fragment callback method
    @Override
    public void onCreateOrUpdateView(String objectDetails) {
        if(objectDetails!=null && !objectDetails.isEmpty()) {
            Toast.makeText(CustomerProfile.this, "Profile Update Successfully", Toast.LENGTH_SHORT).show();
            customerDetails = objectDetails;

            Log.e(TAG,"Get Profile update :----"+customerDetails);

            setCurrentTabFragment(interactionPosition);
        }

    }

    // DynamicForm fragment callback method
    @Override
    public void onFailureCreateOrUpdate(String errorMsg) {
        Log.e(TAG,"onFailureCreateOrUpdate Dynamic Form:----"+errorMsg);

    }

    // CustomerInteractions fragment callback method
    @Override
    public void switchFragment(Fragment fragment) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment,"EDIT_PROFILE");
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    // Call Back method  to get the Message form other Activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG,"onActivityResult:------------"+requestCode +" resultCode:------   "+resultCode);
        if (resultCode == RESULT_OK) { // Activity.RESULT_OK
            if (requestCode == CUSTOMER_ORDER_CLICKED_REQUEST_CODE) {
                HashMap<String, String> intentDetails = (HashMap<String, String>) data.getSerializableExtra("CustomerData");
                Log.e(TAG,"onActivityResult CUSTOMER_ORDER_CLICKED_REQUEST_CODE:-------------------"+intentDetails);
                setOpenOrderIdInDaveHelper(intentDetails.get("openOrderId"));

            }else {
                Log.e(TAG,"onActivityResult1 REQUESTCODE DIDN'T MATCH :------------"+requestCode);
            }
        }else {
            Log.e(TAG,"onActivityResult1 Activity.RESULT_CANCEL :------------"+requestCode +" resultCode:------"+resultCode);
        }

    }

    boolean cancelled = false;
    private Dialog dialog;

    // CustomerInteractions fragment callback method
    @Override
    public void onClickDynamicButtonAdded(String buttonName, String btnAction, HashMap<String, Object> buttonDetails) {

        Log.e("onDynamicButton"," btnAction msg:-------"+btnAction +" buttonDetails:--"+buttonDetails);
        switch (btnAction) {
            case "_order_checkout":
                String currentOrderId =  sharedPreference.readString(strCustomerId);
                Log.e(TAG,"Check Current Order Id:---------   "+currentOrderId);
                if (currentOrderId != null) {
                    OrderCheckoutDialog cdd = new OrderCheckoutDialog(context, currentOrderId, true,
                        new OrderCheckoutDialog.OrderCheckoutListener() {
                        @Override
                        public void onCheckoutOrder(JSONObject orderDetails) {

                        }

                        @Override
                        public void onCancelOrder() {

                        }
                    });
                    cdd.show();
                }else
                    Toast.makeText(context,"Please create New Order",Toast.LENGTH_SHORT).show();
                break;

            case "_scan_product":
                if(buttonDetails!=null) {
                    String customerId = buttonDetails.get("customer_id").toString();
                    Intent intent1 = new Intent(context, AddScanProduct.class);
                    intent1.putExtra("customer_id",customerId);
                    intent1.putExtra("action_name",AddScanProduct.ACTION_SCAN_PRODUCT);
                    startActivity(intent1);

                }
                break;

            case "_share":
                if(buttonDetails!=null) {
                    Log.e(TAG,"clicked share!!!");
                    String cOrderId =  sharedPreference.readString(strCustomerId);
                    if (cOrderId != null) {
                        OrderCheckoutDialog cdd = new OrderCheckoutDialog(CustomerProfile.this, cOrderId,
                                new OrderCheckoutDialog.OrderCheckoutListener() {
                                    @Override
                                    public void onCheckoutOrder(JSONObject orderDetails) {
                                        long queueCount = DatabaseManager.getInstance(CustomerProfile.this).forceUpdateQueueSize(DatabaseConstants.APIQueuePriority.ONE.getValue());

                                        Log.e(TAG,"share>> on order checkout");

                                        if(queueCount== 0){
                                            ShareProductCatalogue cSPD = new ShareProductCatalogue(context,buttonDetails);
                                            cSPD.show();

                                        }else {
                                            ProgressLoaderDialog progressLoaderDialog = new ProgressLoaderDialog(context);
                                            dialog = progressLoaderDialog.progressDialog();
                                            progressLoaderDialog.setMessage("Sync is in progress please wait.");
                                            dialog.show();
                                            dialog.setCancelable(true);
                                            dialog.setCanceledOnTouchOutside(true);
                                            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                                @Override
                                                public void onDismiss(DialogInterface dialog) {
                                                    cancelled = true;
                                                }
                                            });
                                            CountDownTimer countDownTimer = new CountDownTimer(60000, 30000) {

                                                public void onTick(long millisUntilFinished) {

                                                    Log.e(TAG,"share>> waiting timer onTick() "+millisUntilFinished+" ms left");
                                                }

                                                public void onFinish() {
                                                    Log.e(TAG,"share>> waiting countdown timer onFinish()");
                                                    dialog.dismiss();
                                                    Toast.makeText(CustomerProfile.this,R.string.share_msg,Toast.LENGTH_LONG).show();
                                                }
                                            }.start();

                                            new DaveService().startBackgroundService(CustomerProfile.this);
                                            DatabaseManager.getInstance(CustomerProfile.this).alertOnAPIQueueIsEmpty(new DatabaseManager.OnAPIQueueEmptyListener() {
                                                @Override
                                                public void isEmpty() {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {

                                                            Log.e(TAG,"share>> queue is empty and cancelled is == "+cancelled);
                                                            if(!cancelled) {
                                                                ShareProductCatalogue cSPD = new ShareProductCatalogue(context, buttonDetails);
                                                                cSPD.show();
                                                                dialog.dismiss();
                                                                countDownTimer.cancel();
                                                            }
                                                        }
                                                    });
                                                }
                                            },5000, DatabaseConstants.APIQueuePriority.ONE.getValue());



                                            /*Toast.makeText(CustomerProfile.this,R.string.share_msg,Toast.LENGTH_LONG).show();*/
                                        }
                                    }

                                    @Override
                                    public void onCancelOrder() {

                                    }
                                });
                        cdd.show();
                    }else
                        Toast.makeText(CustomerProfile.this,"Please create New Order",Toast.LENGTH_SHORT).show();




                }
                break;

            case "_form_view":
                if(buttonDetails!=null) {
                    checkCurrentOrderStatus();
                    //createNewOrderId();
                }
                break;
            case "_search_by_image":
                if(buttonDetails!=null) {
                    String customerId = buttonDetails.get("customer_id").toString();
                    Intent intent1 = new Intent(context, DaveFunction.class);
                    intent1.putExtra("customer_id",customerId);
                    intent1.putExtra(DaveFunction.DAVE_ACTION, DaveFunction.SEARCH_BY_IMAGE_ACTION);
                    startActivity(intent1);

                }
                break;
        }

    }



    @Override
    public void OnConnectTCPSocket() {
         String communicationType =  sharedPreference.readString(DISPLAY_COMMUNICATION_TYPE);
         if( communicationType!= null && communicationType.equalsIgnoreCase("intranet")) {
             int serverPort= sharedPreference.readInteger(TCP_SERVER_PORT);
             String ipAddress = sharedPreference.readString(INTRANET_IP_ADDRESS);
             Log.e(TAG,"Connecting with TCP SOCKET :---  "+ ipAddress +" ServerPort:- "+ serverPort);
             if (ipAddress != null && !ipAddress.isEmpty()) {
                tcpSocketService = new TCPSocketService(this);
                tcpSocketService.connectToTCPServerSocket(ipAddress,serverPort);
             } else {
                Toast.makeText(this, "Please provide IP Address.", Toast.LENGTH_SHORT).show();
             }
         }

    }

    @Override
    public void onExpandView(String productId, JSONObject productDetails) {
        sendMessageToDisplay();
    }

    @Override
    public void OnDisplaySettingSubmit() {
        sendMessageToDisplay();
    }

    private void sendMessageToDisplay(){
        try {
            String productId = sharedPreference.readString(DISPLAY_PRODUCT_ID);
            String userId = sharedPreference.readString(USER_ID);
            String  communicationType =  sharedPreference.readString(DISPLAY_COMMUNICATION_TYPE);
            //Log.e(TAG,"Print type of communication***************  "+ communicationType + " productID:- "+ productId);
            if(communicationType!=null && productId!=null && !productId.isEmpty()) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id",userId);
                jsonObject.put("product_id", productId);
                //jsonObject.put("product_details",productDetails);

                switch (communicationType) {
                    case "bluetooth":

                        String room = sharedPreference.readString(DISPLAY_ROOM);
                        if(room == null || room.isEmpty())
                            jsonObject.put("room", JSONObject.NULL);
                        else
                            jsonObject.put("room", room);

                        String category = sharedPreference.readString(DISPLAY_CATEGORY);
                        if(category == null || category.isEmpty() )
                            jsonObject.put("category", JSONObject.NULL);
                        else
                            jsonObject.put("category", category);

                        Log.e(TAG, " Print bluetooth msg before send :-------------------- " + jsonObject);

                        BluetoothService bluetoothService = new BluetoothService(this);
                        bluetoothService.sendMessgaeToPairedDevice(jsonObject.toString());

                        break;
                    case "intranet":

                        jsonObject.put("laying_option", sharedPreference.readString(DISPLAY_LAYOUT));
                        jsonObject.put("texture_quality", sharedPreference.readString(DISPLAY_TEXTURE));

                        Log.e(TAG, "Print sent msg through intranet :" + jsonObject);
                        tcpSocketService.sendMessageFromTCPClient(jsonObject.toString());
                        break;
                }
            }else {
                Log.e(TAG,"Product id/ communication type is null or empty don't send message************ productID : "+ productId);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error Send Message To Display :-- "+ e.getMessage());
        }

    }

    private  void checkCurrentOrderStatus(){

        String currentOrderId = sharedPreference.readString(strCustomerId);
        String userId = sharedPreference.readString(SharedPreference.USER_ID);
        Log.e(TAG,"checkCurrentOrderStatus Open Order Id>>>>>>>>>>"+currentOrderId+" "+ strCustomerId);
        if (currentOrderId != null) {
           // dialogToAskContinueWithPreviousOrder();
            getInvoiceDetails(currentOrderId, new InvioceDetailsCallback() {
                @Override
                public void onGetInvoiceDetails(JSONObject jsonObject) {
                    dialogToAskContinueWithPreviousOrder(currentOrderId,jsonObject);
                }

                @Override
                public void onFailedtInvoiceDetails(String responseMsg) {

                }
            });

        }else{
            if (strCustomerId != null && userId != null) {
               /* createNewOrderId(new OnCreateNewOderListener() {
                    @Override
                    public void onCreateNewOrder(String orderId) {
                        intentToFormView();
                    }
                });*/

                crudUtil.createNewOrderId(strCustomerId, new CRUDUtil.OnCreateNewOderListener() {
                    @Override
                    public void onCreateNewOrder(String orderId, JSONObject orderDetails) {
                        Toast.makeText(context,"New Order created",Toast.LENGTH_LONG);
                        intentToFormView();
                    }

                    @Override
                    public void onFailedCreateOrder(int requestCode, String responseMsg) {

                    }
                });
            }
        }
    }

    private void intentToFormView(){
        //Intent intentFV = new Intent(CustomerProfile.this, QuickOrderHierarchy.class);
        Intent intentFV = new Intent(CustomerProfile.this, FormView.class);
        intentFV.putExtra("NewCustomerId",strCustomerId);
        startActivity(intentFV);
    }



    private interface InvioceDetailsCallback{
        void onGetInvoiceDetails(JSONObject jsonObject);
        void onFailedtInvoiceDetails(String responseMsg);
    }
    public void getInvoiceDetails(String orderId, InvioceDetailsCallback invioceDetailsCallback){

        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                Log.e("OrderClosed"," After Order Closed  update data:-------   "+response);


                if(invioceDetailsCallback!=null)
                    invioceDetailsCallback.onGetInvoiceDetails(response);
            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                if(invioceDetailsCallback!=null)
                    invioceDetailsCallback.onFailedtInvoiceDetails(responseMsg);
            }
        };
        try {
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                DaveModels daveModels = new DaveModels(context, true);
                daveModels.getObject(daveAIPerspective.getInvoice_model_name(), orderId, daveAIListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("","Error During update Order Details:-------------  "+e.getMessage());
        }


    }

    private void dialogToAskContinueWithPreviousOrder(String orderId,JSONObject orderDetails){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(CustomerProfile.this);

        builder1.setMessage("Do you want to continue with previous Order?");
        builder1.setCancelable(false);
        try {
            if(orderDetails.has(daveAIPerspective.getInvoice_date_attr())){
            builder1.setTitle("Ordered created on "+orderDetails.getString(daveAIPerspective.getInvoice_date_attr()));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        builder1.setPositiveButton("Continue", (dialog, id) -> {
            intentToFormView();

        });

        builder1.setNegativeButton("Abandon", (dialog, id) -> {
            //deletePreviousOrderId();

            deleteInteractionsOfOrder(orderId);
            dialog.cancel();

        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    ProgressDialog pDialog;
    private void deleteInteractionsOfOrder(String currentOrderId){
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
        ModelUtil modelUtil = new ModelUtil(context);
        DaveModels daveModels = new DaveModels(context, true);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    if (response != null && response.length() > 0 && response.has("data") && response.getJSONArray("data").length() > 0) {
                        JSONArray jsonArr = response.getJSONArray("data");
                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObject = jsonArr.getJSONObject(i);
                            // Log.e(TAG, "Print Jsonobject which has qty:- :---------   " + jsonObject);
                            // String interactionId = jsonObject.getString(modelUtil.getInteractionIdAttrName());
                            daveModels.deleteObject(modelUtil.getInteractionModelName(), jsonObject.getString(modelUtil.getInteractionIdAttrName()),
                                    null);
                        }
                    }
                    deletePreviousOrderId();

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error show Qty for productId:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                if (pDialog!=null && pDialog.isShowing())
                    pDialog.dismiss();

            }
        };
        try {
            if(modelUtil.getInteractionModelName()!= null && !modelUtil.getInteractionModelName().isEmpty()) {
                HashMap<String, Object> queryParam = new HashMap<>();
                queryParam.put(daveAIPerspective.getInvoice_id_attr_name(),currentOrderId);
                Log.e(TAG, "call get interaction Jsonobject :- :---------   "+queryParam);
                daveModels.getObjects(new ModelUtil(context).getInteractionModelName(), queryParam, daveAIListener, false, false);
            }else {
                Log.e(TAG, "interaction model is empty or null :- :---------   " +modelUtil.getInteractionModelName());
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error during get Interactions of order:-------------  "+e.getMessage());
        }


    }


    private void deletePreviousOrderId(){
        String currentOrderId = sharedPreference.readString(strCustomerId);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                //Log.e(TAG,"After Deleting Previous Order Id Response:-----------"+response);
                sharedPreference.removeKey( strCustomerId);
                crudUtil.createNewOrderId(strCustomerId, new CRUDUtil.OnCreateNewOderListener() {
                    @Override
                    public void onCreateNewOrder(String orderId, JSONObject orderDetails) {
                        if (pDialog!=null && pDialog.isShowing())
                            pDialog.dismiss();
                        Toast.makeText(context,"New Order created",Toast.LENGTH_LONG);
                        intentToFormView();
                    }

                    @Override
                    public void onFailedCreateOrder(int requestCode, String responseMsg) {
                        if (pDialog!=null && pDialog.isShowing())
                            pDialog.dismiss();
                    }
                });

            }
            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                if (pDialog!=null && pDialog.isShowing())
                    pDialog.dismiss();

            }
        };
        DaveModels daveModels = new DaveModels(context,false);
        daveModels.deleteObject(daveAIPerspective.getInvoice_model_name(), currentOrderId, daveAIListener);

    }

    private void checkOpenInvoiceId(){
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    if(response!= null && response.has("data") && !response.isNull("data") && response.getJSONArray("data").length()>0 ){
                        JSONArray jsonArray = response.getJSONArray("data");
                        JSONObject jsonOrderDetails = new JSONObject(jsonArray.get(0).toString());
                        Log.e(TAG," Customer Profile  Get  Open Order Details :-  "+jsonOrderDetails);
                        String openOrderId = jsonOrderDetails.getString(daveAIPerspective.getInvoice_id_attr_name());
                        if (openOrderId != null && !openOrderId.isEmpty()) {
                            sharedPreference.writeString(strCustomerId, openOrderId);
                            sharedPreference.writeString(AppConstants.CUSTOMER_ORDER_DETAILS,jsonOrderDetails.toString());
                            setOpenOrderIdInDaveHelper(openOrderId);

                            progressBar.setVisibility(View.GONE);
                            showTabView();
                        }
                    }
                    else { //check previous closed order
                        progressBar.setVisibility(View.GONE);
                        sharedPreference.writeString(AppConstants.CUSTOMER_ORDER_DETAILS,"");
                        showTabView();
                        DaveAIListener daveAIListener = new DaveAIListener() {
                            @Override
                            public void onReceivedResponse(JSONObject response) {
                                try {
                                    if(response!= null && response.has("data") && !response.isNull("data")){
                                        JSONArray jsonArray = response.getJSONArray("data");
                                        if(jsonArray!=null && jsonArray.length()>0) {
                                            try {
                                                JSONObject getResponse = new JSONObject(jsonArray.get(0).toString());
                                                String previousOrderId = getResponse.getString(daveAIPerspective.getInvoice_id_attr_name());
                                                DaveAIHelper setData = new DaveAIHelper();
                                                if (previousOrderId != null && !previousOrderId.isEmpty()) {
                                                    setData.setPreOrderId(strCustomerId, previousOrderId);
                                                    setData.setCurrentOrderId(strCustomerId,"");
                                                    if(daveAIPerspective.getInvoice_id_attr_name()!=null)
                                                        setData.addInteractionFilterAttr(daveAIPerspective.getInvoice_id_attr_name(), previousOrderId);
                                                }
                                            } catch (JSONException e) {
                                                Log.e(TAG,"Error Get Previous Closed OrderId :-  "+e.getMessage());
                                            }

                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e(TAG,"Error during getting close order status:-------------  "+e.getMessage());
                                }

                            }

                            @Override
                            public void onResponseFailure(int requestCode, String responseMsg) {

                            }
                        };
                        DaveModels daveModels = new DaveModels(CustomerProfile.this, true);
                        daveModels.getObjects(daveAIPerspective.getInvoice_model_name(), invoiceQueryParam(true), daveAIListener, false, false);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            Log.e(TAG, "onCustomerSelected customer Id Value*****  "+strCustomerId + " && Invoice model name:- " +daveAIPerspective.getInvoice_model_name());
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                DaveModels daveModels = new DaveModels(CustomerProfile.this, true);
                daveModels.getObjects(daveAIPerspective.getInvoice_model_name(), invoiceQueryParam(false), daveAIListener, false, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error during getting Open order status:-------------  "+e.getMessage());
        }
    }

    private HashMap<String,Object> invoiceQueryParam( boolean orderClosed){

        HashMap<String,Object> queryParam = new HashMap<>();
        queryParam.put(modelUtil.getCustomerIdAttrName(),strCustomerId);
        if(daveAIPerspective.getInvoice_close_attr_name()!=null)
            queryParam.put(daveAIPerspective.getInvoice_close_attr_name(), orderClosed);
        queryParam.put("_page_size", 1);
        if(daveAIPerspective.getInvoice_date_attr()!=null)
            queryParam.put("_sort_by",daveAIPerspective.getInvoice_date_attr());
        queryParam.put("_sort_reverse", Boolean.TRUE.toString());

        Log.e(TAG,"invoiceQueryParam:--------"+queryParam);

        return queryParam;
    }

    private void setOpenOrderIdInDaveHelper(String openOrderId){

        Log.i(TAG," ****************************************setOpenOrderIdInDaveHelper>>>>>>>>>>>>>>>  "+openOrderId);
        sharedPreference.writeString(strCustomerId, openOrderId);
        DaveAIHelper setData = new DaveAIHelper();
        setData.setPreOrderId(strCustomerId, openOrderId);
        setData.setCurrentOrderId(strCustomerId, openOrderId);
        if(daveAIPerspective.getInvoice_id_attr_name()!=null)
            setData.addInteractionFilterAttr(daveAIPerspective.getInvoice_id_attr_name(),openOrderId);
    }

    public  void postInteraction() {
        DaveAIHelper.BeforePostInteractionListener d = new DaveAIHelper.BeforePostInteractionListener() {
            @Override
            public void beforePostInteraction(final Context context, final String retailerId, final DaveAIHelper.PostInteractionListener piListener) {

                String currentOrderId = sharedPreference.readString(retailerId);
                String userId = sharedPreference.readString( SharedPreference.USER_ID);

                Log.e(TAG," Check Current Order Id during Post Interaction:-"+currentOrderId+" &&RetailerID:-"+retailerId+" &&UserID:-"+userId);
                if (currentOrderId != null) {
                    piListener.postInteraction();

                }else {
                    if (retailerId != null && userId != null) {

                        crudUtil.createNewOrderId(strCustomerId, new CRUDUtil.OnCreateNewOderListener() {
                            @Override
                            public void onCreateNewOrder(String orderId, JSONObject orderDetails) {
                                piListener.postInteraction();
                            }

                            @Override
                            public void onFailedCreateOrder(int requestCode, String responseMsg) {

                            }
                        });

                    }
                }
            }
        };
        DaveAIHelper.setBeforePostInteractionListener(d);
    }




}

