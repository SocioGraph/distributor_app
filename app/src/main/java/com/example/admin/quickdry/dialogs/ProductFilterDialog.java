package com.example.admin.quickdry.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


import com.example.admin.daveai.model.Model;
import com.example.admin.quickdry.activity.DaveFunction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ProductFilterDialog  extends Dialog {//implements android.view.View.OnClickListener {

   // private Activity c;
    private Context mContext;
    public Dialog d;
    private ListView mListView;
    private Button buttonCancel,buttonOk;
    ArrayAdapter<String> mAdapter;
    private HashMap<String, Object> intentDetails;
    private HashMap<String, String> productDetails;
    private HashMap<String, String> attrsTitleName = new HashMap<>();
    boolean closeActivity = false;

 /*   public ProductFilterDialog(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }*/

    public ProductFilterDialog(Context mContext, HashMap<String, Object> intentDetails, HashMap<String, String> productDetails,
                               boolean closeActivity) {
        super(mContext);
        this.mContext = mContext;
        this.intentDetails=intentDetails;
        this.productDetails=productDetails;
        this.closeActivity=closeActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(com.example.admin.daveai.R.layout.change_flt_attr_dailog);

        buttonOk = (Button) findViewById(com.example.admin.daveai.R.id.btn_ok);
        buttonCancel = (Button) findViewById(com.example.admin.daveai.R.id.btn_cancel);
        mListView = (ListView)findViewById(com.example.admin.daveai.R.id.list_view);
        mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);


        Model productModel = Model.getModelInstance(mContext, "product");
        if(productDetails!=null &&!productDetails.isEmpty() ){
            ArrayList<String> filterAttrs = new ArrayList<>();
            filterAttrs.add("All");
            for (Map.Entry<String, String> entry : productDetails.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if(productModel!=null){
                    //filterAttrs.add(entry.getKey()+" - "+entry.getValue());
                    filterAttrs.add(productModel.getAttributeTitle(entry.getKey())+" - "+entry.getValue());
                    attrsTitleName.put(productModel.getAttributeTitle(entry.getKey()),entry.getKey());
                }

            }
            mAdapter = new ArrayAdapter<>(mContext, com.example.admin.daveai.R.layout.checked_textview, filterAttrs);
            mListView.setAdapter(mAdapter);
        }else{
            Toast.makeText(mContext,"Product details is null.",Toast.LENGTH_SHORT).show();
        }

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        buttonOk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                HashMap<String, String> selectedItems = new HashMap<>();
                SparseBooleanArray checked = mListView.getCheckedItemPositions();
                for (int i = 0; i < checked.size(); i++) {
                    int item_position = checked.keyAt(i);
                    if(item_position !=0 && checked.valueAt(i)){
                        System.out.println("**************item_position**********"+ item_position );
                        String getCheckedAttr = mAdapter.getItem(item_position);
                        if (getCheckedAttr != null) {
                            String[] parts = getCheckedAttr.split(" - ");
                            selectedItems.put(attrsTitleName.get(parts[0]), parts[1]);
                        }
                    }
                }
                intentDetails.put("productDetails",selectedItems);
                System.out.println("************** HashMap<String, Object> intentDetails**********"+ intentDetails );
                if(checked.size()>0){
                    Intent intent = new Intent(mContext, DaveFunction.class);
                    intent.putExtra("msg_to_dave_func", intentDetails);
                    mContext.startActivity(intent);
                    if(closeActivity)
                        ((DaveFunction) mContext).finish();
                    dismiss();
                }else
                    Toast.makeText(mContext,"Please select from above.",Toast.LENGTH_SHORT).show();
                //dismiss();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                    if (mListView.isItemChecked(0)) {
                        for(int i=1 ;i<mListView.getAdapter().getCount();i++){
                            mListView.setItemChecked(i, true);
                        }
                    }else{
                        for(int i=1 ;i<mListView.getAdapter().getCount();i++){
                            mListView.setItemChecked(i, false);
                        }
                    }
                }else{
                    if (mListView.isItemChecked(0))
                        mListView.setItemChecked(0,false);
                }

            }
        });
    }

}
