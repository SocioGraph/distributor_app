package com.example.admin.quickdry.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.adapter.CustomerAdapter;
import com.example.admin.quickdry.utils.CRUDUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;



public class CustomerList extends Fragment implements CategoryFilter.OnCategoryFilterListener{

        private final String TAG = getClass().getSimpleName();
        private static final String FRAGMENT_TITLE = "fragment_title";

        private Context context;
        private RecyclerView mRecyclerView;
        LinearLayoutManager mLayoutManager ,shLayoutManager;
        EditText editTextSearch;
        ImageView searchViewIcon,searchButton;
        Button cancelBtn;
        RelativeLayout searchView;
        LinearLayout customerParentLayout,objectsListView;
        View includeSearchLayout;
        TextView resultNotFound,subHeaderTitle,subHeaderCount;
        LinearLayout  menuItemActionLayout,breadCrumbsLayout;
        ListView filterParentView,filterChildView;
        HorizontalScrollView breadcrumbWrapper;
        CrystalRangeSeekbar rangeSeekbar;
        TextView priceMin, priceMax;
        View includeSubHeader;
        ProgressBar progressBar;
        SwipeRefreshLayout mSwipeRefreshLayout;
        FrameLayout frameLayout;

        ArrayAdapter<String> filterParentAdapter,filterChildAdapter;
        private CustomerAdapter mAdapter;

        public Model customerModelInstance;
        private DaveAIPerspective daveAIPerspective;
        ModelUtil modelUtil;
        CategoryFilter childFragment;

        //pagination
        private int mPageNumber;
        String strDefaultCount = "0";
        int pastVisiblesItems, visibleItemCount, totalItemCount;
        private boolean isPageLoading = true;
        private boolean isLastPage =false;

        boolean isAnyActionApply = false;
        private String strSearchTerm;

        //filter variables
        HashMap<String,ArrayList<String>> defaultFilterList = new HashMap<>();
        HashMap<String, String> filterAttrTitleMap = new HashMap<>();
        ArrayList<String> filterTitleList = new ArrayList<>();
        HashMap<String, Object> selectedFilterMap ;//= new HashMap<>();
        int filterTitlePosition;



    public CustomerList() {
        //  An empty constructor for Android System to use, otherwise exception may occur.
    }

    private CustomerSelectedListener customerSelectedCallback;
    public interface CustomerSelectedListener {
        void onCustomerSelected(String customerId, String customerTitle, JSONObject customerDetails);

    }

    public static CustomerList newInstance(String customerListTitle) {
        CustomerList fragment = new CustomerList();
        Bundle args = new Bundle();
        args.putString(FRAGMENT_TITLE, customerListTitle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CustomerSelectedListener) {
            customerSelectedCallback = (CustomerSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement CustomerSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            String strTitle = getArguments().getString(FRAGMENT_TITLE);
        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_recyclelist_view, container, false);

        context = getActivity();
        customerParentLayout =  mainView.findViewById(R.id.customerParentLayout);
        objectsListView =  mainView.findViewById(R.id.objectListLayout);
        resultNotFound =   mainView.findViewById(R.id.noSearchFound);
        //resultNotFound =   resultNotFoundLayout.findViewById(R.id.noResultFound);


        mRecyclerView =  mainView.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
       // mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new CustomerAdapter(customerSelectedCallback,getActivity(), new ArrayList<>());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.scrollToPosition(pastVisiblesItems);

        searchView = mainView.findViewById(R.id.search_view);
        searchViewIcon = mainView.findViewById(R.id.searchViewIcon);

        includeSearchLayout =  mainView.findViewById(R.id.includeSearchLayout);
        includeSearchLayout.setVisibility(View.GONE);
        editTextSearch = includeSearchLayout.findViewById(R.id.editTextSearch);
        cancelBtn = includeSearchLayout.findViewById(R.id.cancelButton);
        searchButton = includeSearchLayout.findViewById(R.id.searchButton);

        //  subHeader initialization
        includeSubHeader = mainView.findViewById(R.id.includeSubHeader);
        subHeaderTitle = includeSubHeader.findViewById(R.id.item_name);
        subHeaderCount = includeSubHeader.findViewById(R.id.item_value);
        subHeaderTitle.setText("Total Number");

        breadcrumbWrapper = mainView.findViewById(R.id.breadcrumbWrapper);
        breadcrumbWrapper.setVisibility(View.GONE);
        breadCrumbsLayout =  mainView.findViewById(R.id.breadCrumbsLayout);
        menuItemActionLayout =  mainView.findViewById(R.id.menuItemActionLayout);

        frameLayout =  mainView.findViewById(R.id.frameLayout);
        progressBar =  mainView.findViewById(R.id.progressBar);
        mSwipeRefreshLayout = mainView.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,//R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        daveAIPerspective = DaveAIPerspective.getInstance();
        modelUtil = new ModelUtil(context);
        customerModelInstance = Model.getModelInstance(context,modelUtil.getCustomerModelName());

        childFragment = CategoryFilter.newInstance(modelUtil.getCustomerModelName(),daveAIPerspective.getCustomer_category_hierarchy(),
                modelUtil.getCustomerCategoryHierarchy(),null);
        Log.e(TAG,"Print Catgeory Filter instance1:----------"+childFragment);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.child_fragment_container, childFragment,"CategoryFilter").commit();
        childFragment.registerCategoryFilterCallback(CustomerList.this);

        return mainView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        Objects.requireNonNull(getActivity()).setTitle(daveAIPerspective.getMenu_title_for_customer_list());
        try {
            defaultFilterList = modelUtil.getCustomerFilterAttrList();
            if (defaultFilterList != null && !defaultFilterList.isEmpty()) {
                for (Map.Entry<String, ArrayList<String>> entry : defaultFilterList.entrySet()) {
                    if (customerModelInstance != null)
                        filterAttrTitleMap.put(customerModelInstance.getAttributeTitle(entry.getKey()), entry.getKey());
                }
                filterTitleList.addAll(filterAttrTitleMap.keySet());
            }

        }catch (Exception e){
            e.printStackTrace();
        }





        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                progressBar.setVisibility(View.GONE);
                if(mSwipeRefreshLayout != null ) {
                    if(!isAnyActionApply) {
                        mSwipeRefreshLayout.setRefreshing(true);
                        mPageNumber = 1;
                        selectedFilterMap.clear();
                        childFragment.resetCategoryHierarchy();
                        getCustomersList(mPageNumber, true, true);
                    }else {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (isPageLoading  && CRUDUtil.checkNextPage(mAdapter.getItemCount()) && (visibleItemCount + pastVisiblesItems) >= totalItemCount ) {
                        isPageLoading = false;
                        mPageNumber++;
                        //Do pagination.. i.e. fetch new data from Server
                        if(isAnyActionApply){
                            getCustomersList(mPageNumber,false,false);
                        }else {
                            getCustomersList(mPageNumber,false,true);
                        }

                    }
                }
            }
        });

        searchViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setVisibility(View.GONE);
                includeSearchLayout.setVisibility(View.VISIBLE);
                strSearchTerm = "";
               // mAdapter.notifyDataSetChanged();
                strDefaultCount = subHeaderCount.getText().toString();

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                includeSearchLayout.setVisibility(View.GONE);
                searchView.setVisibility(View.VISIBLE);
                resultNotFound.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.VISIBLE);
                editTextSearch.setText("");
                strSearchTerm = "";
                isAnyActionApply = false;
                hideSoftKeyword(v);
                mAdapter.resetPreviousCustomerList();
                subHeaderCount.setText(strDefaultCount);
            }
        });


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyword(v);
                actionOnSearchButton();

            }
        });

        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideSoftKeyword(editTextSearch);
                    actionOnSearchButton();
                    return true;
                }
                return false;
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {
            boolean searchedFlag = false;
            Timer timer;
            Handler handler;

            public void afterTextChanged(Editable s) {
                //String searchedTerm = s.toString().trim();
                strSearchTerm = s.toString().trim();
                timer = new Timer();
                handler = new Handler();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                //do Stuff here
                                if(strSearchTerm.length() > 0) {
                                    mPageNumber = 1;
                                    getListBasedOnSearch();
                                }
                            }
                        });

                    }
                }, 2000); // 2000ms delay before the timer executes the „run“ method from TimerTask
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {
               // String searchedTerm = query.toString().trim();
                strSearchTerm = query.toString().trim();
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
                if(!searchedFlag && strSearchTerm.length() > 2){
                    searchedFlag= true;
                    mPageNumber = 1;
                    getListBasedOnSearch();

                }
            }
        });



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onResume(){
        super.onResume();

        Log.i(TAG,"onResume>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        mPageNumber =1;
        isAnyActionApply = false;
        strSearchTerm = "";
        filterTitlePosition = 0;
        selectedFilterMap = new HashMap<>();
        includeSearchLayout.setVisibility(View.GONE);
        searchView.setVisibility(View.VISIBLE);
        subHeaderCount.setText("");

        if(childFragment.selectedHierarchyMap!=null && childFragment.selectedHierarchyMap.size()>0){
            selectedFilterMap.putAll(childFragment.selectedHierarchyMap);
           // selectedFilterMap = childFragment.selectedHierarchyMap;
        }
        getCustomersList(mPageNumber,true,true);


     /*   HashMap<String, String> intent_details = new HashMap<>();
        intent_details.put("customer_id", "");
        intent_details.put("product_id", "");
        intent_details.put("Dave_function", "");

        Intent serviceIntent = new Intent(getActivity(), DaveIconService.class);
        serviceIntent.putExtra("message_to_dave", intent_details);
        getActivity().startService(serviceIntent);*/
    }

    @Override
    public void onPause(){
        super.onPause();
        //getActivity().stopService(new Intent(getActivity(), DaveIconService.class));
    }


    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        customerSelectedCallback = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dave_function, menu);
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.actionSearch).setVisible(false);
        menu.findItem(R.id.actionCategory).setVisible(false);
        menu.findItem(R.id.actionSort).setVisible(false);

        if (defaultFilterList == null || defaultFilterList.isEmpty()) {
            menu.findItem(R.id.actionFilter).setVisible(false);
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        menuItemActionLayout.removeAllViews();
        if (id == R.id.actionSearch) {
           // addNewDataFlag=true;
            //actionOnSearchMenuButton();
            return true;

        } else if (id == R.id.actionFilter) {
            actionOnFilterMenuButton();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }


    private void actionOnSearchButton(){
        strSearchTerm = editTextSearch.getText().toString().trim();
        if(strSearchTerm.length()>0) {
            mPageNumber = 1;
            getListBasedOnSearch();
        }
        else
            Toast.makeText(getActivity(),"Please Enter Search Term",Toast.LENGTH_SHORT).show();
    }
    private void getListBasedOnSearch(){
        isAnyActionApply = true;
        selectedFilterMap.clear();
        getCustomersList(mPageNumber, true, false);
    }

    private void getCustomersList(int mPageNumber, boolean isNewList, boolean isUpdateDefault ){
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(false);
                    Log.e(TAG,"getCustomersList  Total Customer List count :- "+response.getJSONArray("data").length());
                    if( response!= null && response.has("data") && response.getJSONArray("data").length()>0){

                        mRecyclerView.setVisibility(View.VISIBLE);
                        frameLayout.setVisibility(View.VISIBLE);
                        resultNotFound.setVisibility(View.GONE);
                        JSONArray jsonArray = response.getJSONArray("data");
                        showSubHeaderDetails(subHeaderCount.getText().toString(),jsonArray.length());
                        List<JSONObject> objectData = new ArrayList<>();

                        for(int i =0 ;i< jsonArray.length();i++){
                            objectData.add(jsonArray.getJSONObject(i));
                        }
                        if(isNewList) {
                            mAdapter.addNewSeedList(objectData,isUpdateDefault);
                        }
                        else {
                            mAdapter.updateSeedList(objectData,isUpdateDefault);
                        }

                        isPageLoading = true;
                        // mRecyclerView.scrollToPosition(pastVisiblesItems);
                        //mAdapter.notifyDataSetChanged();

                    }else {
                        Log.i(TAG,"Print data not found:-----------"+mAdapter.getItemCount());
                        if(mAdapter.getItemCount() == 0 ){
                            frameLayout.setVisibility(View.GONE);
                            mRecyclerView.setVisibility(View.GONE);
                            resultNotFound.setVisibility(View.VISIBLE);
                        }else {
                            frameLayout.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            resultNotFound.setVisibility(View.GONE);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error updateRecyclerAdapter:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                Toast.makeText(context,responseMsg,Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
                if(mAdapter.getItemCount() == 0 ){
                    mRecyclerView.setVisibility(View.GONE);
                    resultNotFound.setVisibility(View.VISIBLE);
                }else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    resultNotFound.setVisibility(View.GONE);
                }


            }
        };
        try {
            if(isNewList) {
                mAdapter.clearCustomerList();
                subHeaderCount.setText("0");
            }
            progressBar.setVisibility(View.VISIBLE);
            if(modelUtil.getCustomerModelName()!=null && !modelUtil.getCustomerModelName().isEmpty()) {
                HashMap<String, Object> queryParam = new HashMap<>();
                queryParam.put("_page_size", 50);
                queryParam.put("_page_number", mPageNumber);

                if(strSearchTerm!=null && !strSearchTerm.isEmpty())
                    queryParam.put("_keyword", strSearchTerm);

                for (String Getkey : selectedFilterMap.keySet()) {
                    queryParam.put(Getkey,selectedFilterMap.get(Getkey));
                }
                Log.i(TAG,"Print  customer Query:------------isNewList------ "+ isNewList +" isUpdateDefault:-"+isUpdateDefault +"== "+queryParam);
                DaveModels daveModels = new DaveModels(getActivity(), true);
                daveModels.getObjects(modelUtil.getCustomerModelName(), queryParam, daveAIListener, false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During showing Customer LIst:-------------  "+e.getMessage());
        }

    }

    private void showRecyclerView(){
        if(mRecyclerView.getVisibility() == View.GONE)
            mRecyclerView.setVisibility(View.VISIBLE);

        if(resultNotFound.getVisibility() == View.VISIBLE)
            resultNotFound.setVisibility(View.GONE);

    }

    private void showResultNotFound(){

        if(mRecyclerView.getVisibility() == View.VISIBLE)
            mRecyclerView.setVisibility(View.GONE);

        if(resultNotFound.getVisibility() == View.GONE)
            resultNotFound.setVisibility(View.VISIBLE);

    }

  /*  private void setSubHeader(ArrayList< JSONObject> subHeaderList){
        if(subHeaderList.size() > 0){
            SubHeaderAdapter subHeaderAdapter = new SubHeaderAdapter(getActivity(), subHeaderList);
            shRecyclerView.setAdapter(subHeaderAdapter);
        }

    }*/

    private void showSubHeaderDetails( String preValue, int currentCount){
        subHeaderCount.setText(CRUDUtil.calculatePageSize(preValue,currentCount));

    }

    private void hideSoftKeyword(View view){
        InputMethodManager imm = ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE));
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    EditText filterTypeView;
    RelativeLayout priceRangeLayout;
    private void actionOnFilterMenuButton(){
        try {
            View  filterView = getLayoutInflater().inflate(R.layout.filter_layout, menuItemActionLayout, true);
            filterParentView = filterView.findViewById(R.id.filterParentView);
            filterChildView = filterView.findViewById(R.id.filterChildView);
            priceRangeLayout =  filterView.findViewById(R.id.priceRangeLayout);
            rangeSeekbar =  filterView.findViewById(R.id.rangeSeekbar1);
            priceMin =  filterView.findViewById(R.id.priceMin);
            priceMax =  filterView.findViewById(R.id.priceMax);
            filterTypeView = filterView.findViewById(R.id.typeView);
            Button btnFilter= filterView.findViewById(R.id.btnFilter);
            Button btnCancel= filterView.findViewById(R.id.btnCancel);

            strSearchTerm = "";
            /* if(filter_map==null || filter_map.isEmpty()){
                filter_map = selectedCategoryMap;
            }*/

            if(filterTitleList!=null && filterTitleList.size()>0) {
                filterParentAdapter = new ArrayAdapter<>(getActivity(), R.layout.filter_title, filterTitleList);
                filterParentView.setAdapter(filterParentAdapter);
            }

            if(filterAttrTitleMap!=null && !filterAttrTitleMap.isEmpty()) {
                String filterFirstItem = filterAttrTitleMap.get(filterTitleList.get(0));
                clickOnParentFilterView(filterFirstItem);
                /*filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(filterFirstItem));
                filterChildView.setAdapter(filterChildAdapter);
                setCheckedMultipleItemAgain(filterFirstItem, filterChildAdapter);*/
            }
            filterParentView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    filterTitlePosition = position;
                    String keyName = (String) parent.getItemAtPosition(position);
                    String filterKey = filterAttrTitleMap.get(keyName);
                    clickOnParentFilterView(filterKey);
                   /* if (defaultFilterList != null && !defaultFilterList.isEmpty() && defaultFilterList.containsKey(filterKey)) {
                        if(customerModelInstance.getAttributeType(filterKey).equalsIgnoreCase("price")||
                                customerModelInstance.getAttributeType(filterKey).equalsIgnoreCase("discount")){
                            Log.e(TAG,"Print Filter attr type>>>>>>>>>>>>>>>>>>>>>>"+customerModelInstance.getAttributeType(filterKey));
                            filterChildView.setVisibility(View.GONE);
                            priceRangeLayout.setVisibility(View.VISIBLE);
                            showPriceRangeLayout(filterKey, defaultFilterList.get(filterKey));
                        }
                        else {
                            priceRangeLayout.setVisibility(View.GONE);
                            filterChildView.setVisibility(View.VISIBLE);
                            filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(filterKey));
                            filterChildView.setAdapter(filterChildAdapter);
                            filterChildAdapter.notifyDataSetChanged();
                            setCheckedMultipleItemAgain(filterKey, filterChildAdapter);
                        }
                    }*/
                }
            });
            filterParentView.setSelection(0);
            filterParentView.setItemChecked(0, true);

            filterChildView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    SparseBooleanArray checked = filterChildView.getCheckedItemPositions();
                    ArrayList<String> selectedItems = new ArrayList<>();
                    for (int i = 0; i < checked.size(); i++) {
                        int pos = checked.keyAt(i);
                        if (checked.valueAt(i)) {
                            selectedItems.add(filterChildAdapter.getItem(pos));
                        }
                    }
                    String filterKeyName = filterAttrTitleMap.get(filterParentView.getAdapter().getItem(filterTitlePosition).toString());
                    if(selectedItems.size() == 0 && selectedFilterMap.containsKey(filterKeyName))
                        selectedFilterMap.remove(filterKeyName);
                    else
                        selectedFilterMap.put(filterKeyName, selectedItems);
                    //Log.e(TAG, "***********Print selectedFilterMap HashMap :-  " + selectedFilterMap);

                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    menuItemActionLayout.removeAllViews();
                }
            });

            btnFilter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedFilterMap != null && !selectedFilterMap.isEmpty()) {
                        menuItemActionLayout.removeAllViews();
                        Log.i(TAG,"Print selected Filter list:----------"+selectedFilterMap);
                        isAnyActionApply = true;
                        mPageNumber =1;
                        getCustomersList(mPageNumber,true,false);
                    }else
                        Toast.makeText(getActivity(),"Please select filter option",Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<Error during Filter option:-"+e.getMessage());
        }

    }

    private void clickOnParentFilterView(String filterKey){

        if(checkCustomFilterAttr(customerModelInstance.getAttributeType(filterKey))){
            filterChildView.setVisibility(View.GONE);
            priceRangeLayout.setVisibility(View.GONE);
            filterTypeView.setVisibility(View.VISIBLE);
            filterTypeView.setHint(filterKey);
            if(selectedFilterMap.containsKey(filterKey)) {
                filterTypeView.setText(selectedFilterMap.get(filterKey).toString().replace("~",""));
            }

            filterTypeView.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    String typeValue = s.toString();
                    if(typeValue.length()>0)
                        selectedFilterMap.put(filterKey,"~"+typeValue);
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                public void onTextChanged(CharSequence query, int start, int before, int count) {
                    String searchedTerm = query.toString().trim();

                }
            });


        }
        else if(customerModelInstance.getAttributeType(filterKey).equalsIgnoreCase("price")||
                customerModelInstance.getAttributeType(filterKey).equalsIgnoreCase("discount")){
            Log.i(TAG,"Print Filter attr type>>>>>>>>>>>>>>>>>>>>>>"+customerModelInstance.getAttributeType(filterKey));
            filterChildView.setVisibility(View.GONE);
            priceRangeLayout.setVisibility(View.VISIBLE);
            filterTypeView.setVisibility(View.GONE);
            showPriceRangeLayout(filterKey, defaultFilterList.get(filterKey));
        }
        else {
            priceRangeLayout.setVisibility(View.GONE);
            filterTypeView.setVisibility(View.GONE);
            filterChildView.setVisibility(View.VISIBLE);
            filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(filterKey));
            filterChildView.setAdapter(filterChildAdapter);
            filterChildAdapter.notifyDataSetChanged();
            setCheckedMultipleItemAgain(filterKey, filterChildAdapter);
        }

    }


    private boolean checkCustomFilterAttr(String filterAttrType){

        return filterAttrType.equals("name") || filterAttrType.equals("uid") || filterAttrType.equals("email")
                || filterAttrType.equals("phone_number");

    }

    private void showPriceRangeLayout(final String key, ArrayList<String> priceRange) {
        if (priceRange != null && !priceRange.isEmpty()) {
            int minValue = Integer.parseInt(priceRange.get(0));
            int maxValue = Integer.parseInt(priceRange.get(1));
            if(minValue > maxValue){
                rangeSeekbar.setMinValue(maxValue);
                rangeSeekbar.setMaxValue(minValue);
            }else {
                rangeSeekbar.setMinValue(minValue);
                rangeSeekbar.setMaxValue(maxValue);
            }
        }
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                priceMin.setText(String.valueOf(minValue));
                priceMax.setText(String.valueOf(maxValue));
                String joinedValue = String.valueOf(minValue) + "," + String.valueOf(maxValue);
                ArrayList<String> priceList = new ArrayList<>();
                priceList.add(joinedValue);
                if (selectedFilterMap.containsKey(key)) {
                    selectedFilterMap.remove(key);
                }
                selectedFilterMap.put(key, priceList);
            }
        });
    }

    private void setCheckedMultipleItemAgain(String key, ArrayAdapter<String> adapter) {
        Log.i(TAG,"setCheckedMultipleItemAgain****************:- " + selectedFilterMap);
        for (String Getkey : selectedFilterMap.keySet()) {
            if (Getkey.equalsIgnoreCase(key)) {
                if(selectedFilterMap.get(Getkey) instanceof  ArrayList) {
                    ArrayList<String> get_lis = (ArrayList<String>) selectedFilterMap.get(Getkey);
                    for (int j = 0; j < get_lis.size(); j++) {
                        int checked_position = adapter.getPosition(get_lis.get(j));
                        filterChildView.setItemChecked(checked_position, true);
                    }
                }else{
                    int checked_position = adapter.getPosition(selectedFilterMap.get(Getkey).toString());
                    filterChildView.setItemChecked(checked_position, true);
                }
            }
        }
    }

    @Override
    public void onClickCategoryHierarchy(HashMap<String,Object> selectedCategoryMap) {
        selectedFilterMap = selectedCategoryMap;
        Log.e(TAG, "onClickCategoryHierarchy End selectedFilterMap:-"+ selectedFilterMap );
        mPageNumber = 1;
        isAnyActionApply = true;
        //todo make server call
        getCustomersList(mPageNumber, true, true);
    }

    @Override
    public void onApplyFilter(HashMap<String, ArrayList<String>> selectedFilterDetails) {
        //todo add selectedFilterDetails to selectedFilterMap
       /* selectedFilterMap.putAll(selectedFilterDetails);
        Log.i(TAG,"Print selected Filter list:----------"+selectedFilterMap);
        isAnyActionApply = true;
        mPageNumber =1;
        getCustomersList(mPageNumber,true,false);*/
    }

    @Override
    public void onCancelFilter() {

    }



}



