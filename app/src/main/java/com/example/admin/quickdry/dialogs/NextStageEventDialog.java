package com.example.admin.quickdry.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.squareup.okhttp.HttpUrl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class NextStageEventDialog extends Dialog {

    private static String TAG = "NextStageEventDialog";
    private Context mContext;

    private LinearLayout stageNewAttrLayout;
    private ImageView mainImageView;
    private TextView dialogHeader, title,subTitle,rightAttr;

    private DaveAIPerspective daveAIPerspective;
    private String header = "Main Product";
    private String productId;
    private JSONArray nextStageNewAttributes;
    private String interactionDetails;
    private HashMap<String, Boolean> requiredFieldHashMap= new HashMap<>();


    private NextStageEventListener nextStageEventListener;
    public interface NextStageEventListener {
        void onNextStageEventCancel();
        void onNextStageEventSubmit(JSONObject formDetails);
    }


    public NextStageEventDialog(Context mContext,NextStageEventListener nextStageEventListener,String title, String productId,
                                JSONArray nextStageNewAttributes,String interactionDetails) {
        super(mContext);
        this.mContext = mContext;
        this.nextStageEventListener= nextStageEventListener;
        this.header = title;
        this.productId = productId;
        this.nextStageNewAttributes = nextStageNewAttributes;
        this.interactionDetails = interactionDetails;

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_next_stage);


        stageNewAttrLayout = findViewById(R.id.stageNewAttrLayout);
        dialogHeader = findViewById(R.id.dialogHeader);
        mainImageView = findViewById(R.id.mainImageView);
        title = findViewById(R.id.title);
        subTitle = findViewById(R.id.subTitle);
        rightAttr = findViewById(R.id.rightAttr);
        Button cancelBtn =  findViewById(R.id.cancel_button);
        Button submitBtn = findViewById(R.id.submit_button);

        daveAIPerspective = DaveAIPerspective.getInstance();
        daveAIPerspective.savePerspectivePreferenceValue(mContext);
        DynamicForm dynamicForm = new DynamicForm();

        dialogHeader.setText(header);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nextStageEventListener!=null)
                    nextStageEventListener.onNextStageEventCancel();
                dismiss();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject formDetails =  dynamicForm.methodValidateDynamicView(stageNewAttrLayout,requiredFieldHashMap,null);
                Log.e(TAG, "<<<<<<<<<Click On Submit Button>>>>>>>>>>  "+ formDetails);
                if(formDetails != null) {
                    if(nextStageEventListener!=null)
                        nextStageEventListener.onNextStageEventSubmit(formDetails);
                    dismiss();
                }
            }
        });

        getObjectDetails();
        if(nextStageNewAttributes!=null && nextStageNewAttributes.length()>0) {
            stageNewAttrLayout.removeAllViews();
            ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < nextStageNewAttributes.length(); i++) {
                try {
                    list.add(nextStageNewAttributes.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.e(TAG, "Print NextStageEventDialog showNewAttrView interaction attr List :---  " + list + " \n " + interactionDetails);
            dynamicForm.createDynamicForm(new DynamicForm.OnFormViewCreatedListener() {
                @Override
                public void onFormVewCreated(HashMap<String, Boolean> hashMap) {
                    requiredFieldHashMap = hashMap;
                    Log.i(TAG, "After Create new Attr view :----------" + stageNewAttrLayout.getChildCount() + " " + requiredFieldHashMap);
                }
            }, mContext, stageNewAttrLayout, new ModelUtil(mContext).getInteractionModelName(), list, interactionDetails);
        }

    }


    private void getObjectDetails(){

        //Log.e(TAG, " <<<<<<<<<<<<<<<<<<<Before calling getObject product Details with Id>>>>>>>>>>>:------"+productID);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {

                if(response!=null && response.length()>0){
                    try {
                        if(daveAIPerspective.getProduct_card_image_view()!= null && response.has(daveAIPerspective.getProduct_card_image_view())) {

//                            URL conceptImageUrl = HttpUrl.parse(response.getString(daveAIPerspective.getProduct_card_image_view())).url();
                            // Log.v(TAG,"CardImageUrl******bindQuickView************:- " +conceptImageUrl);
                            Glide.with(mContext)
                                    .load(response.getString(daveAIPerspective.getProduct_card_image_view())).diskCacheStrategy(DiskCacheStrategy.ALL)
                                    //.placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image)
                                    .into(mainImageView);
                        }

                        if(daveAIPerspective.getProduct_card_header()!=null && response.has(daveAIPerspective.getProduct_card_header())) {
                            title.setText(response.getString(daveAIPerspective.getProduct_card_header()));
                        }
                        if(daveAIPerspective.getProduct_card_sub_header()!=null && response.has(daveAIPerspective.getProduct_card_sub_header())) {
                            subTitle.setText(response.getString(daveAIPerspective.getProduct_card_sub_header()));
                        }

                        if(daveAIPerspective.getProduct_card_right_attr()!=null && response.has(daveAIPerspective.getProduct_card_right_attr())) {
                           rightAttr.setText(response.getString(daveAIPerspective.getProduct_card_right_attr()));
                        }

                    } catch (JSONException e) {
                        Log.e(TAG, "Error Get Tagged  Product Details >>>>>>>>>>>>>>  " + e.getMessage());
                    }

                }


            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(productId!=null && !productId.isEmpty()) {
                DaveModels daveModels = new DaveModels(mContext, true);
                daveModels.getObject(new ModelUtil(mContext).getProductModelName(), productId, daveAIListener, false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"error during getting product details:--"+e.getMessage());

        }
    }




}
