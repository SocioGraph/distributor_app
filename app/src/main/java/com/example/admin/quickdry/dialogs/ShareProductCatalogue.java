package com.example.admin.quickdry.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.others.DaveAIHelper;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.storage.SharedPreference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;



public class ShareProductCatalogue extends Dialog {

    private final String TAG = getClass().getSimpleName();
    private EditText enterEmailId,enterPhone;
    private Button sendBtn,btnCancel;
    private Context mContext;
    private CheckBox checkBox;
    private Boolean socialButtonState=false;
    HashMap<String, Object> shareDetailsMap;
    private DaveAIPerspective daveAIPerspective;
    private ModelUtil modelUtil;
    private JSONObject customerObject= new JSONObject();
    private HashMap<String,Object> shareTemplateListMap = new HashMap<>();
    private Spinner selectTemplate;
    private String interactionStage = "";


    public ShareProductCatalogue(Context mContext, HashMap<String, Object> shareDetailsMap) {
        super(mContext);
        this.mContext = mContext;
        this.shareDetailsMap = shareDetailsMap;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_share_catalogue);

        TextView shareTitle = findViewById(R.id.shareTitle);
        selectTemplate = findViewById(R.id.selectTemplate);
        enterEmailId = findViewById(R.id.enterEmailId);
        enterPhone = findViewById(R.id.enterPhone);
        sendBtn = findViewById(R.id.sendBtn);
        btnCancel = findViewById(R.id.btnCancel);
        checkBox = findViewById(R.id.checkBox);

        daveAIPerspective=DaveAIPerspective.getInstance();
        modelUtil= new ModelUtil(mContext);

        try {
            shareTemplateListMap = modelUtil.getInteractionStageTemplateList();
            Log.i(TAG,"Print TemplateList && ShareDetailsMap :-------"+shareTemplateListMap+"  "+shareDetailsMap);

            if (shareDetailsMap.containsKey("interaction_stage") && shareDetailsMap.get("interaction_stage")!= null) {
                interactionStage = shareDetailsMap.get("interaction_stage").toString();
                if(!shareTemplateListMap.isEmpty() &&  shareTemplateListMap.containsKey(interactionStage)){
                    String strTemplate = shareTemplateListMap.get(interactionStage).toString();
                    ArrayList<String> aList= new ArrayList(Arrays.asList(strTemplate.split(",")));
                    ArrayAdapter spinnerArrayAdapter = new ArrayAdapter<String>
                            (mContext, android.R.layout.simple_spinner_dropdown_item, aList);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    selectTemplate.setAdapter(spinnerArrayAdapter);
                }

            }

            if(shareDetailsMap.containsKey("button_name"))
                shareTitle.setText(shareDetailsMap.get("button_name").toString());

            if(shareDetailsMap.containsKey("customer_object")) {
                //customerObject = new JSONObject(shareDetails.get("customer_object").toString()) ;
                setDefaultEmailAndPhone(new JSONObject(shareDetailsMap.get("customer_object").toString()));
            }else{
                DaveModels daveModels = new DaveModels(mContext,true);
                customerObject = daveModels.getObject(modelUtil.getCustomerModelName(), shareDetailsMap.get("customer_id").toString(),
                        new DaveAIListener() {
                    @Override
                    public void onReceivedResponse(JSONObject jsonObject) {
                        setDefaultEmailAndPhone(jsonObject);
                    }

                    @Override
                    public void onResponseFailure(int i, String s) {

                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG," Error ShareDetailsMap :-------"+shareTemplateListMap+"  "+shareDetailsMap);
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                socialButtonState= isChecked;
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(enterEmailId.getText().toString().isEmpty() && enterPhone.getText().toString().isEmpty() && !socialButtonState){
                   Toast.makeText(mContext, "Please enter Email Id or Phone no", Toast.LENGTH_SHORT).show();
               }else{
                   addCustomerContacttoPhone(enterPhone.getText().toString(),enterEmailId.getText().toString());
                   shareProductCatalogue(enterEmailId.getText().toString(),enterPhone.getText().toString(),
                           selectTemplate.getSelectedItem().toString());
               }

            }
        });
    }


    private void addCustomerContacttoPhone(String mobile,String email){

        try {
            ModelUtil modelUtil = new ModelUtil(mContext);
                DaveAIPerspective daveAIPerspective = DaveAIPerspective.getInstance();
                if ((!TextUtils.isEmpty(daveAIPerspective.getCustomer_card_header()) || !daveAIPerspective.getCustomer_card_header().equalsIgnoreCase("_NULL_") || !daveAIPerspective.getCustomer_card_header().equalsIgnoreCase("__NULL__"))
                        &&
                        (!TextUtils.isEmpty(daveAIPerspective.getCustomer_mobile_attr()) || !daveAIPerspective.getCustomer_mobile_attr().equalsIgnoreCase("_NULL_") || !daveAIPerspective.getCustomer_mobile_attr().equalsIgnoreCase("__NULL__"))) {
                    String nameAttrName = daveAIPerspective.getCustomer_card_header();
                    String contactAttrName = daveAIPerspective.getCustomer_mobile_attr();
                    Log.e(TAG, "button clicked for name attr =" + contactAttrName + " = " + custObject.getString(contactAttrName));
                    if(custObject.has(nameAttrName)) {
                        modelUtil.addToContacts(custObject.getString(nameAttrName), custObject.getString(contactAttrName));
                    }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    JSONObject custObject = new JSONObject();
    private void setDefaultEmailAndPhone(JSONObject customerObject){
        try {
            custObject = customerObject;
            Model customerModel = Model.getModelInstance(mContext, modelUtil.getCustomerModelName());
            if(daveAIPerspective.getCustomer_email_attr()!= null && customerObject.has(daveAIPerspective.getCustomer_email_attr())){
                enterEmailId.setText(customerObject.getString(daveAIPerspective.getCustomer_email_attr()));
            }else {
                List emails = customerModel.getEmails();
                if (emails != null && emails.size() > 0 && customerObject.has(emails.get(0).toString())) {
                    enterEmailId.setText(customerObject.getString(emails.get(0).toString()));
                }
            }
            Log.i(TAG, "Share Details  email && mobile:-------" + daveAIPerspective.getCustomer_email_attr() + "  " + daveAIPerspective.getCustomer_mobile_attr());

            if(daveAIPerspective.getCustomer_mobile_attr()!= null && customerObject.has(daveAIPerspective.getCustomer_mobile_attr())){
                enterPhone.setText(customerObject.getString(daveAIPerspective.getCustomer_mobile_attr()));
            }else {
                List mobileNos = customerModel.getMobile_numbers();
                if (mobileNos != null && mobileNos.size() > 0 && customerObject.has(mobileNos.get(0).toString())) {
                    enterPhone.setText(customerObject.getString(mobileNos.get(0).toString()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void shareProductCatalogue(String emails, String phoneNumbers ,String templateFormat){

        String customerId = new SharedPreference(mContext).readString(SharedPreference.CUSTOMER_ID);
        if (customerId != null) {
            String currentOrderId = new SharedPreference(mContext).readString(customerId);
            if(currentOrderId ==null ||currentOrderId.isEmpty()){
               currentOrderId = DaveAIHelper.preOrderId.get(customerId);
            }

            if(currentOrderId!=null && !currentOrderId.isEmpty()) {

                JSONObject postObject = new JSONObject();
                try {
                    JSONObject pReceiver = new JSONObject();
                    pReceiver.put("emails", emails);
                    pReceiver.put("phone_numbers", phoneNumbers);

                    postObject.put("receiver", pReceiver);
                    postObject.put("_subtitle_attr", daveAIPerspective.getProduct_card_sub_header());
                    postObject.put("_title_attr", daveAIPerspective.getProduct_card_header());
                    postObject.put("_image_attr", daveAIPerspective.getProduct_card_image_view());
                    postObject.put("_price_attr", daveAIPerspective.getProduct_card_right_attr());
                    postObject.put("_view_attrs", convertListTOJsonArray(daveAIPerspective.getProduct_expand_view()));

                /*postObject.put("message", "userId");
                postObject.put("title", "");*/

               /* if (shareDetailsMap.containsKey("interaction_stage")) {
                    String interactionStage = shareDetailsMap.get("interaction_stage").toString();
                    postObject.put("interaction_stage", interactionStage);
                    if(!shareTemplateListMap.isEmpty() &&  shareTemplateListMap.containsKey(interactionStage))
                        postObject.put("_template", shareTemplateListMap.get(interactionStage).toString());
                }*/
                    postObject.put("interaction_stage", interactionStage);
                    postObject.put("_template", templateFormat);

                    postObject.put(daveAIPerspective.getInvoice_id_attr_name(), currentOrderId);

                    if (shareDetailsMap.containsKey("total_qty_attr"))
                        postObject.put("_quantity_attr", shareDetailsMap.get("total_qty_attr").toString());

                    Log.i(TAG, " Print Share Catalogue Post Body************ :-  " + postObject);
                    APIResponse postTaskListener = new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {

                            Toast.makeText(mContext, "Share Success.Please check your email and message..", Toast.LENGTH_LONG).show();
                            dismiss();
                            if (socialButtonState) {

                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                intent.putExtra(Intent.EXTRA_TEXT, response);
                                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out this link!");
                                mContext.startActivity(Intent.createChooser(intent, "Share With"));

                           /* //Intent sharingIntent = new Intent(Intent.ACTION_VIEW);
                            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                            sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            sharingIntent.setData(Uri.parse(response));
                            Intent chooserIntent = Intent.createChooser(sharingIntent, "Share With");
                            chooserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.startActivity(chooserIntent);*/


                            }
                        }

                        @Override
                        public void onTaskFailure(int requestCode, String responseMsg) {
                            if (requestCode == 10 || requestCode == -1)
                                Toast.makeText(mContext, "Please try it again", Toast.LENGTH_LONG).show();
                            else {
                                Toast.makeText(mContext, responseMsg, Toast.LENGTH_LONG).show();
                            }

                        }
                    };
                    Model model1 = new Model(mContext);
                    model1.shareCatalogue(customerId, postObject, true, postTaskListener);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(TAG, " Error During Share Catalogue:- " + e.getLocalizedMessage());
                }
            }else {
                Toast.makeText(mContext, "Please create new Order.", Toast.LENGTH_LONG).show();
            }

        }

    }

    private static JSONArray convertListTOJsonArray(ArrayList list){

        JSONArray jsonArray = new JSONArray();
        for(int i = 0; i < list.size(); i++){
            jsonArray.put(list.get(i));
        }
        return jsonArray;
    }



}
