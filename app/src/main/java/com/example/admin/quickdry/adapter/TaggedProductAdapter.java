package com.example.admin.quickdry.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.utils.RecycleViewSwipeHelper;
import com.squareup.okhttp.HttpUrl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;



public class TaggedProductAdapter extends RecyclerView.Adapter<TaggedProductAdapter.ViewHolder> {

    private static final String TAG = "TaggedRecycleAdapter";
    private Context context;
    private JSONArray taggedProductList;
    private int counter =0;
    private boolean checkQtyStatus;
    private List<ViewHolder> viewHolders = null;
    private DaveAIPerspective daveAIPerspective;



    public TaggedProductAdapter(Context context, JSONArray taggedProductList, boolean checkQtyStatus ) {
        this.context = context;
        this.taggedProductList =taggedProductList;
        this.checkQtyStatus=checkQtyStatus;
        daveAIPerspective = DaveAIPerspective.getInstance();
       // daveAIPerspective =new  DaveAIPerspective(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_tagged_product_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {

        try {
            String productId = taggedProductList.getString(position);
          //  getProductDetailsFromServer(viewHolder,taggedProductList.getString(position));
            getProductDetails(viewHolder,taggedProductList.getString(position));

            viewHolder.btnAddTaggedProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(checkQtyStatus){
                        if(viewHolder.btnAddTaggedProduct.getText().toString().equals("Add")) {
                            viewHolder.btnAddTaggedProduct.setVisibility(View.GONE);
                            viewHolder.qtyActionLayout.setVisibility(View.VISIBLE);
                        }

                    }else{
                        if(viewHolder.btnAddTaggedProduct.getText().toString().equals("Add")) {
                            viewHolder.btnAddTaggedProduct.setText("Added");
                            RecycleViewSwipeHelper.taggedProduct.put(productId,0);
                            //RecycleViewSwipeHelper.taggedProduct.add(productId);

                        }
                        else if (viewHolder.btnAddTaggedProduct.getText().toString().equals("Added")) {
                            viewHolder.btnAddTaggedProduct.setText("Add");
                            if(RecycleViewSwipeHelper.taggedProduct.containsKey(productId))
                                RecycleViewSwipeHelper.taggedProduct.remove(productId);

                            /*if(RecycleViewSwipeHelper.taggedProduct.contains(productId))
                                RecycleViewSwipeHelper.taggedProduct.remove(productId);*/
                        }

                    }


                }
            });

            viewHolder.increaseCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // notifyDataSetChanged();

                    String checkQty = String.valueOf(viewHolder.countEditText.getText());
                    if(checkQty!= null && !checkQty.isEmpty())
                        counter = Integer.parseInt(checkQty);
                    counter++;
                    viewHolder.countEditText.setText(String.valueOf(counter));
                    RecycleViewSwipeHelper.taggedProduct.put(productId,counter);

                }
            });

            viewHolder.decreaseCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // notifyDataSetChanged();
                    String checkQty = String.valueOf(viewHolder.countEditText.getText());
                    if(checkQty!=null && !checkQty.isEmpty())
                        counter=Integer.parseInt(checkQty);
                    if (counter <=1) {
                        //counter = 1;
                        viewHolder.btnAddTaggedProduct.setVisibility(View.VISIBLE);
                        viewHolder.qtyActionLayout.setVisibility(View.GONE);
                    } else {
                        counter--;
                    }
                    viewHolder.countEditText.setText(String.valueOf(counter));
                    if(counter>1){
                        RecycleViewSwipeHelper.taggedProduct.put(productId,counter);
                    }else
                        RecycleViewSwipeHelper.taggedProduct.remove(productId);


                }
            });
        } catch (JSONException e) {
            Log.e(TAG, "Error Get Tagged  Product Id>>>>>>>>>>>>>>  " + e.getMessage());
        }


        if(!viewHolder.btnAddTaggedProduct.getText().toString().equals("Add")|| viewHolder.btnAddTaggedProduct.getVisibility()==View.GONE){
            setViewHolder(viewHolder);
        }


    }

    @Override
    public int getItemCount() {
        return taggedProductList.length();
    }

    private void setViewHolder(ViewHolder viewHolder) {
        if(viewHolders==null)
            viewHolders = new ArrayList<>();
        viewHolders.add(viewHolder);

    }

    public List<ViewHolder> getViewHolder() {
        return viewHolders;
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {

         ImageView mainImageView,decreaseCount,increaseCount;
         TextView title,subTitle,rightAttr;
         EditText countEditText;
         Button btnAddTaggedProduct;
         LinearLayout qtyActionLayout;
        // ScrollView scrollView;

        ViewHolder(View itemView) {
            super(itemView);

            mainImageView = itemView.findViewById(R.id.mainImageView);
           // scrollView = itemView.findViewById(R.id.scrollView);
            title = itemView.findViewById(R.id.title);
            subTitle = itemView.findViewById(R.id.subTitle);
            rightAttr = itemView.findViewById(R.id.rightAttr);
            btnAddTaggedProduct=itemView.findViewById(R.id.btnAddTaggedProduct);
            qtyActionLayout=itemView.findViewById(R.id.qtyActionLayout);
            decreaseCount = itemView.findViewById(R.id.decreaseCount);
            increaseCount = itemView.findViewById(R.id.increaseCount);
            countEditText = itemView.findViewById(R.id.countEditText);
            countEditText.setText(String.valueOf(1));

          /*  scrollView.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.e(TAG, "CHILD TOUCH");
                    // Disallow the touch request for parent scroll on touch of  child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });*/



        }


    }

    private void getProductDetailsFromServer(final ViewHolder viewHolder,String productId){

        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {

                if(response!=null){
                    try {
                        JSONObject getDetails = new JSONObject(response);

                        if(daveAIPerspective.getProduct_card_image_view()!= null && getDetails.has(daveAIPerspective.getProduct_card_image_view())) {
                            URL conceptImageUrl = HttpUrl.parse(getDetails.getString(daveAIPerspective.getProduct_card_image_view())).url();
                            // Log.v(TAG,"CardImageUrl******bindQuickView************:- " +conceptImageUrl);
                            Glide.with(context)
                                    .load(conceptImageUrl.toString()).diskCacheStrategy(DiskCacheStrategy.ALL)
                                    // .placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image)
                                    .into(viewHolder.mainImageView);
                        }

                        if(daveAIPerspective.getProduct_card_header()!=null && getDetails.has(daveAIPerspective.getProduct_card_header())) {
                            viewHolder.title.setText(getDetails.getString(daveAIPerspective.getProduct_card_header()));
                        }
                        if(daveAIPerspective.getProduct_card_sub_header()!=null && getDetails.has(daveAIPerspective.getProduct_card_sub_header())) {
                            viewHolder.subTitle.setText(getDetails.getString(daveAIPerspective.getProduct_card_sub_header()));
                        }

                        if(daveAIPerspective.getProduct_card_right_attr()!=null && getDetails.has(daveAIPerspective.getProduct_card_right_attr())) {
                            viewHolder.rightAttr.setText(getDetails.getString(daveAIPerspective.getProduct_card_right_attr()));
                        }

                    } catch (JSONException e) {
                        Log.e(TAG, "Error Get Tagged  Product Details >>>>>>>>>>>>>>  " + e.getMessage());
                    }

                }

            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {

            }
        };
        new APICallAsyncTask(postTaskListener, context,"GET",false)
                .execute(APIRoutes.updateOrGetObjectAPI(new ModelUtil(context).getProductModelName(),productId));
    }

    private void getProductDetails(final ViewHolder viewHolder,String productId){

        //Log.e(TAG, " <<<<<<<<<<<<<<<<<<<Before calling getObject product Details with Id>>>>>>>>>>>:------"+productID);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {

                if(response!=null && response.length()>0){
                    try {


                        if(daveAIPerspective.getProduct_card_image_view()!= null && response.has(daveAIPerspective.getProduct_card_image_view())) {
                            URL conceptImageUrl = HttpUrl.parse(response.getString(daveAIPerspective.getProduct_card_image_view())).url();
                            // Log.v(TAG,"CardImageUrl******bindQuickView************:- " +conceptImageUrl);
                            Glide.with(context)
                                    .load(conceptImageUrl.toString()).diskCacheStrategy(DiskCacheStrategy.ALL)
                                    // .placeholder(R.drawable.placeholder)
                                    .error(R.drawable.no_image)
                                    .into(viewHolder.mainImageView);
                        }

                        if(daveAIPerspective.getProduct_card_header()!=null && response.has(daveAIPerspective.getProduct_card_header())) {
                            viewHolder.title.setText(response.getString(daveAIPerspective.getProduct_card_header()));
                        }
                        if(daveAIPerspective.getProduct_card_sub_header()!=null && response.has(daveAIPerspective.getProduct_card_sub_header())) {
                            viewHolder.subTitle.setText(response.getString(daveAIPerspective.getProduct_card_sub_header()));
                        }

                        if(daveAIPerspective.getProduct_card_right_attr()!=null && response.has(daveAIPerspective.getProduct_card_right_attr())) {
                            viewHolder.rightAttr.setText(response.getString(daveAIPerspective.getProduct_card_right_attr()));
                        }

                    } catch (JSONException e) {
                        Log.e(TAG, "Error Get Tagged  Product Details >>>>>>>>>>>>>>  " + e.getMessage());
                    }

                }


            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(productId!=null && !productId.isEmpty()) {
                DaveModels daveModels = new DaveModels(context, true);
                Log.e(TAG,"calling daveModel.getObject() for :---------------- "+new ModelUtil(context).getProductModelName()+"/"+productId);
                daveModels.getObject(new ModelUtil(context).getProductModelName(), productId, daveAIListener, false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"error during getting product details:--"+e.getMessage());

        }
    }


}
