package com.example.admin.quickdry.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.example.admin.daveai.broadcasting.DaveService;

public class NetWatcher extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //here, check that the network connection is available. If yes, start your service. If not, stop your service.
        Log.e("LOGGER","I WAS CALLED");
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null) {
            if (info.isConnected()) {
                Log.e("LOGGER","NETWORK CONNECTED");
                //start service
                new DaveService().startBackgroundService(context);
            }
        }
    }
}
