package com.example.admin.quickdry.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.daveUtil.DaveCachedUtil;
import com.example.admin.daveai.daveUtil.ModelUtil;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.model.CardSwipeDetails;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.model.ObjectData;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveAIStatic;
import com.example.admin.daveai.others.DaveException;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.AddScanProduct;
import com.example.admin.quickdry.activity.CategoryHierarchy;
import com.example.admin.quickdry.activity.DaveFunction;
import com.example.admin.quickdry.adapter.RecommendationAdapter;
import com.example.admin.quickdry.dialogs.NextStageEventDialog;
import com.example.admin.quickdry.storage.SharedPreference;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.utils.CardSwipeListener;
import com.example.admin.quickdry.utils.PostInteractionUtil;
import com.example.admin.quickdry.utils.RecycleViewSwipe;
import com.example.admin.quickdry.utils.RecycleViewSwipeHelper;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import static com.example.admin.daveai.others.DaveAIStatic.categoryBackStack;
import static com.example.admin.quickdry.activity.CustomerProfile.tabHeaderList;
import static com.example.admin.daveai.others.DaveAIStatic.defaultQuantityStatus;
import static com.example.admin.daveai.others.DaveAIStatic.interactionPosition;
import static com.example.admin.daveai.others.DaveAIStatic.interactionStageTitle;
import static com.example.admin.daveai.others.DaveAIStatic.quantityPredictStatus;


public class DaveRecommendations extends Fragment implements CardSwipeListener, AppConstants,RecommendationAdapter.RecommendationAdapterListener {

    private final String TAG = getClass().getSimpleName();
    private Context context;

    private RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    LinearLayout  menuItemActionLayout,breadCrumbsLayout;
    TextView priceMin, priceMax,notFound;
    CrystalRangeSeekbar rangeSeekbar;
    ProgressBar progressBar;

    RecommendationAdapter recommendationAdapter;
    HorizontalScrollView breadcrumbWrapper;
    ArrayAdapter<String> filterParentAdapter,filterChildAdapter;
    ListView filterParentView,filterChildView;


    public static String daveAction = "";
    String customerId = "";
    String strProductId = "";
    private String defaultRecommendationId="";
    private String strRecommendationId="";
    String productModelName;
    String productIdAttrName;
    Model productModelInstance;
    private DaveAIPerspective daveAIPerspective;

    private boolean loading = true;
    private int mPageNumber;
    private boolean isLastPage = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    boolean isAnyActionApply = false;
    
    int filter_position = 0;
    HashMap<String,ArrayList<String>> defaultFilterList;
    HashMap<String, String> filterTitleMap = new HashMap<>();
    //HashMap<String, ArrayList<String>> selectedFilterMap = new HashMap<>();
    HashMap<String, Object> selectedFilterMap = new HashMap<>();
    ArrayList<String> filterAttr = new ArrayList<>();
   // HashMap<String, ArrayList<String>> selectedCategoryMap = new HashMap<>();
    HashMap<String, Object> selectedCategoryMap = new HashMap<>();

    String strRadioBtnClicking = "";
    private String searchedTerm="";

    private HashMap<String, Object> categorySelectedItems = new HashMap<>();
    ArrayList hierarchyAttributes = new ArrayList<>();
    HashMap<String, String> hierarchyAttrTitleMap = new HashMap<>();
    private ArrayList<Object> breadCrumbDetails = new ArrayList<>();
    Stack<Map<Object, Object>> categoryStackFlow = categoryBackStack;

    private JSONObject mapSearchByImageUrl = new JSONObject();
    boolean isSearchByImage = false;

    private boolean isOnlineMode = true;

    /* private RecommendationListener recommendationListener;
    public interface RecommendationListener { }*/

    public static DaveRecommendations newInstance( String daveAction,HashMap<String, Object> dataMap) {
        DaveRecommendations fragment = new DaveRecommendations();
        Bundle extras = new Bundle();
        extras.putString(DaveFunction.DAVE_ACTION,daveAction);
        extras.putSerializable("hashMap",dataMap);
        fragment.setArguments(extras);
       // Log.i("DaveRecommendations","new Instance DaveRecommendation  daveAction:- " +daveAction +" details"+ extras);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof CustomerSelectedListener) {
            customerSelectedCallback = (CustomerSelectedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement CustomerSelectedListener");
        }*/
        //Log.i(TAG,"onAttach>>>>>>>>>>>>>>>>>>>>>>>>>>>>:- ");

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //Log.i(TAG,"onCreate>>>>>>>>>>>>>>>>>>>>>>>>>>>>:- ");
        try {
            HashMap<String, String> hashMap = (HashMap<String, String>) getActivity().getIntent().getSerializableExtra("message_from_dave");
            if (hashMap != null) {
                daveAction = hashMap.get("Dave_function");
                customerId = hashMap.get("customer_id");
                strProductId = hashMap.get("product_id");

            }
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                Log.e(TAG, " Print  Intent Bundle***************:- " + bundle);
                daveAction = bundle.getString(DaveFunction.DAVE_ACTION);
                HashMap<String, Object> mMap = (HashMap<String, Object>) bundle.getSerializable("hashMap");
                if (mMap != null) {
                    if (mMap.containsKey("customer_id") && mMap.get("customer_id").toString() != null)
                        customerId = mMap.get("customer_id").toString();

                    if (mMap.containsKey("product_id") && mMap.get("product_id").toString() != null)
                        strProductId = mMap.get("product_id").toString();

                    if (mMap.containsKey("bread_crumb_details") && mMap.get("bread_crumb_details")!=null ) {
                        breadCrumbDetails = (ArrayList<Object>) mMap.get("bread_crumb_details");
                    }

                    if (mMap.containsKey("category_selected_items") && mMap.get("category_selected_items").toString() != null)
                        categorySelectedItems = (HashMap<String, Object>) mMap.get("category_selected_items");

                    if (daveAction.equalsIgnoreCase(DaveFunction.SIMILAR_ACTION)) {
                        HashMap<String, String> productAttr = (HashMap<String, String>) mMap.get("productFilterDetails");
                        if (productAttr != null)
                            for (String key : productAttr.keySet()) {
                                addCheckedItemToMapDefault(key, productAttr.get(key));
                            }
                    }
                    if (daveAction.equalsIgnoreCase(DaveFunction.RECOMMENDATION_ACTION)) {
                        HashMap<String, Object> productAttr = (HashMap<String, Object>) mMap.get("productFilterDetails");
                        if (productAttr != null)
                            for (String key : productAttr.keySet()) {
                                if (productAttr.get(key) instanceof ArrayList) {
                                    selectedFilterMap.put(key, (ArrayList<String>) productAttr.get(key));
                                    selectedCategoryMap.put(key, (ArrayList<String>) productAttr.get(key));
                                } else
                                    addCheckedItemToMapDefault(key, productAttr.get(key).toString());
                            }
                    }

                    if (mMap.containsKey("_image_url_map") && mMap.get("_image_url_map") != null) {
                        mapSearchByImageUrl = new JSONObject(mMap.get("_image_url_map").toString());
                        isSearchByImage = true;
                    }

                    if (mMap.containsKey("isOnlineMode") ) {
                        Log.e(TAG, " Testing get IntentDetails isOnlineMode >>>>>>>>>>>>>>>>>>>> "+ mMap.get("isOnlineMode"));
                        isOnlineMode = (boolean) mMap.get("isOnlineMode");

                    }

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dave_recomm, container, false);
        view.setTag(TAG);
        context = getContext();

        breadcrumbWrapper =  view.findViewById(R.id.breadcrumbWrapper);
        breadCrumbsLayout =  view.findViewById(R.id.breadCrumbsLayout);
        menuItemActionLayout =  view.findViewById(R.id.menuItemActionLayout);
        notFound =  view.findViewById(R.id.notFound);
        progressBar =  view.findViewById(R.id.progressBar);
        mRecyclerView = view.findViewById(R.id.recycler_view);

        breadcrumbWrapper.setVisibility(View.GONE);
        notFound.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        recommendationAdapter = new RecommendationAdapter(DaveRecommendations.this,getActivity(),new ArrayList<JSONObject>());

       /* if(customerId!=null && !customerId.isEmpty())
            recommendationAdapter = new RecommendationAdapter(getActivity(),customerId,new ArrayList<JSONObject>(),previousProductIdsSet);
        else
            recommendationAdapter = new RecommendationAdapter(getActivity(),new ArrayList<JSONObject>());*/
        mRecyclerView.setAdapter(recommendationAdapter);


        daveAIPerspective= DaveAIPerspective.getInstance();
        ModelUtil modelUtil = new ModelUtil(getActivity());
        productIdAttrName = modelUtil.getProductIdAttrName();
        productModelName = modelUtil.getProductModelName();
        productModelInstance = Model.getModelInstance(context, productModelName);
        defaultFilterList = modelUtil.getProductFilterAttrList();

        if (defaultFilterList != null && !defaultFilterList.isEmpty()) {
            for (Map.Entry<String, ArrayList<String>> entry : defaultFilterList.entrySet()) {
                if (productModelInstance != null)
                    filterTitleMap.put(productModelInstance.getAttributeTitle(entry.getKey()), entry.getKey());
            }
            filterAttr.addAll(filterTitleMap.keySet());
        }

        //show hierarchy
        //hierarchyAttributes = daveAIPerspective.getProduct_category_hierarchy();
       // if(daveAction.equalsIgnoreCase(RECOMMENDATION_ACTION) && hierarchyAttributes!=null && hierarchyAttributes.size()>0){
        if(daveAction.equalsIgnoreCase(DaveFunction.RECOMMENDATION_ACTION)){
            breadcrumbWrapper.setVisibility(View.VISIBLE);
            showDefaultCategoryHierarchy();
        }else {
            breadcrumbWrapper.setVisibility(View.GONE);
        }

        mPageNumber = 1;
        getRecommFromServer(true,true,true);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) { //check for scroll down
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if (!isLastPage && (visibleItemCount + pastVisiblesItems) >= (totalItemCount-daveAIPerspective.getCount_after_next_recommendations())) {
                            loading = false;
                            //Do pagination.. i.e. fetch new data from Server
                            //needToAddProductIds = true;
                            mPageNumber++;
                            if(isAnyActionApply){
                                getRecommFromServer(false,false,false);
                            }else {

                                getRecommFromServer(false,false,true);
                            }

                        }
                    }
                   /* if (!isLoading() && !isLastPage()) {
                        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                            loadMoreItems();
                        }
                    }*/
                }
            }
        });


        return view;
    }

  /*  // Fires when a configuration change occurs and fragment needs to save state
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("customer_id",customerId);
        outState.putSerializable("restore_list", (Serializable) serverDataSet);
        Log.e(TAG,"onSaveInstanceState:--------------"+outState);

    }*/

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // Log.i(TAG,"onViewCreated>>>>>>>>>>>>>>>>>>>>>>>>>>>>:- ");
        //you can set the title for your toolbar here for different fragments different titles
         if(daveAction.equalsIgnoreCase(DaveFunction.RECOMMENDATION_ACTION))
             getActivity().setTitle(daveAIPerspective.getRecommendations_btn_name());

         else if(daveAction.equalsIgnoreCase(DaveFunction.SIMILAR_ACTION))
             getActivity().setTitle(daveAction);

         /* Log.e(TAG,"GET Save Instance savedInstanceState:------------------"+savedInstanceState);
          if(savedInstanceState!=null){
            String getCustomerId= savedInstanceState.getString("customer_id");
            assert getCustomerId != null;
            if(getCustomerId.equals(customerId)){
                serverDataSet = (ArrayList<JSONObject>) savedInstanceState.getSerializable("restore_list");
                Log.e(TAG,"GET Save Instance :------------------"+serverDataSet);
            }else
                serverDataSet= new ArrayList<>();

        }else
            serverDataSet= new ArrayList<>();*/


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dave_function, menu);
        super.onCreateOptionsMenu(menu, inflater);
        //Log.i(TAG,"onCreateOptionsMenu>>>>>>>>>>>>>>>>>>>>>>>>>>>>:- ");
        if(daveAIPerspective.isEnable_manage_product()) {
            MenuItem item = menu.findItem(R.id.actionAddProduct);
            item.setVisible(true);
        }

        if(daveAIPerspective.getScan_btn_name()!=null) {
            MenuItem item = menu.findItem(R.id.actionScanProduct);
            item.setTitle(daveAIPerspective.getScan_btn_name());
            item.setVisible(true);
        }

        if(daveAIPerspective.isEnable_set_default_quantity()) {
            MenuItem item = menu.findItem(R.id.actionSetQuantity);
            item.setVisible(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Log.i(TAG,"onOptionsItemSelected>>>>>>>>>>>>>>>>>>>>>>>>>>>>:- ");
        int id = item.getItemId();
        searchedTerm= "";
        menuItemActionLayout.removeAllViews();

        if (id == R.id.actionSearch) {
            actionOnSearchMenuButton();
            return true;

        } else if (id == R.id.actionFilter) {
            actionOnFilterMenuButton();
            return true;

        } else if (id == R.id.actionCategory) {

            DaveAIStatic.categoryBackStack.clear();
            selectedFilterMap.clear();
            selectedCategoryMap.clear();

            Intent intent = new Intent(getActivity(), CategoryHierarchy.class);
            intent.putExtra("NewCustomerId", customerId);
            startActivity(intent);
            // startActivityForResult(intent, CategoryHierarchy.CATEGORY_HIERARCHY_CODE);// Activity is started with requestCode 2
            //getActivity().finish();

            return true;

        } else if (id == R.id.actionSort) {
            if (CheckNetworkConnection.networkHasConnection(context)) {
                actionOnSortMenuButton();
            } else {
                CheckNetworkConnection.showNetDisabledAlertToUser(context,"Internet Connection is required for this feature.");
            }

            return true;

        } else if (id == R.id.actionSetQuantity) {
            actionOnSetQtyMenuButton();
            return true;
        }
        else if (id == R.id.actionScanProduct) {
            Bundle b= new Bundle();
            b.putString("action_name", AddScanProduct.ACTION_SCAN_PRODUCT);
            b.putString("customer_id",customerId);
            Intent intent= new Intent(getActivity(), AddScanProduct.class);
            intent.putExtras(b);
            startActivity(intent);

            return true;
        }
        else if (id == R.id.actionAddProduct) {

            Bundle b= new Bundle();
            b.putString("action_name", AddScanProduct.ACTION_ADD_PRODUCT);
            b.putString("customer_id",customerId);
            Intent intent= new Intent(getActivity(), AddScanProduct.class);
            intent.putExtras(b);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG,"onResume>>>>>>>>>daveAction:- "+daveAction +" categoryStackFlow:- "+categoryStackFlow);

    }

        @Override
    public void onPause(){
        super.onPause();
       // Log.i(TAG,"onPause>>>>>>>>>>>>>>>>>>>>>>>>>>>>:- ");

    }


    @Override
    public void onStop() {
        super.onStop();
       // Log.i(TAG,"onStop>>>>>>>>>>>>>>>>>>>>>>>>>>>>:- ");

    }

    @Override
    public void onDetach() {
        super.onDetach();
        //Log.i(TAG,"onDetach>>>>>>>>>>>>>>>>>>>>>>>>>>>>:- ");

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here
        Log.e(TAG,"CategoryHierarchy.CATEGORY_HIERARCHY_CODE:--------   "+ CategoryHierarchy.CATEGORY_HIERARCHY_CODE);
        if(requestCode == CategoryHierarchy.CATEGORY_HIERARCHY_CODE) {
            Intent intent = new Intent(getActivity(), DaveFunction.class);
            intent.putExtra("msg_to_dave_func", data.getSerializableExtra("CategoryMsg"));
            startActivity(intent);
        }
    }

    private String getRecommOrSimilarURL() {

        String apiUrl = "";
        if(customerId!=null && !customerId.isEmpty()){
            apiUrl = new APIRoutes().recommendationAPI(customerId, strProductId);
        }
        else{
            apiUrl = APIRoutes.similarAPI(strProductId);
        }
        HttpUrl.Builder urlBuilder = HttpUrl.parse(apiUrl).newBuilder();
        urlBuilder.addQueryParameter(POST_PAGE_NUMBER, String.valueOf(mPageNumber));
        urlBuilder.addQueryParameter(POST_PAGE_SIZE, String.valueOf(daveAIPerspective.getNo_of_recommendations()));

        if(quantityPredictStatus)
            urlBuilder.addQueryParameter("_quantity_predict", Boolean.TRUE.toString());

        if(strRecommendationId!=null && !strRecommendationId.isEmpty()){
            urlBuilder.addQueryParameter("_recommendation_id", strRecommendationId);
        }
        if(!categorySelectedItems.isEmpty() && categorySelectedItems!=null){
            for (String Getkey : categorySelectedItems.keySet()) {
                if(categorySelectedItems.get(Getkey) instanceof ArrayList) {
                    ArrayList getObject = (ArrayList) categorySelectedItems.get(Getkey);
                    for (int j = 0; j < getObject.size(); j++) {
                        urlBuilder.addQueryParameter("_additional_values[_s_" + Getkey + "]", getObject.get(j).toString());
                    }
                }else if(categorySelectedItems.get(Getkey) instanceof String){
                    urlBuilder.addQueryParameter("_additional_values[_s_" + Getkey + "]", categorySelectedItems.get(Getkey).toString());

                }
            }
        }
        for (String Getkey : selectedFilterMap.keySet()) {
            Object object = selectedFilterMap.get(Getkey);
            if(object instanceof ArrayList) {
                ArrayList<String> get_lis = (ArrayList<String>) selectedFilterMap.get(Getkey);
                for (int j = 0; j < get_lis.size(); j++) {
                    urlBuilder.addQueryParameter(Getkey, get_lis.get(j));
                }
            }else if(object instanceof  String){
                urlBuilder.addQueryParameter(Getkey,object.toString());
            }

        }
        if(searchedTerm!=null && !searchedTerm.isEmpty()){
            urlBuilder.addQueryParameter("_keyword", searchedTerm);
        }
        if (!strRadioBtnClicking.equalsIgnoreCase("")) {
            urlBuilder.addQueryParameter("_sort_by", strRadioBtnClicking);
        }

        return urlBuilder.build().toString();
    }

    private HashMap<String,Object> getQueryParamForOfflineCall() {

        HashMap<String,Object> queryParam = new HashMap<>();
        queryParam.put(POST_PAGE_NUMBER,mPageNumber);
        queryParam.put(POST_PAGE_SIZE,daveAIPerspective.getNo_of_recommendations());
        for (String Getkey : selectedFilterMap.keySet()) {
            Object object = selectedFilterMap.get(Getkey);
            if(object instanceof ArrayList) {
                ArrayList<String> getListValue = (ArrayList<String>) selectedFilterMap.get(Getkey);
                for (int i = 0; i < getListValue.size(); i++) {
                    String checkNull = getListValue.get(i);
                    if (checkNull == null || checkNull.isEmpty() || checkNull.equals("null"))
                        getListValue.remove(i);
                }
                if (getListValue.size() > 0) {
                    queryParam.put(Getkey, getListValue);
                }
            }else if(object instanceof String){
                if(!object.equals("null"))
                    queryParam.put(Getkey,selectedFilterMap.get(Getkey));
            }
        }
        /*for (String Getkey : selectedFilterMap.keySet()) {
            queryParam.put(Getkey,selectedFilterMap.get(Getkey));
        }*/
        if(searchedTerm!=null && !searchedTerm.isEmpty()){
            queryParam.put("_keyword", searchedTerm);
        }

        return queryParam;
    }

    private void getRecommFromServer( boolean loader ,boolean isLoadNewData, boolean isUpdateDefault) {

        if(!isAnyActionApply)
            defaultRecommendationId = strRecommendationId;
        if(isLoadNewData) {
            strRecommendationId="";
            recommendationAdapter.clearPreviousList();
            Log.d(TAG,"<<<<<Clear Previous Recommendation >>>>>>>>>>>>>>>>>>>>"+ recommendationAdapter.getItemCount());

        }
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    JSONObject recomResponse = new JSONObject(response);
                    if(recomResponse.has(RECOMMENDATION_ID)&& !recomResponse.isNull(RECOMMENDATION_ID))
                        strRecommendationId = recomResponse.getString(RECOMMENDATION_ID);
                    if(recomResponse.has(IS_LAST_PAGE) && !recomResponse.isNull(IS_LAST_PAGE))
                        isLastPage = recomResponse.getBoolean(IS_LAST_PAGE);

                    //JSONArray jsonArray = recomResponse.getJSONArray("recommendations");
                    JSONArray jsonArray = null;
                    if(recomResponse.has("recommendations")){
                        jsonArray = recomResponse.getJSONArray("recommendations");

                    }else if(recomResponse.has("similar_products")){
                        jsonArray = recomResponse.getJSONArray("similar_products");
                    }

                    List<JSONObject> recommList = new ArrayList<>();
                    DaveModels daveModels = new DaveModels(context,true);
                    ModelUtil modelUtil = new ModelUtil(context);

                    if(jsonArray!=null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            // if product not exist in table add it in database
                            JSONObject data = jsonArray.getJSONObject(i);
                            DatabaseManager.getInstance(context).insertObjectDataIfNotThere(
                                    modelUtil.getProductModelName(),
                                    new ObjectsTableRowModel(data.getJSONObject("product_attributes").getString(modelUtil.getProductIdAttrName()),
                                            data.getJSONObject("product_attributes").toString(), System.currentTimeMillis())
                            );
                            //replace serverfile with local file
                            recommList.add(jsonArray.getJSONObject(i)
                                    .put("product_attributes", new DaveCachedUtil(context).replaceServerPathWithLocalFilePath(productModelName, jsonArray.getJSONObject(i).getJSONObject("product_attributes"))));
                        }
                    }
                    Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<Print Recommendation count>>>>>>>>>>>>>>>>>>>"+recommList.size());

                    if (recommList.size() > 0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        notFound.setVisibility(View.GONE);
                        if(isLoadNewData) {
                            recommendationAdapter.addNewSeedList(recommList,isUpdateDefault);

                        }else
                            recommendationAdapter.updateSeedList(recommList,isUpdateDefault);

                        mRecyclerView.scrollToPosition(pastVisiblesItems);
                        recommendationAdapter.notifyDataSetChanged();
                        loading = true;
                        if(customerId!=null && !customerId.isEmpty()) {
                           /* RecycleViewSwipeHelper callback = new RecycleViewSwipeHelper(DaveRecommendations.this, getActivity(),
                                    recommendationAdapter, customerId, strProductId);
                            ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                            touchHelper.attachToRecyclerView(mRecyclerView);*/

                            //todo testing new swipe feature
                            RecycleViewSwipe recycleViewSwipe = new RecycleViewSwipe(DaveRecommendations.this,getActivity());
                            ItemTouchHelper touchHelper = new ItemTouchHelper(recycleViewSwipe);
                            touchHelper.attachToRecyclerView(mRecyclerView);

                        }

                    }

                    if(mRecyclerView.getAdapter().getItemCount() == 0){
                        notFound.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                }
                catch (Exception exception){
                    exception.printStackTrace();
                    Log.e(TAG, "Error in showing recommendation or similar data+++++++++++" + exception.getMessage());
                }

            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                progressBar.setVisibility(View.GONE);
                Log.e(TAG,"onTaskFailure>>>>>>>>>>>>>>>>>>   "+requestCode +" "+ responseMsg );
                if( responseMsg.contains("does not have any objects on ids"))
                    Toast.makeText(getActivity(),"Error saving customer, please check profile to resolve problem",Toast.LENGTH_LONG).show();
                else {
                   // Toast.makeText(context, responseMsg, Toast.LENGTH_LONG).show();
                    isOnlineMode = false;
                    getOfflineRecommendations(isLoadNewData,isUpdateDefault);
                }
            }

        };
        try {
            if(loader){
                progressBar.setVisibility(View.VISIBLE);
            }

            Log.e(TAG,"<<<<<getRecommFromServer isOnlineMode>>>>>>>>>>>>>>>>>>>>"+ isOnlineMode);
            if (isOnlineMode && CheckNetworkConnection.networkHasConnection(context)) {
                if(mapSearchByImageUrl!= null && mapSearchByImageUrl.length()>0){
                    // Example :--  {'_instance' :{(attributes key value map)}}
                    JSONObject postBody = new JSONObject();
                    postBody.put("_instance", mapSearchByImageUrl);
                    RequestBody body_part = RequestBody.create(APIRoutes.MEDIA_TYPE_JSON, postBody.toString());
                    new APICallAsyncTask(postTaskListener, context, "POST", body_part,false).execute(getRecommOrSimilarURL());
                }else
                    new APICallAsyncTask(postTaskListener, getActivity(), "GET", false,12L).execute(getRecommOrSimilarURL());


            } else {
                //CheckNetworkConnection.showNetDisabledAlertToUser(context);
                //todo call offline recomm if no wifi
                if(mapSearchByImageUrl != null && mapSearchByImageUrl.length()>0){
                    JSONObject postBody = new JSONObject();
                    postBody.put("_instance", mapSearchByImageUrl);
                    RequestBody body_part = RequestBody.create(APIRoutes.MEDIA_TYPE_JSON, postBody.toString());
                    //new APICallAsyncTask(postTaskListener, context, "POST", body_part,true).execute(getRecommOrSimilarURL());
                }else {
                    getOfflineRecommendations(isLoadNewData,isUpdateDefault);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Error During get Response:- " + e.getLocalizedMessage());
        }

    }

    // get offline recommendation
    private void getOfflineRecommendations(boolean isLoadNewData, boolean isUpdateDefault){
        DaveModels daveModels = new DaveModels(getActivity(),true);
        try {
            SharedPreference sharedPreference = new SharedPreference(getActivity());
            String orderId = sharedPreference.readString(customerId);
            daveModels.getRecommendationAndSimilar(customerId, strProductId, orderId, getQueryParamForOfflineCall(), new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    progressBar.setVisibility(View.GONE);
                    setOrUpdateDataInAdapter(jsonObject,isLoadNewData,isUpdateDefault);
                }

                @Override
                public void onResponseFailure(int i, String s) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(),s,Toast.LENGTH_SHORT).show();
                }
            });
        } catch (DaveException e) {
            e.printStackTrace();
        }
    }

    private void setOrUpdateDataInAdapter(JSONObject response,boolean isLoadNewData, boolean isUpdateDefault){
        try {

            List<JSONObject> recommList = ObjectData.getJSONObjectDataList(response);
            Log.e(TAG,"<<<<<<<<<<<<<setOrUpdateDataInAdapter Print Recommendation count>>>>>>>>>>>>>>>>>>>"+recommList.size());
            if (recommList.size() > 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
                notFound.setVisibility(View.GONE);
                if(isLoadNewData) {
                    recommendationAdapter.addNewSeedList(recommList,isUpdateDefault);

                }else
                    recommendationAdapter.updateSeedList(recommList,isUpdateDefault);

                recommendationAdapter.notifyDataSetChanged();
                loading = true;
                if(customerId!=null && !customerId.isEmpty()) {
                /*    RecycleViewSwipeHelper callback = new RecycleViewSwipeHelper(DaveRecommendations.this, getActivity(),
                            recommendationAdapter, customerId, strProductId);
                    ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                    touchHelper.attachToRecyclerView(mRecyclerView);*/

                    //todo testing new swipe feature
                    RecycleViewSwipe recycleViewSwipe = new RecycleViewSwipe(DaveRecommendations.this,getActivity());
                    ItemTouchHelper touchHelper = new ItemTouchHelper(recycleViewSwipe);
                    touchHelper.attachToRecyclerView(mRecyclerView);
                }

            }else {
                if (mRecyclerView.getAdapter().getItemCount() == 0) {
                    notFound.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
            }
        }
        catch (Exception exception){
            exception.printStackTrace();
            Log.e(TAG, "Error in showing recommendation or similar data+++++++++++" + exception.getMessage());
        }

    }

    private void showDefaultCategoryHierarchy(){
        try {
            hierarchyAttributes = daveAIPerspective.getProduct_category_hierarchy();
            if (hierarchyAttributes != null && !hierarchyAttributes.isEmpty()) {
                for(int i = 0;i < hierarchyAttributes.size();i++) {
                    if(productModelInstance!=null){
                        hierarchyAttrTitleMap.put(hierarchyAttributes.get(i).toString(),productModelInstance.getAttributeTitle(hierarchyAttributes.get(i).toString()));
                    }
                }
                Log.e(TAG,"Print hierarchyAttrTitleMap:------------"+hierarchyAttrTitleMap);

            }

            Log.i(TAG, "showDefaultCategoryHierarchy List:- " + hierarchyAttributes + " breadCrumbDetails:-" + breadCrumbDetails);
            if(hierarchyAttributes != null) {
                breadcrumbWrapper.setVisibility(View.VISIBLE);
                //Map<String, Object> categoryLevelData = new ModelUtil(context).getProductCategoryHierarchy();
                for (int index = 0; index < hierarchyAttributes.size(); index++) {

                    final ViewGroup crumbLayout = (ViewGroup) LayoutInflater.from(context)
                            .inflate(R.layout.bread_crumbs_view, breadcrumbWrapper, false);

                    TextView crumbArrow = crumbLayout.findViewById(R.id.crumbArrow);
                    crumbArrow.setVisibility(View.GONE);
                    Spinner crumbSpinner = crumbLayout.findViewById(R.id.crumbSpinner);
                    breadCrumbsLayout.addView(crumbLayout);
                    if (index > 0) {
                        crumbArrow.setVisibility(View.VISIBLE);
                    }
                    String hierarchyTitle = hierarchyAttrTitleMap.get(hierarchyAttributes.get(index));
                    ArrayList<String> categories = new ArrayList<String>();
                    categories.add(hierarchyTitle);
                    Log.e(TAG, "showDefaultCategoryHierarchy  hierarchyTitle:-" + hierarchyTitle);
                    if (breadCrumbDetails != null && !breadCrumbDetails.isEmpty()  && categoryStackFlow.get(index).containsKey(breadCrumbDetails.get(index))) {
                        Map<String, Object> categoryLevelData = (Map<String, Object>) categoryStackFlow.get(index).get(breadCrumbDetails.get(index));
                        ArrayList<String> cat = new ArrayList(categoryLevelData.keySet());
                        Collections.sort(cat);
                        categories.addAll(cat);
                        Log.i(TAG, " set Previous Value  InBreadCrumbLayout ArrayList" + categoryLevelData.keySet());
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, categories);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        crumbSpinner.setAdapter(dataAdapter);

                        int selectedPos = categories.indexOf(breadCrumbDetails.get(index));
                        crumbSpinner.setSelection(selectedPos,false);


                    } /*else {
                        if (breadCrumbDetails == null)
                            breadCrumbDetails = new ArrayList<>();
                        breadCrumbDetails.add(index, hierarchyTitle);
                        Log.e(TAG, "************set Default Value In BreadCrumbLayout:-" + breadCrumbDetails.get(index) + " && position " + index);
                        if (index == 0) {
                            Map<String, Object>  categoryLevelData = new ModelUtil(context).getProductCategoryHierarchy();
                            ArrayList<String> cat = new ArrayList(categoryLevelData.keySet());
                            Collections.sort(cat);
                            categories.addAll(cat);
                            Map<Object, Object> updatedLevel = new HashMap<>();
                            updatedLevel.put(hierarchyTitle, categoryLevelData);
                            categoryStackFlow.add(index, updatedLevel);
                        }
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, categories);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        crumbSpinner.setAdapter(dataAdapter);
                    }*/

                    final HashMap<String, Object> intentDetails = new HashMap<>();
                    intentDetails.put("category_level", index);
                    intentDetails.put("back_navigation_item", hierarchyTitle);
                    intentDetails.put("back_navigation_position", index);

                    crumbSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            //Log.e(TAG,"Spinner setOnItemSelectedListener:----"+isSpinnerTouched +" Selected Item:-"+ crumbSpinner.getSelectedItem());
                             /* if (position > 0) {
                                    intentDetails.put("selected_item", crumbSpinner.getSelectedItem());
                                    onClickCategoryHierarchy(intentDetails);
                                }*/
                            intentDetails.put("selected_item", crumbSpinner.getSelectedItem());
                            onClickCategoryHierarchy(intentDetails);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }
            }


        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during show Category Hierarchy:--- "+e.getMessage());
        }

    }

    private void onClickCategoryHierarchy(HashMap<String, Object> hashMap ){
        try {
            int categoryLevel = (int) hashMap.get("category_level");
            String preValue = breadCrumbDetails.get(categoryLevel).toString();
            String selectedItem = hashMap.get("selected_item").toString();
            String categoryName = hierarchyAttributes.get(categoryLevel).toString();
            String hierarchyTitle = hierarchyAttrTitleMap.get(categoryName);

            Log.e(TAG,"************onClickCategoryHierarchy Start position: "+ categoryLevel +" preValue: "+preValue+ " selectedItem: "+selectedItem );
            if(categoryLevel == 0) {
                selectedFilterMap.clear();
                categorySelectedItems.clear();
            }else{
                for (int i = categoryLevel; i < breadCrumbDetails.size(); i++) {
                    if (selectedFilterMap.containsKey(hierarchyAttributes.get(i).toString())) {
                        selectedFilterMap.remove(hierarchyAttributes.get(i).toString());
                    }
                    if (categorySelectedItems.containsKey(hierarchyAttributes.get(i).toString())) {
                        categorySelectedItems.remove(hierarchyAttributes.get(i).toString());
                    }

                }
            }
            Log.i(TAG, "Print Filter selectedFilterMap :--------" +categoryLevel+" selectedFilterMap== "+selectedFilterMap +" \n categoryStackFlow"+categoryStackFlow);
            if (!preValue.equalsIgnoreCase(selectedItem) ||
                    (preValue.equalsIgnoreCase(hierarchyTitle) && !selectedItem.equalsIgnoreCase(hierarchyTitle))){
                Map<Object, Object> getLevelValue = categoryStackFlow.get(categoryLevel);
                for(int i= categoryStackFlow.size()-1; i >= categoryLevel;i--){
                    categoryStackFlow.pop();

                }
                Map<String, Object> categoryLevelData = (Map<String, Object>)getLevelValue.get(preValue);
                //add current level selected item
                breadCrumbDetails.set(categoryLevel, selectedItem);
                Map<Object, Object> updatedLevel = new HashMap<>();
                updatedLevel.put(selectedItem, categoryLevelData);
                categoryStackFlow.add(categoryLevel, updatedLevel);

                Log.i(TAG," Print onClickCategoryHierarchy:-"+ breadCrumbDetails +" "+ categoryStackFlow);

                if (categoryLevel < breadCrumbDetails.size() - 1 ) {

                    String nextHierarchyTitle = hierarchyAttrTitleMap.get(hierarchyAttributes.get(categoryLevel + 1));
                    breadCrumbDetails.set(categoryLevel + 1, nextHierarchyTitle);
                    View v = breadCrumbsLayout.getChildAt(categoryLevel + 1);
                    Spinner getSpinner = v.findViewById(R.id.crumbSpinner);
                    ArrayList<String> nextCategoriesList = new ArrayList<String>();
                    nextCategoriesList.add(nextHierarchyTitle);

                    if(!selectedItem.equalsIgnoreCase(hierarchyTitle)){
                        categoryLevelData = (Map<String, Object>) categoryLevelData.get(selectedItem);
                        Map<Object, Object> updatedNextLevel = new HashMap<>();
                        updatedNextLevel.put(nextHierarchyTitle, categoryLevelData);
                        categoryStackFlow.add(categoryLevel + 1, updatedNextLevel);

                        ArrayList<String> cat = new ArrayList(categoryLevelData.keySet());
                        Collections.sort(cat);
                        nextCategoriesList.addAll(cat);
                        getSpinner.performClick();
                    }else{
                        Log.i(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<<<Selected value is not select>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    }
                    Log.i(TAG, "nextLevelDetails List Element :- " + nextCategoriesList + " nextLevelDetails:- " + categoryLevelData);
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, nextCategoriesList);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    getSpinner.setAdapter(dataAdapter);
                    getSpinner.setSelection(getSpinner.getSelectedItemPosition(), false);

                    Log.i(TAG, " if nextLevelData   :- " + (categoryLevel + 1) + " is ==  " + (breadCrumbDetails.size() - 1));
                    if (categoryLevel + 1 < breadCrumbDetails.size() - 1) {
                        for (int k = categoryLevel + 2; k < breadCrumbsLayout.getChildCount(); k++) {
                            View view = breadCrumbsLayout.getChildAt(k);
                            Spinner getNextSpinner = view.findViewById(R.id.crumbSpinner);
                            ArrayList<String> nextSelectList = new ArrayList<String>();
                            nextSelectList.add(hierarchyAttrTitleMap.get(hierarchyAttributes.get(k)));
                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, nextSelectList);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            getNextSpinner.setAdapter(spinnerAdapter);
                            getNextSpinner.setSelection(getNextSpinner.getSelectedItemPosition(), false);
                            breadCrumbDetails.set(k, hierarchyAttrTitleMap.get(hierarchyAttributes.get(k)));
                            Log.i(TAG, " for loop  if nextLevelData   :- " + breadCrumbDetails +"categoryStackFlow:- "+categoryStackFlow);
                        }

                    }
                }
                Log.i(TAG, "onClickCategoryHierarchy End  preValue is Select breadCrumbDetails :- " +breadCrumbDetails+" && selectedFilterMap:-"+ selectedFilterMap
                        +" \n categoryStackFlow:-  "+categoryStackFlow);
                if(!selectedItem.equalsIgnoreCase(hierarchyTitle)) {
                    addCheckedItemToMapDefault(categoryName, selectedItem);
                    categorySelectedItems.put(categoryName, selectedItem);
                }
                mPageNumber = 1;
                strRecommendationId ="";
                searchedTerm="";
                strRadioBtnClicking="";
                isAnyActionApply= false;
                getRecommFromServer(true,true,true);


            }else{
                Log.e(TAG,"Previous value is same as current selected value>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during updateBreadcrumb :- " + e.getMessage());
        }
    }


    private void actionOnSearchMenuButton(){

        selectedFilterMap.clear();
        Log.e("ActionSearch", "*****Click On Search Button *******"+selectedFilterMap);

        View searchView = getLayoutInflater().inflate(R.layout.search_layout, menuItemActionLayout, true);
        RelativeLayout searchLayout= searchView.findViewById(R.id.searchLayout);
        final EditText  editTextSearch = searchView.findViewById(R.id.editTextSearch);
        final ImageView searchButton = searchView.findViewById(R.id.searchButton);
        Button cancelButton = searchView.findViewById(R.id.cancelButton);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextSearch.setText("");
                searchedTerm= "";
                menuItemActionLayout.removeAllViews();
                strRecommendationId = defaultRecommendationId;
                notFound.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                recommendationAdapter.resetPreviousList();

            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionOnSearchButton(editTextSearch);
            }
        });
        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    actionOnSearchButton(editTextSearch);
                    return true;
                }
                return false;
            }
        });
        editTextSearch.addTextChangedListener(new TextWatcher() {
            boolean searchedFlag = false;
            Timer timer;
            Handler handler;

            public void afterTextChanged(Editable s) {
                searchedTerm = s.toString();
                timer = new Timer();
                handler = new Handler();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            public void run() {
                                //do Stuff here
                                if (searchedTerm.length() > 0) {
                                    getDataOnSearch(false);
                                }
                            }
                        });
                    }
                }, 2000); // 2000ms delay before the timer executes the „run“ method from TimerTask
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                // final String term = query.toString();
                searchedTerm = query.toString();
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
                if(!searchedFlag && searchedTerm.length() > 2){
                    searchedFlag= true;
                    getDataOnSearch(false);

                }
            }
        });


    }

    private void actionOnSearchButton(EditText editTextSearch){
        searchedTerm=editTextSearch.getText().toString();
        if(searchedTerm.length()>0) {
            getDataOnSearch(true);
        }
        else
            Toast.makeText(getActivity(),"Please Enter Search Term",Toast.LENGTH_SHORT).show();
    }

    private void getDataOnSearch(boolean loader){
        //getFilteredItemFromServer(editTextSearch);
        isAnyActionApply = true;
        strRecommendationId ="";
        strRadioBtnClicking="";
        mPageNumber =1;
        getRecommFromServer(loader,true,false);


    }
    EditText filterTypeView;
    RelativeLayout priceRangeLayout;
    private void actionOnFilterMenuButton(){
        if(selectedFilterMap==null || selectedFilterMap.isEmpty()){
            selectedFilterMap = selectedCategoryMap;
        }
        // Log.e("ActionFilter", "***** After pasting Data Filter Action*******"+selectedFilterMap);
        View  filterView = getLayoutInflater().inflate(R.layout.filter_layout, menuItemActionLayout, true);
        filterParentView = filterView.findViewById(R.id.filterParentView);
        filterChildView = filterView.findViewById(R.id.filterChildView);
        priceRangeLayout =  filterView.findViewById(R.id.priceRangeLayout);
        rangeSeekbar =  filterView.findViewById(R.id.rangeSeekbar1);
        priceMin =  filterView.findViewById(R.id.priceMin);
        priceMax =  filterView.findViewById(R.id.priceMax);
        filterTypeView = filterView.findViewById(R.id.typeView);
        Button btnFilter= filterView.findViewById(R.id.btnFilter);
        Button btnCancel= filterView.findViewById(R.id.btnCancel);

        if(filterAttr!=null && filterAttr.size()>0) {
            filterParentAdapter = new ArrayAdapter<>(getActivity(), R.layout.filter_title, filterAttr);
            filterParentView.setAdapter(filterParentAdapter);
        }

        if(filterTitleMap!=null && !filterTitleMap.isEmpty()) {
            String filterFirstItem = filterTitleMap.get(filterAttr.get(0));
            clickOnParentFilterView(filterFirstItem);
            /*filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(filterFirstItem));
            filterChildView.setAdapter(filterChildAdapter);
            setCheckedMultipleItemAgain(filterFirstItem, filterChildAdapter);*/
        }
        filterParentView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String key_name = (String) parent.getItemAtPosition(position);
                String filterKey = filterTitleMap.get(key_name);
                filter_position = position;
                clickOnParentFilterView(filterKey);
                /*if (defaultFilterList != null && !defaultFilterList.isEmpty()) {
                    for (String key : defaultFilterList.keySet()) {
                        if (key.equalsIgnoreCase(filterKey)) {
                            if (filterKey.equals("price") || filterKey.equals("discount")) {
                                filterChildView.setVisibility(View.GONE);
                                priceRangeLayout.setVisibility(View.VISIBLE);
                                showPriceRangeLayout(key, defaultFilterList.get(key));
                            } else {
                                priceRangeLayout.setVisibility(View.GONE);
                                filterChildView.setVisibility(View.VISIBLE);
                                filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(key));
                                filterChildView.setAdapter(filterChildAdapter);
                                filterChildAdapter.notifyDataSetChanged();
                                setCheckedMultipleItemAgain(filterKey, filterChildAdapter);
                            }
                            break;
                        }
                    }
                }*/
            }
        });
        filterParentView.setSelection(0);
        filterParentView.setItemChecked(0, true);

        filterChildView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < filterParentView.getAdapter().getCount(); i++) {
                    if (i == filter_position) {
                        String filterKeyName = filterTitleMap.get(filterParentView.getAdapter().getItem(i).toString());
                        saveMultiselectedList(filterKeyName);
                        break;
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemActionLayout.removeAllViews();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedFilterMap != null && !selectedFilterMap.isEmpty()) {
                    menuItemActionLayout.removeAllViews();
                    mPageNumber =1;
                    strRecommendationId ="";
                    isAnyActionApply = true;
                    getRecommFromServer(true,true,false);

                }else
                    Toast.makeText(getActivity(),"Please select filter option",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void clickOnParentFilterView(String filterKey){

        if(checkCustomFilterAttr(productModelInstance.getAttributeType(filterKey))){
            filterChildView.setVisibility(View.GONE);
            priceRangeLayout.setVisibility(View.GONE);
            filterTypeView.setVisibility(View.VISIBLE);
            filterTypeView.setHint(filterKey);
            if(selectedFilterMap.containsKey(filterKey)) {
                filterTypeView.setText(selectedFilterMap.get(filterKey).toString().replace("~",""));
            }

            filterTypeView.addTextChangedListener(new TextWatcher() {

                public void afterTextChanged(Editable s) {
                    String typeValue = s.toString();
                    if(typeValue.length()>0)
                        selectedFilterMap.put(filterKey,"~"+typeValue);
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                public void onTextChanged(CharSequence query, int start, int before, int count) {
                    String searchedTerm = query.toString().trim();

                }
            });


        }
        else if(productModelInstance.getAttributeType(filterKey).equalsIgnoreCase("price")||
                productModelInstance.getAttributeType(filterKey).equalsIgnoreCase("discount")){
            Log.i(TAG,"Print Filter attr type>>>>>>>>>>>>>>>>>>>>>>"+productModelInstance.getAttributeType(filterKey));
            filterChildView.setVisibility(View.GONE);
            priceRangeLayout.setVisibility(View.VISIBLE);
            filterTypeView.setVisibility(View.GONE);
            showPriceRangeLayout(filterKey, defaultFilterList.get(filterKey));
        }
        else {
            priceRangeLayout.setVisibility(View.GONE);
            filterTypeView.setVisibility(View.GONE);
            filterChildView.setVisibility(View.VISIBLE);
            filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(filterKey));
            filterChildView.setAdapter(filterChildAdapter);
            filterChildAdapter.notifyDataSetChanged();
            setCheckedMultipleItemAgain(filterKey, filterChildAdapter);
        }

    }


    private boolean checkCustomFilterAttr(String filterAttrType){

        return filterAttrType.equals("name") || filterAttrType.equals("uid") || filterAttrType.equals("email")
                || filterAttrType.equals("phone_number");

    }

    private void actionOnSortMenuButton(){
        View  sortView = getLayoutInflater().inflate(R.layout.sorting_layout, menuItemActionLayout, true);
        RadioGroup sortingRadiogroup = sortView.findViewById(R.id.sortingRadiogroup);
        sortingRadiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                View radioButton = group.findViewById(checkedId);
                int index = group.indexOfChild(radioButton);
                switch (index) {
                    case 0:
                        strRadioBtnClicking = "price_low";
                        break;
                    case 1:
                        strRadioBtnClicking = "price_high";
                        break;
                    case 2:
                        strRadioBtnClicking = "discount";
                        break;
                    case 3:
                        strRadioBtnClicking = "trending";
                        break;
                    case 4:
                        strRadioBtnClicking = "popularity";
                        break;
                    case 5:
                        strRadioBtnClicking = "new";
                        break;

                }
                mPageNumber=1;
                isAnyActionApply = true;
                strRecommendationId = "";
                getRecommFromServer(true,true,false);
                menuItemActionLayout.removeAllViews();
            }
        });

    }

    int counter = 0;
    private void actionOnSetQtyMenuButton(){

        View  setQtyView = getLayoutInflater().inflate(R.layout.set_quantity_layout, menuItemActionLayout, true);
        Button btnUnSetQty, btnSetQty;
        final EditText applyQtyEditText;
        ImageView addQty,subtractQty;

        btnUnSetQty = setQtyView.findViewById(R.id.btnUnSetQty);
        btnSetQty =  setQtyView.findViewById(R.id.btnSetQty);
        applyQtyEditText = setQtyView.findViewById(R.id.applyQtyEditText);
        subtractQty = setQtyView.findViewById(R.id.subtract_qty);
        addQty = setQtyView.findViewById(R.id.add_qty);

        if(defaultQuantityStatus!= -1)
            applyQtyEditText.setText( String.valueOf(defaultQuantityStatus));

        addQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkQty = String.valueOf(applyQtyEditText.getText());
                if(checkQty!=null && !checkQty.isEmpty())
                    counter=Integer.parseInt(checkQty);
                counter++;
                applyQtyEditText.setText(Integer.toString(counter));
            }
        });

        subtractQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String checkQty = String.valueOf(applyQtyEditText.getText());
                if(checkQty!=null && !checkQty.isEmpty())
                    counter=Integer.parseInt(checkQty);
                if (counter <=1) {
                    counter = 1;
                } else {
                    counter--;
                }
                applyQtyEditText.setText(Integer.toString(counter));

            }
        });
        btnUnSetQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyQtyEditText.setText("");
                defaultQuantityStatus = -1;
                menuItemActionLayout.removeAllViews();
                Log.e("SetQty", "*****After Unset value ******* "+defaultQuantityStatus);
            }
        });

        btnSetQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(v);
                boolean isQtyEmpty = applyQtyEditText.getText().toString().trim().equals("");
                if (isQtyEmpty) {
                    Toast.makeText(context, "Please Enter Qty", Toast.LENGTH_SHORT).show();
                } else {
                    defaultQuantityStatus = Integer.parseInt(applyQtyEditText.getText().toString());
                    Log.e(TAG,"**************Set  defaultQuantityStatus****************   "+defaultQuantityStatus);
                    menuItemActionLayout.removeAllViews();
                }

            }
        });

    }
    private void addCheckedItemToMapDefault(String key, String value) {
        ArrayList<String> defaultselectedItems = new ArrayList<>();
        defaultselectedItems.add(value);
        selectedFilterMap.put(key, defaultselectedItems);
    }


    private void saveMultiselectedList(String key ) {
        SparseBooleanArray checked = filterChildView.getCheckedItemPositions();
        ArrayList<String> selectedItems = new ArrayList<>();
        for (int i = 0; i < checked.size(); i++) {
            int pos = checked.keyAt(i);
            if (checked.valueAt(i)) {
                Log.e(TAG, "***********Print selectedItems.add :-  " + filterChildAdapter.getItem(pos));
                selectedItems.add(filterChildAdapter.getItem(pos));
            }
        }
        selectedFilterMap.put(key, selectedItems);
        Log.e(TAG, "***********Print FILTERAttr HashMap :-  " + selectedFilterMap);
    }


    private void setCheckedMultipleItemAgain(String key, ArrayAdapter<String> adapter) {
        Log.e(TAG,"setCheckedMultipleItemAgain****************:- " + selectedFilterMap);
        for (String Getkey : selectedFilterMap.keySet()) {
            if (Getkey.equalsIgnoreCase(key)) {
                if(selectedFilterMap.get(Getkey) instanceof  ArrayList) {
                    ArrayList<String> get_lis = (ArrayList<String>) selectedFilterMap.get(Getkey);
                    for (int j = 0; j < get_lis.size(); j++) {
                        int checked_position = adapter.getPosition(get_lis.get(j));
                        filterChildView.setItemChecked(checked_position, true);
                    }
                }else{
                    int checked_position = adapter.getPosition(selectedFilterMap.get(Getkey).toString());
                    filterChildView.setItemChecked(checked_position, true);
                }
            }
        }
    }

    private void showPriceRangeLayout(final String key, ArrayList<String> priceRange) {

        if (priceRange != null && !priceRange.isEmpty()) {
            int minValue = Integer.parseInt(priceRange.get(0));
            int maxValue = Integer.parseInt(priceRange.get(1));
            if(minValue > maxValue){
                rangeSeekbar.setMinValue(maxValue);
                rangeSeekbar.setMaxValue(minValue);
            }else {
                rangeSeekbar.setMinValue(minValue);
                rangeSeekbar.setMaxValue(maxValue);
            }
        }
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                priceMin.setText(String.valueOf(minValue));
                priceMax.setText(String.valueOf(maxValue));
                String joinedValue = String.valueOf(minValue) + "," + String.valueOf(maxValue);
                ArrayList<String> priceList = new ArrayList<>();
                priceList.add(joinedValue);
                if (selectedFilterMap.containsKey(key)) {
                    selectedFilterMap.remove(key);
                }
                selectedFilterMap.put(key, priceList);
            }
        });
    }

   /* @Override
    public CardSwipeDetails setDirectionFlags(int cardViewPosition) {

        String interactionStage= "";
        String previousStage= "";
        String nextStage= "";
        int swipeFlags = 0;
        CardSwipeDetails cardSwipeDetails = new CardSwipeDetails();
        if (recommendationAdapter != null) {
            try {
                JSONObject cardItemDetails = recommendationAdapter.getJsonObject(cardViewPosition);
                JSONObject productDetails = cardItemDetails.getJSONObject("product_attributes");

                if(cardItemDetails.has("quantity_predictions")) {
                    JSONObject quantityPredictions = cardItemDetails.getJSONObject("quantity_predictions");
                    cardSwipeDetails.setQuantityPredictions(quantityPredictions);

                }
                JSONObject stageDetails = cardItemDetails.getJSONObject("current_stage");
                if(stageDetails.has("quantity_attributes")) {
                    JSONObject quantityAttrs = stageDetails.getJSONObject("quantity_attributes");
                    cardSwipeDetails.setQuantityAttrs(quantityAttrs);
                }

                if(stageDetails.has("next_stage_quantity_attributes")) {
                    JSONArray nextStageQuantityAttrs = stageDetails.getJSONArray("next_stage_quantity_attributes");
                    cardSwipeDetails.setNextStageQuantityAttrs(nextStageQuantityAttrs);
                }
                if(stageDetails.has("next_stage_new_attributes")) {
                    JSONArray nextStageNewAttrs = stageDetails.getJSONArray("next_stage_new_attributes");
                    cardSwipeDetails.setNextStageNewAttributes(nextStageNewAttrs);
                }

                cardSwipeDetails.setCardItemDetails(cardItemDetails);
                cardSwipeDetails.setProductDetails(productDetails);

                if (daveAction.equalsIgnoreCase("Similar") && interactionPosition != -1 && interactionPosition != 0) {
                    //interactionStage=tabHeaderList.get(interactionPosition).toString();
                    interactionStage=interactionStageTitle;

                    previousStage = tabHeaderList.get(interactionPosition - 1);
                    nextStage = tabHeaderList.get(interactionPosition + 1);
                    if (interactionPosition == 1) {
                        swipeFlags = ItemTouchHelper.RIGHT;
                    } else if (interactionPosition == tabHeaderList.size() - 1) {
                        swipeFlags = ItemTouchHelper.LEFT;
                    } else {
                        swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                    }
                }else{

                    if (stageDetails.has("interaction_stage")) {
                        interactionStage = stageDetails.getString("interaction_stage");
                    }
                    if (stageDetails.has("previous_stage")) {
                        previousStage = stageDetails.getString("previous_stage");
                    }
                    if (stageDetails.has("next_stage")) {
                        nextStage = stageDetails.getString("next_stage");
                    }

                    if (previousStage.equals("__NULL__")) {
                        swipeFlags = ItemTouchHelper.RIGHT;
                    } else if (nextStage.equals("__NULL__")) {
                        swipeFlags = ItemTouchHelper.LEFT;
                    } else {
                        swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                    }
                }

                cardSwipeDetails.setInteractionStage(interactionStage);
                cardSwipeDetails.setPreviousStage(previousStage);
                cardSwipeDetails.setNextStage(nextStage);
                cardSwipeDetails.setSwipeDirection(swipeFlags);


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Get RecommSwipe Error>>>>>>>>>>" + e.getLocalizedMessage());
            }
        }
        return cardSwipeDetails;
    }

    @Override
    public void onCardSwipe(RecyclerView.ViewHolder viewHolder, int direction) {
       // Log.i(TAG, "*********** onCardSwipe direction**********:==  " + direction);
    }

    @Override
    public void onCardSwipe(String customerDetails, JSONObject productDetails, String currentStage, String previousStage) {
        if (recommendationAdapter.getItemCount() == daveAIPerspective.getCount_after_next_recommendations()) {
            Log.e(TAG,"Msg :-- Adapter Size Or recommendation Count"+recommendationAdapter.getItemCount());

        }

    }

    @Override
    public void onItemRestore(int itemPosition, JSONObject cardDetails) {
        //Log.e(TAG, "*********onCardSwipe customerDetails**********: " + cardDetails);
    }
*/
    @Override
    public CardSwipeDetails setDirectionFlags(int cardViewPosition) {

        String interactionStage= "";
        String previousStage= "";
        String nextStage= "";
        int swipeFlags = 0;

        CardSwipeDetails cardSwipeDetails = new CardSwipeDetails();
        if (recommendationAdapter != null) {
            try {
                Log.e(TAG,"setDirectionFlags position "+ cardViewPosition);
                JSONObject cardItemDetails = recommendationAdapter.getJsonObject(cardViewPosition);
                if(cardItemDetails.length()>0) {
                    cardSwipeDetails.setCardItemDetails(cardItemDetails);
                    //JSONObject productDetails = cardItemDetails.getJSONObject("product_attributes");

                    JSONObject stageDetails = cardItemDetails.getJSONObject("current_stage");
                    if (daveAction.equalsIgnoreCase("Similar") && interactionPosition != -1 && interactionPosition != 0) {

                        interactionStage = interactionStageTitle;
                        previousStage = tabHeaderList.get(interactionPosition - 1);
                        nextStage = tabHeaderList.get(interactionPosition + 1);
                        if (interactionPosition == 1) {
                            swipeFlags = ItemTouchHelper.RIGHT;
                        } else if (interactionPosition == tabHeaderList.size() - 1) {
                            swipeFlags = ItemTouchHelper.LEFT;
                        } else {
                            swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                        }
                    } else {

                        if (stageDetails.has("interaction_stage")) {
                            interactionStage = stageDetails.getString("interaction_stage");
                        }
                        if (stageDetails.has("previous_stage")) {
                            previousStage = stageDetails.getString("previous_stage");
                        }
                        if (stageDetails.has("next_stage")) {
                            nextStage = stageDetails.getString("next_stage");
                        }

                        if (previousStage.equals("__NULL__")) {
                            swipeFlags = ItemTouchHelper.RIGHT;
                        } else if (nextStage.equals("__NULL__")) {
                            swipeFlags = ItemTouchHelper.LEFT;
                        } else {
                            swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
                        }
                    }

                    cardSwipeDetails.setInteractionStage(interactionStage);
                    cardSwipeDetails.setPreviousStage(previousStage);
                    cardSwipeDetails.setNextStage(nextStage);
                    cardSwipeDetails.setSwipeDirection(swipeFlags);

                }


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Get RecommSwipe Error>>>>>>>>>>" + e.getLocalizedMessage());
            }

        }
        return cardSwipeDetails;

    }

    @Override
    public void onCardSwipe(RecyclerView.ViewHolder viewHolder, int direction) {

        if (recommendationAdapter.getItemCount() == daveAIPerspective.getCount_after_next_recommendations()) {
            Log.e(TAG,"Msg :-- Adapter Size Or recommendation Count"+recommendationAdapter.getItemCount());
            //todo call getRecommendation
            mPageNumber++;
            if(isAnyActionApply){
                getRecommFromServer(false,false,false);
            }else {

                getRecommFromServer(false,false,true);
            }
        }
        try{
            int itemPosition = viewHolder.getAdapterPosition();
            // Log.e(TAG, "*********** onCardSwipe direction**********:==  " + direction +" cardPosition:- "+ itemPosition);

            JSONObject cardItemDetails = recommendationAdapter.getJsonObject(itemPosition);
            String cardProductId = recommendationAdapter.getRemoveItemId(itemPosition);
            JSONObject stageDetails = cardItemDetails.getJSONObject("current_stage");

            PostInteractionUtil postInteractionUtil = new PostInteractionUtil(getActivity(),stageDetails);
            String swipeStage = postInteractionUtil.getCardSwipeStageName(direction);
            recommendationAdapter.remove(itemPosition);

            boolean isNextStageAction = postInteractionUtil.checkNextStageEvent(direction);
            if(isNextStageAction){
                NextStageEventDialog nextStageEventDialog = new NextStageEventDialog(getActivity(), new NextStageEventDialog.NextStageEventListener() {
                    @Override
                    public void onNextStageEventCancel() {
                        recommendationAdapter.restoreItem(cardItemDetails, itemPosition);
                    }

                    @Override
                    public void onNextStageEventSubmit(JSONObject formDetails) {
                        postInteractionUtil.postInteraction(customerId,cardProductId, swipeStage,formDetails);
                        showSnackBar(swipeStage);
                        JSONArray jsonArray = postInteractionUtil.getNextStageTaggedProducts();
                        if(jsonArray.length()>0 ){
                            addTaggedProducts(0,jsonArray,swipeStage,postInteractionUtil);
                        }
                    }
                },"Main Product",cardProductId,postInteractionUtil.getNextStageNewAttributes(),cardItemDetails.toString());
                nextStageEventDialog.show();

            }else {
                postInteractionUtil.postInteraction(customerId,cardProductId, swipeStage,null);
                showSnackBar(swipeStage);
            }

        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Exception onCardSwipe********* " + e.getLocalizedMessage());
        }

    }

    @Override
    public void onCardSwipe(String customerDetails, JSONObject cardDetails, String currentStage, String previousStage) {

    }

    @Override
    public void onItemRestore(int itemPosition, JSONObject cardDetails) {

    }

    private void showSnackBar(String swipeStage) {
        Snackbar.make(mRecyclerView, swipeStage, Snackbar.LENGTH_SHORT).show();
    }

    private void addTaggedProducts(int index ,JSONArray taggedList ,String swipeStage, PostInteractionUtil postIUtil){

        if(index < taggedList.length()) {
            try{
                String productId = taggedList.getString(index);
                NextStageEventDialog nextStageEventDialog = new NextStageEventDialog(getActivity(), new NextStageEventDialog.NextStageEventListener() {
                    @Override
                    public void onNextStageEventCancel() {

                    }
                    @Override
                    public void onNextStageEventSubmit(JSONObject formDetails) {
                        postIUtil.postTaggedProduct(customerId, productId, swipeStage, formDetails);
                        showSnackBar(swipeStage);
                        int tempIndex = index;
                        tempIndex++;
                        addTaggedProducts(tempIndex,taggedList,swipeStage,postIUtil);
                    }
                },"Tagged Products", productId, postIUtil.getNextStageNewAttributes(),null);
                nextStageEventDialog.show();
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Exception onCardSwipe********* " + e.getLocalizedMessage());
            }
        }

    }


    private void hideSoftKeyboard(View v){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onClickSimilarProduct(String productId,HashMap<String, String> similarFilterDetails) {
        Log.e(TAG,"Print on click Similar product customer id and product id :-------"+customerId +
                " SelectedProductId:- "+ productId +" \n Similar Attrs List"+daveAIPerspective.getProduct_similar_attributes()
                +" \n similarFilterDetails:- "+similarFilterDetails);

        android.support.v4.app.FragmentManager fm = getActivity().getSupportFragmentManager();
        Fragment fragmentSimilar =  fm.findFragmentByTag("Similar");
        if(fragmentSimilar == null) {
            Log.e(TAG,"Add Similar Product Fragment:-----------"+fragmentSimilar+" categoryStackFlow:-"+categoryStackFlow);

            HashMap<String, Object> intent_details = new HashMap<>();
            intent_details.put("product_id", productId);
            intent_details.put("customer_id",customerId);
            intent_details.put("productFilterDetails",similarFilterDetails);

           /* DaveRecommendations daveRecommendations = DaveRecommendations.newInstance(SIMILAR_ACTION,intent_details);
            android.support.v4.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameContainer, daveRecommendations, SIMILAR_ACTION);
            // Add Recommendation fragment  in back stack.So it will not be destroyed. Press back menu can pop it up from the stack.
            if(fm.findFragmentByTag(RECOMMENDATION_ACTION)!= null)
                fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();*/

            //call DaveFunction activity  to show similar
            Intent intent = new Intent(getActivity(), DaveFunction.class);
            intent.putExtra("msg_to_dave_func", intent_details);
            intent.putExtra(DaveFunction.DAVE_ACTION, DaveFunction.SIMILAR_ACTION);
            getActivity().startActivity(intent);
        }else{
            // get Similar product based on filter
            for (String key : similarFilterDetails.keySet()) {
                addCheckedItemToMapDefault(key, similarFilterDetails.get(key));
            }
            strProductId = productId;
            searchedTerm="";
            strRecommendationId ="";
            strRadioBtnClicking="";
            daveAction = "Similar";
            mPageNumber = 1;

            if(mapSearchByImageUrl!= null && mapSearchByImageUrl.length()>0){
                mapSearchByImageUrl = new JSONObject();
            }
            getRecommFromServer(true,true,true);
        }

    }

    /*private  void enableSearchByImageFlag(){
        if(mapSearchByImageUrl!=null && mapSearchByImageUrl.length()>0){
            isSearchByImage= true;
            strRecommendationId = "";
        }
    }

    private  void disableSearchByImageFlag(){
        isSearchByImage = false;
    }*/
}









