package com.example.admin.quickdry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveAIStatic;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.adapter.QuickOrderAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import static com.example.admin.daveai.others.DaveAIStatic.categoryBackStack;



public class QuickOrderHierarchy extends BaseActivity implements View.OnClickListener{

    private final String TAG = getClass().getSimpleName();
    ListView mListView;
    Button buttonShow, buttonSkip,buttonNext;
    RelativeLayout customerParentLayout,productCategoryLayout;
    LinearLayout customerAttrLayout; //,productCategoryLayout;
    TextView categoryTitle;
    ArrayAdapter<String> mAdapter;
    int categoryLevel = 0;
    String categoryName = "";
    String categoryValue = "";
    String newCustomerId = "";
    HashMap<String, Boolean> fieldValidationMap = new HashMap<>();
    HashMap<String, Object> productSelectedItems = new HashMap<>();
    HashMap<String, Object> categorySelectedItems = new HashMap<>();
    Map<String, Object> categoryLevelData = new HashMap<>();
    ArrayList<Object> breadCrumbDetails = new ArrayList<>();
    boolean flagCategoryStatus = false;
    Object backNavigationItem=null;
    private DaveAIPerspective daveAIPerspective;


/*
    private CategorySelectedListener categorySelectedListener;
    public interface CategorySelectedListener {
        void onCategorySelected(HashMap<String, Object> categoryDetails);

    }
    public void registerCategoryCallback(CategorySelectedListener callbackClass){
        categorySelectedListener = callbackClass;
    }*/


    //filter_attribute_link

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_hierarchy);

        Toolbar toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //daveAIPerspective = new DaveAIPerspective(QuickOrderHierarchy.this);
        daveAIPerspective= DaveAIPerspective.getInstance();
        categoryTitle =  findViewById(R.id.categoryTitle);
        customerParentLayout =  findViewById(R.id.customerCategoryLayout);
        customerAttrLayout =  findViewById(R.id.customerAttrLayout);
        buttonNext =  findViewById(R.id.buttonNext);
        productCategoryLayout =  findViewById(R.id.productCategoryLayout);
        mListView =  findViewById(R.id.list_view);
        buttonSkip = findViewById(R.id.buttonSkip);
        buttonShow =  findViewById(R.id.buttonShow);

        productCategoryLayout.setVisibility(View.GONE);
        buttonShow.setVisibility(View.GONE);

        buttonNext.setOnClickListener(this);
        buttonSkip.setOnClickListener(this);
        buttonShow.setOnClickListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            newCustomerId = bundle.getString("NewCustomerId");
            if(bundle.containsKey("_selected_categoty"))
                categorySelectedItems = (HashMap<String, Object>)getIntent().getSerializableExtra("_selected_categoty");
        }

        HashMap<String, Object> hashMap = (HashMap<String, Object>)getIntent().getSerializableExtra("msg_to_category");
        if (hashMap != null) {
            categoryLevel = (int) hashMap.get("back_navigation_position");
            backNavigationItem = hashMap.get("back_navigation_item");

        }
        Log.e(TAG,"************backNavigationItem  Before backNavigationItem***********"+backNavigationItem+"*");
        if(backNavigationItem != null){

           for(int i= categoryBackStack.size()-1; i>=0; i--){
                if(categoryBackStack.get(i).containsKey(backNavigationItem.toString())){
                    Log.e(TAG, "************Check backNavigationItem is dere get CategoriesDetails " + categoryBackStack.get(i).get(backNavigationItem.toString()));
                    categoryLevelData = (Map<String, Object>) categoryBackStack.get(i).get(backNavigationItem.toString());
                    showProductHierarchyLayout();
                    showProductCategoryInList(categoryLevelData);
                    categoryBackStack.remove(i);
                    break;

                }
                else {
                    Log.e(TAG, "************else Contain Key:---  " + categoryBackStack.get(i).containsKey(backNavigationItem));
                    categoryBackStack.remove(i);
                }

            }
            Log.e(TAG,"************backNavigationItem  After Operation backNavigationPosition"+categoryBackStack);
            Iterator iterator = categoryBackStack.iterator();
            while (iterator.hasNext()) {
                Map<Object,Object> objectObjectMap = (Map<Object, Object>) iterator.next();
                for (Object o : objectObjectMap.entrySet()) {
                    Map.Entry pair = (Map.Entry) o;
                    if(!pair.getKey().equals("Customer"))
                        breadCrumbDetails.add(pair.getKey());

                }
            }
            Log.e(TAG,"************ After Operation breadCrumbDetails:==  "+breadCrumbDetails);

        }else {
            if (checkCustomerHierarchy() && checkProductHierarchy()) {
                intentToDaveFunction();

            } else {
                if (daveAIPerspective.isCustomer_attr_hierarchy_first()&& !checkCustomerHierarchy()) {
                    showCustomerVariableAttr(null);
                } else if (!checkProductHierarchy()) {
                    showProductCategoryHierarchy();
                } else
                    intentToDaveFunction();
            }
        }
    }

 /*   @Override
    public void onResume(){
        super.onResume();

    }*/

    private boolean checkCustomerHierarchy(){
        return (daveAIPerspective.getCustomer_variable_attr() == null ||
                daveAIPerspective.getCustomer_variable_attr().size() == 0||
                daveAIPerspective.getCustomer_variable_attr().isEmpty());
    }

    private boolean checkProductHierarchy(){
        return (daveAIPerspective.getProduct_category_hierarchy() == null ||
                daveAIPerspective.getProduct_category_hierarchy().size() == 0 ||
                daveAIPerspective.getProduct_category_hierarchy().isEmpty());
    }


    @Override
    public void onClick(View v) {

        int k = v.getId();
        if (k == R.id.buttonSkip) {
            actionOnButtonSkip();

        } else if (k == R.id.buttonShow) {
            actionOnButtonShow();

        } else if (k == R.id.buttonNext) {
            actionOnButtonNext();

        } else {
            Log.i(TAG,"No Button  Click Action ");
        }
    }

    private void actionOnButtonNext(){

        DynamicForm dynamicView = new DynamicForm();
        JSONObject customerDetails = dynamicView.methodValidateDynamicView(customerAttrLayout, fieldValidationMap,null);
        if (customerDetails != null && customerDetails.length()>0) {
            Iterator<?> keys = customerDetails.keys();
            while(keys.hasNext() ) {
                String key = (String) keys.next();
                try {
                    categorySelectedItems.put(key, customerDetails.get(key));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error storing Customer selected Category***********"+e.getLocalizedMessage());
                }
            }
            updateCustomerVariable(customerDetails);
            if(!daveAIPerspective.isCustomer_attr_hierarchy_first()) {
                buttonShow.setText("Show");
                intentToDaveFunction();
            }else {
                Map<Object,Object> backStack = new HashMap<>();
                backStack.put("Customer",customerDetails);
                categoryBackStack.push(backStack);
                showProductCategoryHierarchy();
            }
        }else
            Toast.makeText(QuickOrderHierarchy.this,"Please Select From Given Settings",Toast.LENGTH_SHORT).show();
    }

    private void actionOnButtonSkip(){
        if(!daveAIPerspective.isCustomer_attr_hierarchy_first()){
            showCustomerVariableAttr(null);
        }else {
            intentToDaveFunction();
        }

    }


    private void actionOnButtonShow(){
        ArrayList<String> selectedList = new ArrayList<>();
        SparseBooleanArray checked = mListView.getCheckedItemPositions();
        Log.e(TAG,"************actionOnButtonShow checked************   "+checked);
        if(checked.size()>0){
            for (int i = 0; i < checked.size(); i++) {
                int item_position = checked.keyAt(i);
                if (item_position != 0 && checked.valueAt(i)) {
                    selectedList.add(mAdapter.getItem(item_position));
                }
            }
            productSelectedItems.put(categoryName, selectedList);
           // saveBreadCrumbData(categoryName,selectedList);
            saveBreadCrumbData(selectedList);
            if(!daveAIPerspective.isCustomer_attr_hierarchy_first() && daveAIPerspective.getCustomer_variable_attr() != null && daveAIPerspective.getCustomer_variable_attr().size() > 0 ){
                //categoryBackStack.push(categoryLevelData);
                flagCategoryStatus=true;
                showCustomerVariableAttr(null);
            }else {
                intentToDaveFunction();
            }
        }else
            Toast.makeText(QuickOrderHierarchy.this,"Please Select from  given Option",Toast.LENGTH_SHORT).show();

    }



    private void showCustomerVariableAttr(String objectDetails){
        if (daveAIPerspective.getCustomer_variable_attr() != null && daveAIPerspective.getCustomer_variable_attr().size() > 0) {
            showCustomerHierarchyLayout();
            if(!daveAIPerspective.isCustomer_attr_hierarchy_first())
                buttonNext.setText("Show");
            categoryTitle.setText(daveAIPerspective.getCustomer_variable_attr_title());
            DynamicForm dynamicForm = new DynamicForm();
           /* fieldValidationMap = dynamicForm.createDynamicForm(QuickOrderHierarchy.this,customerAttrLayout,
                    "customer",daveAIPerspective.getCustomer_variable_attr(),objectDetails);*/

            dynamicForm.createDynamicForm(new DynamicForm.OnFormViewCreatedListener() {
                      @Override
                      public void onFormVewCreated(HashMap<String, Boolean> hashMap) {
                          fieldValidationMap = hashMap;

                      }
                  },QuickOrderHierarchy.this, customerAttrLayout,"customer", daveAIPerspective.getCustomer_variable_attr(), objectDetails);
        }
    }


    private void updateCustomerVariable(JSONObject customerVariableDetails){
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {

            }
            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {

            }
        };
        Model model1 = new Model(QuickOrderHierarchy.this);
        model1.updateObject(new ModelUtil(QuickOrderHierarchy.this).getCustomerModelName(), newCustomerId, customerVariableDetails, false, postTaskListener);
    }


    private void listViewClickEvent() {

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                categoryValue = (String) parent.getItemAtPosition(position);
                productSelectedItems.put(categoryName,categoryValue);
                saveBreadCrumbData(categoryValue);
                if(categoryLevel== getCategoryLevel()){
                    intentToDaveFunction();
                }
                else {
                    categoryLevel++;
                    showProductCategoryHierarchy();
                }

            }
        });

    }

    private void showProductCategoryHierarchy(){
        if (daveAIPerspective.getProduct_category_hierarchy() != null && daveAIPerspective.getProduct_category_hierarchy().size() > 0) {

            Log.e(TAG,"Print Category Level showProductCategoryHierarchy :------"+categoryLevel);
            if(categoryLevel==0){
                //categoryLevelData = productCategoryMap;
                categoryLevelData =  new ModelUtil(QuickOrderHierarchy.this).getProductCategoryHierarchy();
            }else{
                categoryLevelData = (Map<String, Object>) categoryLevelData.get(categoryValue);
            }
            showProductCategoryInList(categoryLevelData);
            // Log.e(TAG,"showProductCategoryInList:--  Next Level categoryDetails Size =====  "+categoryLevelData.keySet().size());

           /* if(categoryLevelData.keySet().size()==1){
                //intentToDaveFunction();
                productSelectedItems.put(categoryName,categoryValue);
                saveBreadCrumbData(categoryValue);
                showProductCategoryHierarchy();
            }else
                showProductCategoryInList(categoryLevelData);*/
        } else {
            Log.e("Error:-", "*******Please Share your Category Hierarchy**********");
        }
    }

    private void showProductCategoryInList( Map<String,Object> categoryDetails) {
        Log.e(TAG,"showProductCategoryInList:--  CategoryLevel  "+categoryLevel +"   categoryDetails=====  "+categoryDetails.keySet());
        showProductHierarchyLayout();
        categoryName = daveAIPerspective.getProduct_category_hierarchy().get(categoryLevel).toString();
        ArrayList<String> categoryKeySet = new ArrayList(categoryDetails.keySet());
        buttonShow.setVisibility(View.GONE);
        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mAdapter = new ArrayAdapter<>(this, R.layout.category_row, R.id.category_title,
                categoryKeySet);
        mListView.setAdapter(mAdapter);
        listViewClickEvent();

    }



    @Override
    public void onBackPressed() {
        if(!breadCrumbDetails.isEmpty() && breadCrumbDetails.size()>0)
            breadCrumbDetails.remove(breadCrumbDetails.size()-1);

        Log.e(TAG, "*******On back breadCrumbsDetails*********"+breadCrumbDetails);
        //Log.e(TAG, "*******On back pressed categoryBackStack*********"+categoryBackStack);

        if (!categoryBackStack.empty() ) {
            if (categoryBackStack.peek().containsKey("Customer")) {
                if(backNavigationItem != null) {
                    categoryBackStack.pop();
                    super.onBackPressed();
                }else {
                    showCustomerHierarchyLayout();
                    categoryBackStack.pop();
                }
            } else {
                showProductHierarchyLayout();
                Map<Object,Object> getStackValue = categoryBackStack.pop();
                for (Object key : getStackValue.keySet()) {
                    categoryLevelData= (Map<String, Object>) getStackValue.get(key);
                }
                if(flagCategoryStatus){
                    showProductCategoryInList(categoryLevelData);
                    categoryLevel--;
                    flagCategoryStatus=false;
                }else{
                    categoryLevel--;
                    showProductCategoryInList(categoryLevelData);
                }
            }

        }else {
            super.onBackPressed();
        }
        //Log.e("Error:-", "*******After Operation On back pressed*********"+categoryBackStack);



    }

    private void intentToDaveFunction(){

       // Log.e(TAG,"INtent to Dave Function categoryBackStack:--   "+categoryBackStack);
        if(!productSelectedItems.isEmpty()){
            categorySelectedItems.putAll(productSelectedItems);
            DaveAIStatic.breadCrumbsStatus=true;
           // Log.e(TAG,"INtent to Dave Function categorySelectedItems:--   "+categorySelectedItems);
        }
       // Log.e(TAG,"INtent to Dave Function breadCrumbs:--   "+breadCrumbDetails);
        HashMap<String, Object> intent_details = new HashMap<>();
        intent_details.put("customer_id", newCustomerId);
        intent_details.put("product_id", "");
        intent_details.put("Dave_function", "Recommendations");
        intent_details.put("productDetails", productSelectedItems);
        intent_details.put("category_selected_items", categorySelectedItems);
        intent_details.put("bread_crumb_details", breadCrumbDetails);

        Intent intent = new Intent(QuickOrderHierarchy.this, FormView.class);
        intent.putExtra("msg_to_form_view", intent_details);
        startActivity(intent);
        finish();
    }

    private void showCustomerHierarchyLayout(){
        if(productCategoryLayout.getVisibility()==View.VISIBLE)
            productCategoryLayout.setVisibility(View.GONE);
        customerParentLayout.setVisibility(View.VISIBLE);

    }

    private void showProductHierarchyLayout(){
        categoryTitle.setText(daveAIPerspective.getProduct_category_hierarchy_title());
        if(customerParentLayout.getVisibility()==View.VISIBLE)
            customerParentLayout.setVisibility(View.GONE);
        productCategoryLayout.setVisibility(View.VISIBLE);

    }

    private void saveBreadCrumbData(Object value){
        breadCrumbDetails.add(value);
        HashMap<Object,Object> saveBackStack= new HashMap<>();
        saveBackStack.put(value.toString(),categoryLevelData);
        categoryBackStack.push(saveBackStack);
    }

    private int getCategoryLevel(){
        if (!checkProductHierarchy())
            return daveAIPerspective.getProduct_category_hierarchy().size()-1;
        else
            return 0;
    }

}

