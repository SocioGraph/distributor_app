package com.example.admin.quickdry.models;


import java.util.ArrayList;

public class AddButtonView {

    private String buttonName;
    private int buttonIcon;
    private String broadcastMsg;
   // private String buttonAction;
    private ArrayList<String> whichPage = new ArrayList<>();


    public AddButtonView(String buttonName){
        setButtonName(buttonName);
    }

    public String getButtonName() {
        return buttonName;
    }

    private void setButtonName(String buttonName) {
        this.buttonName = buttonName;
    }

    public int getButtonIcon() {
        return buttonIcon;
    }

    public void setButtonIcon(int buttonIcon) {
        this.buttonIcon = buttonIcon;
    }

    public String getBroadcastMsg() {
        return broadcastMsg;
    }

    public void setBroadcastMsg(String broadcastMsg) {
        this.broadcastMsg = broadcastMsg;
    }

    public ArrayList<String> getWhichPage() {
        return whichPage;
    }

    public void setWhichPage(ArrayList<String> whichPage) {
        this.whichPage = whichPage;
    }

    public void addWhichPage(String which_page) {
        this.whichPage.add(which_page);
    }
}
