package com.example.admin.quickdry.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.DaveFunction;
import com.example.admin.quickdry.utils.AppConstants;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Objects;
import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION_CODES.M;


public class SearchByImage extends Fragment implements AppConstants ,View.OnClickListener{

    private final String TAG = getClass().getSimpleName();
    private static final String CUSTOMER_ID = "customer_id";

    private static final int PERMISSION_REQUEST_CODE = 200;
    private Context context;

    //keep track of camera capture intent
    final int CAMERA_CAPTURE = 1;
    //keep track of cropping intent
    final int PIC_CROP = 3;

    private ImageView imageView;
    private Button btnUploadImage,btnSearchByImage;

    private String strCustomerId= "";
    //captured picture uri
    private Uri picUri;
    private File tempImageFile = null;
    private DaveAIPerspective daveAIPerspective;
    private  String strImageUploadedPath ="";





    private OnFragmentInteractionListener mListener;
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public SearchByImage() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @param customerId customerId.
     * @return A new instance of fragment SearchByImage.
     */
    public static SearchByImage newInstance(String customerId) {
        SearchByImage fragment = new SearchByImage();
        Bundle args = new Bundle();
        args.putString(CUSTOMER_ID, customerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       /* if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && !getArguments().isEmpty()) {
            strCustomerId = getArguments().getString(CUSTOMER_ID,"");

        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mainView =  inflater.inflate(R.layout.fragment_search_by_image, container, false);
        context= getActivity();

        imageView = mainView.findViewById(R.id.imageView);
        Button buttonCancel =  mainView.findViewById(R.id.buttonCancel);
        btnUploadImage = mainView.findViewById(R.id.uploadImage);
        btnSearchByImage = mainView.findViewById(R.id.searchByImage);
        btnSearchByImage.setVisibility(View.GONE);

        buttonCancel.setOnClickListener(this);
        btnUploadImage.setOnClickListener(this);
        btnSearchByImage.setOnClickListener(this);

        daveAIPerspective = DaveAIPerspective.getInstance();
        if(daveAIPerspective.getSearch_by_image_button_name()!= null && !daveAIPerspective.getSearch_by_image_button_name().isEmpty()) {
            btnSearchByImage.setText(daveAIPerspective.getSearch_by_image_button_name());
            Objects.requireNonNull(getActivity()).setTitle(daveAIPerspective.getSearch_by_image_button_name());
        }

        return mainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles


    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();

    }


    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onDetach() {
        super.onDetach();
       // mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonCancel){
            imageView.setImageResource(R.drawable.placeholder);
            btnUploadImage.setVisibility(View.VISIBLE);
            btnSearchByImage.setVisibility(View.GONE);
        }
        else if(v.getId() == R.id.uploadImage){
            if (checkCameraPermission()) {
                openCameraIntent();
            } else {
                requestPermission();
                // askForPermission(Manifest.permission.CAMERA, PERMISSION_REQUEST_CODE);
            }

        }else if(v.getId() == R.id.searchByImage){
            String searchByImageAttr = daveAIPerspective.getSearch_by_image_attr();
            Log.i(TAG,"Print searchByImageAttr name>>>>>>>>>>>>>>>>>> "+searchByImageAttr);
            if(searchByImageAttr == null || searchByImageAttr.isEmpty()){
                Toast.makeText(context,"Please set search_by_image_attr in perspective",Toast.LENGTH_SHORT).show();
            }else {
                methodSearchByImage(searchByImageAttr);
            }

        }

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private boolean checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(permission)) {
                //This is called if user has denied the permission before In this case I am just asking the permission again
                requestPermissions(new String[]{permission}, requestCode);
            } else {
                requestPermissions( new String[]{permission}, requestCode);
            }
        } else {
            Toast.makeText(getActivity(), "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    openCameraIntent();
                } else {
                    Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            requestPermission();
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void openCameraIntent(){
        try {
            //use standard intent to capture an image
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            tempImageFile = new MultiUtil(context).createImageFile();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                picUri = FileProvider.getUriForFile(context, new MultiUtil(context).getProviderAuthority(), tempImageFile);
            } else {
                picUri = Uri.fromFile(tempImageFile); // convert path to Uri
            }
            Log.i(TAG,"openCameraIntent:-----temp file path-------"+tempImageFile +" picUri:-"+ picUri);
            takePictureIntent.putExtra( MediaStore.EXTRA_OUTPUT, picUri );
            takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            takePictureIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            takePictureIntent.putExtra("return-data", "true");
            startActivityForResult(takePictureIntent, CAMERA_CAPTURE);

        } catch(ActivityNotFoundException anfe){
            String errorMessage = "Whoops - your device doesn't support capturing images!";
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE) {
                CropImage();

            } else if (requestCode ==PIC_CROP) {
                //setImageInImageView();
                uploadImage(data);
            }
        }
    }

    private void CropImage() {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            if(Build.VERSION.SDK_INT > M){
                context.getApplicationContext().grantUriPermission("com.android.camera", picUri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

                cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            }
            Log.i(TAG,"Crop Image:--------------------------"+picUri);
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //cropIntent.setData(picUri);


            //indicate aspect of desired crop
            //cropIntent.putExtra("aspectX", 1);
            //cropIntent.putExtra("aspectY", 1);

            //indicate output X and Y
            //cropIntent.putExtra("outputX", 256);
            //cropIntent.putExtra("outputY", 256);

            // cropIntent.putExtra("scale", true); // boolean
            //cropIntent.putExtra("scaleUpIfNeeded", true); // boolean

            // cropIntent.putExtra("spotlightX", spotlightX); // int
            // cropIntent.putExtra("spotlightY", spotlightY); // int

            //retrieve data on return true - don't return uri |  false - return uri
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);

            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Your device is not supportting the crop action", Toast.LENGTH_SHORT).show();

        }
    }

    private void uploadImage(Intent data){
        Log.e(TAG,"Print crop image intent data:-----------"+data);
        if (data != null) {
            if(data.getExtras()!= null && data.getData() == null){
                Log.i(TAG,"Print if data has only bitmap :-----------"+data.getData() +"  "+ data.getExtras() );
                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUriFromBitmap(getActivity(), data.getExtras().getParcelable("data"));
                // CALL THIS METHOD TO GET THE ACTUAL PATH
                tempImageFile = new File(getRealPathFromURI(tempUri));

            }
            DaveModels daveModels = new DaveModels(context,false);
            daveModels.uploadImageToServer(new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    try {
                        if(data.getData()!=null) {
                            Log.i(TAG,"Print After cropped get URI:-----------"+data.getData());
                            imageView.setImageURI(data.getData());
                        }
                        else if(data.getExtras()!=null){
                            Bitmap croppedBitmap = data.getExtras().getParcelable("data");
                            Log.i(TAG,"Print After cropped get Bitmap:-----------"+croppedBitmap);
                            imageView.setImageBitmap(croppedBitmap);
                        }

                       /* {
                            "filename": "4fc69487_51bb_3892_9e51_f316b0b22873.png",
                                "path": "https://d2elmgyodtb8k7.cloudfront.net/static/uploads/srinath/4fc69487_51bb_3892_9e51_f316b0b22873.png"
                        }*/
                        strImageUploadedPath = jsonObject.getString("path");
                        Log.e(TAG,"Print uploaded Image path:-----"+strImageUploadedPath);
                        btnSearchByImage.setVisibility(View.VISIBLE);
                        btnUploadImage.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onResponseFailure(int i, String s) {
                    Toast.makeText(getActivity(),s,Toast.LENGTH_SHORT).show();

                }
            },"file","image.png",tempImageFile.getAbsolutePath(),null,null);
        }

    }

    public Uri getImageUriFromBitmap(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void methodSearchByImage(String imageAttrKey){

        HashMap<String, Object> intent_details = new HashMap<>();
        try {
            JSONObject mapImageUrl = new JSONObject();
            mapImageUrl.put(imageAttrKey,strImageUploadedPath);
            intent_details.put("_image_url_map",mapImageUrl.toString());

            if(strCustomerId!=null && !strCustomerId.isEmpty()){
                intent_details.put("customer_id",strCustomerId);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG,"CLick on SImilar button:-------------------"+intent_details);
       /* Intent intent = new Intent(context, DaveFunction.class);
        intent.putExtra("msg_to_dave_func", intent_details);
        intent.putExtra(DAVE_ACTION, SIMILAR_ACTION);
        context.startActivity(intent);*/

        if (CheckNetworkConnection.networkHasConnection(context)) {
            DaveRecommendations daveRecommendations = DaveRecommendations.newInstance(DaveFunction.SIMILAR_ACTION,intent_details);
            android.support.v4.app.FragmentManager fm = getActivity().getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frameContainer, daveRecommendations, DaveFunction.SIMILAR_ACTION);
            fragmentTransaction.commit();
        }
        else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context,"Internet Connection is required for this feature.");
        }


    }


}

   /* public void startCropAnImage(int outputX, int outputY, boolean scale, boolean scaleUpIfNeeded,
                                 int aspectX, int aspectY, int spotlightX, int spotlightY, boolean returnData,
                                 String output, boolean setAsWallpaper, boolean showWhenLocked, String outputFormat) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setData(picUri); // location of the image, can be empty
        intent.putExtra("outputX", outputX); // int
        intent.putExtra("outputY", outputY); // int
        intent.putExtra("scale", scale); // boolean
        intent.putExtra("scaleUpIfNeeded", scaleUpIfNeeded); // boolean
        intent.putExtra("aspectX", aspectX); // int
        intent.putExtra("aspectY", aspectY); // int
        intent.putExtra("spotlightX", spotlightX); // int
        intent.putExtra("spotlightY", spotlightY); // int
        intent.putExtra("return-data", returnData); // boolean
        intent.putExtra("output", output); // string
        intent.putExtra("set-as-wallpaper", setAsWallpaper); // boolean
        intent.putExtra("showWhenLocked", showWhenLocked); // boolean
        intent.putExtra("outputFormat", outputFormat); // String
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(intent, PIC_CROP);
        }
    }*/

