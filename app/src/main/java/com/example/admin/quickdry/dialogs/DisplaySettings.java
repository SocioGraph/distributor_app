package com.example.admin.quickdry.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.admin.quickdry.R;
import com.example.admin.quickdry.storage.SharedPreference;
import com.example.admin.quickdry.utils.AppConstants;

import org.json.JSONObject;

import java.util.ArrayList;






public class DisplaySettings extends Dialog implements AppConstants {

    private final String TAG = getClass().getSimpleName();
    private Context context;


    private Spinner selectCategory,selectRoom,selectLayout,selectTexture;
    SharedPreference sharedPreference;


    private DisplaySettingCallback  displaySettingCallback;
    public interface DisplaySettingCallback {
        void OnDisplaySettingSubmit();
    }

    public DisplaySettings(@NonNull Context context,DisplaySettingCallback displaySettingCallback) {
        super(context);
        this.context = context;
        this.displaySettingCallback= displaySettingCallback;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        setContentView(R.layout.dialog_display_settings);

        Button btnCancel = findViewById(R.id.btnCancel);
        selectCategory = findViewById(R.id.selectCategory);
        selectRoom = findViewById(R.id.selectRoom);
        selectLayout = findViewById(R.id.selectLayout);
        selectTexture = findViewById(R.id.selectTexture);
        Button btnSubmit = findViewById(R.id.btnSubmit);


        sharedPreference = new SharedPreference(context);

        setValueInSpinner(selectCategory, R.id.categoryTitle, DISPLAY_CATEGORY_LIST, DISPLAY_CATEGORY);
        setValueInSpinner(selectRoom, R.id.roomTitle, DISPLAY_ROOM_LIST, DISPLAY_ROOM);
        setValueInSpinner(selectLayout, R.id.layoutTitle, DISPLAY_LAYOUT_LIST, DISPLAY_LAYOUT);
        setValueInSpinner(selectTexture, R.id.textureTitle, DISPLAY_TEXTURE_LIST, DISPLAY_TEXTURE);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                dismiss();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //save selected room and category in share pref
                if(selectCategory.getSelectedItem().toString()!=null)
                    sharedPreference.writeString(DISPLAY_CATEGORY,selectCategory.getSelectedItem().toString());
                if(selectRoom.getSelectedItem().toString()!=null)
                    sharedPreference.writeString(DISPLAY_ROOM,selectRoom.getSelectedItem().toString());
                if(selectLayout.getSelectedItem().toString()!=null)
                    sharedPreference.writeString(DISPLAY_LAYOUT,selectLayout.getSelectedItem().toString());
                if(selectTexture.getSelectedItem().toString()!=null)
                    sharedPreference.writeString(DISPLAY_TEXTURE,selectTexture.getSelectedItem().toString());

                if(displaySettingCallback!=null)
                    displaySettingCallback.OnDisplaySettingSubmit();

                dismiss();
            }
        });
    }

    private void setValueInSpinner(Spinner spinner, int textViewId, String listAttrNAme, String defaultAttr){

        ArrayList<String> arrayList = sharedPreference.getList(listAttrNAme);
        String defaultValue = sharedPreference.readString(defaultAttr);
        Log.e(TAG,"Print listAttrNAme default value*************************  "+ defaultValue +"    list: "+arrayList );
        if(arrayList!=null && arrayList.size()>0)
        {
            ArrayAdapter categoryAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item,arrayList);
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(categoryAdapter);


            if(defaultValue!=null && !defaultValue.isEmpty())
                spinner.setSelection(arrayList.indexOf(defaultValue));
        }else {
            spinner.setVisibility(View.GONE);
            findViewById(textViewId).setVisibility(View.GONE);
        }
    }
}
