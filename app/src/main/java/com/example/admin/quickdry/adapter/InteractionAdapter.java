package com.example.admin.quickdry.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.admin.daveai.activity.Visualization;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dynamicView.ShowDynamicView;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.DaveFunction;
import com.example.admin.quickdry.activity.ImageFullScreen;
import com.example.admin.quickdry.dialogs.InteractionEditDialog;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.utils.RecycleViewHolder;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.admin.daveai.database.DatabaseConstants.ERROR_MESSAGE_TEXT;


public class InteractionAdapter extends RecyclerView.Adapter<RecycleViewHolder>
    implements RecyclerView.OnClickListener ,AppConstants, DaveConstants {

    private static final String TAG = "InteractionAdapter";
    private Context mContext;
    private String customer_id;
    private List<JSONObject> seedInteractionList;
    private int expandedPosition = -1;
    private Model productModel;
    private DaveAIPerspective daveAIPerspective;
    private ModelUtil modelUtil;
    String productIdAttrName;


    private InteractionAdapterCallback interactionAdapterCallback;
    public interface InteractionAdapterCallback {
        void onUpdateInteractionQty(JSONObject previousQtyAttrs, JSONObject updatedResponse);
        void onExpandView(String productId,JSONObject productDetails);
    }

    public InteractionAdapter(InteractionAdapterCallback interactionAdapterCallback,
                              Context context, List<JSONObject> seedInteractionList, String customer_id){

        this.interactionAdapterCallback=interactionAdapterCallback;
        this.seedInteractionList = seedInteractionList;
        this.mContext = context;
        this.customer_id=customer_id;

        productModel = Model.getModelInstance(mContext,new ModelUtil(context).getProductModelName());
        daveAIPerspective= DaveAIPerspective.getInstance();
        modelUtil = new ModelUtil(mContext);
        productIdAttrName = modelUtil.getProductIdAttrName();
       // daveAIPerspective= new DaveAIPerspective(context);
       // mInteractionList = new ArrayList<>();

    }


    @NonNull
    @Override
    public RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_interaction_card_view,viewGroup, false);
        RecycleViewHolder holder = new RecycleViewHolder(view);
        holder.itemView.setOnClickListener(InteractionAdapter.this);
        holder.itemView.setTag(holder);
        // return new CustomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecycleViewHolder customViewHolder, int position) {

        if (position == expandedPosition) {
            customViewHolder.collapseViewLayout.setVisibility(View.GONE);
            customViewHolder. expandViewLayout.setVisibility(View.VISIBLE);
        } else {
            customViewHolder. collapseViewLayout.setVisibility(View.VISIBLE);
            customViewHolder.expandViewLayout.setVisibility(View.GONE);
        }

        String product_id = null;
        JSONObject seedItem = seedInteractionList.get(position);
        Log.e(TAG," onBindViewHolder get Card Details:----"+ position +"  "+  seedItem);
        try{
            if(seedItem.has("details")){
                JSONObject productDetails = seedItem.getJSONObject("details");
                //Log.e(TAG,"get Card Product Details at postion:"+position+" productDetails:-"+productDetails);
                if(productDetails!=null) {
                    customViewHolder.bindQuickView(mContext, productDetails);
                    customViewHolder.bindExpandView(mContext, productModel, productDetails);

                    if(daveAIPerspective.getProduct_card_image_view()!= null && productDetails.has(daveAIPerspective.getProduct_card_image_view())
                            && !productDetails.isNull(daveAIPerspective.getProduct_card_image_view())) {
                        customViewHolder.zoomImage.setVisibility(View.VISIBLE);
                    }else {

                        customViewHolder.zoomImage.setVisibility(View.GONE);
                    }
                }
            }
            bindOtherCardViewDetails(customViewHolder,seedItem);

            JSONObject interactionAttributes = seedItem.getJSONObject("_interaction_attributes");


            if(interactionAttributes!=null) {
                showInteractionEditView(customViewHolder,seedItem.getString("interaction_id"),interactionAttributes);
            }

            if(seedItem.has(ERROR_MSG_ATTRIBUTE) && !seedItem.isNull(ERROR_MSG_ATTRIBUTE) && !seedItem.getString(ERROR_MSG_ATTRIBUTE).isEmpty()){
                customViewHolder.errorIconView.setVisibility(View.VISIBLE);
                customViewHolder.errorMsgView.setVisibility(View.VISIBLE);

                if(seedItem.getString(ERROR_MSG_ATTRIBUTE).equals(ERROR_MESSAGE_TEXT)){
                    customViewHolder.errorIconView.setImageResource(R.drawable.ic_report_error_green);
                    customViewHolder.errorMsgView.setTextColor(mContext.getResources().getColor(R.color.sync_pending));
                }
                customViewHolder.errorMsgView.setText(seedItem.getString(ERROR_MSG_ATTRIBUTE).replaceAll("\\s{2,}", " ").trim());
            }else {
                customViewHolder.errorIconView.setVisibility(View.GONE);
                customViewHolder.errorMsgView.setVisibility(View.GONE);
            }




        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during bind Card view+++++++++++" + e.getMessage());
        }

        customViewHolder.getSimilar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Object> intent_details = new HashMap<>();
                HashMap<String, String> similarFilterDetails = new HashMap<>();
                try {
                    JSONObject productObject = seedInteractionList.get(customViewHolder.getAdapterPosition()).getJSONObject("details");
                    if(daveAIPerspective.getProduct_similar_attributes()!=null){
                        for(int i=0; i<daveAIPerspective.getProduct_similar_attributes().size(); i++){
                            String key = daveAIPerspective.getProduct_similar_attributes().get(i).toString();
                            if(productObject.has(key)){
                                similarFilterDetails.put(key,productObject.get(key).toString());
                            }
                        }
                    }
                    String product_id = productObject.getString(productIdAttrName);
                    intent_details.put("product_id", product_id);
                    intent_details.put(CUSTOMER_ID,customer_id);
                    intent_details.put("productFilterDetails",similarFilterDetails);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e(TAG,"CLick on SImilar button:-------------------"+intent_details);
                Intent intent = new Intent(mContext, DaveFunction.class);
                intent.putExtra("msg_to_dave_func", intent_details);
                intent.putExtra(DaveFunction.DAVE_ACTION, DaveFunction.SIMILAR_ACTION);
                mContext.startActivity(intent);

               /* ProductFilterDialog cdd = new ProductFilterDialog(mContext,intent_details,product_details,false);
                cdd.show();*/
            }
        });

        customViewHolder.zoomImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject productDetails = seedInteractionList.get(customViewHolder.getAdapterPosition()).getJSONObject("details");
                    if(daveAIPerspective.getProduct_card_image_view()!= null && productDetails.has(daveAIPerspective.getProduct_card_image_view())
                            && !productDetails.isNull(daveAIPerspective.getProduct_card_image_view())) {

                        Intent intent = new Intent(mContext, ImageFullScreen.class);
                        intent.putExtra(ImageFullScreen.IMAGE_URL,productDetails.getString(daveAIPerspective.getProduct_card_image_view()));
                        mContext.startActivity(intent);

                        /*Bundle b=new Bundle();
                        b.putString(Visualization.VISUALIZATION_VIEW_TYPE,Visualization.VisualisationType.IMAGE.toString());
                        b.putString(Visualization.IMAGE_URL,productDetails.getString(daveAIPerspective.getProduct_card_image_view()));

                        Intent intent = new Intent(mContext, Visualization.class);
                        intent.putExtras(b);
                        mContext.startActivity(intent);*/
                    }else{
                        Toast.makeText(mContext,"Image Not Available..",Toast.LENGTH_SHORT).show();
                        Log.e(TAG,"error during click on Zoom Image position  :-" +customViewHolder.getAdapterPosition() +" imageAttrName:- "+daveAIPerspective.getProduct_card_image_view() +" isDetaildHasImageUrl:-  "+productDetails.has(daveAIPerspective.getProduct_card_image_view())
                                +"\n productDetails:-"+productDetails);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });




       /* customViewHolder.showItemCount.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                Log.e(TAG,"After Text Change Listner:--------"+s);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
               // Log.e(TAG,"Before Text Change Listner:--------"+s);
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.e(TAG,"Before Text Change Listner:--------"+s+ "  "+start+ "  "+before + "  "+count);

            }
        });
*/

    }

    @Override
    public int getItemCount() {
        return (null != seedInteractionList ? seedInteractionList.size() : 0);
    }

    public JSONObject getJsonObject(int position) {
        return seedInteractionList.get(position);
    }


   /* public void addItem(int position, JSONObject jsonObject){
        seedInteractionList.add(position, jsonObject);
        notifyItemInserted(position);
        notifyItemRangeChanged(position, seedInteractionList.size());
    }*/

    public void remove(int position) {
        seedInteractionList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, seedInteractionList.size());
    }

    public String getRemoveItemId(int position){
        if(seedInteractionList.size()>0)
            try {
                return seedInteractionList.get(position).getJSONObject("details").getString(new ModelUtil(mContext).getProductIdAttrName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return null;
    }

    public void restoreItem(JSONObject deletedItem, int position) {
        seedInteractionList.add(position, deletedItem);
        // notify item added by position
        notifyItemInserted(position);
    }

    //This method will filter the list
    public void setFilteredList(List<JSONObject> productDetails) {
        seedInteractionList = new ArrayList<>();
        seedInteractionList.clear();
        seedInteractionList.addAll(productDetails);
        notifyDataSetChanged();
    }

    public List<JSONObject> getAllDataList(){
        return seedInteractionList;

    }

    public void resetPreviousCustomerList(List<JSONObject> interactionList){
        //seedInteractionList = mInteractionList;
        seedInteractionList = interactionList;
        notifyItemInserted(seedInteractionList.size());
        notifyDataSetChanged();

    }


    @Override
    public void onClick(View view) {

        RecycleViewHolder holder = (RecycleViewHolder) view.getTag();
        int cardPosition = holder.getAdapterPosition();
        if(holder.expandViewLayout.getVisibility()==View.VISIBLE){
            expandedPosition = -1 ;
        }else{
            expandedPosition = cardPosition;
            if(interactionAdapterCallback!=null){
                try {
                    JSONObject cardDetails = seedInteractionList.get(cardPosition).getJSONObject("details");
                    String productId = cardDetails.getString(modelUtil.getProductIdAttrName());
                    interactionAdapterCallback.onExpandView(productId, cardDetails);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        notifyDataSetChanged();

    }

    //method to bind timestamp and qty data in view
    private void bindOtherCardViewDetails(RecycleViewHolder customViewHolder, JSONObject seedItem) {
        try{
            if(seedItem.has("interaction_timestamp")&& !seedItem.isNull("interaction_timestamp")) {
                String timeStamp = seedItem.getString("interaction_timestamp");
                if (timeStamp != null) {
                    customViewHolder.showTimeStamp.setVisibility(View.VISIBLE);
                    customViewHolder.showTimeStamp.setText(timeStamp);
                }else {
                    customViewHolder.showTimeStamp.setVisibility(View.GONE);
                }
            }else {
                customViewHolder.showTimeStamp.setVisibility(View.GONE);
            }
            String interactionQtyAttr = daveAIPerspective.getInteraction_qty();
            //Log.i(TAG,"bindOtherCardViewDetails interactionQtyAttr Name:------------------------- "+interactionQtyAttr);
            //to set qty first check in quantity_attributes after that _interaction_attributes at last check in jsonobj
            if(interactionQtyAttr != null){
                JSONObject interactionAttributes = seedItem.getJSONObject("_interaction_attributes");
                JSONObject currentQtyDetails = seedItem.getJSONObject("quantity_attributes");
                if(currentQtyDetails != null && currentQtyDetails.length()>0 && currentQtyDetails.has(interactionQtyAttr)){
                    int qtyValue = -1;
                    if(currentQtyDetails.get(interactionQtyAttr) instanceof Integer)
                        qtyValue = currentQtyDetails.getInt(interactionQtyAttr);

                    else if(currentQtyDetails.get(interactionQtyAttr) instanceof Double)
                        qtyValue = (int)currentQtyDetails.getDouble(interactionQtyAttr);

                    else if(currentQtyDetails.get(interactionQtyAttr) instanceof String)
                        qtyValue = Integer.parseInt(currentQtyDetails.getString(interactionQtyAttr));

                    if(qtyValue!=-1) {
                        customViewHolder.showItemCount.setVisibility(View.VISIBLE);
                        customViewHolder.showItemCount.setText(String.valueOf(qtyValue));
                    }

                }else if(interactionAttributes!=null && interactionAttributes.length()>0 && interactionAttributes.has(interactionQtyAttr) ){
                    double d = (double) interactionAttributes.get(interactionQtyAttr);
                    int qtyValue = (int) d;
                    customViewHolder.showItemCount.setVisibility(View.VISIBLE);
                    customViewHolder.showItemCount.setText(String.valueOf(qtyValue));

                }else if(seedItem.has(interactionQtyAttr) && !seedItem.isNull(interactionQtyAttr)){
                    double d = (double) seedItem.get(interactionQtyAttr);
                    int qtyValue = (int) d;
                    customViewHolder.showItemCount.setVisibility(View.VISIBLE);
                    customViewHolder.showItemCount.setText(String.valueOf(qtyValue));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error During bind  Others Interaction Card Details +++++++++++" + e.getMessage());
        }

    }




    // show Interaction edit view.
    // interaction edit view attr List has taken from perspective(getInteraction_stage_attr_list) and value of that
    // attrs from _interaction_attributes from interaction obj.
    private void showInteractionEditView( RecycleViewHolder customViewHolder,String interactionId, final JSONObject interactionDetails ) {

        JSONObject getCardDetails = seedInteractionList.get(customViewHolder.getAdapterPosition());
        Model interactionModel = Model.getModelInstance(mContext, new ModelUtil(mContext).getInteractionModelName());

        ArrayList interactionStageAttrList = DaveAIPerspective.getInstance().getInteraction_stage_attr_list();
        //Log.e(TAG,"showInteractionEditView interactionStageAttrList details:--"+interactionStageAttrList +" details:-"+ interactionDetails);

        if (interactionStageAttrList != null && interactionDetails != null){
            customViewHolder.interactRecyclerView.setVisibility(View.VISIBLE);
            customViewHolder.editInteraction.setVisibility(View.VISIBLE);

            ShowDynamicView dynamicForm = new ShowDynamicView(mContext);
            dynamicForm.expandMultiView(mContext, customViewHolder.interactRecyclerView, interactionStageAttrList, interactionModel, interactionDetails);

            customViewHolder.editInteraction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    InteractionEditDialog cdd = new InteractionEditDialog(new InteractionEditDialog.InteractionEditCallback() {
                        @Override
                        public void onInteractionUpdated(JSONObject updatedJson) {
                            try {

                               // Log.e(TAG,"<<<<<<<<<<<<<<onInteractionUpdated>>>>>>>>>>>>>>>>>>"+updatedJson);
                                JSONObject interactionAttributes = getCardDetails.getJSONObject("_interaction_attributes");
                                JSONObject currentQtyDetails = getCardDetails.getJSONObject("quantity_attributes");

                                for(int i =0;i<interactionStageAttrList.size();i++){
                                   String key =interactionStageAttrList.get(i).toString() ;
                                   if(updatedJson.has(key)){
                                       if(interactionAttributes.has(key))
                                           interactionAttributes.put(key,updatedJson.get(key));
                                       if(currentQtyDetails.has(key))
                                           currentQtyDetails.put(key,updatedJson.get(key));
                                   }
                                }
                                for(int i =0;i<daveAIPerspective.getSummation_attributes().size();i++){
                                    String key = daveAIPerspective.getSummation_attributes().get(i).toString() ;
                                    if(updatedJson.has(key)){
                                        if(currentQtyDetails.has(key))
                                            currentQtyDetails.put(key,updatedJson.get(key));
                                    }
                                }
                                getCardDetails.put("_interaction_attributes",interactionAttributes);
                                getCardDetails.put("quantity_attributes",currentQtyDetails);

                                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                                JSONObject updatedObj =  databaseManager.getObjectData(DatabaseConstants.INTERACTIONS_TABLE_NAME,interactionId);
                                //Log.e(TAG,"<<<<<<<<<<<<<<onInteractionUpdated After update >>>>>>>>>>>>>>>>>>"+getCardDetails+"\n"+ updatedObj);
                                //refresh card data
                                seedInteractionList.set(customViewHolder.getAdapterPosition(),updatedObj);
                                notifyItemChanged(customViewHolder.getAdapterPosition());

                                if(interactionAdapterCallback!=null){
                                    interactionAdapterCallback.onUpdateInteractionQty(currentQtyDetails,updatedJson);
                                }


                            }catch (Exception e){
                                e.printStackTrace();
                                Log.e(TAG,"error during Updating Interaction Attrs:--"+e.toString());
                            }

                        }

                        @Override
                        public void onInteractionUpdatedFailed(int requestCode, String errorMsg) {
                            Log.e(TAG,"onInteractionUpdatedFailed:--"+errorMsg);

                        }

                    }, mContext, interactionDetails, interactionId);
                    cdd.show();
                }
            });

        }else{
            customViewHolder.editInteraction.setVisibility(View.GONE);
            customViewHolder.interactRecyclerView.setVisibility(View.GONE);

        }

    }




}






