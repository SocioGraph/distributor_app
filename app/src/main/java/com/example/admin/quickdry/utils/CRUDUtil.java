package com.example.admin.quickdry.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.others.DaveAIHelper;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.BuildConfig;
import com.example.admin.quickdry.storage.SharedPreference;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;




public class CRUDUtil implements AppConstants, DaveConstants {

    private  final static String TAG = "CRUDUtil";
    private Context context;
    SharedPreference sharedPreference;
    private DaveAIPerspective daveAIPerspective;

    public CRUDUtil(Context context){
        this.context=context;
        sharedPreference= new SharedPreference(context);
        daveAIPerspective= DaveAIPerspective.getInstance();

    }

   // public static boolean checkNextPage(int itemCount, int pageNumber){
    public static boolean checkNextPage(int itemCount){

        //int number = pageNumber * 50;
        if(itemCount % 50 == 0){
            return true;
        }
        return false;
    }


    public static String calculatePageSize( String preCount ,int currentCount){

        if(preCount.isEmpty())
            preCount ="0";
        if(preCount.contains("+"))
            preCount = preCount.replace("+","");

        int previousValue = Integer.valueOf(preCount);
        String pageSize = String.valueOf(previousValue + currentCount);
        if(currentCount == 0 ||(currentCount > 0 && currentCount< 50) ){
            return pageSize;
        }
        else if(currentCount == 50) {
            return pageSize + "+";
        }
        else
            return pageSize;
    }

    public static String methodToday(){
        Calendar cal = Calendar.getInstance();
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
        return df.format(cal.getTime());
    }

    // Method to share either text or URL.
    private void shareImageUrl(Context context, String imageUrl) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        // share.putExtra(Intent.EXTRA_TEXT, "http://www.codeofaninja.com");
        share.putExtra(Intent.EXTRA_TEXT, imageUrl);
        context.startActivity(Intent.createChooser(share, "Share Image!"));
    }


    // Returns the URI path to the Bitmap displayed in specified ImageView
    public Uri getLocalBitmapUri(Context context,ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;

        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            //Log.e(TAG,"drawable instanceof BitmapDrawable>>>>>>>>>>>>>>>>> "+ drawable+"  "+bmp );
        }
        else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file =  new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.e(TAG,"Print Build version code version is >=24:-- "+Build.VERSION.SDK_INT);
                // wrap File object into a content provider. NOTE: authority here should match authority in manifest declaration
                bmpUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);

            } else {
                Log.e(TAG,"Print Build version code version is <=23 :-- "+Build.VERSION.SDK_INT);
                bmpUri = Uri.fromFile(file);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public void shareImage(Context context,String productId, Uri localBitmapUri ,String shareMsg, boolean sendViewUrl){
        if (localBitmapUri != null) {

            Intent shareIntent = new Intent();
            shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);

            shareIntent.putExtra(Intent.EXTRA_STREAM, localBitmapUri);

           // shareIntent.putExtra(Intent.EXTRA_SUBJECT, shareMsg);
           // shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMsg);

            if(sendViewUrl) {
                String viewUrl = MultiUtil.getManifestMetadata(context, MANIFEST_BASE_URL) + "/view-product/" + MultiUtil.getManifestMetadata(context, MANIFEST_ENTERPRISE_ID) + "/" + productId;
                shareMsg = shareMsg + "\n View Details:" + Uri.parse(viewUrl);
            }
            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMsg);
            shareIntent.setType("image/*");
            //shareIntent.setType("text/html");

            // Launch sharing dialog for image
            context.startActivity(Intent.createChooser(shareIntent, "Share Image"));

        } else {
            // ...sharing failed, handle error
            Log.e(TAG," Share Image is null :---------- "+localBitmapUri);
        }



    }

    public interface OnCreateNewOderListener {
        void onCreateNewOrder(String orderId ,JSONObject orderDetails);
        void onFailedCreateOrder(int requestCode, String responseMsg);
    }

    public void createNewOrderId(String strCustomerId, OnCreateNewOderListener onCreateNewOderListener){
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                if (response != null && response.length()>0) {
                    try {
                        Log.e(TAG," After Create Order id for form view************ :-  "+response);
                        String newOrderId = response.getString(daveAIPerspective.getInvoice_id_attr_name());
                        sharedPreference.writeString( strCustomerId, newOrderId);
                        sharedPreference.writeString(CUSTOMER_ORDER_DETAILS, response.toString());

                        sharedPreference.readString(strCustomerId);
                        Log.e(TAG," After  Save order id************ :-  "+newOrderId);

                        DaveAIHelper setData = new DaveAIHelper();
                        setData.setPreOrderId(strCustomerId, newOrderId);
                        setData.setCurrentOrderId(strCustomerId, newOrderId);
                        if(daveAIPerspective.getInvoice_id_attr_name()!=null)
                            setData.addInteractionFilterAttr(daveAIPerspective.getInvoice_id_attr_name(),newOrderId);

                        if(onCreateNewOderListener!=null)
                            onCreateNewOderListener.onCreateNewOrder(newOrderId,response);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                if(onCreateNewOderListener!=null)
                    onCreateNewOderListener.onFailedCreateOrder(requestCode,responseMsg);
            }
        };
        try {
            String userId = sharedPreference.readString( SharedPreference.USER_ID);
            JSONObject postObject = new JSONObject();
            postObject.put(new ModelUtil(context).getCustomerIdAttrName(), strCustomerId);
            if(daveAIPerspective.getUser_login_id_attr_name()!=null){
                postObject.put(daveAIPerspective.getUser_login_id_attr_name(), userId);
            }
            if(daveAIPerspective.getInvoice_date_attr()!=null){
                postObject.put(daveAIPerspective.getInvoice_date_attr(), CRUDUtil.methodToday());
            }
            postObject.put(daveAIPerspective.getInvoice_close_attr_name(), false);
            //Log.e(TAG,"Create Order Post Body************ :-  "+postObject);
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                DaveModels daveModels = new DaveModels(context, true);
                daveModels.postObject(daveAIPerspective.getInvoice_model_name(),postObject, daveAIListener);
            }
        } catch (Exception e) {
            Log.e(TAG,"Error During create new OrderId:-------------  "+e.getMessage());
        }

    }




    public void updateInvoiceDetails(String orderId,JSONObject orderDetails){

        Log.e(TAG,"updateInvoiceDetails:---------" +orderId +" update object:--"+orderDetails);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                Log.e("OrderClosed"," After Order Closed  update data:-------   "+response);
            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                DaveModels daveModels = new DaveModels(context, true);
                daveModels.patchObject(daveAIPerspective.getInvoice_model_name(),orderId, orderDetails, daveAIListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("","Error During update Order Details:-------------  "+e.getMessage());
        }


    }

    public void updateInvoiceDetails(JSONObject orderDetails){

        Log.i(TAG,"updateInvoiceDetails************  "+orderDetails);

        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                Log.e("OrderClosed"," After Order Closed  update data:-------   "+response);
            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                DaveModels daveModels = new DaveModels(context, true);
                daveModels.patchObject(daveAIPerspective.getInvoice_model_name(), orderDetails, daveAIListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("","Error During update Order Details:-------------  "+e.getMessage());
        }


    }

    public void deleteOrderId(String orderId){
        DaveModels daveModels = new DaveModels(context,false);
        daveModels.deleteObject(daveAIPerspective.getInvoice_model_name(), orderId, null);

    }

    public void deleteOrdersOfCustomer(String customerId){

        ModelUtil modelUtil = new ModelUtil(context);
        DaveModels daveModels = new DaveModels(context, true);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    if (response != null && response.length() > 0 && response.has("data") && response.getJSONArray("data").length() > 0) {
                        JSONArray jsonArr = response.getJSONArray("data");
                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObject = jsonArr.getJSONObject(i);
                            daveModels.deleteObjectFromLocalDB(daveAIPerspective.getInvoice_model_name(), jsonObject.getString(daveAIPerspective.getInvoice_id_attr_name()),
                                    null);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error deleteOrdersOfCustomer:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(daveAIPerspective.getInvoice_model_name()!= null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                HashMap<String, Object> queryParams = new HashMap<>();
                queryParams.put(modelUtil.getCustomerIdAttrName(),customerId);
                Log.e(TAG, "deleteInteractionsOfCustomer :- :---------   "+queryParams);
                daveModels.getObjects(daveAIPerspective.getInvoice_model_name(), queryParams, daveAIListener, false, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error during deleteOrdersOfCustomer:-------------  "+e.getMessage());
        }


    }
    public void deleteInteractions(HashMap<String, Object> queryParams){

        ModelUtil modelUtil = new ModelUtil(context);
        DaveModels daveModels = new DaveModels(context, true);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    if (response != null && response.length() > 0 && response.has("data") && response.getJSONArray("data").length() > 0) {
                        JSONArray jsonArr = response.getJSONArray("data");
                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObject = jsonArr.getJSONObject(i);
                            daveModels.deleteObjectFromLocalDB(modelUtil.getInteractionModelName(), jsonObject.getString(modelUtil.getInteractionIdAttrName()),
                                    null);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error deleteInteractions:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(modelUtil.getInteractionModelName()!= null && !modelUtil.getInteractionModelName().isEmpty()) {
                Log.e(TAG, "deleteInteractionsOfCusomer :- :---------   "+queryParams);
                daveModels.getObjects(new ModelUtil(context).getInteractionModelName(), queryParams, daveAIListener, false, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error during get Interactions of order:-------------  "+e.getMessage());
        }


    }



}
