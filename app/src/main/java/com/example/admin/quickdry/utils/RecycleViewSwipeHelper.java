package com.example.admin.quickdry.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import static com.example.admin.quickdry.adapter.QuantityAdapter.predicatedQty;
import static com.example.admin.quickdry.activity.CustomerProfile.customerDetails;
import static com.example.admin.quickdry.fragments.DaveRecommendations.daveAction;
import com.example.admin.daveai.broadcasting.SendBroadcast;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.model.CardSwipeDetails;
import com.example.admin.daveai.model.InteractionStages;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.DaveAIHelper;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.adapter.InteractionAdapter;
import com.example.admin.quickdry.adapter.QuantityAdapter;
import com.example.admin.quickdry.adapter.RecommendationAdapter;
import com.example.admin.quickdry.adapter.RecycleViewBaseAdapter;
import com.example.admin.quickdry.adapter.TaggedProductAdapter;
import com.example.admin.quickdry.storage.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
//import static com.example.admin.daveai.broadcasting.SendBroadcast.CARD_SWIPE_MSG;
import static com.example.admin.quickdry.activity.CustomerProfile.tabHeaderList;
import static com.example.admin.daveai.others.DaveAIHelper.currentOrderId;
import static com.example.admin.daveai.others.DaveAIHelper.interactionFilterAttr;
import static com.example.admin.daveai.others.DaveAIStatic.defaultQuantityStatus;
import static com.example.admin.daveai.others.DaveAIStatic.interactionPosition;



public class RecycleViewSwipeHelper extends ItemTouchHelper.Callback implements DatabaseConstants ,AppConstants{

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private String swipeStage = null;
    private RecyclerView.Adapter adapterrecyle;
    private RecyclerView.Adapter<RecycleViewHolder> mAdapter;
    private RecyclerView qtyRecyclerView,taggedRecycleView;
    private TaggedProductAdapter taggedRecycleViewAdapter;
    private JSONObject cardItemDetails;
    private CardSwipeListener mSwipeCallBack;
    private InteractionAdapter interactionAdapter;
    private RecommendationAdapter recommendationAdapter;
    private RecycleViewBaseAdapter baseInteractionAdapter;
    private RecyclerView mRecycleView;
    private String customerId = "";
    private String productId = "";
    private String cardProductId = "";
    private int cardItemPosition = -1;
    private CardSwipeDetails cardSwipeInstance;
    private DynamicForm dynamicForm = new DynamicForm();
    private HashMap<String, Boolean> requiredFieldHashMap= new HashMap<>();
    private JSONObject interactionPostDetails = new JSONObject();
    private DaveAIPerspective daveAIPerspective;
    private int cardPosition = -1;
    public static HashMap<String,Integer> taggedProduct = new HashMap<>();




    public RecycleViewSwipeHelper(CardSwipeListener mSwipe, Context context, InteractionAdapter adapter, String customerId) {
        this.mSwipeCallBack = mSwipe;
        this.context = context;
        this.interactionAdapter = adapter;
        this.customerId = customerId;
        daveAIPerspective = DaveAIPerspective.getInstance();

    }

    public RecycleViewSwipeHelper(CardSwipeListener mSwipe, Context context, RecommendationAdapter adapter, String customerId, String productId) {
        this.mSwipeCallBack = mSwipe;
        this.context = context;
        this.recommendationAdapter = adapter;
        this.customerId = customerId;
        this.productId = productId;
        daveAIPerspective = DaveAIPerspective.getInstance();

    }

    public RecycleViewSwipeHelper(CardSwipeListener mSwipe, Context context, RecycleViewBaseAdapter adapter, String customerId,
                                  String productId) {
        this.mSwipeCallBack = mSwipe;
        this.context = context;
        this.baseInteractionAdapter = adapter;
        this.customerId = customerId;
        this.productId = productId;
        daveAIPerspective = DaveAIPerspective.getInstance();

    }


    public RecycleViewSwipeHelper(CardSwipeListener mSwipe, Context context, RecyclerView.Adapter adapter, String customerId,
                                  String adapterProductid, JSONObject cardItemDetails) {
        this.mSwipeCallBack = mSwipe;
        this.context = context;
        this.adapterrecyle = adapter;
        this.customerId = customerId;
        this.cardProductId = adapterProductid;
        this.cardItemDetails = cardItemDetails;
        daveAIPerspective = DaveAIPerspective.getInstance();


    }

    public RecycleViewSwipeHelper(CardSwipeListener mSwipe, Context context, String customerId, String productId, JSONObject cardItemDetails) {
        this.mSwipeCallBack = mSwipe;
        this.context = context;
        this.customerId = customerId;
        this.cardProductId = productId;
        this.cardItemDetails = cardItemDetails;
        daveAIPerspective = DaveAIPerspective.getInstance();
        Log.e(TAG," Print customer id :--------------"+customerId);

    }




    /*  @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }*/

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        mRecycleView = recyclerView;
        int position = viewHolder.getAdapterPosition();
        cardPosition = viewHolder.getAdapterPosition();
        if (mSwipeCallBack != null) {
            cardSwipeInstance = mSwipeCallBack.setDirectionFlags(position);

        }
        // return makeMovementFlags(dragFlags, swipeFlags);
        return makeMovementFlags(0, cardSwipeInstance.getSwipeDirection());
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        // mSwipe.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        cardItemPosition = viewHolder.getAdapterPosition();
        if (mSwipeCallBack != null) {
            mSwipeCallBack.onCardSwipe(viewHolder, direction);
        }
        try {
            onSwipeCardView(viewHolder,direction);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void onSwipeCardView(RecyclerView.ViewHolder viewHolder, final int direction) {

        if(cardSwipeInstance!=null){
            if (recommendationAdapter != null) {
                cardProductId = recommendationAdapter.getRemoveItemId(viewHolder.getAdapterPosition());
                if (daveAction.equalsIgnoreCase("Recommendations")) {
                    getStageOnSwipe(direction);
                } else if (daveAction.equalsIgnoreCase("Similar")) {
                    if (interactionPosition == -1 || interactionPosition==0) {
                        getStageOnSwipe(direction);
                    } else {
                        if (direction == ItemTouchHelper.LEFT) {
                            Log.e(TAG,"onSwipeCardView interactionPosition:---------"+tabHeaderList +" interactionPosition:-"+ interactionPosition);
                            swipeStage = tabHeaderList.get(interactionPosition - 1).toString();
                        } else if (direction == ItemTouchHelper.RIGHT) {
                            swipeStage = tabHeaderList.get(interactionPosition + 1).toString();
                        }
                    }
                }
                recommendationAdapter.remove(viewHolder.getAdapterPosition());
            } else if (interactionAdapter != null) {
                cardProductId = interactionAdapter.getRemoveItemId(viewHolder.getAdapterPosition());
                getStageOnSwipe(direction);
                interactionAdapter.remove(viewHolder.getAdapterPosition());
            } else {
                getStageOnSwipe(direction);
            }
            if (mSwipeCallBack != null) {
               // mSwipeCallBack.onCardSwipe(customerDetails, cardSwipeDetails.getProductDetails(), swipeStage, cardSwipeDetails.getInteractionStage());
                mSwipeCallBack.onCardSwipe(customerDetails, cardSwipeInstance.getCardItemDetails(), swipeStage, cardSwipeInstance.getInteractionStage());
            }

            HashMap<String, Object> broadcastDetails = new HashMap<>();
            if (customerDetails != null) {
                broadcastDetails.put("customer_object", customerDetails);
            }
            if (cardSwipeInstance.getProductDetails() != null && cardSwipeInstance.getProductDetails().length() >= 0) {
                broadcastDetails.put("product_details", cardSwipeInstance.getProductDetails().toString());
            }
            if (swipeStage != null && !swipeStage.isEmpty()) {
                broadcastDetails.put("stage", swipeStage);
            }
           /* SendBroadcast sendBroadcast = new SendBroadcast(context);
            sendBroadcast.sendBroadcast(SendBroadcast.CARD_SWIPE_ACTION, CARD_SWIPE_MSG, broadcastDetails);
*/
            nextStageEvent(direction);


        }else
            Toast.makeText(context,"Please Set CardSwipeDetails",Toast.LENGTH_SHORT).show();

    }

    private void getStageOnSwipe(int direction) {
        if (direction == ItemTouchHelper.LEFT) {
            swipeStage = cardSwipeInstance.getPreviousStage();
        } else if (direction == ItemTouchHelper.RIGHT) {
            swipeStage = cardSwipeInstance.getNextStage();
        }
    }


    private void nextStageEvent(final int direction){

        Log.i(TAG, "<<<<<<<<<DaveAIHelper.listener>>>>>>>>>>  " + DaveAIHelper.listener);
        Log.e(TAG, "<<<<<<<<<getNextStageQuantityAttrs>>>>>>>>>>  " + cardSwipeInstance.getNextStageQuantityAttrs());
        Log.e(TAG, "<<<<<<<<<getNextStageQuantityAttrs>>>>>>>>>>  " + cardSwipeInstance.getNextStageQuantityAttrs());
        Log.e(TAG, "<<<<<<<<<getNextStageTaggedProduct()>>>>>>>>>> " + cardSwipeInstance.getNextStageTaggedProduct());
        Log.e(TAG, "<<<<<<<<<getNextStageNewAttributes>>>>>>>>>> " + cardSwipeInstance.getNextStageNewAttributes());

        if (DaveAIHelper.listener != null) {
            DaveAIHelper.PostInteractionListener p = new DaveAIHelper.PostInteractionListener() {
                @Override
                public void postInteraction() {
                    postInteractionAfterSwipe(context, customerId, cardProductId, swipeStage, null);
                    showSnackBar();
                }
            };
            DaveAIHelper.PostInteractionListener q = new DaveAIHelper.PostInteractionListener() {
                @Override
                public void postInteraction() {
                    if(direction == ItemTouchHelper.RIGHT) {
                        showNextStageEventDialog();
                    }
                    else {
                        postInteractionAfterSwipe(context, customerId, cardProductId, swipeStage, null);
                        showSnackBar();
                    }
                }
            };
            if (checkNextStageStatus() && (checkNextStageQuantityAttrs() || checkTaggedProductStatus()||checkNextStageAttrStatus())) {
                Log.e(TAG, "<<<<<<<<<DaveAIHelper.listener q>>>>>>>>>>  " );
                DaveAIHelper.listener.beforePostInteraction(context, customerId, q);
            } else {
                Log.e(TAG, "<<<<<<<<<DaveAIHelper.listener P>>>>>>>>>>  " );
                DaveAIHelper.listener.beforePostInteraction(context, customerId, p);
            }

        } else if (checkNextStageStatus() && (checkNextStageQuantityAttrs() || checkTaggedProductStatus()||checkNextStageAttrStatus())) {
            if(direction == ItemTouchHelper.RIGHT) {
                showNextStageEventDialog();
            }
            else {
                postInteractionAfterSwipe(context, customerId, cardProductId, swipeStage, null);
                showSnackBar();
            }
        } else {
           // Log.e(TAG, "<<<<<<<<<No tagged Product and Next Stage qty>>>>>>>>>>" + cardSwipeDetails.getNextStageQuantityAttrs());
            postInteractionAfterSwipe(context, customerId, cardProductId, swipeStage, null);
            showSnackBar();
        }
    }

    private void showSnackBar() {
        Snackbar.make(mRecycleView, swipeStage, Snackbar.LENGTH_SHORT).show();
    }

    private boolean checkNextStageStatus(){
        Log.e(TAG,"checkNextStageStatus>>>>>>>>>>>>>>>>>>>>>>>>."+cardSwipeInstance.getNextStage());
        return ( cardSwipeInstance.getNextStage()!= null && !cardSwipeInstance.getNextStage().equals("__NULL__"));
    }

    private boolean checkNextStageQuantityAttrs(){
        return !(cardSwipeInstance.getNextStageQuantityAttrs()== null || cardSwipeInstance.getNextStageQuantityAttrs().length() == 0);
    }

    private boolean checkTaggedProductStatus(){
        return !(cardSwipeInstance.getNextStageTaggedProduct() == null || cardSwipeInstance.getNextStageTaggedProduct().length() == 0);
    }

    private boolean checkNextStageAttrStatus(){
        return !(cardSwipeInstance.getNextStageNewAttributes() == null || cardSwipeInstance.getNextStageNewAttributes().length() == 0);
    }


    private void showNextStageEventDialog() {

        Log.e(TAG,"showNextStageEventDialog Check defaultQuantityStatus****************   "+defaultQuantityStatus);
        if(checkNextStageQuantityAttrs() && defaultQuantityStatus != -1 && recommendationAdapter != null)
        {
            checkPredicatedQuantity();
        }else {
            NextStageEventDialog(
                    (checkNextStageQuantityAttrs() && (defaultQuantityStatus == -1 || recommendationAdapter == null)),
                    checkTaggedProductStatus(),
                    checkNextStageAttrStatus()
            );
        }

    }


    private void NextStageEventDialog(boolean showQtyView, boolean showTagView,boolean showNewAttrView) {
        Log.i(TAG,"******NextStageEventDialog  showQtyView:- "+showQtyView+" showTagView:-"+ showTagView +" showNewAttrView:-"+ showNewAttrView);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_next_stage_event);

        Button cancelBtn, submitBtn;

        cancelBtn =  dialog.findViewById(R.id.cancel_button);
        submitBtn = dialog.findViewById(R.id.submit_button);

        qtyRecyclerView = dialog.findViewById(R.id.qtyRecyclerView);
        qtyRecyclerView.setVisibility(View.GONE);
        qtyRecyclerView.setHasFixedSize(true);
        qtyRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        qtyRecyclerView.setLayoutManager(mLayoutManager);

        taggedRecycleView = dialog.findViewById(R.id.taggedRecycleView);
        taggedRecycleView.setVisibility(View.GONE);
        taggedRecycleView.setHasFixedSize(true);
        taggedRecycleView.setItemAnimator(new DefaultItemAnimator());
        LinearLayoutManager horizontalLayout = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        taggedRecycleView.setLayoutManager(horizontalLayout);
        taggedRecycleView.setClipToPadding(false);

        final LinearLayout stageNewAttrLayout = dialog.findViewById(R.id.stageNewAttrLayout);
        stageNewAttrLayout.setVisibility(View.GONE);

        if(showQtyView){
            qtyRecyclerView.setVisibility(View.VISIBLE);
            checkPredicatedQuantity();
        }
        if(showTagView){
            taggedRecycleView.setVisibility(View.VISIBLE);
            showTaggedProductDetails();
        }
        if(showNewAttrView){
            stageNewAttrLayout.setVisibility(View.VISIBLE);
            stageNewAttrLayout.removeAllViews();
            ArrayList<String> list = new ArrayList<String>();
            for(int i = 0; i < cardSwipeInstance.getNextStageNewAttributes().length(); i++){
                try {
                    list.add(cardSwipeInstance.getNextStageNewAttributes().get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.e(TAG,"Print NextStageEventDialog showNewAttrView interaction attr List :---  "+list +" \n "+cardSwipeInstance.getCardItemDetails() );
            dynamicForm.createDynamicForm(new DynamicForm.OnFormViewCreatedListener() {
                      @Override
                      public void onFormVewCreated(HashMap<String, Boolean> hashMap) {
                          requiredFieldHashMap= hashMap;
                          Log.i(TAG,"After Create new Attr view :----------"+ stageNewAttrLayout.getChildCount() +" "+ requiredFieldHashMap);
                      }
                  },context, stageNewAttrLayout,new ModelUtil(context).getInteractionModelName(), list, cardSwipeInstance.getCardItemDetails().toString());

        }

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // undo is selected, restore the deleted item
                if (recommendationAdapter != null) {
                    recommendationAdapter.restoreItem(cardSwipeInstance.getCardItemDetails(), cardItemPosition);
                } else if (interactionAdapter != null) {
                    interactionAdapter.restoreItem(cardSwipeInstance.getCardItemDetails(), cardItemPosition);
                }

                if(mSwipeCallBack!=null)
                    mSwipeCallBack.onItemRestore(cardItemPosition,cardSwipeInstance.getCardItemDetails());
                dialog.dismiss();
            }
        });


        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                Log.e(TAG, "<<<<<<<<<Click On Submit Button>>>>>>>>>>  ");
                if(checkTaggedProductStatus()){
                    Log.e(TAG, "<<<<<<<<<Click On Submit Button checkTaggedProductStatus>>>>>>>>>>  ");
                    //postTaggedProductToServer();
                    postTaggedProduct();
                }

                interactionPostDetails =  dynamicForm.methodValidateDynamicView(stageNewAttrLayout,requiredFieldHashMap,null);
                Log.e(TAG,"bindInteractionView******NextinteractionStageAttrList************:- " +interactionPostDetails);
                if(checkNextStageQuantityAttrs()){
                    if(qtyRecyclerView.getAdapter()!=null) {
                        boolean isQuantityValid = true;
                        int count = qtyRecyclerView.getAdapter().getItemCount();
                        JSONObject currentQtyData = new JSONObject();
                        for (int i = 0; i < count; i++) {
                            View view = qtyRecyclerView.getChildAt(i);
                            if (view != null) {
                                EditText textValue = view.findViewById(R.id.quantity_count);
                                if (textValue.getText().toString().isEmpty()) {
                                    isQuantityValid = false;
                                    break;
                                } else {
                                    TextView textKey = view.findViewById(R.id.recommended_qty);
                                    String keyName= textKey.getText().toString();
                                    String[] arrayString =keyName.split(" ");
                                    String recommKey = arrayString[0];
                                    double getValue = Double.parseDouble(textValue.getText().toString());
                                    try {
                                        currentQtyData.put(recommKey, getValue);
                                    } catch (JSONException e) {
                                        Log.e(TAG, "Get All Qty Error**** " + e.getLocalizedMessage());
                                    }
                                }
                            }
                        }
                        if (isQuantityValid) {
                            // Log.e(TAG, "<<<<<<<<<Click On Submit Button Post Main Product if qty is dere >>>>>>>>>>  ");

                            postInteractionAfterSwipe(context, customerId, cardProductId, swipeStage, currentQtyData);
                            dialog.dismiss();
                            Snackbar.make(mRecycleView, swipeStage, Snackbar.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(v.getContext(), "Please Enter Quantity ", Toast.LENGTH_SHORT).show();
                        }

                    }

                }else {
                    //Log.e(TAG, "<<<<<<<<<Click On Submit Button Post Main Product if qty is not dere >>>>>>>>>>  ");

                    postInteractionAfterSwipe(context, customerId, cardProductId, swipeStage, null);
                    dialog.dismiss();
                    Snackbar.make(mRecycleView, swipeStage, Snackbar.LENGTH_SHORT).show();
                }





            }
        });

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }



    private void checkPredicatedQuantity(){
        if(cardSwipeInstance.getQuantityPredictions().length()==0 ||cardSwipeInstance.getQuantityPredictions().equals(new JSONObject())){
            getNextStageQuantity();
        }else {
            try {
                //JSONObject getStageQty = quantityPredictions.getJSONObject(interactionStageTitle);
                JSONObject getStageQty = cardSwipeInstance.getQuantityPredictions().getJSONObject(cardSwipeInstance.getNextStage());
                JSONObject insightObject = getStageQty.getJSONObject("__insight__");
                String strInSight = insightObject.getString("qty");

                Double getQty = getStageQty.optDouble("qty");
                JSONObject getNextStageQty = new JSONObject();
                getNextStageQty.put("qty",getQty);
                //Log.e(TAG,"******** Default QuantityPredictions value:--   "+getNextStageQty);
                mapCurrentWithRecommendedQty(getNextStageQty,strInSight);
            } catch (JSONException e) {
                Log.e(TAG,"Error During reading Predicated Qty****:--   "+e.getMessage());
            }

        }
    }


    private void getNextStageQuantity() {

        if (CheckNetworkConnection.networkHasConnection(context)) {
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        JSONObject qtyPredictions = new JSONObject(response);
                        JSONObject allNextStageQty = qtyPredictions.getJSONObject(cardSwipeInstance.getNextStage());
                        JSONObject insightObject = allNextStageQty.getJSONObject("__insight__");
                        String strInSight = insightObject.getString("qty");
                        JSONObject getNextStageQty = new JSONObject();
                        for (int i = 0; i < cardSwipeInstance.getNextStageQuantityAttrs().length(); i++) {
                            String qtyKey = cardSwipeInstance.getNextStageQuantityAttrs().get(i).toString();
                            getNextStageQty.put(qtyKey, allNextStageQty.get(qtyKey));
                        }
                        mapCurrentWithRecommendedQty(getNextStageQty,strInSight);
                    } catch (Exception e) {
                        Log.e(TAG, "<<<<<<<<<Get Next Predicated Qty Attr Error>>>>>>>>>>" + e.getLocalizedMessage());
                    }
                }

                @Override
                public void onTaskFailure(int requestCode, String responseMsg) {

                }
            };
            new APICallAsyncTask(postTaskListener, context, "GET", false)
                    .execute(APIRoutes.quantityPredictionsAPI(cardProductId, customerId,cardSwipeInstance.getNextStage()));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }

    private void mapCurrentWithRecommendedQty(JSONObject recommendedQty,String strInSight) {

        ArrayList<String> qtyList = new ArrayList<>();
        Iterator<?> rKeys = recommendedQty.keys();
        while (rKeys.hasNext()) {
            String key = (String) rKeys.next();
            if (!qtyList.contains(key))
                qtyList.add(key);
        }
        Log.e(TAG, "<<<<<<<<<mapCurrentWithRecommendedQty  qtyList>>>>>>>>>" + qtyList);
        List<JSONObject> quantityDetails = new ArrayList<>();
        for (int i = 0; i < qtyList.size(); i++) {
            JSONObject quantityList = new JSONObject();
            try {
                String key = qtyList.get(i);
                if (recommendedQty.getInt(key) > 0 && cardSwipeInstance.getQuantityAttrs().has(key)) {
                    quantityList.put("Previous " + key, cardSwipeInstance.getQuantityAttrs().get(key));
                    quantityList.put(key , recommendedQty.get(key));

                } else if (recommendedQty.getInt(key) > 0 ) {
                    quantityList.put(key, recommendedQty.get(key));
                } else if (cardSwipeInstance.getQuantityAttrs().has(key)) {
                    quantityList.put(key, cardSwipeInstance.getQuantityAttrs().get(key));
                    quantityList.put("Previous " + key, cardSwipeInstance.getQuantityAttrs().get(key));
                } else {
                    quantityList.put(key, 1);
                }
                quantityDetails.add(quantityList);
                if (defaultQuantityStatus != -1 && recommendationAdapter != null) {
                    JSONObject recommQtyObjects = new JSONObject();
                    for (int j = 0; j < qtyList.size(); j++)
                        recommQtyObjects.put(qtyList.get(j), defaultQuantityStatus);

                    postInteractionAfterSwipe(context, customerId, cardProductId, swipeStage, recommQtyObjects);
                } else {
                    setQtyInAdapter(quantityDetails,strInSight);
                }

            } catch (Exception e) {
                Log.e(TAG, "<<<<<<<<<Get Predicated QtyAttr Error>>>>>>>>>>" + e.getLocalizedMessage());
            }
        }
    }


    private void setQtyInAdapter(List<JSONObject> quantityDetails ,String strInSight){
        if (quantityDetails.size() > 0) {
            QuantityAdapter mAdapter = new QuantityAdapter(context,strInSight,quantityDetails);
            qtyRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }
    }
    private  void showTaggedProductDetails(){

        taggedRecycleViewAdapter = new TaggedProductAdapter(context,cardSwipeInstance.getNextStageTaggedProduct()
                ,checkNextStageQuantityAttrs());
        taggedRecycleView.setAdapter(taggedRecycleViewAdapter);
        taggedRecycleViewAdapter.notifyDataSetChanged();

    }

    private void postTaggedProduct(){
        Log.e(TAG,"Get Tagged ProductList:-------"+taggedProduct);
        try {
            if(!taggedProduct.isEmpty())
                for (Map.Entry<String,Integer> entry : taggedProduct.entrySet()) {
                    JSONObject taggedQtyData = new JSONObject();
                    if(entry.getValue()>0)
                        taggedQtyData.put(DaveAIPerspective.getInstance().getInteraction_qty(), entry.getValue());
                   // postInteractionAfterSwipe(context, customerId, entry.getKey(), swipeStage, taggedQtyData);
                    postTaggedProduct(context, customerId, entry.getKey(), swipeStage, taggedQtyData);
                }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Post  Tagged Product Error**** " + e.getLocalizedMessage());
        }

    }

    private void postTaggedProduct(Context context,String customerId,String productId, String interactionStage,JSONObject qtyDetails){

        JSONObject post_body = new JSONObject();
        try {
            /*post_body.put(interactionCustomerId, customerId);
            post_body.put(interactionProductId, productId);
            post_body.put(interactionName, interactionStage);
*/
            ModelUtil modelUtil = new ModelUtil(context);
            post_body.put(modelUtil.getInteractionCustomerId(), customerId);
            post_body.put(modelUtil.getInteractionProductId(), productId);
            post_body.put(modelUtil.getInteractionStageAttrName(), interactionStage);

            if(qtyDetails!=null && qtyDetails.length()>0){
                Iterator<?> keys = qtyDetails.keys();
                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    post_body.put(key, qtyDetails.get(key));
                }
            }

            String cOrderId = currentOrderId.get(customerId);
            if(cOrderId!=null) {
                if (interactionFilterAttr != null && interactionFilterAttr.length() > 0) {
                    Iterator<?> keys = interactionFilterAttr.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        post_body.put(key, interactionFilterAttr.get(key));
                    }
                }
            }else{
                if(daveAIPerspective.getUser_login_id_attr_name()!=null){
                    String  userId =  new SharedPreference(context).readString(SharedPreference.USER_ID);
                    post_body.put(daveAIPerspective.getUser_login_id_attr_name(),userId);
                }
            }

            DaveModels daveModels = new DaveModels(context, false);
            daveModels.postObject(new ModelUtil(context).getInteractionModelName(), post_body,new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {

                }
                @Override
                public void onResponseFailure(int requestCode, String responseMsg) {
                    Log.e(TAG,"Error During Post :--------"+responseMsg);

                }
            } );

        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG, "********post TaggedInteraction Error*********   " + e.getLocalizedMessage());
        }

    }

    public  void postInteractionAfterSwipe(Context context,String customerId,String productId, String interactionStage,JSONObject qtyDetails){

        JSONObject post_body = new JSONObject();
        try {

            JSONObject cardDetails = cardSwipeInstance.getCardItemDetails();
            Log.e(TAG,"postInteractionAfterSwipe Get Card Swipe Details :------------------------:--------  "+cardDetails);

            String interactionIdAttrName = new ModelUtil(context).getInteractionIdAttrName();
            if(cardDetails.has(interactionIdAttrName))
                post_body.put(interactionIdAttrName,cardDetails.getString(interactionIdAttrName));

            ModelUtil modelUtil = new ModelUtil(context);
            post_body.put(modelUtil.getInteractionCustomerId(), customerId);
            post_body.put(modelUtil.getInteractionProductId(), productId);
            post_body.put(modelUtil.getInteractionStageAttrName(), interactionStage);
            // post_body.put("_async", true);

            String cOrderId = currentOrderId.get(customerId);
            if(cOrderId!=null) {
                if (interactionFilterAttr != null && interactionFilterAttr.length() > 0) {
                    Iterator<?> keys = interactionFilterAttr.keys();
                    while (keys.hasNext()) {
                        String key = (String) keys.next();
                        post_body.put(key, interactionFilterAttr.get(key));
                    }
                }
            }else{
                if(daveAIPerspective.getUser_login_id_attr_name()!=null){
                    String userId = new SharedPreference(context).readString(SharedPreference.USER_ID);
                    post_body.put(daveAIPerspective.getUser_login_id_attr_name(),userId);
                }
            }

            if(daveAIPerspective.getPredicated_qty_attr()!= null)
                post_body.put(daveAIPerspective.getPredicated_qty_attr(),predicatedQty);

            if(qtyDetails!=null && qtyDetails.length()>0){
                Iterator<?> keys = qtyDetails.keys();
                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    post_body.put(key, qtyDetails.get(key));
                }
            }

            if(interactionPostDetails!=null &&interactionPostDetails.length()>0){
                Iterator<?> keys = interactionPostDetails.keys();
                while( keys.hasNext() ) {
                    String key = (String)keys.next();
                    post_body.put(key, interactionPostDetails.get(key));
                }

            }
            //todo add current Stage in post Interaction body
            // product_attributes,  customer_attributes,  tagged_products, interaction_stage_attr_list,
            if(cardDetails.has("current_stage")) {
                post_body = PostInteractionUtil.addStageAttributesInInteractionBody(cardDetails.getJSONObject("current_stage"),post_body);
            }else{
                post_body = PostInteractionUtil.addStageAttributesInInteractionBody(cardDetails,post_body);
            }

            //Log.e(TAG,"postInteractionAfterSwipe Get Card Swipe Details :------------------------:--------  "+cardDetails);
            InteractionStages interactionInstance = ModelUtil.getStageInstance(context,interactionStage);
            List<String> customerAttributes = interactionInstance.getCustomer_attributes();
            List<String> productAttributes = interactionInstance.getProduct_attributes();
            JSONObject productDetails = new JSONObject();
            if(cardDetails.has("details") && !cardDetails.isNull("details")  && cardDetails.getJSONObject("details").length()>0){
                productDetails = cardDetails.getJSONObject("details");

            }else if(cardDetails.has("product_attributes") && !cardDetails.isNull("product_attributes")&& cardDetails.getJSONObject("product_attributes").length()>0){
                productDetails = cardDetails.getJSONObject("product_attributes");
            }
           // Log.i(TAG,"Print current Stage  customer Attributes >>>>"+customerAttributes );
           // Log.i(TAG,"Print current Stage  product_attributes>>>>"+productAttributes +"  :--- "+ productDetails);
            post_body = PostInteractionUtil.addModelAttributesInPostInteraction(productAttributes,productDetails,post_body);

            Log.e(TAG,"Print customer details on swipe:----------"+customerDetails);
            if( customerDetails!=null && !customerDetails.isEmpty())
                post_body = PostInteractionUtil.addModelAttributesInPostInteraction(customerAttributes,new JSONObject(customerDetails),post_body);


            Log.e(TAG, "******** After Adding All Attributes Post Interaction Body**********   " + post_body);

            DaveModels daveModels = new DaveModels(context, false);
            daveModels.postObject(new ModelUtil(context).getInteractionModelName(), post_body,new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {

                }
                @Override
                public void onResponseFailure(int requestCode, String responseMsg) {
                    Log.e(TAG,"Error During Post Interaction :--------"+responseMsg);

                }
            } );

        }catch(Exception e){
            e.printStackTrace();
            Log.e(TAG, "********postInteraction Error*********   " + e.getLocalizedMessage());
        }

    }
}

