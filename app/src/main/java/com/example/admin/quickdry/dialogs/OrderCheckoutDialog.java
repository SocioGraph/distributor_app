package com.example.admin.quickdry.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import org.json.JSONObject;
import java.util.HashMap;
import com.example.admin.daveai.others.DaveAIPerspective;


public class OrderCheckoutDialog extends Dialog {

    private static final String TAG = "OrderCheckoutDialog";
    private final Context mContext;

    private LinearLayout orderLayout;
    private ProgressBar progressBar;


    private String currentOrderId="";
    private DynamicForm dynamicForm;
    private HashMap<String, Boolean> requiredFieldHashMap= new HashMap<>();
    private DaveAIPerspective daveAIPerspective;
    private Boolean isCheckout = null;

    private String currentOrderDetail = null;

    private OrderCheckoutListener orderCheckoutListener;
    public interface OrderCheckoutListener {
        void onCheckoutOrder(JSONObject orderDetails);
        void onCancelOrder();

    }

    public OrderCheckoutDialog(Context context) {
        super(context);
        this.mContext = context;

    }

    public OrderCheckoutDialog(Context context,String currentOrderId,OrderCheckoutListener orderCheckoutListener) {
        super(context);
        this.mContext = context;
        this.currentOrderId=currentOrderId;
        this.orderCheckoutListener=orderCheckoutListener;
    }


    public OrderCheckoutDialog(Context context,String currentOrderId,Boolean isCheckout,OrderCheckoutListener orderCheckoutListener) {
        super(context);
        this.mContext = context;
        this.currentOrderId=currentOrderId;
        this.isCheckout = isCheckout;
        this.orderCheckoutListener=orderCheckoutListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.order_checkout);

        RelativeLayout orderDetailLayout = findViewById(R.id.orderDetailLayout);
        TextView noResultFound = findViewById(R.id.noResultFound);
        orderLayout = findViewById(R.id.orderLayout);
        Button okReview = findViewById(R.id.okReview);
        Button cancelReview = findViewById(R.id.cancelReview);
        progressBar= findViewById(R.id.progressBar);

        daveAIPerspective= DaveAIPerspective.getInstance();
        dynamicForm = new DynamicForm();
        //currentOrderId = SharedPreference.readString(mContext, selectedCustomerIdValue, null);

        Log.e(TAG,"print orderId and OrderList :----"+ currentOrderId+"  "+daveAIPerspective.getInvoice_checkout_attr_list());
        if(currentOrderId!=null && daveAIPerspective.getInvoice_checkout_attr_list()!=null &&
                daveAIPerspective .getInvoice_checkout_attr_list().size()>0){
            orderDetailLayout.setVisibility(View.VISIBLE);
            noResultFound.setVisibility(View.GONE);
            getInvoiceDetails();

        }else {
            orderDetailLayout.setVisibility(View.GONE);
            noResultFound.setVisibility(View.VISIBLE);
            noResultFound.setText("Please Set details in Perspective.");
        }

        cancelReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                dismiss();
                if(orderCheckoutListener!=null){
                    orderCheckoutListener.onCancelOrder();
                }
               // dismiss();

            }
        });

        okReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //JSONObject  orderDetails =  dynamicForm.methodValidateDynamicView(orderLayout,requiredFieldHashMap);
                JSONObject  orderDetails =  dynamicForm.methodValidateDynamicView(orderLayout,requiredFieldHashMap,currentOrderDetail);
               // Log.e("okReview","post data:-------   "+orderDetails);
                if(orderDetails!=null){
                    updateInvoiceDetails(orderDetails);
                }
            }
        });
    }

    private void getInvoiceDetails(){

        progressBar.setVisibility(View.VISIBLE);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);
                Log.e(TAG,"Order Details:-------------  "+response);
                if (response != null) {
                    currentOrderDetail = response.toString();
                    dynamicForm.createDynamicForm(new DynamicForm.OnFormViewCreatedListener() {
                          @Override
                          public void onFormVewCreated(HashMap<String, Boolean> hashMap) {
                              requiredFieldHashMap= hashMap;
                          }
                    },mContext,orderLayout,daveAIPerspective.getInvoice_model_name(),daveAIPerspective.getInvoice_checkout_attr_list(),response.toString());
                }
            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                DaveModels daveModels = new DaveModels(mContext, true);
                daveModels.getObject(daveAIPerspective.getInvoice_model_name(), currentOrderId, daveAIListener, false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During showing Order Details:-------------  "+e.getMessage());
        }
    }

    public void updateInvoiceDetails(JSONObject orderDetails){
        ShowProgressDialog("Updating the details... Please Wait...");
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                Log.e("OrderClosed"," After Order Closed  update data:-------   "+response);
                if (response != null) {
                    Toast.makeText(mContext,"Saved Successfully, Pending Sync",Toast.LENGTH_LONG).show();
                    dismiss();
                    if(orderCheckoutListener!=null){
                        orderCheckoutListener.onCheckoutOrder(response);
                    }

                }
                HideProgressDialog();
            }

            @Override
            public void onResponseFailure(int responseCode, String responseMsg) {
                HideProgressDialog();
                Toast.makeText(mContext,responseMsg,Toast.LENGTH_SHORT).show();
            }
        };
        try {
            if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty() ) {
                if( isCheckout!=null && isCheckout) {
                    orderDetails.put(daveAIPerspective.getInvoice_close_attr_name(), isCheckout);
                }

                Log.e(TAG,"Order update  post data:-------   "+isCheckout+" "+orderDetails);
                DaveModels daveModels = new DaveModels(mContext, true);
                daveModels.patchObject(true,daveAIPerspective.getInvoice_model_name(), orderDetails, daveAIListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During update Order Details:-------------  "+e.getMessage());
            HideProgressDialog();
        }


    }
    AlertDialog b;
    AlertDialog.Builder dialogBuilder;

    public void ShowProgressDialog(String message) {
        Log.e("FLOW--","showProgressDialog()");
        dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        TextView textView = dialogView.findViewById(R.id.textView9);
        if(TextUtils.isEmpty(message)){
            textView.setText(message);
        }
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog(){
        Log.e("FLOW--","HideProgressDialog()");

        if(b.isShowing()) {
            b.dismiss();
        }
    }
}

   /* private void getOrderDetails(){

        progressBar.setVisibility(View.VISIBLE);
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {

                progressBar.setVisibility(View.GONE);
                if (response != null) {
                    requiredFieldHashMap = dynamicForm.createDynamicForm(mContext,orderLayout,daveAIPerspective.getInvoice_model_name(),
                            daveAIPerspective.getInvoice_checkout_attr_list(),response);
                }
            }
            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {

            }
        };
        if (currentOrderId!=null && !currentOrderId.isEmpty()) {
            if (CheckNetworkConnection.networkHasConnection(mContext)) {
                Model model1 = new Model(mContext);
                model1.getObject(daveAIPerspective.getInvoice_model_name(), currentOrderId,false, postTaskListener);

            } else {
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }
        }
    }
*/

/*    private void updateOrderDetails(JSONObject orderDetails){
        try {
            orderDetails.put(daveAIPerspective.getInvoice_close_attr_name(),true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("OrderClosed","Order Closed  post data:-------   "+orderDetails);
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                if (response != null) {
                    SharedPreference sharedPreferences = new SharedPreference() ;
                    String customerId = sharedPreferences.readString( mContext, SharedPreference.CUSTOMER_ID);
                    sharedPreferences.removeKey(mContext,customerId);
                    dismiss();
                    ((Activity) mContext).finish();
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {

            }
        };
        if (currentOrderId!=null && !currentOrderId.isEmpty()) {
            if (CheckNetworkConnection.networkHasConnection(mContext)) {
                Model model1 = new Model(mContext);
                model1.updateObject(daveAIPerspective.getInvoice_model_name(), currentOrderId, orderDetails, true, postTaskListener);

            } else {
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }

        }
    }*/




