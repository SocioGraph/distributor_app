package com.example.admin.quickdry.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.dialogs.OrderCheckoutDialog;
import com.example.admin.quickdry.dialogs.ShareProductCatalogue;
import com.example.admin.quickdry.utils.AppConstants;
import com.example.admin.quickdry.storage.SharedPreference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.admin.daveai.database.DatabaseConstants.ERROR_MESSAGE_TEXT;
import static com.example.admin.daveai.database.DatabaseConstants.INTERACTION_STAGES_TABLE_NAME;


public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.CustomViewHolder>
        implements RecyclerView.OnClickListener ,AppConstants, DaveConstants {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;
    private List<JSONObject> seedOrderList;
    private List<JSONObject> mOrderList;
    private DaveAIPerspective daveAIPerspective;
    private HashMap<String,Object> shareButtonListMap = new HashMap<>();
    private ModelUtil modelUtil;


    private RecyclerViewClickListener recyclerViewClickListener;
    public interface RecyclerViewClickListener {
        void onClickCard(int position,HashMap<String, Object> orderDetails);
        void onDeleteCard(int cardPosition, String orderId, JSONObject orderDetails);
    }

    public OrderListAdapter(RecyclerViewClickListener recyclerViewClickListener, Context context, List<JSONObject> customerList) {
        this.recyclerViewClickListener = recyclerViewClickListener;
        this.seedOrderList = customerList;
        this.mContext = context;

        daveAIPerspective= DaveAIPerspective.getInstance();
        mOrderList = new ArrayList<>();
        modelUtil = new ModelUtil(context);
        shareButtonListMap = modelUtil.getInteractionStageButtonList();

    }


    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_order_card_view,viewGroup, false);
        CustomViewHolder holder = new CustomViewHolder(view);
        holder.itemView.setOnClickListener(OrderListAdapter.this);
        holder.itemView.setTag(holder);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder customViewHolder, int _position) {

        JSONObject seedItem = seedOrderList.get(_position);
        Log.e(TAG,"************************onBindViewHolder:------  orderlistadapter  "+ seedItem);
        try{
            if(daveAIPerspective.getInvoice_card_header()!=null && seedItem.has(daveAIPerspective.getInvoice_card_header())) {
                customViewHolder.header.setVisibility(View.VISIBLE);
                customViewHolder.header.setText(seedItem.getString(daveAIPerspective.getInvoice_card_header()));
            }else {
                customViewHolder.header.setVisibility(View.GONE);
            }

            if(daveAIPerspective.getInvoice_card_sub_header()!=null && seedItem.has(daveAIPerspective.getInvoice_card_sub_header()) &&
                    seedItem.getString(daveAIPerspective.getInvoice_card_sub_header())!=null) {
                customViewHolder.subHeader.setVisibility(View.VISIBLE);
                customViewHolder.subHeader.setText(seedItem.getString(daveAIPerspective.getInvoice_card_sub_header()));
            }else {
                customViewHolder.subHeader.setVisibility(View.GONE);
            }

            if(daveAIPerspective.getInvoice_card_right_attr()!=null && seedItem.has(daveAIPerspective.getInvoice_card_right_attr()) &&
                    seedItem.getString(daveAIPerspective.getInvoice_card_right_attr())!=null) {
                customViewHolder.rightAttr.setVisibility(View.VISIBLE);
                customViewHolder.rightAttr.setText(seedItem.getString(daveAIPerspective.getInvoice_card_right_attr()));
            }else {
                customViewHolder.rightAttr.setVisibility(View.GONE);
            }

            if(daveAIPerspective.getInvoice_date_attr()!=null && seedItem.has(daveAIPerspective.getInvoice_date_attr()) &&
                    seedItem.getString(daveAIPerspective.getInvoice_date_attr())!=null) {
                customViewHolder.date.setVisibility(View.VISIBLE);
                customViewHolder.date.setText(seedItem.getString(daveAIPerspective.getInvoice_date_attr()));
            }else {
                customViewHolder.date.setVisibility(View.GONE);
            }

            //if close attr name value if false show checkout and delete button
            if(seedItem.has(daveAIPerspective.getInvoice_close_attr_name()) && !seedItem.getBoolean(daveAIPerspective.getInvoice_close_attr_name())){
                String stageAttr = daveAIPerspective.getInvoice_stage_attr();

                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                String stageDetail = databaseManager.getObjectData(INTERACTION_STAGES_TABLE_NAME, stageAttr).toString();
                try {
                    JSONObject stageJobj = new JSONObject(stageDetail);
                    if(stageJobj.has("order_checkout_name")&&!stageJobj.getString("order_checkout_name").equals("__NULL__")&&!stageJobj.getString("order_checkout_name").equals("_NULL_")){
                        customViewHolder.checkout.setVisibility(View.VISIBLE);
                        customViewHolder.buttonParentView.setWeightSum(customViewHolder.buttonParentView.getChildCount());
                        customViewHolder.checkout.setText(stageJobj.getString("order_checkout_name"));
                        customViewHolder.delete.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }else{
                customViewHolder.checkout.setVisibility(View.GONE);
                customViewHolder.buttonParentView.setWeightSum(customViewHolder.buttonParentView.getChildCount()-1);
                customViewHolder.delete.setVisibility(View.GONE);
            }

            if(seedItem.has(ERROR_MSG_ATTRIBUTE) && !seedItem.isNull(ERROR_MSG_ATTRIBUTE) && !seedItem.getString(ERROR_MSG_ATTRIBUTE).isEmpty()){
                customViewHolder.delete.setVisibility(View.VISIBLE);
               // customViewHolder.edit.setVisibility(View.VISIBLE);
                customViewHolder.errorIconView.setVisibility(View.VISIBLE);
                if(seedItem.getString(ERROR_MSG_ATTRIBUTE).equals(ERROR_MESSAGE_TEXT)){
                    customViewHolder.errorIconView.setImageResource(R.drawable.ic_report_error_green);
                }

            }else {
                customViewHolder.delete.setVisibility(View.GONE);
                //customViewHolder.edit.setVisibility(View.GONE);
                customViewHolder.errorIconView.setVisibility(View.GONE);

            }

            //set share button name ....take button name from shareButtonList of all stages
            String stageName = daveAIPerspective.getInvoice_stage_attr();
            Log.e(TAG,"Testing  stageNAme:-- "+stageName +"  "+shareButtonListMap );
            if(!shareButtonListMap.isEmpty() && stageName!= null) {
                Log.e(TAG,"Testing  stageNAme:-- "+stageName +"  "+shareButtonListMap );
                if(shareButtonListMap.containsKey(stageName)) {
                    String getShareButtonNAme = shareButtonListMap.get(stageName).toString();
                    Log.e(TAG,"Testing  getShareButtonNAme:-- "+getShareButtonNAme );
                    customViewHolder.share.setText(getShareButtonNAme);
                    customViewHolder.buttonParentView.setWeightSum(customViewHolder.buttonParentView.getChildCount());
                }else {
                    customViewHolder.share.setVisibility(View.GONE);
                    customViewHolder.buttonParentView.setWeightSum(customViewHolder.buttonParentView.getChildCount()-1);
                }
            }else {
                customViewHolder.share.setVisibility(View.GONE);
                customViewHolder.buttonParentView.setWeightSum(customViewHolder.buttonParentView.getChildCount()-1);
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during Order Detail View+++++++++++" + e.getMessage());
        }

        customViewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateOrderDetails(customViewHolder.getAdapterPosition(),null,false,seedItem,customViewHolder);
            }
        });

        customViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String orderID= seedOrderList.get(customViewHolder.getAdapterPosition()).getString(daveAIPerspective.getInvoice_id_attr_name());
                    dialogToAskDeleteOrder(customViewHolder.getAdapterPosition(),orderID,seedOrderList.get(customViewHolder.getAdapterPosition()));

                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "Error during Click on orderDelete +++++++++++" + e.getMessage());
                }



            }
        });

        customViewHolder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                shareOrder(seedItem,customViewHolder);
                updateOrderDetails(customViewHolder.getAdapterPosition(),null,true,seedItem,customViewHolder);

            }
        });

        customViewHolder.checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    OrderCheckoutDialog ocd = new OrderCheckoutDialog(mContext,seedItem.getString(daveAIPerspective.getInvoice_id_attr_name()),true,
                            new OrderCheckoutDialog.OrderCheckoutListener() {
                                @Override
                                public void onCheckoutOrder(JSONObject orderDetails) {
                                    updateItem(orderDetails,customViewHolder.getAdapterPosition());
                                }

                                @Override
                                public void onCancelOrder() {

                                }
                            });
                    ocd.show();

                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "Error during  getting Order Id +++++++++++" + e.getMessage());
                }

            }
        });

    }

    private void shareOrder(JSONObject seedItem, CustomViewHolder customViewHolder){
        try {

            long queueCount = DatabaseManager.getInstance(mContext).forceUpdateQueueSize(DatabaseConstants.APIQueuePriority.ONE.getValue());
            if(queueCount==0){
                HashMap<String,Object> buttonDetails = new HashMap<>();
                buttonDetails.put("customer_id",seedItem.getString(new ModelUtil(mContext).getCustomerIdAttrName()));
                buttonDetails.put("button_name",customViewHolder.share.getText().toString());
                buttonDetails.put("interaction_stage",daveAIPerspective.getInvoice_stage_attr());

                String customerID = seedOrderList.get(customViewHolder.getAdapterPosition()).getString(new ModelUtil(mContext).getCustomerIdAttrName());
                String orderID= seedOrderList.get(customViewHolder.getAdapterPosition()).getString(daveAIPerspective.getInvoice_id_attr_name());

                SharedPreference sharedPreference = new SharedPreference(mContext);
                sharedPreference.writeString(SharedPreference.CUSTOMER_ID, customerID);
                sharedPreference.writeString(customerID,orderID);

                ShareProductCatalogue cSPD = new ShareProductCatalogue(mContext,buttonDetails);
                cSPD.show();

            }else {
                Toast.makeText(mContext,R.string.share_msg,Toast.LENGTH_LONG).show();
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during  getting Order Id +++++++++++" + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return (null != seedOrderList ? seedOrderList.size() : 0);
    }


    @Override
    public void onClick(View view) {

        CustomViewHolder holder = (CustomViewHolder) view.getTag();
        int cardPos = holder.getAdapterPosition();
        try{
            //if(daveAIPerspective.getForm_view_button_name()!=null && !daveAIPerspective.getForm_view_button_name().isEmpty()){
            String customerID = seedOrderList.get(cardPos).getString(new ModelUtil(mContext).getCustomerIdAttrName());
            String orderID = seedOrderList.get(cardPos).getString(daveAIPerspective.getInvoice_id_attr_name());

            SharedPreference sharedPreference = new SharedPreference(mContext);
            sharedPreference.writeString(SharedPreference.CUSTOMER_ID, customerID);
            sharedPreference.writeString(customerID,orderID);
            // Log.e(TAG," on click Get order details orderID:-"+ sharedPreference.readString(mContext,customerID));


            HashMap<String, Object> intent_details = new HashMap<>();
            intent_details.put("customer_id", customerID);
            intent_details.put("order_id", orderID);
            intent_details.put("product_id", "");
            intent_details.put("orderDetails", seedOrderList.get(cardPos).toString());

            if(recyclerViewClickListener!=null){
                recyclerViewClickListener.onClickCard(cardPos,intent_details);
            }

        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(mContext,e.getMessage(),Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Error during Click on Order Card +++++++++++" + e.getMessage());
        }


    }

    public void addNewSeedList(List<JSONObject> newItemList,boolean isClearPrevious) {
        seedOrderList.clear();
        seedOrderList.addAll(newItemList);
        notifyItemInserted(seedOrderList.size());
        notifyDataSetChanged();
        if(isClearPrevious) {
            mOrderList.clear();
            mOrderList.addAll(newItemList);
        }
        Log.e(TAG," After addNewItem size of adapter****************:- " + mOrderList.size());
    }

    public void updateSeedList(List<JSONObject> newItemList,boolean isAllData) {
        seedOrderList.addAll(newItemList);
        notifyItemInserted(seedOrderList.size());
        notifyDataSetChanged();
        if(isAllData)
            mOrderList.addAll(newItemList);

        //Log.e(TAG," After updateSeedList seedCustomerList of adapter****************:- " + seedCustomerList.size());

    }

    public void updateItem(JSONObject newItemDetails, int position) {
        seedOrderList.set(position,newItemDetails);
        notifyItemChanged(position);
    }

    public void clear() {
        int size = seedOrderList.size();
        seedOrderList.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void resetPreviousOrderList(){
        seedOrderList.clear();
       // Log.e(TAG,"resetPreviousOrderList of adapter****************:- " + mOrderList.size() );
       // Log.e(TAG,"resetPreviousOrderList of adapter****************:- " +seedOrderList.size());
        seedOrderList = new ArrayList<>(mOrderList);
        notifyItemInserted(seedOrderList.size());
        notifyDataSetChanged();

    }
    public void deleteItem(int index) {
        seedOrderList.remove(index);
        notifyItemRemoved(index);
    }


    class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView header,rightAttr,subHeader,date;
        ImageView delete,edit,errorIconView;
        Button checkout,share;
        LinearLayout buttonParentView;


        CustomViewHolder(View view) {
            super(view);

            edit = view.findViewById(R.id.edit);
            delete = view.findViewById(R.id.delete);
            errorIconView = view.findViewById(R.id.errorIconView);
            header = view.findViewById(R.id.header);
            subHeader = view.findViewById(R.id.subHeader);
            rightAttr = view.findViewById(R.id.rightAttr);
            date = view.findViewById(R.id.date);
            buttonParentView = view.findViewById(R.id.buttonParentView);
            checkout = view.findViewById(R.id.checkout);
            share = view.findViewById(R.id.share);

        }
    }

    private void updateOrderDetails(int position, Boolean isCheckOut, boolean isShare, JSONObject seeditem, CustomViewHolder customViewHolder){
        try {
            OrderCheckoutDialog ocd = new OrderCheckoutDialog(mContext,
                seedOrderList.get(position).getString(daveAIPerspective.getInvoice_id_attr_name()),isCheckOut,
                new OrderCheckoutDialog.OrderCheckoutListener() {
                    @Override
                    public void onCheckoutOrder(JSONObject orderDetails) {
                        updateItem(orderDetails,position);
                        if(isShare){
                            shareOrder(seeditem, customViewHolder);
                        }
                    }

                    @Override
                    public void onCancelOrder() {

                    }
                });
            ocd.show();

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during Update Order Id +++++++++++" + e.getMessage());
        }
    }

    private void dialogToAskDeleteOrder(int position, String orderID,JSONObject orderDertails){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setMessage("Are you sure you want to delete this order?");
        builder1.setCancelable(false);

        builder1.setPositiveButton("Yes", (dialog, id) -> {
            deleteInteractionsOfOrder(position,orderID,orderDertails);
        });

        builder1.setNegativeButton("Cancel", (dialog, id) -> {
            dialog.cancel();

        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    ProgressDialog pDialog;
    //private void deleteInteractionsOfOrder(String currentOrderId){
    private void deleteInteractionsOfOrder(int position ,String orderID, JSONObject orderDetails){
        /*pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();*/
        ModelUtil modelUtil = new ModelUtil(mContext);
        DaveModels daveModels = new DaveModels(mContext, true);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    if (response != null && response.length() > 0 && response.has("data") && response.getJSONArray("data").length() > 0) {
                        JSONArray jsonArr = response.getJSONArray("data");
                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObject = jsonArr.getJSONObject(i);
                            // Log.e(TAG, "Print Jsonobject which has qty:- :---------   " + jsonObject);
                            daveModels.deleteObject(modelUtil.getInteractionModelName(), jsonObject.getString(modelUtil.getInteractionIdAttrName()),
                                    null);
                        }

                    }
                    deleteOrder(position,orderID,orderDetails);


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"Error show Qty for productId:-------------  "+e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
              /*  if (pDialog!=null && pDialog.isShowing())
                    pDialog.dismiss();*/

            }
        };
        try {
            if(modelUtil.getInteractionModelName()!=null && !modelUtil.getInteractionModelName().isEmpty()) {
                HashMap<String, Object> queryParam = new HashMap<>();
                queryParam.put(daveAIPerspective.getInvoice_id_attr_name(),orderID);
                Log.e(TAG, "call get interaction Jsonobject :- :---------   "+queryParam);
                daveModels.getObjects(new ModelUtil(mContext).getInteractionModelName(), queryParam, daveAIListener, false, false);
            }else {
                Log.e(TAG, "interaction model is empty or null :- :---------   " +modelUtil.getInteractionModelName());
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error during get Interactions of order:-------------  "+e.getMessage());
        }


    }

    private void deleteOrder(int position, String orderID, JSONObject orderDetails){
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                //Log.e(TAG,"After Deleting Previous Order Id Response:-----------"+response);
               /* if (pDialog!=null && pDialog.isShowing())
                    pDialog.dismiss();*/
                try {
                    deleteItem(position);
                    SharedPreference sharedPreference = new SharedPreference(mContext);
                    String strCustomerId = orderDetails.getString(new ModelUtil(mContext).getCustomerIdAttrName());
                    sharedPreference.removeKey( strCustomerId);
                    if(recyclerViewClickListener!=null){
                        recyclerViewClickListener.onDeleteCard(position,orderID, orderDetails);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
               /* if (pDialog!=null && pDialog.isShowing())
                    pDialog.dismiss();*/
            }
        };
        try {
            Log.e(TAG,"Print orderDetail delete>>>>>>>> "+seedOrderList.get(position));
            DaveModels daveModels = new DaveModels(mContext,false);
            daveModels.deleteObject(daveAIPerspective.getInvoice_model_name(), orderID, daveAIListener);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}

