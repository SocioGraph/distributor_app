package com.example.admin.quickdry.activity;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.admin.daveai.activity.Visualization;
import com.example.admin.daveai.others.MyScaleGestures;
import com.example.admin.quickdry.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

public class ImageFullScreen extends AppCompatActivity {

    private static final String TAG = "ImageFullView";
    public static final String IMAGE_URL = "image_url";

    private ProgressBar progressBar;

    private String imageURL= "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_full_screen);

        ImageView imageView = findViewById(R.id.imageView);
        Button buttonCancel = findViewById(R.id.buttonCancel);
        progressBar = findViewById(R.id.progressBar);

        imageView.setOnTouchListener(new MyScaleGestures(this));

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null) {
            imageURL = bundle.getString(IMAGE_URL);
        }

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


        try {
            Log.i(TAG,"image loading from = "+imageURL);
            if(imageURL.startsWith("http")) {

                Picasso.with(ImageFullScreen.this)
                        .load(imageURL)
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.no_image)
                        //.fit()
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                                Log.i(TAG, "PICASSO SUccess!!!");

                            }

                            @Override
                            public void onError() {
                                progressBar.setVisibility(View.GONE);
                                Log.e(TAG, "PICASSO ERROR!!!");
                            }
                        });
            }else{
                Glide.with(ImageFullScreen.this)
                        .load(imageURL)
                        .asBitmap()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.no_image)
                        .listener(new RequestListener<String, Bitmap>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                Log.e(TAG,"Glide error issue "+e+" target:----------"+ target+" isFirstResource:-----"+isFirstResource);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                Log.e(TAG,"Glide Success onResourceReady target:----------"+ resource+" isFirstResource:-----"+isFirstResource);
                                return false;
                            }
                        })
                        // .skipMemoryCache(false)
                        // .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(imageView);
            }



        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During loading Image in ImageFUllView :- "+e.getLocalizedMessage());
        }

    }
}
