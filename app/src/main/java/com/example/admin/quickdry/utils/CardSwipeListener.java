package com.example.admin.quickdry.utils;


import android.support.v7.widget.RecyclerView;

import com.example.admin.daveai.model.CardSwipeDetails;

import org.json.JSONObject;

public interface CardSwipeListener {

   // void onItemMove(int fromPosition, int toPosition);
   //  void onItemDismiss(int position);
     //int setDirectionFlags(int cardViewPosition);
     CardSwipeDetails setDirectionFlags(int cardViewPosition);
     void onCardSwipe(RecyclerView.ViewHolder viewHolder, int direction);
     void onCardSwipe(String customerDetails, JSONObject productDetails, String swipeStage, String currentStage);
     void onItemRestore(int itemPosition, JSONObject cardDetails);

}
