package com.example.admin.quickdry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.fragments.DaveRecommendations;
import com.example.admin.quickdry.fragments.OrderList;
import com.example.admin.quickdry.fragments.SearchByImage;
import com.example.admin.quickdry.utils.AppConstants;
import java.util.HashMap;


public class DaveFunction extends BaseActivity implements AppConstants {

    private static final String TAG = "DaveFunction";

    public static String DAVE_ACTION = "dave_action";
    public static String RECOMMENDATION_ACTION = "Recommendations";
    public static String SIMILAR_ACTION = "Similar";
    public static String SEARCH_BY_IMAGE_ACTION = "search_by_image";
    public static String CUSTOMER_ORDER_LIST = "customer_order_list";

    private TextView show_title;
    private TabLayout allTabs;

    String dave_recom = "";
    String interaction_stage = "";
    DaveAIPerspective daveAIPerspective;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dave_function);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_dave_function);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //finish();
                onBackPressed();
            }
        });
      /*  daveAIPerspective= DaveAIPerspective.getInstance();
        show_title = findViewById(R.id.show_title);
        allTabs = findViewById(R.id.tab_layout);

        Intent intent = getIntent();
        HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("message_from_dave");
        if (hashMap != null) {
            dave_recom = hashMap.get("Dave_function");

        }
        HashMap<String, Object> hashMap2 = (HashMap<String, Object>) intent.getSerializableExtra("msg_to_dave_func");
        if (hashMap2 != null) {
            dave_recom = hashMap2.get("Dave_function").toString();
            interaction_stage = hashMap2.get("interaction_stage").toString();
        }

        if (dave_recom.isEmpty() || dave_recom == null) {
            show_title.setText("Ask Dave");
        } else if (dave_recom.equals("Similar")) {
            show_title.setText("Similar Products");

        } else {
            show_title.setText(daveAIPerspective.getRecommendations_btn_name());
        }
        bindWidgetsWithAnEvent();
        setupTabLayout();*/


        String daveAction = "";
        String strCustomerId = "";
        HashMap<String, Object> hashMap = new HashMap<>();
        Intent iIntent = getIntent();
        Log.i(TAG,"Intent details:--------------"+iIntent);
        if(iIntent != null) {
            daveAction = iIntent.getStringExtra(DAVE_ACTION);
            hashMap = (HashMap<String, Object>) iIntent.getSerializableExtra("msg_to_dave_func");
            if(iIntent.hasExtra(CUSTOMER_ID))
                strCustomerId = iIntent.getStringExtra(CUSTOMER_ID);

        }

        //todo add different fragment
        if(daveAction!=null && !daveAction.isEmpty()) {

            if (daveAction.equals(RECOMMENDATION_ACTION) || daveAction.equals(SIMILAR_ACTION))
            {
                addFragment(DaveRecommendations.newInstance(daveAction,hashMap),daveAction);
            }
            else if(daveAction.equals(SEARCH_BY_IMAGE_ACTION))
            {
                addFragment(SearchByImage.newInstance(strCustomerId),daveAction);
            }
            else if(daveAction.equals(CUSTOMER_ORDER_LIST))
            {
                if(iIntent.hasExtra(CUSTOMER_TITLE) && iIntent.getStringExtra(CUSTOMER_TITLE)!= null && !iIntent.getStringExtra(CUSTOMER_TITLE).isEmpty())
                    addFragment(OrderList.newInstance("Order List: "+iIntent.getStringExtra(CUSTOMER_TITLE),strCustomerId),daveAction);
                else
                    addFragment(OrderList.newInstance("Order List: "+strCustomerId,strCustomerId),daveAction);

            }
        }


    }

    @Override
    public void onResume(){
        super.onResume();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void addFragment(Fragment fragment ,String fragmentTag ){
        // Get FragmentManager and FragmentTransaction object.
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frameContainer, fragment, fragmentTag);
        fragmentTransaction.commit();

    }

   /* private void setupTabLayout() {

        String tabTitles[] = new String[]{"Search", "Suggestions", "Alert"};
        int tabIcons[] = new int[]{R.drawable.dave_ask, R.drawable.dave_suggestion, R.drawable.dave_alert};
        for (int i = 0; i < tabTitles.length; i++) {
            allTabs.addTab(allTabs.newTab().setText(tabTitles[i]).setIcon(tabIcons[i]));
            TabLayout.Tab tab = allTabs.getTabAt(i);
            if (tab != null)
                tab.setCustomView(getTabIndicator(tabIcons[i], tabTitles[i]));
        }
        if (!dave_recom.isEmpty() && dave_recom != null) {
            TabLayout.Tab tab = allTabs.getTabAt(1);
            if (tab != null) {
                tab.select();
            }
        }
    }

    private View getTabIndicator(int icon, String title) {

        final ViewGroup nullParent = null;
        View view = LayoutInflater.from(DaveFunction.this).inflate(R.layout.tab_icon, nullParent);
        ImageView iv = view.findViewById(R.id.imageView3);
        iv.setImageResource(icon);
        TextView tv = view.findViewById(R.id.textView);
        tv.setText(title);
        return view;
    }

    private void bindWidgetsWithAnEvent() {

        allTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTabFragment(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }


    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                replaceFragment(new DaveSearch(),"Search");
                break;
            case 1:
                replaceFragment(new DaveRecommendations(),"Recommendation");
                break;
            *//*case 2:
                replaceFragment(new DaveAlert(),"Alert");
                break;*//*
        }
    }

    public void replaceFragment(Fragment fragment, String tagName) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frameContainer, fragment,tagName);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

    }*/


}

