package com.example.admin.quickdry.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class CategoryFilter extends Fragment {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private DaveAIPerspective daveAIPerspective;
    private String strModelName;
    private Model modelInstance;
    private ModelUtil modelUtil;


    HorizontalScrollView breadcrumbWrapper,itemDetailsParent;
    LinearLayout menuItemActionLayout,breadCrumbsLayout;
    ListView filterParentView,filterChildView;
    CrystalRangeSeekbar rangeSeekbar;
    TextView priceMin, priceMax,notFound;
    ArrayAdapter<String> filterParentAdapter,filterChildAdapter;

    //category hierarchy
    ArrayList hierarchyAttributes = new ArrayList<>();
    Map<String, Object> hierarchyDetails = new HashMap<>();
    HashMap<String, String> hierarchyAttrTitleMap = new HashMap<>();
    ArrayList<Object> selectedHierarchyList = new ArrayList<>();
    HashMap<String, Object> selectedHierarchyMap = new HashMap<>();
    Stack<Map<Object, Object>> hierarchyStackFlow = new Stack<>();

    //filter list
    HashMap<String,ArrayList<String>> defaultFilterList = new HashMap<>();
    HashMap<String, String> filterAttrTitleMap = new HashMap<>();
    ArrayList<String> filterTitleList = new ArrayList<>();
    HashMap<String, ArrayList<String>> selectedFilterMap = new HashMap<>();
    int filterTitlePosition = 0;


    public CategoryFilter() {
        // Required empty public constructor
    }

    public static CategoryFilter newInstance(String modelName,ArrayList hierarchyAttributes, Map<String, Object> hierarchyDetails,
                                             ArrayList<Object> selectedHierarchyList) {
        Log.i("CategoryFilter","Catgeory Filter new Instance:-------------"+modelName);
        CategoryFilter fragment = new CategoryFilter();
        Bundle args = new Bundle();
        args.putString("model_name", modelName);
        args.putStringArrayList("_hierarchy_attrs",hierarchyAttributes);
        args.putSerializable("_hierarchy_details", (Serializable) hierarchyDetails);
        args.putSerializable("_selected_hierarchy_list",selectedHierarchyList);
        //args.putSerializable("_default_filter_list",null);
        fragment.setArguments(args);
        return fragment;
    }

    private OnCategoryFilterListener onCategoryFilterListener;
    public interface OnCategoryFilterListener {
        void onClickCategoryHierarchy(HashMap<String, Object> selectedCategoryDetails);
        void onApplyFilter(HashMap<String, ArrayList<String>> selectedFilterDetails);
        void onCancelFilter();
    }

    public void registerCategoryFilterCallback(OnCategoryFilterListener onCategoryFilterListener){
        this.onCategoryFilterListener = onCategoryFilterListener;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<CategoryFilter Fragment attach>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
       /* if (context instanceof OnCategoryFilterListener) {
            onCategoryFilterListener = (OnCategoryFilterListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnCategoryFilterListener");
        }*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        //if (getArguments() != null) {
        if (getArguments() != null) {
            strModelName = bundle.getString("model_name");
            hierarchyAttributes = bundle.getStringArrayList("_hierarchy_attrs");
            hierarchyDetails = (Map<String, Object>) bundle.getSerializable("_hierarchy_details");
            if(bundle.getSerializable("_selected_hierarchy_list")!=null)
                selectedHierarchyList = (ArrayList<Object>) bundle.getSerializable("_selected_hierarchy_list");
            //selectedHierarchyList = (ArrayList<Object>) getArguments().get(SELECTED_HIERARCHY_LIST);
            if(bundle.containsKey("_default_filter_list") && bundle.getSerializable("_default_filter_list")!=null)
                defaultFilterList = (HashMap<String, ArrayList<String>>) bundle.getSerializable("_default_filter_list");
        }

       // Log.i(TAG,"CategoryFilter Fragment onCreate strModelNAme :------------"+ strModelName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_category_filter, container, false);

        context= getActivity();
        breadcrumbWrapper = mainView.findViewById(R.id.breadcrumbWrapper);
        breadcrumbWrapper.setVisibility(View.GONE);
        breadCrumbsLayout =  mainView.findViewById(R.id.breadCrumbsLayout);
        menuItemActionLayout =  mainView.findViewById(R.id.menuItemActionLayout);

        daveAIPerspective = DaveAIPerspective.getInstance();
        modelInstance = Model.getModelInstance(context,strModelName);
        modelUtil = new ModelUtil(context);


        return mainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<CategoryFilter Fragment onViewCreated:------------"+ strModelName +" modelInstance:- "+modelInstance );
        if (hierarchyAttributes != null && !hierarchyAttributes.isEmpty()) {
            for(int i = 0;i < hierarchyAttributes.size();i++) {
                if(modelInstance!=null){
                    hierarchyAttrTitleMap.put(hierarchyAttributes.get(i).toString(),modelInstance.getAttributeTitle(hierarchyAttributes.get(i).toString()));
                }
            }
            Log.e(TAG,"Print hierarchyAttrTitleMap:------------"+hierarchyAttrTitleMap);
            showCategoryHierarchy();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onCategoryFilterListener = null;
    }

    public void showCategoryHierarchy(){
        try {
            Log.i(TAG,"get Product Category List :------------"+ hierarchyAttributes +" == "+ breadcrumbWrapper);
            if(hierarchyAttributes!=null && hierarchyAttributes.size()>0) {
                breadcrumbWrapper.setVisibility(View.VISIBLE);
                for (int index = 0; index < hierarchyAttributes.size(); index++) {

                    final ViewGroup crumbLayout = (ViewGroup) LayoutInflater.from(context)
                            .inflate(R.layout.bread_crumbs_view, breadcrumbWrapper, false);

                    TextView crumbArrow = crumbLayout.findViewById(R.id.crumbArrow);
                    crumbArrow.setVisibility(View.GONE);
                    Spinner crumbSpinner = crumbLayout.findViewById(R.id.crumbSpinner);
                    breadCrumbsLayout.addView(crumbLayout);

                    if (index > 0) {
                        crumbArrow.setVisibility(View.VISIBLE);
                    }
                    String hierarchyTitle = hierarchyAttrTitleMap.get(hierarchyAttributes.get(index));
                    selectedHierarchyList.add(index, hierarchyTitle);
                    ArrayList<String> categories = new ArrayList<String>();
                    categories.add(hierarchyTitle);
                    if (index == 0) {
                        Map<String, Object> categoryLevelData = hierarchyDetails;
                        Map<Object, Object> updatedLevel = new HashMap<>();
                        updatedLevel.put(hierarchyTitle, categoryLevelData);
                        hierarchyStackFlow.add(index, updatedLevel);
                        ArrayList<String> cat = new ArrayList(categoryLevelData.keySet());
                        Collections.sort(cat);
                        categories.addAll(cat);

                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, categories);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    crumbSpinner.setAdapter(dataAdapter);
                    crumbSpinner.setSelection(crumbSpinner.getSelectedItemPosition(), false);

                    final HashMap<String, Object> intentDetails = new HashMap<>();
                    intentDetails.put("category_level", index);
                    intentDetails.put("back_navigation_item", hierarchyTitle);
                    intentDetails.put("back_navigation_position", index);

                    crumbSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            intentDetails.put("selected_item", crumbSpinner.getSelectedItem());
                            onClickCategoryHierarchy(intentDetails);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void onClickCategoryHierarchy(HashMap<String, Object> hashMap ){
        try {
            int categoryLevel = (int) hashMap.get("category_level");
            String preValue = selectedHierarchyList.get(categoryLevel).toString();
            String selectedItem = hashMap.get("selected_item").toString();
            String categoryName = hierarchyAttributes.get(categoryLevel).toString();
            String hierarchyTitle = hierarchyAttrTitleMap.get(categoryName);

            Log.i(TAG,"************onClickCategoryHierarchy1 categoryLevel: "+ categoryLevel +" preLevel: "+preValue+ " selectedItem: "+selectedItem );
            if(categoryLevel == 0) {
                selectedHierarchyMap.clear();
            }else{

                Log.e(TAG,"************onClickCategoryHierarchy2 categoryLevel: "+ categoryLevel +" selectedHierarchyList: "+selectedHierarchyList+ " selectedHierarchyMap: "+selectedHierarchyMap );
                for (int i = categoryLevel; i < selectedHierarchyList.size(); i++) {
                    if (selectedHierarchyMap.containsKey(hierarchyAttributes.get(i).toString())) {
                        selectedHierarchyMap.remove(hierarchyAttributes.get(i).toString());
                    }

                }
            }
            //Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<Print Filter selectedHierarchyMap "+selectedHierarchyMap);
            if (!preValue.equalsIgnoreCase(selectedItem) ||
                    (preValue.equalsIgnoreCase(hierarchyTitle) && !selectedItem.equalsIgnoreCase(hierarchyTitle))){

                Map<Object, Object> getLevelValue = hierarchyStackFlow.get(categoryLevel);
                //Log.i(TAG, " Print getLevelValue>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + getLevelValue);
                for(int i= hierarchyStackFlow.size()-1; i >= categoryLevel;i--){
                    hierarchyStackFlow.pop();
                }
                //Log.i(TAG, "Print After pop categoryStackFlow:-------"+categoryStackFlow);

                Map<String, Object> categoryLevelData = (Map<String, Object>)getLevelValue.get(preValue);
                Log.i(TAG, " Print categoryLevelData>>>>>>>>>>>>>>" + categoryLevelData);

                //add current level selected item to breadcrumbDetails
                selectedHierarchyList.set(categoryLevel, selectedItem);
                Map<Object, Object> updatedLevel = new HashMap<>();
                updatedLevel.put(selectedItem, categoryLevelData);
                hierarchyStackFlow.add(categoryLevel, updatedLevel);

                Log.e(TAG," Print After updating breadcrumb and stackFlow:-"+ selectedHierarchyList +" "+ hierarchyStackFlow);
                if (categoryLevel < selectedHierarchyList.size() - 1 ) {

                    String nextHierarchyTitle = hierarchyAttrTitleMap.get(hierarchyAttributes.get(categoryLevel + 1));
                    selectedHierarchyList.set(categoryLevel + 1, nextHierarchyTitle);
                    View v = breadCrumbsLayout.getChildAt(categoryLevel + 1);
                    Spinner getSpinner = v.findViewById(R.id.crumbSpinner);
                    ArrayList<String> nextCategoriesList = new ArrayList<String>();
                    nextCategoriesList.add(nextHierarchyTitle);

                    if(!selectedItem.equalsIgnoreCase(hierarchyTitle)){
                        categoryLevelData = (Map<String, Object>) categoryLevelData.get(selectedItem);
                        Map<Object, Object> updatedNextLevel = new HashMap<>();
                        updatedNextLevel.put(nextHierarchyTitle, categoryLevelData);
                        hierarchyStackFlow.add(categoryLevel + 1, updatedNextLevel);

                        ArrayList<String> cat = new ArrayList(categoryLevelData.keySet());
                        Collections.sort(cat);
                        nextCategoriesList.addAll(cat);
                        getSpinner.performClick();
                    }else{
                        Log.e(TAG,"<<<<<<<<<<<<Selected value is not select>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    }
                    Log.i(TAG, "nextLevelDetails List Element :- " + nextCategoriesList + " nextLevelDetails:- " + categoryLevelData);
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, nextCategoriesList);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    getSpinner.setAdapter(dataAdapter);
                    getSpinner.setSelection(0, false);

                    //Log.i(TAG, " if nextLevelData   :- " + (categoryLevel + 1) + " is ==  " + (breadCrumbDetails.size() - 1));
                    if (categoryLevel + 1 < selectedHierarchyList.size() - 1) {
                        for (int k = categoryLevel + 2; k < breadCrumbsLayout.getChildCount(); k++) {
                            View view = breadCrumbsLayout.getChildAt(k);
                            Spinner getNextSpinner = view.findViewById(R.id.crumbSpinner);
                            ArrayList<String> nextSelectList = new ArrayList<String>();
                            nextSelectList.add(hierarchyAttrTitleMap.get(hierarchyAttributes.get(k)));
                            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text_layout, nextSelectList);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            getNextSpinner.setAdapter(spinnerAdapter);
                            getNextSpinner.setSelection(0, false);
                            selectedHierarchyList.set(k, hierarchyAttrTitleMap.get(hierarchyAttributes.get(k)));
                            Log.i(TAG, " for loop  if nextLevelData  SpinnerPosition :-"+k+"" +
                                    " breadCrumbDetails" + selectedHierarchyList +"hierarchyStackFlow:- "+hierarchyStackFlow);
                        }

                    }


                }
                if(!selectedItem.equalsIgnoreCase(hierarchyTitle)) {
                    selectedHierarchyMap.put(categoryName, selectedItem);
                }
                Log.e(TAG, " Final value onClickCategoryHierarchy  selectedHierarchyList :- " +selectedHierarchyList+" \n  selectedHierarchyMap:-"+ selectedHierarchyMap
                        +" \n hierarchyStackFlow:-  "+hierarchyStackFlow);

                if(onCategoryFilterListener!=null)
                    onCategoryFilterListener.onClickCategoryHierarchy(selectedHierarchyMap);


            }else{
                Log.e(TAG,"Previous value is same as current selected value>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during Click CategoryHierarchy:- " + e.getMessage());
        }


    }

    public void resetCategoryHierarchy(){
        try {
            selectedFilterMap.clear();
            hierarchyStackFlow.clear();
            selectedHierarchyList.clear();
            Log.i(TAG,"resetCategoryHierarchy:-----selectedFilterMap:-"+ selectedFilterMap +" hierarchyStackFlow:- "+ hierarchyStackFlow);
            if(hierarchyAttributes!=null && hierarchyAttributes.size()>0) {
                for (int index = 0; index < breadCrumbsLayout.getChildCount(); index++) {
                    View view = breadCrumbsLayout.getChildAt(index);
                    Spinner getSpinner = view.findViewById(R.id.crumbSpinner);
                    getSpinner.setSelection(0,false);

                    String hierarchyTitle = getSpinner.getAdapter().getItem(0).toString();
                    selectedHierarchyList.add(index, hierarchyTitle);
                    ArrayList<String> categories = new ArrayList<String>();
                    categories.add(hierarchyTitle);
                    if (index == 0) {
                        Map<String, Object> categoryLevelData = hierarchyDetails;
                        Map<Object, Object> updatedLevel = new HashMap<>();
                        updatedLevel.put(hierarchyTitle, categoryLevelData);
                        hierarchyStackFlow.add(index, updatedLevel);

                    }

                }

                Log.e(TAG,"After Reset Hierarchy :------------"+selectedFilterMap +" \n "+selectedHierarchyList +"\n "+ hierarchyStackFlow);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void actionOnFilterMenuButton(){
       /* if(filter_map==null || filter_map.isEmpty()){
            filter_map = selectedCategoryMap;
        }*/
         Log.e(TAG, "***** After pasting Data Filter Action*******"+selectedFilterMap);

       /* if(menuItemActionLayout.getChildCount()>0)
            menuItemActionLayout.removeAllViews();*/
        View  filterView = getLayoutInflater().inflate(R.layout.filter_layout, menuItemActionLayout, true);
        filterParentView = filterView.findViewById(R.id.filterParentView);
        filterChildView = filterView.findViewById(R.id.filterChildView);
        final RelativeLayout priceRangeLayout = (RelativeLayout) filterView.findViewById(R.id.priceRangeLayout);
        rangeSeekbar = (CrystalRangeSeekbar) filterView.findViewById(R.id.rangeSeekbar1);
        priceMin =  filterView.findViewById(R.id.priceMin);
        priceMax =  filterView.findViewById(R.id.priceMax);
        Button btnFilter= filterView.findViewById(R.id.btnFilter);
        Button btnCancel= filterView.findViewById(R.id.btnCancel);

        if(filterTitleList!=null && filterTitleList.size()>0) {
            filterParentAdapter = new ArrayAdapter<>(getActivity(), R.layout.filter_title, filterTitleList);
            filterParentView.setAdapter(filterParentAdapter);
        }

        if(filterAttrTitleMap!=null && !filterAttrTitleMap.isEmpty()) {
            String filterFirstItem = filterAttrTitleMap.get(filterTitleList.get(0));
            filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(filterFirstItem));
            filterChildView.setAdapter(filterChildAdapter);
            setCheckedMultipleItemAgain(filterFirstItem, filterChildAdapter);
        }
        filterParentView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                filterTitlePosition = position;
                String keyName = (String) parent.getItemAtPosition(position);
                String filterKey = filterAttrTitleMap.get(keyName);
                if (defaultFilterList != null && !defaultFilterList.isEmpty() && defaultFilterList.containsKey(filterKey)) {
                    if(modelInstance.getAttributeType(filterKey).equalsIgnoreCase("price")||
                            modelInstance.getAttributeType(filterKey).equalsIgnoreCase("discount")){
                        Log.e(TAG,"Print Filter attr type>>>>>>>>>>>>>>>>>>>>>>"+modelInstance.getAttributeType(filterKey));
                        filterChildView.setVisibility(View.GONE);
                        priceRangeLayout.setVisibility(View.VISIBLE);
                        showPriceRangeLayout(filterKey, defaultFilterList.get(filterKey));
                    }
                    else {
                        priceRangeLayout.setVisibility(View.GONE);
                        filterChildView.setVisibility(View.VISIBLE);
                        filterChildAdapter = new ArrayAdapter<>(getActivity(), R.layout.checked_textview, defaultFilterList.get(filterKey));
                        filterChildView.setAdapter(filterChildAdapter);
                        filterChildAdapter.notifyDataSetChanged();
                        setCheckedMultipleItemAgain(filterKey, filterChildAdapter);
                    }
                }
            }
        });
        filterParentView.setSelection(0);
        filterParentView.setItemChecked(0, true);

        filterChildView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SparseBooleanArray checked = filterChildView.getCheckedItemPositions();
                ArrayList<String> selectedItems = new ArrayList<>();
                for (int i = 0; i < checked.size(); i++) {
                    int pos = checked.keyAt(i);
                    if (checked.valueAt(i)) {
                        selectedItems.add(filterChildAdapter.getItem(pos));
                    }
                }
                String filterKeyName = filterAttrTitleMap.get(filterParentView.getAdapter().getItem(filterTitlePosition).toString());
                selectedFilterMap.put(filterKeyName, selectedItems);
                //Log.e(TAG, "***********Print selectedFilterMap HashMap :-  " + selectedFilterMap);

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemActionLayout.removeAllViews();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedFilterMap != null && !selectedFilterMap.isEmpty()) {
                    menuItemActionLayout.removeAllViews();
                    Log.e(TAG,"Print selected Filter list:----------"+selectedFilterMap);
                    //todo set Listner
                    if(onCategoryFilterListener!=null)
                        onCategoryFilterListener.onApplyFilter(selectedFilterMap);
                }else
                    Toast.makeText(getActivity(),"Please select filter option",Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showPriceRangeLayout(final String key, ArrayList<String> priceRange) {

        if (priceRange != null && !priceRange.isEmpty()) {
            int minValue = Integer.parseInt(priceRange.get(0));
            int maxValue = Integer.parseInt(priceRange.get(1));
            rangeSeekbar.setMinValue(minValue);
            rangeSeekbar.setMaxValue(maxValue);
        }
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                priceMin.setText(String.valueOf(minValue));
                priceMax.setText(String.valueOf(maxValue));
                String joinedValue = String.valueOf(minValue) + "," + String.valueOf(maxValue);
                ArrayList<String> priceList = new ArrayList<>();
                priceList.add(joinedValue);
                if (selectedFilterMap.containsKey(key)) {
                    selectedFilterMap.remove(key);
                }
                selectedFilterMap.put(key, priceList);
            }
        });
    }

    private void setCheckedMultipleItemAgain(String key, ArrayAdapter<String> adapter) {
        System.out.println("setCheckedMultipleItemAgain****************:- " + selectedFilterMap);
        for (String Getkey : selectedFilterMap.keySet()) {
            if (Getkey.equalsIgnoreCase(key)) {
                ArrayList<String> get_lis = selectedFilterMap.get(Getkey);
                for (int j = 0; j < get_lis.size(); j++) {
                    int checked_position = adapter.getPosition(get_lis.get(j));
                    filterChildView.setItemChecked(checked_position, true);
                }
            }
        }
    }

    private void addCheckedItemToMapDefault(String key, String value) {
        ArrayList<String> defaultSelectedItems = new ArrayList<>();
        defaultSelectedItems.add(value);
        defaultFilterList.put(key, defaultSelectedItems);
    }

}




