package com.example.admin.quickdry.storage;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.admin.daveai.daveUtil.DaveConstants;


import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;



public class SharedPreference implements DaveConstants {

        private final String TAG = getClass().getSimpleName();
        private static final int MODE = Context.MODE_PRIVATE;
        public static final String API_KEY = "api_key";
        public static final String USER_ID = "user_id";
        public static final String CUSTOMER_ID = "customer_id";
        public final String ORDER_DETAILS = "order_details";
        private SharedPreferences sharedPreferences;


     public SharedPreference(Context context){
        sharedPreferences = context.getSharedPreferences("distributor_app", 0);
    }

   /* public SharedPreference(Context context){
        sharedPreferences = context.getSharedPreferences("distributor_app_" + MultiUtil.getManifestMetadata(context,MANIFEST_ENTERPRISE_ID), 0);
    }*/

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    public void writeString(String key, String value) {
        getEditor().putString(key, value).commit();

    }

    public String readString(String key) {
        return sharedPreferences.getString(key, null);
    }

    public void writeBoolean(String key, boolean value) {
        getEditor().putBoolean(key, value).commit();
    }

    public  boolean readBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public void writeInteger(String key, int value) {
        getEditor().putInt(key, value).commit();
    }

    public int readInteger(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public void writeFloat( String key, float value) {
        getEditor().putFloat(key, value).commit();
    }

    public float readFloat( String key) {
        return sharedPreferences.getFloat(key, 0.0f);
    }

    public void writeLong(String key, long value) {
        getEditor().putLong(key, value).commit();
    }

    public long readLong(String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    public  void removeKey( String key) {
        SharedPreferences.Editor editor = getEditor();
        editor.remove(key);
        editor.commit();

    }

    public void removeAll() {
        SharedPreferences.Editor editor = getEditor();
        editor.clear();
        editor.commit();

    }


    public void saveList( String name_of_key, ArrayList<String> value) {

        Set<String> set = new HashSet<String>();
        set.addAll(value);
        getEditor().putStringSet(name_of_key, set).commit();

    }

    public ArrayList<String> getList(String name_of_key) {

        ArrayList<String> array_list = new ArrayList<String>();
        Set<String> set = sharedPreferences.getStringSet(name_of_key, null);
        if (set != null) {
            array_list.addAll(set);
        }
        return array_list;
    }

    public boolean preferenceFileExist(Context context, String fileName) {
        File f = new File(context.getApplicationInfo().dataDir + "/shared_prefs/" + fileName + ".xml");
        Log.e(TAG,"Check File exist or not in SP FileNAme:- ****** "+fileName +" isFileExist:-"+ f.exists());
        return f.exists();
    }

}
