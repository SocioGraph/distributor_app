package com.example.admin.quickdry.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.admin.quickdry.R;
import com.example.admin.quickdry.storage.SharedPreference;
import com.example.admin.quickdry.utils.AppConstants;



public class IntranetSettings extends Dialog implements AppConstants {


    private final String TAG = getClass().getSimpleName();
    private static final int SERVER_PORT = 8000;
    private Context mContext;




    public IntranetSettings(Context context){
        super(context);
        this.mContext = context;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_intranet);
        setCancelable(false);

        Button btnCancel = findViewById(R.id.btnCancel);
        EditText serverPortView = findViewById(R.id.serverPort);
        EditText ipAddressView = findViewById(R.id.ip_address);
        Button btnSubmit = findViewById(R.id.btnSubmit);


        SharedPreference sharedPreference = new SharedPreference(mContext);
        //if ip address not null set in edit text
         String strIpAddress = sharedPreference.readString(INTRANET_IP_ADDRESS);
         if(strIpAddress!=null)
             ipAddressView.setText(strIpAddress);

        int serverPort = sharedPreference.readInteger(TCP_SERVER_PORT);
        if(serverPort!= 0)
            serverPortView.setText(Integer.toString(serverPort));
        else
            serverPortView.setText(SERVER_PORT);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                dismiss();

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(TextUtils.isEmpty(ipAddressView.getText().toString())){
                    Toast.makeText(mContext,"Please Enter Ip Address.",Toast.LENGTH_SHORT).show();

                }else if(TextUtils.isEmpty(serverPortView.getText().toString())){
                    Toast.makeText(mContext,"Please Enter Server Port.",Toast.LENGTH_SHORT).show();

                }else{
                    Log.i(TAG,"Print ip address from edit text**********  " +ipAddressView.getText() +" port:- "+ serverPortView.getText());
                    sharedPreference.writeString(INTRANET_IP_ADDRESS,ipAddressView.getText().toString());
                    sharedPreference.writeInteger(TCP_SERVER_PORT,Integer.parseInt(serverPortView.getText().toString()));
                    dismiss();

                }
            }
        });
    }

}
