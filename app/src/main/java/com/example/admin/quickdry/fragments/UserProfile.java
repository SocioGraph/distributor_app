package com.example.admin.quickdry.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.daveai.dynamicView.ShowDynamicView;
import com.example.admin.daveai.fragments.DynamicForm;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.HomePage;
import  com.example.admin.daveai.others.DaveAIPerspective;

import org.json.JSONObject;


public class UserProfile extends Fragment  {

    private final String TAG = getClass().getSimpleName();

    private LinearLayout profileView;
    private RecyclerView recycleDetailsView;
    private ImageButton btnEditProfile;
    private TextView noDetailsFound;
    private ImageView errorImg;
    private TextView errorMsg;


    String userProfileDetails ="";
    private DaveAIPerspective daveAIPerspective;


    public UserProfile() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View mainView = inflater.inflate(R.layout.fragment_user_profile, container, false);

        profileView = mainView.findViewById(R.id.profileView);
        recycleDetailsView = mainView.findViewById(R.id.recycleDetailsView);
        btnEditProfile = mainView.findViewById(R.id.btnEditProfile);
        noDetailsFound = mainView.findViewById(R.id.noDetailsFound);
        errorMsg = mainView.findViewById(R.id.error_message);
        errorImg = mainView.findViewById(R.id.errorIconView);


        daveAIPerspective=DaveAIPerspective.getInstance();

        return mainView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(daveAIPerspective.getUser_profile_details_list()!=null) {
            getProfileDetails();
        }
        else
            Toast.makeText(getActivity()," Please Set User details want to show in Perspective Setting",Toast.LENGTH_SHORT).show();


        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(daveAIPerspective.getUser_profile_details_list()!=null && daveAIPerspective.getUser_login_model_name()!=null) {
                   /* Fragment fragment= ((HomePage) getActivity()).addCurrentFragment("Edit Profile",
                          "Update", "UPDATE", daveAIPerspective.getUser_login_model_name(),"login",
                          daveAIPerspective.getUser_profile_details_list(),daveAIPerspective.getUser_login_id_attr_name(), userProfileDetails);*/

                    Fragment fragment=  new DynamicForm().updateSingletonView("Edit Profile","Update","UPDATE",
                            daveAIPerspective.getUser_login_model_name(),"login",daveAIPerspective.getUser_profile_details_list()
                            ,userProfileDetails);
                    ((HomePage) getActivity()).replaceFragment(fragment);
                }
            }
        });
    }

    private void getProfileDetails(){

        if (daveAIPerspective.getUser_login_model_name()!=null) {
            DaveModels daveModels = new DaveModels(getActivity(),true);
            JSONObject profileDetails = daveModels.getSingleton(daveAIPerspective.getUser_login_model_name());
            Log.e(TAG,"<<<<<<<<<<<<<<<<getProfile Details user ModelName:----"+daveAIPerspective.getUser_login_model_name()+" \n List"+ daveAIPerspective.getUser_profile_details_list()+"\n"+profileDetails);

            if (profileDetails!=null && profileDetails.length()>0) {
                try {
//                    Log.e(TAG,"has == "+profileDetails.has("__error__")+"  isnt empty = "+!TextUtils.isEmpty(profileDetails.getString("__error__")) + "   not null "+(profileDetails.getString("__error__")!="null"));
                    if(profileDetails.has("__error__") && !TextUtils.isEmpty(profileDetails.getString("__error__")) && (profileDetails.getString("__error__")!="null")){

                            errorImg.setVisibility(View.VISIBLE);
                            errorMsg.setVisibility(View.VISIBLE);
                            errorMsg.setText(profileDetails.getString("__error__"));

                    }else{
                        errorImg.setVisibility(View.GONE);
                        errorMsg.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                userProfileDetails = profileDetails.toString();
                ShowDynamicView dynamicForm = new ShowDynamicView(getActivity());
                Log.e(TAG,"daveAIPerspective == "+daveAIPerspective);
                Log.e(TAG," daveAIPerspective.getUser_login_model_name() == "+ daveAIPerspective.getUser_login_model_name());
                Log.e(TAG," daveAIPerspective.getUser_profile_details_list() == "+ daveAIPerspective.getUser_profile_details_list());
                Log.e(TAG," daveAIPerspective.getUser_profile_details_list() == "+ daveAIPerspective.getUser_profile_details_list());
                Log.e(TAG," profileDetails.toString() == "+ profileDetails.toString());
                dynamicForm.showDetailsInView(recycleDetailsView, daveAIPerspective.getUser_login_model_name(),
                        daveAIPerspective.getUser_profile_details_list(), profileDetails.toString());

                if(recycleDetailsView.getAdapter().getItemCount() == 0){
                    profileView.setVisibility(View.GONE);
                    noDetailsFound.setVisibility(View.VISIBLE);
                }


            }else {
                Toast.makeText(getActivity(),"User details is Empty or null Or check Model name in settings.",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(getActivity(),"Set user model Name in Perspective. ",Toast.LENGTH_SHORT).show();
        }
    }

}

