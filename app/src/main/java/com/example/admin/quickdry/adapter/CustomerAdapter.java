package com.example.admin.quickdry.adapter;


import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveHelper;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.fragments.CustomerList;
import com.example.admin.quickdry.utils.CRUDUtil;
import com.squareup.okhttp.HttpUrl;
import org.json.JSONObject;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.admin.daveai.database.DatabaseConstants.ERROR_MESSAGE_TEXT;


public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.CustomViewHolder> implements RecyclerView.OnClickListener ,
        DaveConstants {

        private static final String TAG = "CustomerAdapter";
        private Context mContext;
        private List<JSONObject> seedCustomerList;
        private List<JSONObject> defaultCustomerList;
        private DaveAIPerspective daveAIPerspective;
        private CustomerList.CustomerSelectedListener customerSelectedCallback;
        private String customerModelName = "";
        private String customerIdAttr = "";


    public CustomerAdapter(CustomerList.CustomerSelectedListener customerSelectedCallback, Context context, List<JSONObject> customerList) {
        this.customerSelectedCallback= customerSelectedCallback;
        this.seedCustomerList = customerList;
        this.mContext = context;
        daveAIPerspective= DaveAIPerspective.getInstance();
        defaultCustomerList = new ArrayList<>();

        customerModelName = new ModelUtil(mContext).getCustomerModelName();
        customerIdAttr = new ModelUtil(mContext).getCustomerIdAttrName();

    }

    @NonNull
    @Override
    public CustomerAdapter.CustomViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_customer_card_view,viewGroup, false);
        //Log.e(TAG,"********onCreateViewHolder:------    ");
        CustomViewHolder holder = new CustomViewHolder(view);
        holder.itemView.setOnClickListener(CustomerAdapter.this);
        holder.itemView.setTag(holder);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull CustomViewHolder customViewHolder, int _position) {

        JSONObject seedItem = seedCustomerList.get(_position);
        try{
            Log.e(TAG,"********onBindViewHolder:------    "+ _position +" "+seedItem);
            if(daveAIPerspective.getCustomer_card_image()!=null && !daveAIPerspective.getCustomer_card_image().isEmpty() &&
                    seedItem.has(daveAIPerspective.getCustomer_card_image()) && !seedItem.isNull(daveAIPerspective.getCustomer_card_image())){

                customViewHolder.customerCardImage.setVisibility(View.VISIBLE);
                URL imageUrl = HttpUrl.parse(seedItem.getString(daveAIPerspective.getCustomer_card_image())).url();
                Glide.with(mContext)
                        .load(imageUrl.toString())
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.no_image)
                        .into(customViewHolder.customerCardImage);
            }
            else {
                try {
                    if(customViewHolder.customerCardImage!=null) {
                        Glide.clear(customViewHolder.customerCardImage);
                        customViewHolder.customerCardImage.setImageResource(R.drawable.no_image);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            if(daveAIPerspective.getCustomer_card_header()!=null && seedItem.has(daveAIPerspective.getCustomer_card_header())
                    && !seedItem.isNull(daveAIPerspective.getCustomer_card_header())) {
                customViewHolder.customerHeader.setText(seedItem.getString(daveAIPerspective.getCustomer_card_header()));
            }else{
                customViewHolder.customerHeader.setText("");
                Log.e(TAG,"onBindViewHolder Header and SubHeader :----------"+daveAIPerspective.getCustomer_card_header() +"    "+ daveAIPerspective.getCustomer_card_sub_header());
            }

            if(daveAIPerspective.getCustomer_card_sub_header()!=null && seedItem.has(daveAIPerspective.getCustomer_card_sub_header())
                    && !seedItem.isNull(daveAIPerspective.getCustomer_card_sub_header())) {
                customViewHolder.customerSubHeader.setText(seedItem.getString(daveAIPerspective.getCustomer_card_sub_header()));
            }else{
                customViewHolder.customerSubHeader.setText("");
                Log.e(TAG,"onBindViewHolder etCustomer_card_sub_header():----------"+daveAIPerspective.getCustomer_card_sub_header());
            }

            if(daveAIPerspective.getCustomer_card_right_attr()!=null && seedItem.has(daveAIPerspective.getCustomer_card_right_attr()) &&
                    !seedItem.isNull(daveAIPerspective.getCustomer_card_right_attr())) {
                customViewHolder.customerRightAttr.setText(seedItem.getString(daveAIPerspective.getCustomer_card_right_attr()));
            }else{
                customViewHolder.customerRightAttr.setText("");
            }

            if(seedItem.has(ERROR_MSG_ATTRIBUTE) && !seedItem.isNull(ERROR_MSG_ATTRIBUTE) && !seedItem.getString(ERROR_MSG_ATTRIBUTE).isEmpty()){
                customViewHolder.errorView.setVisibility(View.VISIBLE);
                customViewHolder.deleteView.setVisibility(View.VISIBLE);
                if(seedItem.getString(ERROR_MSG_ATTRIBUTE).equals(ERROR_MESSAGE_TEXT)){
                    customViewHolder.errorView.setImageResource(R.drawable.ic_report_error_green);
                }
            }else {
                customViewHolder.errorView.setVisibility(View.GONE);
                customViewHolder.deleteView.setVisibility(View.GONE);
            }

            customViewHolder.deleteView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                       dialogToAskDeleteCustomer(customViewHolder.getAdapterPosition(),
                               seedCustomerList.get(customViewHolder.getAdapterPosition()).getString(customerIdAttr));

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });


        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during Customer Card View +++++++++++" + e.getMessage());
        }
    }


    @Override
    public int getItemCount() {
        return (null != seedCustomerList ? seedCustomerList.size() : 0);
    }



    @Override
    public void onClick(View view) {

        CustomViewHolder holder = (CustomViewHolder) view.getTag();
        int card_pos = holder.getAdapterPosition();
        String customer_Id = null;
        String name = "";
        HashMap<String, String> intent_details = new HashMap<>();
        try{
            JSONObject cardDetails = seedCustomerList.get(card_pos);
            DaveHelper.getInstance().setCustomerDetails(seedCustomerList.get(card_pos));
            ModelUtil modelUtil = new ModelUtil(mContext);
            String customerIdAttr = modelUtil.getCustomerIdAttrName();

            if(customerIdAttr!=null && !customerIdAttr.isEmpty())
                customer_Id = seedCustomerList.get(card_pos).getString(customerIdAttr);

           if(cardDetails.has(daveAIPerspective.getCustomer_profile_title()) && !cardDetails.isNull(daveAIPerspective.getCustomer_profile_title()))
               name = seedCustomerList.get(card_pos).getString(daveAIPerspective.getCustomer_profile_title());

            intent_details.put("customer_id", customer_Id);
            intent_details.put("customer_name", name);
            intent_details.put("customerDetails", seedCustomerList.get(card_pos).toString());

            if(customerSelectedCallback!=null){
                customerSelectedCallback.onCustomerSelected(customer_Id,name,seedCustomerList.get(card_pos));
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error during Click on Card +++++++++++" + e.getMessage());
        }

    }

    public void addNewSeedList(List<JSONObject> newItemList,boolean isDefaultData) {
        if(isDefaultData) {
            defaultCustomerList.clear();
            defaultCustomerList.addAll(newItemList);
        }
        if(seedCustomerList.size()>0) {
            seedCustomerList.clear();
        }
        this.seedCustomerList.addAll(newItemList);
        notifyDataSetChanged();
        Log.e(TAG," After addNewItem size of adapter>>>>>>>>>>>>:- " + seedCustomerList.size()+" == "+ defaultCustomerList.size());
    }


    public void updateSeedList(List<JSONObject> newItemList,boolean isDefaultData) {

        int previousPos = seedCustomerList.size();
        Log.e(TAG," before updateSeedList seedCustomerList of adapter****************:- " + previousPos);
        seedCustomerList.addAll(newItemList);

        //notifyItemInserted(seedCustomerList.size());
        // notify adapter
        notifyDataSetChanged();
        //notifyItemChanged(seedCustomerList.size(), seedCustomerList);
        if(isDefaultData)
            defaultCustomerList.addAll(newItemList);

/*        // record this value before making any changes to the existing list
        int curSize = adapter.getItemCount();

// replace this line with wherever you get new records
        ArrayList<Contact> newItems = Contact.createContactsList(20);

// update the existing list
        contacts.addAll(newItems);
// curSize should represent the first element that got added
// newItems.size() represents the itemCount
        adapter.notifyItemRangeInserted(curSize, newItems.size())*/;

        Log.e(TAG," After updateSeedList seedCustomerList of adapter****************:- " + seedCustomerList.size());

    }
    public void resetPreviousCustomerList(){
        if(seedCustomerList.size()>0)
            seedCustomerList.clear();
        seedCustomerList = new ArrayList<>(defaultCustomerList);
        notifyItemInserted(seedCustomerList.size());
        notifyItemRangeChanged(0,seedCustomerList.size()-1);
       // notifyDataSetChanged();
        //Log.e(TAG," resetPreviousCustomerList() of adapter****************:- " +seedCustomerList.size()+"  "+ defaultCustomerList.size());

    }

    public void deleteItem(int index) {
        seedCustomerList.remove(index);
        notifyItemRemoved(index);
    }

    public void clearCustomerList(){
        final int size = seedCustomerList.size();
        if(size>0)
            seedCustomerList.clear();
        notifyItemRangeRemoved(0, size);

    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView customerHeader,customerSubHeader,customerRightAttr;
        ImageView customerCardImage,errorView, deleteView;

        CustomViewHolder(View view) {
            super(view);

            errorView = view.findViewById(R.id.errorView);
            deleteView = view.findViewById(R.id.delete);
            customerHeader = view.findViewById(R.id.customerHeader);
            customerSubHeader = view.findViewById(R.id.customerSubHeader);
            customerRightAttr = view.findViewById(R.id.customerRightAttr);
            customerCardImage = view.findViewById(R.id.imageView);


        }
    }

    private void dialogToAskDeleteCustomer(int position, String customerID){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setMessage("Are you sure you want to delete ?");
        builder1.setCancelable(false);

        builder1.setPositiveButton("Yes", (dialog, id) -> {
            deleteItem(position);

            DaveModels daveModels = new DaveModels(mContext,true);
            daveModels.deleteObjectFromLocalDB(customerModelName,customerID,null);
            //todo delete orderDetails and interaction of that customer
            CRUDUtil crudUtil = new CRUDUtil(mContext);
            crudUtil.deleteOrdersOfCustomer(customerID);

            HashMap<String ,Object> queryParams = new HashMap<>();
            queryParams.put(customerIdAttr,customerID);
            crudUtil.deleteInteractions(queryParams);
        });

        builder1.setNegativeButton("Cancel", (dialog, id) -> {
            dialog.cancel();

        });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }





}


//adapter notes

/*
1) Insert single item
   data.add(insertIndex, item);
   adapter.notifyItemInserted(insertIndex);

2) Insert multiple items
    data.addAll(insertIndex, items);
    adapter.notifyItemRangeInserted(insertIndex, items.size());

3)  Remove single item
    data.remove(removeIndex);
    adapter.notifyItemRemoved(removeIndex);

4)  Remove multiple items

    int startIndex = 2; // inclusive
    int endIndex = 4;   // exclusive
    int count = endIndex - startIndex; // 2 items will be removed
    data.subList(startIndex, endIndex).clear();
    adapter.notifyItemRangeRemoved(startIndex, count);

5) Remove all items
   data.clear();
   adapter.notifyDataSetChanged();

6) Replace old list with new list
    // clear old list
    data.clear();

    // add new list
    ArrayList<String> newList = new ArrayList<>();
    newList.add("Lion");
    newList.add("Wolf");
    newList.add("Bear");
    data.addAll(newList);

    // notify adapter
    adapter.notifyDataSetChanged();

7) Update single item
    String newValue = "I like sheep.";
    int updateIndex = 3;
    data.set(updateIndex, newValue);
    adapter.notifyItemChanged(updateIndex);

8)Move single item
    int fromPosition = 3;
    int toPosition = 1;

    // update data array
    String item = data.get(fromPosition);
    data.remove(fromPosition);
    data.add(toPosition, item);

    // notify adapter
    adapter.notifyItemMoved(fromPosition, toPosition);
 */


/*
    private void insertSingleItem() {
        String item = "Pig";
        int insertIndex = 2;
        data.add(insertIndex, item);
        adapter.notifyItemInserted(insertIndex);
    }

    private void insertMultipleItems() {
        ArrayList<String> items = new ArrayList<>();
        items.add("Pig");
        items.add("Chicken");
        items.add("Dog");
        int insertIndex = 2;
        data.addAll(insertIndex, items);
        adapter.notifyItemRangeInserted(insertIndex, items.size());
    }

    private void removeSingleItem() {
        int removeIndex = 2;
        data.remove(removeIndex);
        adapter.notifyItemRemoved(removeIndex);
    }

    private void removeMultipleItems() {
        int startIndex = 2; // inclusive
        int endIndex = 4;   // exclusive
        int count = endIndex - startIndex; // 2 items will be removed
        data.subList(startIndex, endIndex).clear();
        adapter.notifyItemRangeRemoved(startIndex, count);
    }

    private void removeAllItems() {
        data.clear();
        adapter.notifyDataSetChanged();
    }

    private void replaceOldListWithNewList() {
        // clear old list
        data.clear();

        // add new list
        ArrayList<String> newList = new ArrayList<>();
        newList.add("Lion");
        newList.add("Wolf");
        newList.add("Bear");
        data.addAll(newList);

        // notify adapter
        adapter.notifyDataSetChanged();
    }

    private void updateSingleItem() {
        String newValue = "I like sheep.";
        int updateIndex = 3;
        data.set(updateIndex, newValue);
        adapter.notifyItemChanged(updateIndex);
    }

    private void moveSingleItem() {
        int fromPosition = 3;
        int toPosition = 1;

        // update data array
        String item = data.get(fromPosition);
        data.remove(fromPosition);
        data.add(toPosition, item);

        // notify adapter
        adapter.notifyItemMoved(fromPosition, toPosition);
    }*/
