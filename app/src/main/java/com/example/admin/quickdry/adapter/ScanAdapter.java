package com.example.admin.quickdry.adapter;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dynamicView.ShowDynamicView;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.quickdry.R;
import com.example.admin.quickdry.activity.AddScanProduct;

import org.json.JSONObject;
import java.util.List;



public class ScanAdapter extends RecyclerView.Adapter<ScanAdapter.SimpleViewHolder> implements RecyclerView.OnClickListener {


    private  final String TAG = getClass().getSimpleName();
    private List<JSONObject> seedScannedDetails;
    private Context context;
    private DaveAIPerspective daveAIPerspective;

    private ScanAdapterListener scanAdapterListener;
    public interface ScanAdapterListener {
        void onClickEditProduct(int holderPosition, Bundle bundle);
        //void onCardToggle(View view,int holderPosition);

    }


    public ScanAdapter(Context context, List<JSONObject> productListPojo,ScanAdapterListener scanAdapterListener) {
        this.seedScannedDetails = productListPojo;
        this.context = context;
        this.scanAdapterListener=scanAdapterListener;
        daveAIPerspective = DaveAIPerspective.getInstance();

        Log.e(TAG, "ScanAdapter:---" + productListPojo);

    }

    @NonNull
    @Override
    public SimpleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_scan_detail,parent, false);

        SimpleViewHolder holder = new SimpleViewHolder(view);
        holder.itemView.setOnClickListener(ScanAdapter.this);
        holder.itemView.setTag(holder);
        // return new CustomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, final int position) {

        JSONObject cardDetails = seedScannedDetails.get(position);
        Log.e(TAG, "onBindViewHolder Print productAttrList :---" + cardDetails);
        ModelUtil modelUtil = new ModelUtil(context);
        String productIdAttrName = modelUtil.getProductIdAttrName();
        try {
           String productId = cardDetails.getString(productIdAttrName);

            if(daveAIPerspective.isEnable_manage_product()) {
                holder.editProduct.setVisibility(View.VISIBLE);
            }
            if (daveAIPerspective.getProduct_card_image_view() != null && cardDetails.has(daveAIPerspective.getProduct_card_image_view())) {
                Glide.with(context)
                        .load(cardDetails.getString(daveAIPerspective.getProduct_card_image_view()))
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.no_image)
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(holder.imageView);
            }
            if (cardDetails.has(daveAIPerspective.getProduct_expand_view_title()))
                holder.productTitle.setText(cardDetails.getString(daveAIPerspective.getProduct_expand_view_title()));

            Model productModel = Model.getModelInstance(context, new ModelUtil(context).getProductModelName());
            if(daveAIPerspective.getProduct_expand_view() != null) {
                ShowDynamicView dynamicForm = new ShowDynamicView(context);
                dynamicForm.expandMultiView(context, holder.recycleView, daveAIPerspective.getProduct_expand_view(), productModel, cardDetails);
            }

            holder.editProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(scanAdapterListener!=null){
                        Bundle b= new Bundle();
                        b.putString("action_name", AddScanProduct.ACTION_EDIT_PRODUCT);
                        b.putString("_object_details",cardDetails.toString());
                        b.putString("_product_id",productId);
                        scanAdapterListener.onClickEditProduct(position,b);
                    }


                }
            });



        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "<<<< Show Product Details Error:>>>>>>>>>>>>>>" + e.getLocalizedMessage());
        }



    }


    @Override
    public int getItemCount() {
        return seedScannedDetails != null ? seedScannedDetails.size() : 1;
    }

    @Override
    public void onClick(View v) {

    }

    public void updateItem(JSONObject newItemDetails, int position) {
        seedScannedDetails.set(position,newItemDetails);
        notifyItemChanged(position);
    }


     class SimpleViewHolder extends RecyclerView.ViewHolder  {

        RecyclerView recycleView;
        TextView productTitle;
        ImageView imageView;
        Button editProduct;

        SimpleViewHolder(View view) {
            super(view);

            imageView = view.findViewById(R.id.imageView);
            productTitle = view.findViewById(R.id.productTitle);
            editProduct = view.findViewById(R.id.editProduct);
            editProduct.setVisibility(View.GONE);
            recycleView = view.findViewById(R.id.recycleView);
           // recycleView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL, false));
           // recycleView.setHasFixedSize(true);
        }


    }
}




