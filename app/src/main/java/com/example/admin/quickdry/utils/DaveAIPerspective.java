package com.example.admin.quickdry.utils;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.others.PerspectiveParent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


public class DaveAIPerspective extends PerspectiveParent implements DaveConstants {

    private final String TAG = getClass().getSimpleName();

    private String user_login_model_name;
    private String user_login_id_attr_name;
    private ArrayList user_profile_details_list;

    private String recommendations_btn_name ="Get Recommendations";
    private int no_of_recommendations = 10;
    private HashMap<String,Object> default_recommendation_params;
    private int count_after_next_recommendations = 3;
    private String  scan_btn_name;
    private String scan_code_attribute_name;


    private String customer_card_image;
    private String customer_card_header;
    private String customer_card_sub_header;
    private String customer_card_right_attr;
    private String customer_profile_title;
    private ArrayList customer_details_view;
    private ArrayList customer_sign_up_list;
    private boolean customer_attr_hierarchy_first = true;
    private String customer_variable_attr_title = "Update your settings";
    private ArrayList customer_variable_attr;
    private ArrayList customer_filter_attr;
    private ArrayList customer_category_hierarchy;
    private String customer_email_attr;
    private String customer_mobile_attr;

    private String product_card_image_view;
    private String product_card_header;
    private String product_card_sub_header;
    private String product_card_right_attr;
    private String product_expand_view_title;
    private ArrayList product_expand_view;
    private ArrayList product_add_edit_List;
    private String product_category_hierarchy_title = "What are you looking for?";
    private ArrayList product_category_hierarchy;
    private ArrayList product_similar_attributes;
    private ArrayList product_variants_attributes;
    private String product_price_attr;
    private ArrayList product_filter_attr;

    private ArrayList summation_attributes;
    private String interaction_qty;
    private String predicated_qty_attr;
    private ArrayList interaction_stage_attr_list;

    private String invoice_model_name;
    private String invoice_id_attr_name;
    private ArrayList invoice_checkout_attr_list;
    private String invoice_close_attr_name;
    private String invoice_card_header;
    private String invoice_card_sub_header;
    private String invoice_card_right_attr;
    private String invoice_date_attr;
    private String invoice_stage_attr;
    private boolean invoice_show_zero_qty = true;
    private String invoice_total_count_attr;
    private String invoice_total_value_attr;

    private boolean enable_manage_product = false;
    private String form_view_button_name;
    private String form_view_order_stage_name;

    private String menu_title_for_sign_up = "Sign Up";
    private String menu_title_for_customer_list="Customers List";
    private String menu_title_for_product_list= "Products List";
    private String menu_title_for_order_list= "Order List";
    private String menu_title_for_primary_orders ="__NULL__";
    private String menu_title_for_incentives ="__NULL__";
    private String menu_title_for_reports ="__NULL__";


    private boolean enable_set_default_quantity = false;
    private boolean manage_product_from_customer_profile = false;
    private String add_product_title = "Add Product";
    private long cold_update_time_limit = 6 * 3600 * 1000;
    private long hot_update_time_limit = 24 * 3600 * 1000;

    private String search_by_image_attr;
    private String search_by_image_button_name;


    // static variable single_instance of type Singleton
    private static DaveAIPerspective singleInstance = null;

    // private constructor restricted to this class itself
    private DaveAIPerspective() {}

    // static method to create instance of Singleton class
    public static DaveAIPerspective getInstance() {
        if (singleInstance == null)
            singleInstance = new DaveAIPerspective();
        return singleInstance;
    }

    public void savePerspectivePreferenceValue(Context context){
            setPreferenceJSONResponse(context,"perspective_settings");
            if (prefResponse != null && prefResponse.length() > 0) {
                try {
                    savePerspectivePreferenceValue(context, prefResponse.toString());
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }

    }


    public void savePerspectivePreferenceValue(Context context ,String response) throws Exception{
        Log.d(TAG,"************Set PerspectivePreferenceValue>>>>>>>>>>>>>>>>>");

//        setPreferenceJSONResponse(context,"perspective_settings");
            if(response!= null) {

                prefResponse = new JSONObject((response));

                this.user_login_model_name = getFromPreference(USER_LOGIN_MODEL_NAME,String.class,this.user_login_model_name);

                this.user_login_id_attr_name = getFromPreference(USER_LOGIN_ID_ATTRIBUTE,String.class,this.user_login_id_attr_name);

                this.user_profile_details_list = convertJsonArrayToList(getFromPreference(USER_PROFILE_ATTRIBUTE_LIST,JSONArray.class,new JSONArray()));

                this.recommendations_btn_name = getFromPreference(RECOMMENDATION_BUTTON,String.class,this.recommendations_btn_name);

                this.menu_title_for_primary_orders = getFromPreference(DaveConstants.menu_title_for_primary_orders,String.class,this.menu_title_for_primary_orders);

                this.menu_title_for_incentives = getFromPreference(DaveConstants.menu_title_for_incentives,String.class,this.menu_title_for_incentives);

                this.menu_title_for_reports = getFromPreference(DaveConstants.menu_title_for_reports,String.class,this.menu_title_for_reports);

                this.no_of_recommendations = getFromPreference(RECOMMENDATION_COUNT,Integer.class,this.no_of_recommendations);

                this.count_after_next_recommendations = getFromPreference(SWIPE_COUNT_TO_NEXT_RECOMMENDATION,Integer.class,this.count_after_next_recommendations);

                this.scan_btn_name = getFromPreference(SCAN_BUTTON_NAME,String.class,this.scan_btn_name);

                this.customer_card_image = getFromPreference(CUSTOMER_CARD_IMAGE,String.class,this.customer_card_image);

                this.customer_card_header = getFromPreference(CUSTOMER_CARD_HEADER,String.class,this.customer_card_header);

                this.customer_card_sub_header = getFromPreference(CUSTOMER_CARD_SUB_HEADER,String.class,this.customer_card_sub_header);

                this.customer_card_right_attr = getFromPreference("customer_card_right_attr",String.class,this.customer_card_right_attr);

                this.customer_profile_title = getFromPreference("customer_profile_title",String.class,this.customer_profile_title);

                this.customer_details_view = convertJsonArrayToList(getFromPreference("customer_details_view",JSONArray.class,new JSONArray()));

                this.customer_sign_up_list = convertJsonArrayToList(getFromPreference("customer_sign_up_list",JSONArray.class,new JSONArray()));

                this.customer_attr_hierarchy_first = getFromPreference("customer_attr_hierarchy_first",Boolean.class,this.customer_attr_hierarchy_first);

                this.customer_variable_attr_title = getFromPreference("customer_variable_attr_title",String.class,this.customer_variable_attr_title);

                this.customer_variable_attr = convertJsonArrayToList(getFromPreference("customer_variable_attr",JSONArray.class,new JSONArray()));

                this.customer_filter_attr = convertJsonArrayToList(getFromPreference(CUSTOMER_FILTER_LIST,JSONArray.class,new JSONArray()));

                this.customer_category_hierarchy = convertJsonArrayToList(getFromPreference(CUSTOMER_HIERARCHY_LIST,JSONArray.class,new JSONArray()));

                this.customer_email_attr = getFromPreference(CUSTOMER_EMAIL_ATTRIBUTE,String.class,this.customer_email_attr);

                this.customer_mobile_attr = getFromPreference(CUSTOMER_MOBILE_ATTRIBUTE,String.class,this.customer_mobile_attr);

                this.product_card_image_view = getFromPreference("product_card_image_view",String.class,this.product_card_image_view);

                this.product_card_header = getFromPreference("product_card_header",String.class,this.product_card_header);

                this.product_card_sub_header = getFromPreference("product_card_sub_header",String.class,this.product_card_sub_header);

                this.product_card_right_attr = getFromPreference("product_card_right_attr",String.class,this.product_card_right_attr);

                this.product_expand_view_title = getFromPreference("product_expand_view_title",String.class,this.product_expand_view_title);

                this.product_expand_view = convertJsonArrayToList(getFromPreference("product_expand_view",JSONArray.class,new JSONArray()));

                this.product_add_edit_List = convertJsonArrayToList(getFromPreference("product_add_edit_List",JSONArray.class,new JSONArray()));

                this.product_category_hierarchy_title = getFromPreference("product_category_hierarchy_title",String.class,this.product_category_hierarchy_title);

                this.product_category_hierarchy = convertJsonArrayToList(getFromPreference("product_category_hierarchy",JSONArray.class,new JSONArray()));

                this.product_similar_attributes = convertJsonArrayToList(getFromPreference("product_similar_attributes",JSONArray.class,new JSONArray()));

                this.product_price_attr = getFromPreference("product_price_attr",String.class,this.product_price_attr);

                this.product_filter_attr = convertJsonArrayToList(getFromPreference(PRODUCT_FILTER_LIST,JSONArray.class,new JSONArray()));

                this.interaction_qty = getFromPreference("interaction_qty",String.class,this.interaction_qty);

                this.interaction_stage_attr_list = convertJsonArrayToList(getFromPreference("interaction_stage_attr_list",JSONArray.class,new JSONArray()));

                this.predicated_qty_attr = getFromPreference("predicated_qty_attr",String.class,this.predicated_qty_attr);

                this.summation_attributes = convertJsonArrayToList(getFromPreference("summation_attributes",JSONArray.class,new JSONArray()));

                this.invoice_model_name = getFromPreference("invoice_model_name",String.class,this.invoice_model_name);

                this.invoice_id_attr_name = getFromPreference("invoice_id_attr_name",String.class,this.invoice_id_attr_name);

                this.invoice_checkout_attr_list = convertJsonArrayToList(getFromPreference("invoice_checkout_attr_list",JSONArray.class,new JSONArray()));

                this.invoice_close_attr_name = getFromPreference("invoice_close_attr_name",String.class,this.invoice_close_attr_name);

                this.invoice_card_header = getFromPreference("invoice_card_header",String.class,this.invoice_card_header);

                this.invoice_card_sub_header = getFromPreference("invoice_card_sub_header",String.class,this.invoice_card_sub_header);

                this.invoice_card_right_attr = getFromPreference("invoice_card_right_attr",String.class,this.invoice_card_right_attr);

                this.invoice_date_attr = getFromPreference("invoice_date_attr",String.class,this.invoice_date_attr);

                this.invoice_stage_attr = getFromPreference("invoice_stage_attr",String.class,this.invoice_stage_attr);

                this.invoice_show_zero_qty = getFromPreference("invoice_show_zero_qty",Boolean.class,this.invoice_show_zero_qty);

                this.invoice_total_count_attr = getFromPreference("invoice_total_count_attr",String.class,this.invoice_total_count_attr);

                this.invoice_total_value_attr = getFromPreference("invoice_total_value_attr",String.class,this.invoice_total_value_attr);

                this.scan_code_attribute_name = getFromPreference("scan_code_attribute_name",String.class,this.scan_code_attribute_name);

                this.enable_manage_product = getFromPreference("enable_manage_product",Boolean.class,this.enable_manage_product);

                JSONObject obj = getFromPreference("default_recommendation_params",JSONObject.class,new JSONObject());
                Iterator<String> iter = obj.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        Log.i(TAG," Default recommendations param value = "+obj.get(key) +" ValueType:- "+obj.get(key).getClass());
                        String value = obj.get(key).toString();
                        char first = value.charAt(0);
                        char last = value.charAt(value.length() - 1);
                        if(first=='{' && last=='}'){
                            String getObject = value.substring(1, value.length()-1);
                            String[] splitObject = getObject.split("\\.");
                            // Log.e(TAG,"************splitObject:-----  "+ Arrays.toString(splitObject));
                            String modelName = splitObject[0];
                            String attrName = splitObject[1];

                            if(modelName!=null && !modelName.isEmpty()) {
                                DatabaseManager databaseManager = DatabaseManager.getInstance(context);
                                JSONObject singletonDetails = databaseManager.getSingletonBasedOnRequestType(modelName);

                                /*DaveSharedPreference sharedPreferences = new DaveSharedPreference(context);
                                String singletonDetails = sharedPreferences.readString("_object_" + modelName + "_singleton");
*/
                                Log.e(TAG,"singletonDetails - "+singletonDetails);

                               /* if(singletonDetails==null){
                                    singletonDetails = "";
                                }
                                JSONObject getSingleton = new JSONObject(singletonDetails);*/
                                if (singletonDetails != null && singletonDetails.has(attrName)) {
                                    if (this.default_recommendation_params == null || this.default_recommendation_params.isEmpty())
                                        this.default_recommendation_params = new HashMap<>();
                                    this.default_recommendation_params.put(key, singletonDetails.get(attrName));
                                }
                            }


                        }else {
                            if(this.default_recommendation_params==null || this.default_recommendation_params.isEmpty())
                                this.default_recommendation_params =new HashMap<>();

                            this.default_recommendation_params.put(key,obj.get(key));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                this.form_view_button_name = getFromPreference("form_view_button_name",String.class,this.form_view_button_name);

                this.form_view_order_stage_name = getFromPreference("form_view_order_stage_name",String.class,this.form_view_order_stage_name);

                this.menu_title_for_sign_up = getFromPreference(MENU_TITLE_FOR_SIGNUP,String.class,this.menu_title_for_sign_up);

                this.menu_title_for_customer_list = getFromPreference(MENU_TITLE_FOR_CUSTOMER_LIST,String.class,this.menu_title_for_customer_list);

                this.menu_title_for_product_list = getFromPreference(MENU_TITLE_FOR_PRODUCT_LIST,String.class,this.menu_title_for_product_list);

                this.menu_title_for_order_list = getFromPreference(MENU_TITLE_FOR_ORDER_LIST,String.class,this.menu_title_for_order_list);

                this.enable_set_default_quantity = getFromPreference(ENABLE_SET_DEFAULT_QUANTITY,Boolean.class,this.enable_set_default_quantity);

                this.manage_product_from_customer_profile = getFromPreference(MANAGE_PRODUCT_FROM_CUSTOMER_PROFILE,Boolean.class,this.manage_product_from_customer_profile);

                this.add_product_title = getFromPreference(ADD_PRODUCT_TITLE,String.class,this.add_product_title);

                this.cold_update_time_limit = (getFromPreference(COLD_UPDATE_TIME_LIMIT,Number.class,this.cold_update_time_limit)).longValue();

                this.hot_update_time_limit = getFromPreference(HOT_UPDATE_TIME_LIMIT,Number.class,this.hot_update_time_limit).longValue();

                this.search_by_image_attr = getFromPreference(SEARCH_BY_IMAGE_ATTRIBUTE,String.class,this.search_by_image_attr);

                this.search_by_image_button_name = getFromPreference(SEARCH_BY_IMAGE_BUTTON_NAME,String.class,this.search_by_image_button_name);
            }
    }

    public void setColdAndHotUpdateTime(long coldUpdateTimeLimit, long hotUpdateTimeLimit){
        this.cold_update_time_limit = coldUpdateTimeLimit;
        this.hot_update_time_limit = hotUpdateTimeLimit;

    }



    private static ArrayList convertJsonArrayToList(JSONArray jsonArray){
        //Log.e(TAG,"convertJsonArrayToList( "+jsonArray+")");
        try {
            ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    list.add(jsonArray.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return list;
        }catch (Exception e){
            e.printStackTrace();
            return new ArrayList();
        }
    }

    private static ArrayList convertObjectToArrayList(Object object){
        JSONArray jsonArray = (JSONArray) object;
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < jsonArray.length(); i++){
            try {
                list.add(jsonArray.get(i).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public String getUser_login_model_name() {
        return user_login_model_name;
    }


    public String getUser_login_id_attr_name() {
        return user_login_id_attr_name;

    }

    public ArrayList getUser_profile_details_list() {
        return user_profile_details_list;
    }

    public String getRecommendations_btn_name() {
        return recommendations_btn_name;
    }

    public String getMenu_title_for_primary_orders() {
        return menu_title_for_primary_orders;
    }

    public void setMenu_title_for_primary_orders(String menu_title_for_primary_orders) {
        this.menu_title_for_primary_orders = menu_title_for_primary_orders;
    }

    public String getMenu_title_for_incentives() {
        return menu_title_for_incentives;
    }

    public void setMenu_title_for_incentives(String menu_title_for_incentives) {
        this.menu_title_for_incentives = menu_title_for_incentives;
    }

    public String getMenu_title_for_reports() {
        return menu_title_for_reports;
    }

    public void setMenu_title_for_reports(String menu_title_for_reports) {
        this.menu_title_for_reports = menu_title_for_reports;
    }

    public int getNo_of_recommendations() {
        return no_of_recommendations;
    }

    public HashMap<String, Object> getDefault_recommendation_params() {
        return default_recommendation_params;
    }

    public int getCount_after_next_recommendations() {
        return count_after_next_recommendations;
    }

    public String getScan_btn_name() {
        return scan_btn_name;
    }

    public String getScan_code_attribute_name() {
        return scan_code_attribute_name;
    }

    public String getCustomer_card_image() {

        return customer_card_image;
    }

    public String getCustomer_card_header() {
        return customer_card_header;
    }

    public String getCustomer_card_sub_header() {
        return customer_card_sub_header;
    }

    public String getCustomer_card_right_attr() {
        return customer_card_right_attr;
    }

    public String getCustomer_profile_title() {
        return customer_profile_title;
    }

    public ArrayList getCustomer_details_view() {
        return customer_details_view;
    }

    public ArrayList getCustomer_sign_up_list() {
        return customer_sign_up_list;
    }

    public boolean isCustomer_attr_hierarchy_first() {
        return customer_attr_hierarchy_first;
    }

    public String getCustomer_variable_attr_title() {
        return customer_variable_attr_title;
    }

    public ArrayList getCustomer_filter_attr() {
        return customer_filter_attr;
    }

    public ArrayList getCustomer_category_hierarchy() {
        return customer_category_hierarchy;
    }

    public ArrayList getCustomer_variable_attr() {
        return customer_variable_attr;
    }

    public String getCustomer_email_attr() {
        return customer_email_attr;
    }

    public String getCustomer_mobile_attr() {
        return customer_mobile_attr;
    }

    public String getProduct_card_image_view() {
        return product_card_image_view;
    }

    public String getProduct_card_header() {
        return product_card_header;
    }

    public String getProduct_card_sub_header() {
        return product_card_sub_header;
    }

    public String getProduct_card_right_attr() {
        return product_card_right_attr;
    }

    public String getProduct_expand_view_title() {
        return product_expand_view_title;
    }

    public ArrayList getProduct_expand_view() {
        return product_expand_view;
    }

    public ArrayList getProduct_add_edit_List() {
        return product_add_edit_List;
    }

    public void setProduct_add_edit_List(ArrayList product_add_edit_List) {
        this.product_add_edit_List = product_add_edit_List;
    }

    public String getProduct_category_hierarchy_title() {
        return product_category_hierarchy_title;
    }

    public ArrayList getProduct_category_hierarchy() {
        return product_category_hierarchy;
    }

    public String getProduct_price_attr() {
        return product_price_attr;
    }

    public ArrayList getProduct_filter_attr() {
        return product_filter_attr;
    }

    public ArrayList getSummation_attributes() {
        return summation_attributes;
    }

    public String getInteraction_qty() {
        return interaction_qty;
    }

    public String getPredicated_qty_attr() {
        return predicated_qty_attr;
    }

    public ArrayList getInteraction_stage_attr_list() {
        return interaction_stage_attr_list;
    }

    public String getInvoice_model_name() {
        return invoice_model_name;
    }

    public String getInvoice_id_attr_name() {
        return invoice_id_attr_name;
    }

    public ArrayList getInvoice_checkout_attr_list() {
        return invoice_checkout_attr_list;
    }

    public String getInvoice_close_attr_name() {
        return invoice_close_attr_name;
    }

    public String getInvoice_card_header() {
        return invoice_card_header;
    }

    public String getInvoice_card_sub_header() {
        return invoice_card_sub_header;
    }

    public String getInvoice_card_right_attr() {
        return invoice_card_right_attr;
    }

    public String getInvoice_date_attr() {
        return invoice_date_attr;
    }

    public String getInvoice_stage_attr() {
        return invoice_stage_attr;
    }

    public boolean isInvoice_show_zero_qty() {
        return invoice_show_zero_qty;
    }

    public String getInvoice_total_count_attr() {
        return invoice_total_count_attr;
    }

    public String getInvoice_total_value_attr() {
        return invoice_total_value_attr;
    }

    public boolean isEnable_manage_product() {
        return enable_manage_product;
    }

    public ArrayList getProduct_similar_attributes() {
        return product_similar_attributes;
    }

    public ArrayList getProduct_variants_attributes() {
        return product_variants_attributes;
    }

    public String getForm_view_button_name() {
        return form_view_button_name;
    }

    public String getForm_view_order_stage_name() {
        return form_view_order_stage_name;
    }

    public String getMenu_title_for_sign_up() {
        return menu_title_for_sign_up;
    }

    public String getMenu_title_for_customer_list() {
        return menu_title_for_customer_list;
    }

    public String getMenu_title_for_product_list() {
        return menu_title_for_product_list;
    }

    public String getMenu_title_for_order_list() {
        return menu_title_for_order_list;
    }

    public boolean isEnable_set_default_quantity() {
        return enable_set_default_quantity;
    }

    public boolean isManage_product_from_customer_profile() {
        return manage_product_from_customer_profile;
    }

    public String getAdd_product_title() {
        return add_product_title;
    }

    public long getCold_update_time_limit() {
        return cold_update_time_limit;
    }

    public long getHot_update_time_limit() {
        return hot_update_time_limit;
    }

    public String getSearch_by_image_attr() {
        return search_by_image_attr;
    }

    public String getSearch_by_image_button_name() {
        return search_by_image_button_name;
    }
}
