buildscript {
    repositories {
        maven { url 'https://plugins.gradle.org/m2/' }
        //maven { url 'https://maven.fabric.io/public' }
    }
    dependencies {
        classpath 'gradle.plugin.com.onesignal:onesignal-gradle-plugin:0.12.0'
        //classpath 'io.fabric.tools:gradle:1.+'
    }
}
apply plugin: 'com.onesignal.androidsdk.onesignal-gradle-plugin'

apply plugin: 'com.android.application'
apply plugin: 'io.fabric'

repositories {
    maven { url 'https://maven.google.com' }
   // maven { url 'https://maven.fabric.io/public' }
    maven { url "https://jitpack.io" }
}

android {
    compileSdkVersion 27
    defaultConfig {
        applicationId "com.daveai.__ENTERPRISE-ID__"
        minSdkVersion 17
        targetSdkVersion 27
        versionCode 88
        versionName "2.7.6"


        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"

        manifestPlaceholders = [onesignal_app_id: '_ONE_SIGNAL_APP_ID_',
                                // Project number pulled from dashboard, local value is ignored.
                                onesignal_google_project_number: 'REMOTE'
        ]
        vectorDrawables.useSupportLibrary = true
        vectorDrawables {
            useSupportLibrary = true
        }
        multiDexEnabled true

    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }

    lintOptions {
        // Turns off checks for the issue IDs you specify.
        disable 'TypographyFractions', 'TypographyQuotes'
        // Turns on checks for the issue IDs you specify. These checks are in
        // addition to the default lint checks.
        enable 'RtlHardcoded', 'RtlCompat', 'RtlEnabled'
        // To enable checks for only a subset of issue IDs and ignore all others,
        // list the issue IDs with the 'check' property instead. This property overrides
        // any issue IDs you enable or disable using the properties above.
        check 'NewApi', 'InlinedApi'
        // If set to true, turns off analysis progress reporting by lint.
        quiet true
        // if set to true (default), stops the build if errors are found.
        abortOnError false
        // if true, only report errors.
        ignoreWarnings true


    }
    aaptOptions {
        cruncherEnabled = false
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}

repositories {
    maven { url 'https://maven.google.com' }
    maven { url 'https://oss.sonatype.org/content/repositories/snapshots/' }
    mavenCentral()
    flatDir {
        dirs "libs"
    }
}


dependencies {

    implementation fileTree(include: ['*.jar'], dir: 'libs')
    androidTestImplementation('com.android.support.test.espresso:espresso-core:2.2.2', {
        exclude group: 'com.android.support', module: 'support-annotations'
    })
    implementation 'com.android.support:support-v4:27.1.1'
    implementation 'com.android.support:customtabs:27.1.1'

    implementation 'com.android.support:appcompat-v7:27.1.1'
    implementation 'com.android.support:design:27.1.1'
    implementation 'com.android.support:support-v13:27.1.1'
    testImplementation 'junit:junit:4.12'
    implementation 'com.android.support.constraint:constraint-layout:1.1.0'

    //added android sdk aar file
    implementation(name: 'DaveAI-release', ext: 'aar')

    implementation 'com.android.support:cardview-v7:27.1.1'

    implementation 'com.squareup.okhttp:okhttp-urlconnection:2.0.0'
    implementation 'com.squareup.okhttp:okhttp:2.7.5'
    implementation 'com.squareup.okio:okio:1.13.0'
    implementation 'com.squareup.picasso:picasso:2.5.2'
    implementation 'com.google.code.gson:gson:2.8.0'
    implementation 'com.android.support:multidex:1.0.3'
    implementation 'com.github.amlcurran.showcaseview:library:5.4.3'
    implementation 'com.intuit.sdp:sdp-android:1.0.4'
    implementation 'com.intuit.ssp:ssp-android:1.0.5'
    implementation 'com.github.bumptech.glide:glide:3.8.0'
    implementation 'com.crystal:crystalrangeseekbar:1.0.0'
    implementation 'me.drakeet.multitype:multitype:3.4.0'
    // implementation 'com.google.android.gms:play-services-maps:15.0.1'
    implementation 'com.google.android.gms:play-services-maps:11.8.0'
    implementation 'com.github.mabbas007:TagsEditText:1.0.5'
    implementation 'com.github.chrisbanes:PhotoView:1.3.0'
    implementation 'me.dm7.barcodescanner:zxing:1.9.8'
    // Check for v11.4.2 or higher
    implementation 'com.google.firebase:firebase-core:16.0.8'

    // Add dependency
    implementation 'com.crashlytics.sdk.android:crashlytics:2.9.9'

    implementation 'com.onesignal:OneSignal:3.9.1'
    implementation 'com.google.firebase:firebase-messaging:11.8.0'
    implementation 'com.theartofdev.edmodo:android-image-cropper:2.7.+'

/*
    implementation('com.crashlytics.sdk.android:crashlytics:2.9.4@aar') {
        transitive = true;
    }*/

    implementation 'com.toptoche.searchablespinner:searchablespinnerlibrary:1.3.1'
    implementation 'com.github.PhilJay:MPAndroidChart:v2.2.4'


}

apply plugin: 'com.google.gms.google-services'
